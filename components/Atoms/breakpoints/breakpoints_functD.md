# Breakpoints

Please see the Foundations section of Trail Mix for more information about our breakpoints and grids. This documentation's purpose is to provide a place where the team can inspect an artboard with the grid defined.

![1727726403064](image/breakpoints_functD/1727726403064.png)
