# XL breakpoint

**1920 XXL breakpoint is the maximum breakpoint.**

Only certain components and/or their background color or cover image scale larger than 1920- but these are exceptions.

The Content Container does not scale above 1440. 

Certain components scale to this breakpoint.  Please see the prototype tab to inspect the grid.
