# Buttons are a small but mighty component on the site. Often used for call to actions, they help people take action on the website. We have a range of purposeful buttons to fit different needs, hierarchy, and emphasis.


![1730300199815](image/a_buttons-overview/1730300199815.png)![1730300202735](image/a_buttons-overview/1730300202735.png)
