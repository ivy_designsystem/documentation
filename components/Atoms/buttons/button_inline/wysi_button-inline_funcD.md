# The inline button is a clickable UI element to empower the user to take action. It is a lower emphasis button and can only be placed within a layout with Drupal's CK WYSIWYG editor.

![1730302167708](image/wysi_button-inline_funcD/1730302167708.png)

# Overview

The inline button is a clickable UI element to empower the user to take action, request information or take a logical next step based upon the context. It has a lower emphasis than other buttons and can only be placed within a layout with Drupal's CK WYSIWYG editor.

**When to use**

**The inline button is used to communicate actions the user can take and allow the user to engage with the content on the site. Keep in mind that the inline button is a lower emphasis button. **

**Button labels**

Button labels need to be as short as possible and make it clear to the user what the button is for. There should be no surprises to the user when they click on a button. The button link should take them to an expected destination with an expected outcome.  This provides confidence in the click and supports the integrity and usability or our website.

**When not to used**

Do no use inline buttons as navigation elements. Depend upon the unit and site-level navigation, and higher-level components like 5050s when the desired action is to take the user to a new page.

## Best practices

Make sure that the inline button is nested between 2 completed sections of text content. Do not randomly select text in the middle of a paragraph and then assign it the "inline button" style. This will not be successful.

Make sure you finish your sentence properly before inserting this button. The button text should be a discreet sentence wrapped in the `<p></p>` tag.

# Data

**Mandatory**

Button label: WYSIWYG Text but we control the look of the font.

Link: valid url. One / button

**Anticipated issues: **

Content admins trying to inline style this text  and messing up the design.  For example, changing the text style from the specified style to a headline style, adding links, sticking an image or a table in there...  Jordan, can there be any constraints so it doesn't fall apart when content editors try to get it to do something else? I think you said there will be limits as the style will be on the anchor div rather than the paragraph?

# JORDAN. UPDATE: THE MOBILE version is going to stack vertically so that this button will work within the Callout component. The FIGMA file has been updated and I will update this documentation.

![1730302183061](image/wysi_button-inline_funcD/1730302183061.png)

# Functionality

The inline button is a fluid button. It scales to fill the container that it is placed in.

It can only be placed within a WYSIWYG container using the Drupal CK editor.

The button label should provide clarity and 'confidence in the click' as to what action the button is for. The hit state is the entire button area. On hover / tap the hover state of the button is shown. On-click / on-tap, the user is taken to the link associated with the button.

# Inline Button States

States are simple in case this button needs to scale with more types of text content. The states are:

* Default
* Hover
* Focus

![1730302195201](image/wysi_button-inline_funcD/1730302195201.png)

# Inline Button Sizes

The button is a fluid button. The size of the text and the layout spacing, padding etc vary between desktop and mobile.

**Desktop**

**Mobile**

# JORDAN. THE MOBILE version is going to stack vertically. The FIGMA file has been updated and I will update this documentation.

# Alignment

The inline button is fluid and fills the WYSIWYG container it is in. Drupal's alignment options can be applied to the button but will have not visual effect.

# Stacking the inline button

The inline button should be stacked. We don't recommend placing more than one button at a time as more buttons make it harder for the user to figure out what to do next and could lead to abandonment of the page or site rather than engaging with the site.

There are no limits on the Drupal backend to how many buttons you can stack, but it absolutely needed, you can stack more than one buttons. The integral button margins will mange the vertical spacing in the layout.

# JORDAN. THE MOBILE version is going to stack vertically. The FIGMA file has been updated and I will update this documentation.

![1730302218081](image/wysi_button-inline_funcD/1730302218081.png)

# Interactions

**On page-load:**

[Fade in place](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview)

**Transition between states:**

transition-fast: 0.2s cubic-bezier(.4, 0, .2, 1);

**Keyboard**

Focus state is displayed

# Anatomy and Specs


![1730302228962](image/wysi_button-inline_funcD/1730302228962.png)
