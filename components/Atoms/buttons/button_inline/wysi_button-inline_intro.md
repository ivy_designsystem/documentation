# The inline button is a clickable UI element to empower the user to take action. It is a lower emphasis button and can only be placed within a layout with Drupal's CK WYSIWYG editor.

![1730302299124](image/wysi_button-inline_intro/1730302299124.png)
