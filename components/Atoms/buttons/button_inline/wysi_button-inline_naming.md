# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Common name:

Inline button

# Component naming for the inline button

![1730302147159](image/wysi_button-inline_naming/1730302147159.png)
