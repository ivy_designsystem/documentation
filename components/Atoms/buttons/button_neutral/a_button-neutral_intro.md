# A neutral, solid button. No icon. Good type contrast within the button but low contrast on the page. So more of a secondary button. It comes in 2 sizes: 40px and 48px.
