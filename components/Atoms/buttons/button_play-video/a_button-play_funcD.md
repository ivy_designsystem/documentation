# This is a smaller version of the Watch Video Button. It's just the arrow button and is used to go on smaller media elements, like the card, for example.

![1730303275344](image/a_button-play_funcD/1730303275344.png)



It's an atomic element and is included in parent components.

Here it is used in the mediaCard. *(note, InVision DSM is changing the scale of these PNGs. so size is not exact as shown)*


![1730301881199](image/a_button-play_funcD/1730301881199.png)



![1730301891454](image/a_button-play_funcD/1730301891454.png)

# Button Sizes

The Play Button is currently available in 3 sizes: 24px, 48px, and 64px.

# States

The play button is available in default (link), hover, active, press, and focus states.


![1730301919251](image/a_button-play_funcD/1730301919251.png)
