## Downloadable SVGs

https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview


Organized by size: 24, 48, 64

They look pixelated, but they are SVGs and are fine in Figma. Let me know if you have any issues: smasonla@uvm.edu
