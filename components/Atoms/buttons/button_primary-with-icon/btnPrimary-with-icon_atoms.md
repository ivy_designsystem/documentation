## **Icons**

Just like the text links, we have 2 icon options except this time the right arrow is in a solid circle. We are not putting the “new window” icon in a circle as it just is too complex visually.

1. Right arrow (light and dark)
2. “open new window” icon (light and dark)
3. Video Play icon (light and dark)

![1727727726606](image/btnPrimary-with-icon_atoms/1727727726606.png)

Download Arrow SVG icons from the design system:

Foundations/Icons/UI
