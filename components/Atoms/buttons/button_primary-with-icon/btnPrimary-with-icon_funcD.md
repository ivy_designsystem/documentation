**Primary Buttons are pill shape and available in light and dark. Variations include icon or no icon. Just like the other buttons, the default icon is a right arrow. The “New Window” icon is also offered if the link opens in a new window.**

## **Sizes**

The minimum size of this button is 110px wide. The maximum size is 256px wide. Text does not wrap. The maximum size is 256 because that is the width of the content area at our XXS-320 smallest breakpoint.

## **Character count**

Maximum 24 including spaces

## **Color**

 **We are working with a primaryLight #** FFD416 ** and primaryDark #** 006B51** for the light and dark button backgrounds. **

**Dark Button Colors: **

BG default: PrimaryDark: #006B51.   (palette color is primary1)

Icon default: icon bg #FFFFFF, icon fill primaryDark. #006B51

Icon hover: no change in color

**btn__primaryDarkIcon-default**

![1727727754845](image/btnPrimary-with-icon_funcD/1727727754845.png)

![1727727767354](image/btnPrimary-with-icon_funcD/1727727767354.png)

**btn__primaryDarkIcon-hover**

![1727727794618](image/btnPrimary-with-icon_funcD/1727727794618.png)

![1727727806663](image/btnPrimary-with-icon_funcD/1727727806663.png)

The primary1 shifts to primary1-dark #01533F on hover.

**Light Button Fill Colors:**

bg default: primaryLight: #FFD416

bg hover: primaryLight-hover: #F8C70A

Icon and label default: icon bg #01533F, icon fill primaryLight. #FFD416

Icon and label hover: no change in color

**btn__primaryLightIcon--default**

![1727727823074](image/btnPrimary-with-icon_funcD/1727727823074.png)

![1727727838802](image/btnPrimary-with-icon_funcD/1727727838802.png)

**btn__primaryLightIcon--hover**

![1727727850033](image/btnPrimary-with-icon_funcD/1727727850033.png)

![1727727873847](image/btnPrimary-with-icon_funcD/1727727873847.png)

**Icons**

Just like the text links, we have 2 icon options except this time the right arrow is in a solid circle. We are not putting the “new window” icon in a circle as it just is too complex visually.

1) Right arrow.
2) “open new window” icon

## States

Just 2 states for now:

Link and hover

We are going to roll out buttons with just the link an hover states. Press, active etc. are on hold and TBD.

## Interaction

Quick fade between each.
