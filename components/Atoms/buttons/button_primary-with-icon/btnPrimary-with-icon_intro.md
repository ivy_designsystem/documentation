
Primary buttons indicate the main action that should be taken on a page. Primary Buttons are concise, have only one line of text and a maximum width of 256px. They come in dark and light variations. The light buttons are used on dark backgrounds and the dark buttons are used on light backgrounds.

**Primary button usage guidelines:**

* Keep the button text brief and use sentence case- NOT title case.
* 

Sentence case has the first character capitalized only.

* Use action words for the label that indicate what will happen (for example, Download or Sign Up).
* Use links instead of buttons for navigational elements.
* Use an icon and text where appropriate to reinforce actions.

![1727727240194](image/btnPrimary-with-icon_intro/1727727240194.png)
