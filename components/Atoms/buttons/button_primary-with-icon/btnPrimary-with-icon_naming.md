## **Naming of the atomic elements**

**1) Icon**

icn_circleSolidDark-arrowRight

icn_circleSolidLight-arrowRight

icn_arrow-newWindowLight

icn_arrow-newWindowDark

**2) Text**

The text style = label at the default size, medium weight:

txt__labelDefaultStrong-light

txt__labelDefaultStrong-dark

## **Naming of the button**

### **1) Dark button**

btn__primaryDarkIconRightArrow-link

![1727727919136](image/btnPrimary-with-icon_naming/1727727919136.png)

btn__primaryDarkIconNewWindow-link

![1727727929464](image/btnPrimary-with-icon_naming/1727727929464.png)

## **Naming of the button**

## **1) Light (for on-dark)**

btn__flatIcon_primaryLight—link

btn__flatIcon_primaryLIght—hover

btn__primaryLightIconRightArrow-link

![1727727938629](image/btnPrimary-with-icon_naming/1727727938629.png)

btn__primaryLightIconNewWindow-link

![1727727953230](image/btnPrimary-with-icon_naming/1727727953230.png)
