# The Secondary Outline Button is really similar to the outline Grid Button with the difference is that it does not scale to snap to the grid. Its width is determined by the content within + padding. It is never wider than 256px.

The text does not ever wrap to 2 lines.

The** maximum width** of these buttons is 256 (the XXS mobile breakpoint less the L/R margins, 320-64). Whereas the **minimum width** of the [Grid Button](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/611d00a04d3139da9cc634d8/tab/design?mode=preview) is 256px.
