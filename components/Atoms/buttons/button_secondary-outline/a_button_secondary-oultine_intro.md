# The Secondary Outline Button is really similar to the outline Grid button with the differences that it does not scale to snap to the grid and it is flush left. Its width is determined by the content within with a maximum width of 256px.

![1730301400585](image/a_button_secondary-oultine_intro/1730301400585.png)
