## **The Solid Button is a pill shaped button and available in primaryLight, primaryDark and white. It's related to the "** [**primary button with icon**](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d21f7f97cd73048d986a/tab/design?mode=preview)".

### **Sizes**

The minimum size of this button is determined by the text label and the padding. Text does not wrap. The maximum size is 256 because that is the width of the content area at our XXS-320 smallest breakpoint.

### **Character count**

Maximum 26 characters including spaces (**guestimated** to meet the 256 maximum width)

Minimum button should be about 108px wide. Shown here for minimum has 6 characters.

### **Color**

 **We are working with a primaryLight #** FFD416 ** and primaryDark #** 007155** for the light and dark button backgrounds and a white #FFF version. **

![1727726953419](image/btnSolid_funcD/1727726953419.png)

**primaryDark Button Colors: **

BG default: PrimaryDark: #006B51. (palette color is primary1)

BG hover: PrimaryDark-hover: #01533F. (Palette color is primary1--darkest)

Focus: 1px inside border (so button doesn't 'jump') primary3-lighter

Label default:  #FFF

Label hover: no change in color

**primaryLight Button Fill Colors:**

bg default: primaryLight: #FFD416

bg hover: primaryLight-hover: #F8C70A

Label default:  #006B51

Label hover: no change in color

Focus: 1px inside border (so button doesn't 'jump') primary3-lighter

**White button**

bg default: #FFF

bg hover: neutralDarkest #E7E7E7

Focus: 1px inside border (so button doesn't 'jump') primary3-lighter

### States

Default (Link), hover, active, press, focus

![1727726968554](image/btnSolid_funcD/1727726968554.png)

### Interaction

Quick fade between each.

### Developer Note

**Variation: Grid Button**

This button can have a "grid" variation. It's the same button visually EXCEPT that the button scales to fill the continer it is in. The width is determined by the container rather than the text lable + padding.
