## Solid buttons. No icon. Can be a "grid" button designed to fill the container it is in or a regular button with size determined by the text label, padding and maximum width.
