# Atoms

**Arrow icon**

You can download the svg at

Ivy / Foundations / Icons / UI / [Right arrow](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

icn_arrow-right

![1730301229273](image/a_button_txtBtn_iconBefore-atomic/1730301229273.png)

**Button Text**

The button comes in 3 different text sizes so it can match size with the body text that it follows. The 3 text sizes are:

p.small

p.default

p.bigSister

Ivy / Foundations / Typography / [Body](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f9321382a1828526c9c5381?mode=preview)

**The text button comes in 2 colors: **

darkGray: #26292c

white: #FFF

Ivy / Foundations / Colors / [Grayscale](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f89c5e822a82f2e647a2941?mode=preview)
