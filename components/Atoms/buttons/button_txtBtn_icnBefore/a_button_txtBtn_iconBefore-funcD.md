**Text buttons are composed of text with an icon. This button has the icon before but there is a **[**companion button with the icon after.**](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393b45fa8eb003116191aa8/tab/design?mode=preview)

**This button comes in three sizes (small, default and Big Sister), and 2 colors. Text in the text button can wrap to more than one line. **

## Text Sizes

p.small

p.default

p.bigSister

## Color Variations

darkGray: #26292c

white: #FFF

## Alignment

Flush left only

## Line wrapping:

Can go to multiple lines. Icon "hangs" outside of text margin.

![1730301255262](image/a_button_txtBtn_iconBefore-funcD/1730301255262.png)

![1730301258426](image/a_button_txtBtn_iconBefore-funcD/1730301258426.png)
If the text in the button goes to multiple lines, the icon before aligns top.

# Hover state

**darkGray text**

text turns black with a primary3 underline.

![1730301269653](image/a_button_txtBtn_iconBefore-funcD/1730301269653.png)

Above, the link is on a neutral background. And below, the link is on white. Both are showing the hover state.

**White text**

text gets a primary2 underline
![alt text](A_MGFjZjlkZDY2YjhlM2JmOYwDNLqZmqxyolINJOS3E5MfQZNpip-KbnGRMZGyqMoQECsHbE9Gv2bHF9MaKH0GNkpJCsBY72PPIbP97FBqTV-DJeYaC6bVel5WD7FIWan6WkMPX9vimQI2WATY3_F83g==.png)



## Width

**Minimum width** of ind. link list and link list group is 256px, including the arrow icon.

**Maximum width: **limited to the parent container and component's margins and padding

## Icons

This button as an icon before the text. The default icon is a right arrow .

## Usage:

This is a tertiary button. It has less presence than the solid primary button, secondary outline button, or Flat Icon button which has chunkier text and larger icon in a circle.

It's used as an atom in LinkLists. And it can be useful when you want to have a stand-alone text link that is not a primary call-to-action.
