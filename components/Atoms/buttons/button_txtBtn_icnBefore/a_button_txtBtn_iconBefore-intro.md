**Text buttons are composed of text with an icon. This button has the icon after but there is a **[**companion button with the icon a**](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393b45fa8eb003116191aa8/tab/design?mode=preview)[fter.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393b45fa8eb003116191aa8/tab/design?mode=preview)

**The button comes in three sizes (small, default and Big Sister), and 2 colors. Text in the text button can wrap to more than one line. See the functional narrative for more info.**

![1730301189843](image/a_button_txtBtn_iconBefore-intro/1730301189843.png)
