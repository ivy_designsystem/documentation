# Atoms

**Arrow icon**

You can download the svg at

Ivy / Foundations / Icons / UI / [Right arrow](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

icn_arrow-right



![1730303087954](image/a_button-txt_iconAfter_atomic/1730303087954.png)

Ivy / Foundations / Icons / UI /ui/ [icn__arrow-right-w](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)hite

Note: the arrow was reduced to 16px for the xxs, xs mobile breakpoints.

IMPORTANT: for the button that has the p.xsmallBold text, the arrow is reduced to 16px. Make sure you scale the svg for this size.

**Chevron icon**

Ivy / Foundations / Icons / UI / [chevron right](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview) (bottom of the page)

![1730303080552](image/a_button-txt_iconAfter_atomic/1730303080552.png)

**Button Text**

The button comes in 3 different text sizes so it can match size with the body text that it follows. The 3 text sizes are:

**Default weight buttons**

p.small

p.default

p.bigSister

**Bold weight buttons**

p.xsmallBold

p.smallBold

p.defaultStrong

p.bigSisterBold

Ivy / Foundations / Typography / [Body](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f9321382a1828526c9c5381?mode=preview)

**The text button comes in 2 colors: **

darkGray: #26292c

white: #FFF

Ivy / Foundations / Colors / [Grayscale](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f89c5e822a82f2e647a2941?mode=preview)
