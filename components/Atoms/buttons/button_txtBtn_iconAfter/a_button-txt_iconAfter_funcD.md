**Text buttons are composed of text with an icon. This button has the icon after but there is a **[**companion button with the icon b**](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d6be2fb0aa8bf6e7b976/tab/design?mode=preview)[efore.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d6be2fb0aa8bf6e7b976/tab/design?mode=preview)

**At the regular weight this button comes in three sizes (small, default and Big Sister), and 2 colors. For bold, the same type sizes are offered along with p.xsmallBold. Text in the text button can wrap to more than one line. **

![1730302967504](image/a_button-txt_iconAfter_funcD/1730302967504.png)

## Text Sizes

**Default weight buttons**

p.small

p.default

p.bigSister

**Bold weight buttons**

p.xsmallBold

p.smallBold

p.defaultStrong

p.bigSisterBold

## Color Variations

darkGray: #26292c

white: #FFF

## Alignment

Flush left only

## Line wrapping:

Text can go to multiple lines. Icon hugs the text and follows the last word with a 4px space separation.

![1730302975822](image/a_button-txt_iconAfter_funcD/1730302975822.png)

## Hover state

**Arrow moves +4px to the right on hover**

**darkGray text**

text turns black with a primary3 underline.

![1730302983426](image/a_button-txt_iconAfter_funcD/1730302983426.png)

Above is the txtBtnR-darkGray link state. And below, shows a hover state.

![1730302995085](image/a_button-txt_iconAfter_funcD/1730302995085.png)

**White text**

text gets a primary2 underline, whether the icon is before (as shown) or after.

![alt text](icnbutton-white-text.png)

## Figma prototype limitation

![1730303011886](image/a_button-txt_iconAfter_funcD/1730303011886.png)

Above is not correct. Figma cannot place the icon immediately after the last word as desired. It placed the icon bottom aligned, outside the text container.

Please, the icon goes directly after the last word- not outside the text container, like this:

![1730303019862](image/a_button-txt_iconAfter_funcD/1730303019862.png)

## Width

**Minimum width** of ind. link list and link list group is 256px

**Maximum width: **limited to the parent container and component's margins and padding

## Icon

This button has an arrow icon after the text.

## Usage:

This is a tertiary button. It has less presence than the solid primary button, secondary outline button, or Flat Icon button which has chunkier text and larger icon in a circle. It can be useful when you want to have a stand-alone text link that is not a primary call-to-action.

It's used as an atom in [LinkLists](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393afe4f291f20ef5204390/tab/design?mode=preview).

# States

**States are:**

* Link
* Alt Link (for CTA text lockups where an underline is needed)
* Press
* Hover / on-tap
* Active
* Focus

![1730303027584](image/a_button-txt_iconAfter_funcD/1730303027584.png)

## Text button with chevron icon

The text button has a variation with a chevron icon at the end instead of an arrow icon. It only comes in the default 17px text size in darkGray. (no onDark version).

![1730303035545](image/a_button-txt_iconAfter_funcD/1730303035545.png)

![1730303044946](image/a_button-txt_iconAfter_funcD/1730303044946.png)

# States

![1730303052864](image/a_button-txt_iconAfter_funcD/1730303052864.png)

![1730303064872](image/a_button-txt_iconAfter_funcD/1730303064872.png)
