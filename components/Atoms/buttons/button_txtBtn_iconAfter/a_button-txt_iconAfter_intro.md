**Text buttons are composed of text with an icon. This button has the icon after but there is a **[**companion button with the icon before.**](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d6be2fb0aa8bf6e7b976/tab/design?mode=preview)

**The button comes in three sizes (small, default and Big Sister), and 2 colors. Text in the text button can wrap to more than one line. See the functional narrative for more info.**

![1730303102733](image/a_button-txt_iconAfter_intro/1730303102733.png)
