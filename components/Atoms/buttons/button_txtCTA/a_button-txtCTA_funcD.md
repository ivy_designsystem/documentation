# The Text CTA is an elevated text button.

# Data

Label: simple text, 26 chars max including spaces

# States

* Default
* Hover/on-tap
* Focus

![1730302471971](image/a_button-txtCTA_funcD/1730302471971.png)

# Design

This is a minimal release for inclusion in the [Key Program Info](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64fa38c05829470a5eb9d6d1/tab/design?mode=preview) component. For release, 4/5/24, it's just the onDark version.

### Sizes

* Large (not using now)
* Medium (using now)

# Specs

![1730302486412](image/a_button-txtCTA_funcD/1730302486412.png)



![1730302500219](image/a_button-txtCTA_funcD/1730302500219.png)
