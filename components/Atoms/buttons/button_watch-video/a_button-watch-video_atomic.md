
## **Icons**

This button has a video play icon. One for the primaryLight buttons and one for the primaryDark buttons.

![1730301690903](image/a_button-watch-video_atomic/1730301690903.png)

Download SVG icons from the design system:

Ivy / Foundations  /  Icons  / [UI](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)
