# This button is a CTA used as an overly with a 16x9 image to get people to watch a video.

![1730301711819](image/a_button-watch-video_funcD/1730301711819.png)


**Sizes**

This is a one-off button based on the Primary Button with icon. Unlike its parent, it has a large-56px height in addition to the default-40px high button.

**Character count**

The text for this button is “Watch video”

**Color**

 **We are working with a primaryLight #** FFD416 ** and primaryDark #** 006B51** for the light and dark button backgrounds. **

**Dark Button Colors: **

**Dark Button Colors: **

BG default: PrimaryDark: #006B51. (palette color is primary1)

Icon default: icon bg #FFFFFF, icon fill primaryDark. #006B51

Icon hover: no change in color

Icon default: icon bg #FFD416, icon fill primaryDark. #007155

Icon hover: no change in color

**Light Button Fill Colors:**

bg default: primaryLight: #FFD416

bg hover: primaryLight-hover: #F8C70A

Icon and label default: icon bg #01533F, icon fill primaryLight. #FFD416

Icon and label hover: no change in color

**Icon**

The icon for this button is the “play” button, a little right-facing isosceles triangle

**States **

There are 4 states:

1. Default (link)
2. Hover
3. Active
4. Press

![1730301722947](image/a_button-watch-video_funcD/1730301722947.png)


**Interaction **

Quick fade between states on engagement.
