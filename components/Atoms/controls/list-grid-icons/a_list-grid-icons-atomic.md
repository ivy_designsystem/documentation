# Atoms

**List and Grid Icon assets**

You will find these icon assets as well as the gridList icon group here:

Ivy / Foundations / Icons / Controls / [list and grid icons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64f7637ae1135b7d20e4e097?mode=preview)
