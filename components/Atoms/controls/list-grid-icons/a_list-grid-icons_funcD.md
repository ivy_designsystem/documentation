# The "List and Grid icons" are used in the listGridPicker to allow the user to switch between a grid view and list view for the display of content. These are the icon assets.



![1730304295959](image/a_list-grid-icons_funcD/1730304295959.png)

Above ^: These are the individual icon assets for the ListGridControl component.

# Sizes

Mobile: 56px

Desktop: 48px

*Note: During QA, these may reduce to 48px for mobile and 40px for desktop. *

See Ivy / Molecules / listGridControls for further description and functionality information.
