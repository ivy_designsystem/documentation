# The List and Grid icons are used in the listGridPicker to allow the user to switch between a grid view and list view for the display of content. These are the icon assets.

![1730304308793](image/a_list-grid-icons_intro/1730304308793.png)
