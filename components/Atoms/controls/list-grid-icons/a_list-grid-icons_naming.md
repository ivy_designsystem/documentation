# It's important that everything has a name and that we all use the same name to refer to design elements.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about something.

# Naming for the List and Grid icon assets:

![1730303702777](image/a_list-grid-icons_naming/1730303702777.png)
