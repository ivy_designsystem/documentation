# The quickLinksMenuRow atom is just a row of text that is a hyperlink. The links are unadorned simple text. No additional styles can be added. No icons or arrows. Just linked text. They are white and hover to white with an underline.

Here is an example of a list link row showing both the default state (link) and the hover state.

![1730304697971](image/a_quick-links-menu-row_funcD/1730304697971.png)

## Data

* Simple text. No extra styling permitted.
* Hyperlink for each row of text
* Source: Custom input by content editor
* Character limits: ?
* Can wrap to multiple lines

## Mandatory content

Simple text

Hyperlink

## **Maximum content**

**For the component: **none

**For within the context of the primary menu nav drawer / take-over nav: **probably should limit it... depends on who has editorial control. If editorial is responsible, we can have no limitations but convey maximum recommended in usage guidelines.

## **Width**

Width determined by placement in larger organism

## **Interaction**

Match underline interaction for Ivy/Components/Molecules/Navigation/[primaryNavGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/633ef101133ea0a5b9586d8c/tab/The%20green%20background%20should%20take-over%20the%20screen%20first%20swiping%20on%20from%20right%20to%20left%20with%20the%20type%20and%20other%20elements%20making%20a%20slightly%20delated%20and%20progress%20appearance.%20It%20should%20happen%20quickly%20though.%20They%20user%20should%20not%20have%20to%20wait%20for%20the%20navigation%20to%20appear...%20just%20a%20slight%20delayed%20progression%20much%20like%20you%20can%20see%20in%20this%20example%20if%20you%20click%20on%20the%20left%20Menu%20button.%20%20%20%20%20The%20site%20linked%20above%20is%20also%20a%20good%20example%20for%20the%20hover%20state%20functionality.)
