![1730305044099](image/old_white-logo_funcD/1730305044099.png)

# Usage by breakpoint in the White Overlay Header

**uvmLogoWhite-60**

Used for breakpoint M-1012 and above

**uvmLogoWhite-singleLineSmallest**

Used for S-768 breakpoint

**uvmLogoWhiteStacked-44**

Used for xxs-320 and xxs-480 breakpoints
