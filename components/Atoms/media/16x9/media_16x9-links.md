## 16x9 placeholder images

https://drive.google.com/file/d/14gnEZRQJiJJsqJ63TMofCyUrNFZzjCzD/view?usp=sharing

A bunch of 16x9 placeholder images that can be used during development or for testing.

## Figma file

https://www.figma.com/file/JpfeiKAtw5NoBExOJeCKzX/%F0%9F%93%9A--ds__atoms-and-molecules?node-id=0%3A3712

Placeholder images with base / primitive aspect ratios.
