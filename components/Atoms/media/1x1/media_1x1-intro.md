We use 1x1 images for the smaller mobile views for some hero banners. You can pick up some sample placeholder images to use under the "Placeholder Images" tab.

![1730307382040](image/media_1x1-intro/1730307382040.png)

![1730308283796](image/media_1x1-intro/1730308283796.png)
