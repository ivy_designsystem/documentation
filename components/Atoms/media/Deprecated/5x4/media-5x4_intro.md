## Deprecated

5x4 images have limited use on the site for hero banners at certain breakpoints.  that have a user selected focus with a mask. You can download some sample placeholder images under the "Placeholder" image tab.

![1730308210229](image/media-5x4_intro/1730308210229.png)
