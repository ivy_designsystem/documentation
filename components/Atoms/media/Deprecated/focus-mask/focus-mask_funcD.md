# To deliver the optimal display for hero banner images across breakpoints, some hero components have full viewport-width images that crop to a percentage of viewport height.  Essentially, the image container acts as a mask and the image scales and centers around a manually-selected focal point using the [Drupal Focal Point functionality](https://www.drupal.org/project/focal_point).

## **Description/overview**

Functionality is to allow user to easily establish both a focus and scale for an image at different breakpoints. Image will then be masked within the provided container based upon the focal point, scale and crop defined.

## **Purpose/goals**

To deliver the optimal view and crop of an image to the user no matter what the viewing scenario.

## **Criteria for success**

* Easy for the content editor to use.
* Optimal experience of the image for the user at all viewing scenarios.
* Lightweight

## **Approach and Technical Considerations**

**We suggest using the Drupal 9 "Media" tool to allow the content admin to establish and image focal point. **

With the built-in Drupal tool, the user will be able to manually establish the focus area by dropping a cross hair on the image. Drupal will scale the image around the focal point and determine the image scale needed by breakpoint. On the front-end, the image will be masked to the correct aspect ratio as determined by the organism.

**Note: ***We think that working with this existing Out of the Box (OOTB) tool, we will be able to launch this faster. That said, testing will be needed and there will most likely be room for improvement.*

https://www.drupal.org/project/focal_point

## Working with css viewport height (vh) units

At the largest screen widths, hero banners often start out at 16x9 aspect ratio and progressively change to 5x4 and then 1x1 at the smallest breakpoint.

This chart shows the aspect ratio of the hero banner image at different breakpoints. We are counting on the Drupal Media tool to scale the image appropriately around the chosen focal point. Again, we'll need to test this.



![1730307914320](image/focus-mask_funcD/1730307914320.png)

We are planning for a maximum image width of 1920. Height to be determined by the aspect ratio of the asset.
