# To deliver the optimal display for hero banner images across breakpoints, some hero components have full viewport-width images that crop to a percentage of visual height.  Essentially, the image container acts as a mask and the image scales and centers around a manually-selected focal point.

![1730307930314](image/focus-mask_intro/1730307930314.png)
