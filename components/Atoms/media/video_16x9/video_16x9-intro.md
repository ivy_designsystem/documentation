16x9 placeholder image to represent video

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOZEbh6FQ2yVqYALZLo_-aQbQIyH53FLDzWE-gjoSkoZnU9T1YP3kSX-LSyGtf7SG_VHjKQJUlSOrzdn_rw4JIX9Aee7kOi-bGRp1Pf17vyRs436zgCdJi2MdrvOulCA-sQ==?assetKey=university-of-vermont%2Ftrail-mix%2Fa__video_FPO-16x9-BkeuYFIzt.png)
