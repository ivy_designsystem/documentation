# The [navigation toggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b09936ed8d40b85855ac9/tab/design?mode=preview) is made up of two overlapping, individual *navToggle *buttons. Users can toggle between one and the other. The individual nav toggle shown here is the single button that combined makes the dual button navToggle.

# Functional Description

Please see the [navigationToggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b09936ed8d40b85855ac9/tab/functional%20narrative?mode=preview) component for the functional description for the toggle within context.

# Data

**For school or college websites**

• Name of the school/college/unit/department website

The maximum character count is 54 characters including spaces. The edge case for max content is the Rubenstein School of Environment and Natural Sciences ( 54 characters)

• Academic websites associated with schools or colleges will have an icon for each school or college

Adding the icon is a phase 2 item.

**For units and departments**

1. One toggle button needs to pick up the name of the school/college/unit/section-specific website that the user is visiting.
2. The school/college-specific button has an associated icon for the school/college. *
3. The other toggle button's label is fixed and on-click/on-tap displays the primary uvm.edu navigation. The label Is TBD. For placeholder it is "Explore UVM"

**For child sites**

* IMPORTANT: build for having the icon but we are going to put a placeholder spacer for now as we need a design sprint to make good quality icons for the schools and colleges. We do not have them now.

## Planning for the school / college icon

**IMPORTANT: **We are planning for the school and college icons to appear on the school / college button in the sectionNav Toggle. However we need a design sprint to convert these into SVGs with consistent line weight. This sprint is not scheduled but is anticipated so we wish to plan for these icons in the school/college toggle button.

**Placeholder icon**

Build with a transparent, square svg. xs-xl uses a 24x24px square icon. xxs has a 16px square icon.

**Drupal Admin Needs for the icon**

The icons should **not** be under user control. Recommend that the school/college websites each have a related icon with the association built into the data.

**Future / anticipated needs**

It is conceivable that the use of an icon would expand outside of schools and colleges at some point to child, section or unit websites. This should be factored into decisions about data, Drupal admin interface, construction of the button etc.

# Anatomy and button breakdown

Between the small - xl breakpoint, the size of the toggle button does not change.

![1730311176912](image/ind-nav-toggle_funcD/1730311176912.png)

![1730311183046](image/ind-nav-toggle_funcD/1730311183046.png)

The toggle scales from xs up to 767 pixels wide. At the small-768 breakpoint, the toggle button stops scaling.

Please see the larger component [navToggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b09936ed8d40b85855ac9/tab/functional%20narrative?mode=preview) for information about this. The button needs to be viewed in the context of the dual toggle button to understand how it scales.

![1730311193780](image/ind-nav-toggle_funcD/1730311193780.png)

The text is smaller for the xxs toggle button. It needs to be to accommodate the 54 pixels needed for the max content edge case - the Rubenstein School of Environment and Natural Sciences. (54 characters)

# Toggle sizes

The individual toggle button comes in 3 sizes:

* xxs
* xs
* s-xl

The individual toggle width is computed by:

(breakpoint - margins ) / 2

And then add 24px to the result.

Here is a summary of the widths by breakpoint.

![1730311202693](image/ind-nav-toggle_funcD/1730311202693.png)

![1730311212248](image/ind-nav-toggle_funcD/1730311212248.png)

![1730311223491](image/ind-nav-toggle_funcD/1730311223491.png)

# States

**not-selected link state:** can hover

**not-selected hover state:** on mouse-in, hover state displays indicate interaction with a button

**active state:** shown on-tap / on-click

**selected state: **indicates which navigation is selected and visible

**Focus states** are available for all except for the "selected" state. It is not a clickable state. The user would have to click / tap on the unselected button to change the selection.

**Developer note**

Selected button always is on top of the non-selected button

![1730311249844](image/ind-nav-toggle_funcD/1730311249844.png)
