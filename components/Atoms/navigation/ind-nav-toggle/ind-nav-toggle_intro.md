# The [navigation toggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b09936ed8d40b85855ac9/tab/design?mode=preview) is made up of two overlapping, individual *navToggle *buttons. Users can toggle between one and the other. The individual nav toggle shown here is the single button that combined makes the dual button navToggle.

![1730311269804](image/ind-nav-toggle_intro/1730311269804.png)
