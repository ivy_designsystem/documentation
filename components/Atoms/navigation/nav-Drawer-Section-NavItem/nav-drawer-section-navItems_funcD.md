# The NavDrawerSectionNav provides section-specific navigation in the take-over nav drawer. These are the individual navigation items and their states that compose the [NavDrSectionNavGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b097614ba64626145360a/tab/design?mode=preview).

![1730311435320](image/nav-drawer-section-navItems_funcD/1730311435320.png)

# At the atomic level, the section-specific navigation that appears in the navigation drawer is composed of up to 2 levels deep of navigation. The navigation is composed of vertically stacked rows.

The NavDrSectionNavItems are the building blocks for the[](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61fdad448becb1b0eaf9d3c4?mode=preview)[Section Nav Drawer Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b097614ba64626145360a/tab/design?mode=preview).

# Desktop Items

These are **navigation items for the desktop breakpoints (S-768 to XL-1440).**

![1730311444730](image/nav-drawer-section-navItems_funcD/1730311444730.png)

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOevH8ydRZrbZDUg-rdyJrERuCav8_a9yEQ4qMKEmHQ3nTUlR3lYUbtOb-I4Lt8UYQF2MLyGXGl2XVmIorem3I-2HU9i9cxHEhhZX3GVQfP6g6aH5J7FYpuUQvg_eFrhp0w==?assetKey=university-of-vermont%2Fivy%2FnavDrSectionNavParent-dt-ByT8r62u3.png)


![1730311453932](image/nav-drawer-section-navItems_funcD/1730311453932.png)

![1730311464659](image/nav-drawer-section-navItems_funcD/1730311464659.png)

# Mobile Items

And these are **navigation items for the mobile breakpoints (xxs-320 to xs-480).**


![1730311478866](image/nav-drawer-section-navItems_funcD/1730311478866.png)

![1730311487057](image/nav-drawer-section-navItems_funcD/1730311487057.png)

![1730311494727](image/nav-drawer-section-navItems_funcD/1730311494727.png)

![1730311502614](image/nav-drawer-section-navItems_funcD/1730311502614.png)

# Control icons

The section navigation panel uses the "+" icon to open and the "x" icon to close.

**Hover state micro-interaction**

The "+" and "x" icons are different, but the micro-interaction we are looking for on hover is a 45 degree turn between the open and close state. Start with a 395 degree (360+45 degree) turn to give it more of a "spin" feel.

**Icon sizing**

The open/close icon in the section nav panel is 32px.

The icons for the parent items within the section navigation panel are 24px.

[You can download the SVG icons here.](https://uvmoffice.sharepoint.com/:u:/s/CommunicationsTEAM408/EcQ6ZZfhZbZOp7IBjqDBWL0ByXYTVcH3uFF6z1T-pdzNlA?e=50dmaP) Look for the one in the "on-dark" folders" as they are designed to go on the primary1 dark background.

# **Layout and spacing**

## **Mobile**

**Nav drawer navigation item: mobile default**

![1730311556733](image/nav-drawer-section-navItems_funcD/1730311556733.png)

**layoutSpacing_sectionNavDrParent-mob-default**

![1730311569228](image/nav-drawer-section-navItems_funcD/1730311569228.png)

**layoutSpacing_sectionNavDrParentSite-mob-default**

![1730311586667](image/nav-drawer-section-navItems_funcD/1730311586667.png)

**layoutSpacing_sectionNavDrChild-mob-default**

![1730311595560](image/nav-drawer-section-navItems_funcD/1730311595560.png)

## **Desktop**

**﻿Nav drawer navigation item: desktop default**

![1730311628921](image/nav-drawer-section-navItems_funcD/1730311628921.png)

**layoutSpacing_sectionNavDrParent-desktop-default**

![1730311639495](image/nav-drawer-section-navItems_funcD/1730311639495.png)

**layoutSpacing_sectionNavDrParentSite-desktop-default**

![1730311651759](image/nav-drawer-section-navItems_funcD/1730311651759.png)

**layoutSpacing_sectionNavDrChild-desktop-default**

![1730311658259](image/nav-drawer-section-navItems_funcD/1730311658259.png)

# Differences of Note

**Between mobile and desktop**

* The text size varies be mobile or desktop.
* The text on mobile is smaller
* The child subnav text MAY NEED to be adjusted, so please make separate mobile and desktop styles for the child subnav text. We want to be able to adjust during QA as we actually test it on devices.

**Between the section nav and the primary nav in the navDrawer**

* Unlike the primary nav in the nav drawer, the section nav is always flush left
* Unlike the primary nav in the nav drawer the section nav handles 2 levels deep of nav links

The base width for each navigation item is 256px.
