# The NavDrawerSectionNav provides section-specific navigation in the take-over nav drawer. These are the individual navigation items and their states that compose the [NavDrSectionNavGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b097614ba64626145360a/tab/design?mode=preview).

![1730311747810](image/nav-drawer-section-navItems_intro/1730311747810.png)
