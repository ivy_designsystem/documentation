# At the atomic level, the Primary Navigation is composed of primary navigation categories. The navigation is composed of vertically stacked rows.

Primary navigation categories are the building blocks for the[ Primary navigation group.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61fdad448becb1b0eaf9d3c4?mode=preview)

These are **navigation items for the desktop breakpoints (S-768 to XL-1440).**

![1730311922449](image/primary-nav-items_funcD/1730311922449.png)

And these are **navigation items for the mobile breakpoints (xxs-320 to xs-480).**

![1730311932729](image/primary-nav-items_funcD/1730311932729.png)

**Differences**

* The text is flush left on mobile and flush right on desktop.
* The text on mobile is smaller
* Consequently, the height of the individual nav item row is smaller on mobile

![1730311941444](image/primary-nav-items_funcD/1730311941444.png)

The desktop version is on the left and the flushLeft mobile version is on the right.

The base width for each navigation item is 256px.
