## Section navigation allows users to navigate your website. It is specific to *your* website and is used for all websites that have navigation. Whether the site is for a school, college or unit, section navigation allows users to navigate the site with ease while still being able to access the primary UVM navigation in the header.

![1730312421147](image/section-nav-items_funcD/1730312421147.png)

# Description

**Section specific navigation** is also sometimes called  **"unit" navigation** . The sectionNav items shown here are the navigation title, items, parent items, children subNav items and UI elements that are used in the [section navigation](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b096106ea117c68a970cc/tab/design?mode=preview) panel group.

## Levels deep

The sectionNav goes to 2 levels deep of navigation (item / parent / child subnav). The nav items needed for this depth are shown here.

# Data

Website administrators in Drupal have control over what pages are included in their website's section navigation and the structural organization of the navigation.  UVM's Drupal Guide provides support for[ planning your section navigation ](https://www.uvm.edu/drupalwebguide/plan_your_navigation)and creating a user-centric information architecture (IA) to help make it easy for user's to find content on your site.

The labels for each navigation item as well as the section navigation structure will be created by the website's content admins. The page title will be automatically populated into the section navigation when selected to appear in the menu. Users have the ability to override the default name and provide a name specific to the navigation. If the truncated name is opted for, the name should closely relate to the page title so as to provide confidence in the click and continuity for the user, it should be easily understood and it should be short.

**Title in the section nav is the website group or subgroup name. No substitutions.**

## Things to discuss

The nav items are scalable with recommended text lengths for best practice. We really don't want the text label to go to 3 lines in any scenario as the width of the section nav items changes depending on column widths and breakpoints. The "long" edge case is

The Rubenstein School of Environment and Natural Sciences

This text label goes to **five** lines at the small and medium breakpoints. (sectionNav spans 4 columns at the small breakpoint and 3 columns at the medium breakpoint)

## Custom names / shorter names

How's this:

Menu label: defaults to H1 page title

Menu label custom: We are going to work with OOTB Drupal functionality for menus. H1 page titles by default are the menu names when pages are added to the menu, but Drupal offers the ability to put a substitute menu name.

Issue: how do we make sure that people adhere to best practices and have labels that match the content and provide confidence in the click?

Suggest that we have a tooltip in Drupal admin in concert with education initiatives and documentation.

## Use of acronyms in titles

Avoid using acronyms in titles and opt for more inclusive language. People visiting the website do not know that CAS stands for College of Arts and Sciences and that Rubenstein is the school for environmental studies. Make sure you use language that makes it clear what site the user is visiting and is in a alignment with the actual content on the website.

**Important! **No substitute names for the group (website name) that shows up on the TITLE of the section nav.

# Control icons

The section navigation panel uses the "+" icon to open and the "x" icon to close.

**Hover state micro-interaction**

The "+" and "x" icons are different, but the micro-interaction we are looking for on hover is a 45 degree turn between the open and close state. Start with a 395 degree (360+45 degree) turn to give it more of a "spin" feel.

**Icon sizing**

The open/close icon in the section nav panel is 32px.

The icons for the parent items within the section navigation panel are 24px.

[You can download the SVG icons here.](https://uvmoffice.sharepoint.com/:u:/s/CommunicationsTEAM408/EcQ6ZZfhZbZOp7IBjqDBWL0ByXYTVcH3uFF6z1T-pdzNlA?e=50dmaP)

# Section navigation items

There are 4 different navigation items with all but the spacers having states to provide feedback interaction to the user.

* Section navigation title (group or subgroup name)
* item
* parent item
* subNav child item
* spacers (top and bottom)

Together with their states, that makes 20 different navigation items for mobile and 19 for desktop.

Mobile has a "bottomSpacerClose" item which is put at the bottom of mobile versions of the section nav so users can close an expanded section nav from the bottom as well as the top, eliminating the need to scroll UP to the section navigation title to close the section nav panel. User research has also shown that neurodivergent site users interact better with menus and popups when the "close" functionality displays at both the upper right and lower left.

*Note: the label term "hover" has been used for both the on-tap (mobile) and hover (desktop) states.*

Below is the list of all section navigation items available for xxs mobile.

# SectionNavTitle and states

![1730312440818](image/section-nav-items_funcD/1730312440818.png)

upper left and upper right corners of the title

navigation item have a 4px border radius.  

The 1px outline for the focus state should match that border radius.

# SectionNavItem and states

![1730312447655](image/section-nav-items_funcD/1730312447655.png)

Note: items that have a hover state with a border-left, have a default state with a matching *transparent* border-left. This keeps the text in alignment and not "jumping" on change of states.

# SectionNavParent item and states

![1730312455518](image/section-nav-items_funcD/1730312455518.png)

Note: items that have a hover state with a border-left, have a default state with a matching *transparent* border-left. This keeps the text in alignment and not "jumping" on change of states.

# SectionNavChild item and states

![1730312462135](image/section-nav-items_funcD/1730312462135.png)

# SectionNavParentWebsite link and states

![1730312469560](image/section-nav-items_funcD/1730312469560.png)

Name of parent site is the group name. This happens dynamically so users do not have to manually link to the parent website.



![1730312477086](image/section-nav-items_funcD/1730312477086.png)

# Interaction

Each nav item has a state except for the spacers. The transition between each can be instant- no fade needed.

The control icon to expand / collapse the section nav is a "+" and a "x". The micro-interaction between states is a 45 degree rotation so the icon changes display on rotation to an "x" or a "+" as appropriate.

# Focus state

Focus states are a primary3 1px solid rule around the menu item.

![1730312487243](image/section-nav-items_funcD/1730312487243.png)

![1730312495890](image/section-nav-items_funcD/1730312495890.png)

Note that the upper left and upper right corners of the title navigation item have a 4px border radius.

The 1px outline for the focus state should match that border radius.

# Breakpoints and widths of the section navigation

The section navigation is always present on your website. It varies slightly by breakpoint to optimize for mobile and desktop. Here are the differences by breakpoint.

![1730312503531](image/section-nav-items_funcD/1730312503531.png)

# Layouts and Spacing

## Title

**xxs**

![1730312516881](image/section-nav-items_funcD/1730312516881.png)

**xl**

![1730312525574](image/section-nav-items_funcD/1730312525574.png)

**M**



![1730312534340](image/section-nav-items_funcD/1730312534340.png)

## Section navigation item

**xxs**

![1730312541565](image/section-nav-items_funcD/1730312541565.png)

**xl**

![1730312551452](image/section-nav-items_funcD/1730312551452.png)

## Section navigation Parent item (collapsed)

**xl**

![1730312563843](image/section-nav-items_funcD/1730312563843.png)

**xxs**

![1730312570491](image/section-nav-items_funcD/1730312570491.png)

## Section navigation child item

**xl**

![1730312585534](image/section-nav-items_funcD/1730312585534.png)

**xxs**

![1730312592689](image/section-nav-items_funcD/1730312592689.png)

## Section navigation, parent site link

**xl**

![1730312602636](image/section-nav-items_funcD/1730312602636.png)

**xxs**

![1730312609508](image/section-nav-items_funcD/1730312609508.png)

**Keyboard interactions**

Follows WCAG 2.0 standards for keyboard accessibility guidelines.

# [REFERENCE TABLE TK]
