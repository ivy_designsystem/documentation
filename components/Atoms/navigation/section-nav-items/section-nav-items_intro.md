## Section navigation allows users to navigate your website. It is specific to *your* website and is used for all websites that have navigation. Whether the site is for a school, college or unit, section navigation allows users to navigate the site with ease while still being able to access the primary UVM navigation in the header.

![1730312624241](image/section-nav-items_intro/1730312624241.png)
