# Hero banner images with type get a graduated scrim on the top and the bottom to make both navigation and type visible.

Code:

background: linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(38,41,44,0.804359243697479) 34%, rgba(255,255,255,0) 100%);
