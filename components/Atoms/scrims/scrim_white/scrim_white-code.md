# Code snippet

/* gradations/scrimWhite */

background: linear-gradient(179.09deg, rgba(255, 255, 255, 0.0001) 9.97%, #FFFFFF 89.82%);

transform: rotate(-90deg);



![1730312821381](image/scrim_white-code/1730312821381.png)

## Developer Notes

The scrim can be a fussy  design element.

The following properties will often vary when included within a component, depending on the context.

* Angle
* Decay (how fast does the transition happen?)

*Consider the code above a starting base.*
