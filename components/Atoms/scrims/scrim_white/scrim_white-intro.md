# The white scrim is used on pages with white backgrounds as an overlay to gently fade content away.

![1730312834948](image/scrim_white-intro/1730312834948.png)
