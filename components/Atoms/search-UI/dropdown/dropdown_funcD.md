# The UI Dropdown allows users to select items from a list. This dropdown provides navigation functionality and is used within components such as the Wayfinder.

![1730313190320](image/dropdown_funcD/1730313190320.png)

## Data

**Mandatory**

* Dropdown label
* List

# Purpose

The UI dropdown should only be used to add filter or navigational functionality to components in Drupal. The "pill" design aligns it visually with our brand and specifically, the pill-shaped buttons on our website. Examples of components that use the pill-shaped UI elements include the Search and People Search, Policies, and Wayfinder.

Utilitarian forms for "apply", "Inquire", subscriptions and signups, etc. have a different UI design. The border-radius is less making the form elements for utilitarian forms more rectangular and, well, utilitarian. Also, the rectangular interface aligns with UVMs' single sign on, Slate admissions forms, request more info forms etc.

# States
