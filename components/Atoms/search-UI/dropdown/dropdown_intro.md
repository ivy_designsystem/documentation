# The UI Dropdown allows users to select items from a list. This dropdown provides navigation functionality and is used within components such as the Wayfinder.

![1730313200639](image/dropdown_intro/1730313200639.png)
