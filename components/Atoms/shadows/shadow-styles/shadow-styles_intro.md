# There are 9 levels of shadow elevations- L0 is none and L8 is the highest elevation with the most shadow spread.

InVision DSM does not yet work with layer styles for shadows. These need to be inspected manually. Please see the prototype to pull the CSS information.

![1730313319067](image/shadow-styles_intro/1730313319067.png)
