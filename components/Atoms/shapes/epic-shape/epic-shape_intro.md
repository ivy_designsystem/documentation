# The "epic shape" is a irregular, rounded, slightly skewed, square. Technically, it is in the epitrochoid family of shapes, thus the nickname "epic." It is used minimally on the site and appears in the profile hero as both a mask and a design element.

![1730313582759](image/epic-shape_intro/1730313582759.png)
