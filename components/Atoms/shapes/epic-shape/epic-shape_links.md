## SVG file of the shape asset

https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64e372e98c7cd860bec31746?mode=preview

## Figma asset file

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2056-599&mode=dev

This one is the original shape asset with a solid fill.

## Figma component of shape filled with 1x1 image

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2046%3A102929&mode=design&t=D2cfMHR1IjMMEESF-1

This one has the image fill in place
