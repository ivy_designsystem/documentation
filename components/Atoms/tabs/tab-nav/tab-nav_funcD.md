# Tabs are used within the tabNavBar and Quick Guide components.

# State for individual tabs

* Active
* Default (link)
* Hover
* Press
* Focus


![alt text](A_MGFjZjlkZDY2YjhlM2JmOTZqlBGcURL36NHdsnLkVHkmzX7vtgxAJW2engkE3Veag04PC8nC8JsritmgYgI4zIXtapWWcBw0absUlMfygaqsBiK4kqPKGjmFcP7c_CC72CX9fIMoNE1XSrCL55ZqYA==.png)
# Sizes

* mobile (48px high tab)
* desktop (64px high tab)

# Accessibility

User should be able to navigate between tabs with their keyboard. Keyboard interactions include:

**Left arrow** and **right arrow **keys: moves user between tabs

**Down arrow:** moves user into the content within the active tab selection

**Up arrow:** returns user to the selected tab

# Parameters

Tab label does not wrap to a second line

Keep individual tab label content to ~45 characters total including space

# Data

Manual labels for each.

The total character count of the tab including spaces should not exceed ~48 characters.
