# The vertical tab (molecule) is a navigation UI item for the vertical tab component.

![1730320356158](image/vertical-tab-item_funcD/1730320356158.png)

# Data

**vTab title:** contains the section title and provides the UI control to reveal content Simple text, not more than 100 characters recommended but tab can expand vertically across all breakpoints to accommodate title

**content panel: **contains WYSIWG content

All content is manually entered by the user.

The vertical tab on mobile will be using the accordion display so it is critical that the data and the way it is coded is in alignment between the two components. Please see the [accordion component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview)for more details.

# Content tips

**VTab title **

* The title should be as brief as possible while still being clear and descriptive.
* It should provide confidence in the click and provide a clear indication of the content that will be revealed on engagement.
* All vTab titles should have a symantec head (h1-h6) that is appropriate for the information architecture of the page.

# Interaction

Vertical tabs have three states: default, hover/on-tap, and selected states.

# Responsive notes

# Anatomy
