# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'vTab item' component

![1730320342858](image/vertical-tab-item_naming/1730320342858.png)
