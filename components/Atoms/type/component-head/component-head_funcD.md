# This is a head that is sometimes used in components to provide a short intro.

# Data

Text: simple text

## Purpose

The Component Head was created to standardize an optional head for components and "bake in" the vertical spacing / margins so if used, it appears consistent in size, location, width, and margins.

Compared to the [kicker](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c4ae69c63da73435e2e55/tab/design?mode=preview), which is  typically a categorical description of 1-2 words, the Component Head can be a headline that is the length of a sentence. It is more of an intro head than the kicker which is a category label.

## Design features

The size of the text and margin-top and margin-bottom vary between mobile and desktop.

The width of the component fills the content container of the component it is going into.

For example, if the content of the container is 8 columns, the Component Head is 8 columns.

# Margins

**Desktop**

margin-top: 0

margin-botton: 48

**Mobile**

margin-top: 0

margin-bottom: 32

## Colors

darkGray

## Usage

Used on components with light backgrounds- white or neutral #F7F7F7

# Scalability Notes

The Component head is darkGray for scalability. If we end up with a kicker along with the component head, darkGray will lockup better with the primary1 kicker.
