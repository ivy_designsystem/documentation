# The Kicker is a word or a few words that "kicks off" a section. It gives a categorical label to a component to inform the user of the overarching subject of the content that follows.

# Data:

**Kicker:** simple text. One or two words

# Kicker comes in two sizes:

* desktop
* mobile

desktop: p.largeStrong-uc

mobile: p.bigSisterStrong-uc

Both have a text-transform of uppercase

on-light backgrounds, the color is primary1

![1730314454686](image/component-head_funcD/1730314454686.png)
