# The Kicker is a word or a few words that "kicks off" a section. It gives a categorical label to a component to inform the user of the overarching subject of the content that follows.
