# The titleBar has two variations. onLight and onDark. It is used as an optional section identifier within components. It helps the site visitor understand at a glance the context of the content.

This is the **titleBar-onLight.  **It has **dark** type and is designed to go over light backgrounds.

![1730314538514](image/titleBar_funcD/1730314538514.png)

And here is the titleBar-onDark

![1730314544821](image/titleBar_funcD/1730314544821.png)

By nature, the titleBar text should be short since it is just a categorical anchor to provide context for content on a page.

## Design

The titleBar consists of Roboto uppercase with a 4px solid border left.  Inspect the prototypes for exact specs for type, color, responsive scaling, and spacing.

## Data

The titleBar uses PlainText with a maximum character length of 110.  User enters text.

### Background Color

The titleBar is a pattern that is a part of a larger molecule- it has the same background color of the larger organism it gets placed in. It can also have a transparent / no background. So if the content it is paired with has a white background, the titleBar should have a white background. Background options for solid-neutral currently are white #FFF and neutral #F7F7F7.

* The TitleBar-onLight can go on a white #FFF or neutral #F7F7F7 background
* The TitleBar-onDark can go on a primary1 (green #007155) background

This is the titleBar with a neutral #F7F7F7 background.

![1730314553364](image/titleBar_funcD/1730314553364.png)

This is a titleBar with a white background.

![1730314561989](image/titleBar_funcD/1730314561989.png)

and here it is on a the primaryDark background

![1730314596577](image/titleBar_funcD/1730314596577.png)

## Mobile

For all mobile views, the titleBar is 12 columns wide.

![1730314604388](image/titleBar_funcD/1730314604388.png)

The TitleBar has a maximum character count of 110 characters o keep the character count a readable line length.

The text can wrap and the border-left would scale along with the text, as needed.

![1730314611573](image/titleBar_funcD/1730314611573.png)

## Desktop

![1730314618751](image/titleBar_funcD/1730314618751.png)

## Width of the titleBar by breakpoint

The number of grid columns the titleBar spans varies by breakpoint. This is to ensure a comfortable max width of 70-90 characters for reading. The table below is a reference for titleBar width by breakpoint.

![1730314627384](image/titleBar_funcD/1730314627384.png)
