# The titleBar has two variations. onLight and onDark. It is used as an optional section identifier within components. It helps the site visitor understand at a glance the context of the content.

This is the **titleBar-onLight **it has **dark** type and is designed to go over light backgrounds.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOSA32n4AqoSHqDgmlkUIJ2rrPB648nNP3-Xijf3Y2hc4p6BfYDlLGVXrT8-05YB_OfSsTDFQjxBZbSs2FXiCkVpNPF0pINhIMfbvL35qdWg0UMAfXPJmMcHHvaJqvzp4UQ==?assetKey=university-of-vermont%2Fivy%2FtitleBar-light-and-dark-B1JKBfRzj.png)
