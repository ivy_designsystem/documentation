# These are the base body styles. At present, we do not have separate sizes of these styles for desktop and mobile.

### Body sizes in descending size are:

p.large

plbigSister

p.default

p.small

p.xsmall

### Body Weights for each include:

regular (400)

bold (700)

### Alignments include:

left

center

right

### Colors include:

see Foundations/palette/ for reference

darkGray

lightGray

primary1

white

onDark

onLight

## Vertical Alignment

We need a variable to align them either top or middle.

Top

Middle
