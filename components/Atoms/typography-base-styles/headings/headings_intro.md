# These are the base heading styles for both desktop and mobile. The mobile headings are scaled down a wee bit for reading on a smaller device.

![1730320079530](image/headings_intro/1730320079530.png)

### Base Headings include in descending size

H1.grande

h1.display

h1

h2

h3

h4

h5

h6

### Weights

All weights are bold (700)

### Alignments

left

center

right

### Colors

Please see Foundations / palette for hex values

darkGray

lightGray

primary1

primary2

white

## Vertical Alignment

We need a variable to align them either top or middle.

Top

Middle
