# Labels are the text that go on buttons.

![1730320187439](image/labels_intro/1730320187439.png)

## Sizes

### They come in the following sizes, in descending order.

*See the inspectable prototype above for sizes and style. The default style is intentionally 1px smaller than the p.default.*

bigSister

default

small

xsmall

## Colors

The following colors are needed for the different types and states of buttons.

darkGray

primary1

primary2

primaryLightPOP

white

## Alignments

The following alignments are needed for each button size depending on the button style.

Left

Center

## Vertical Alignment

We need a variable to align them either top or middle.

Top

Middle
