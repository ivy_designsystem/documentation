## The Vertical Tab Group is a group of individual vertical tabs in a list.

See related documentation about the [vertical tab](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64d240f11579ddcf8a638657/tab/functional%20narrative?mode=preview) and the [accordion component.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview)

There is always one tab active at a time. Page loads with initial tab active.

![1729624831334](image/m_vertical-tab-group-funcD/1729624831334.png)

## Anatomy of the Vertical Tab Group ( vTabGroup)

![1729624840137](image/m_vertical-tab-group-funcD/1729624840137.png)
