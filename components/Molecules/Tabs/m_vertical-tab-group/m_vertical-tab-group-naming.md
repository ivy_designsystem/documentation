# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'infoBand nav item



![1729624818478](image/m_vertical-tab-group-naming/1729624818478.png)
