# Individual imageCTA. It's a sub molecule of the imageCTA Group. It has a default state with an image and a hover state with a solid background.

![1728919428769](image/m_cta-image_funcD/1728919428769.png)

## Size Variations

Mobile:

The CTA type on mobile is smaller. It is used from xxs -xs mobile breakpoints, but also on the small desktop breakpoint.



![1728919421313](image/m_cta-image_funcD/1728919421313.png)



![1728919409089](image/m_cta-image_funcD/1728919409089.png)

# Target Content

The CTAsimple text molecule is used in the ImageCTA. The smallest size it appears is in the Small-768 breakpoint. Keep the text to 2 lines max at the small-768 breakpoint. If it goes to 3 lines, it covers the image.



![1728919396800](image/m_cta-image_funcD/1728919396800.png)

## States

The m_imgCTA molecule has a default state and a hover state.

default: has an image background

hover: has a yellow background

Note that the color of the text changes on hover.

![1728919388205](image/m_cta-image_funcD/1728919388205.png)

Image CTA showing the default states with the image and the on-hover / on-tap state with the yellow background.
