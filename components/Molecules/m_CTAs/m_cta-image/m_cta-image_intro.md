# Individual imageCTA. It's a sub molecule of the imageCTA Group. It has a default state with an image and a hover state with a solid background.

![1728919451138](image/m_cta-image_intro/1728919451138.png)
