# Atoms

**Accordion Toggle Controls**

Plus/close icon

Ivy / Foundations / Icons / UI / [plus close icons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

/* Scroll down to the bottom. SVGs can be downloaded */


![1728915205973](image/m_accordion-toggle_atomic/1728915205973.png)
