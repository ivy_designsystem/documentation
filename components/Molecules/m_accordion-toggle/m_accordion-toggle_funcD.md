# The accordion toggle is the list title in an accordion and gives users the ability to expand and collapse the accordion to view and hide additional content.

This is for the accordion toggle (molecule). Please also see the [Accordion component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview).



![1728914975213](image/m_accordion-toggle_funcD/1728914975213.png)

^ accordion toggles and containers: desktop



![1728914997741](image/m_accordion-toggle_funcD/1728914997741.png)

^ accordion toggles and containers: mobile

# Data

Each accordion section consists of an accordion toggle with a category header and a content area, allowing users to toggle the visibility of the content by clicking on the toggle.

﻿

 **Title** : The title is in the accordion toggle. It is manually entered content. Simple text. xxx characters max

**Content:** WYSIWYG content.

 **Important exclusion** : Accordions cannot be nested. Content editors should not be allowed to nest an accordion within an accordion

Note: accordion content is WYSWIG

# Display

Accordions scale horizontally to fill the container they are put into.

**Desktop**

accordion toggle: is 88px min-height and can scale if absolutely needed. Target title length is 100 characters so that 88px height can be maintained on all desktop views... however, it can scale in height if needed. Accordion title is h5-dt head. Accordion +/- control and accordion title center align vertically.

**Mobile**

accordion toggle: Minimum accordion toggle height is 88px. Accordion toggle can scale in height to fit the title while preserving the toggle padding. Accordion title is h5-mobile head. Accordion +/- control and accordion title center align vertically.

# Interaction

Please see the [accordion component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview)for a full description of how the individual accordion toggles perform when placed in the accordion.

# Responsiveness

The Accordion component is responsive and adapts well to various screen sizes and devices. It gracefully adjust its layout to provide an optimal user experience across desktop, tablet, and mobile devices. Maximum column spans for accordions have been defined for each breakpoint to ensure that the line length of the text held in the accordion has an accessible character length for reading.

# Do's and Don'ts

## Media content padding

Image content does not have padding. Text content does. Content container border is outside content.

/* @jordan let's talk about this if you see issues. It's more important on mobile than dt */

![1728915015568](image/m_accordion-toggle_funcD/1728915015568.png)



![1728915029541](image/m_accordion-toggle_funcD/1728915029541.png)

## Text alignments



![1728915042835](image/m_accordion-toggle_funcD/1728915042835.png)

On desktop, the text content aligns left with the text in the accordion toggle

![1728915053604](image/m_accordion-toggle_funcD/1728915053604.png)

On mobile, the text content aligns left with the decorative border-left in the accordion toggle.

# Anatomy

![1728915067202](image/m_accordion-toggle_funcD/1728915067202.png)

![1728915093355](image/m_accordion-toggle_funcD/1728915093355.png)
