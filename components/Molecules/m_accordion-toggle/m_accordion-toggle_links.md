## Figma handoff file, accordion toggle (molecule)

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=1953%3A2083&mode=design&t=qb0Xj2DLZsnTa1f8-1

This is for the individual accordion toggle. Also see documentation for the Accordion component.

## Individual Accordion toggle: Master component

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=1953%3A2059&mode=design&t=2cORTQSZIMOPm58D-1


## Speciications

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2004%3A66413&mode=design&t=2cORTQSZIMOPm58D-1
