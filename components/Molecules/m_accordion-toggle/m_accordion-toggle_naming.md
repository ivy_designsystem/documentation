# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'accordion toggle' component

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOdCnbgHkFMFxu2NdQ0BPQ4cR8snJLwVkSH0Vebnt2a5eyrX3TUAytcm3s4lQiVK9ua0BC0hIS5FuRJ-ihsNlSlDP5Lv75ZLVBFDBUy315ZtFzHtBu-UDB5RcWsTVSuXbfQ==?assetKey=university-of-vermont%2Fivy%2Fnaming_accordionToggle-H1F4KGx33.png)

Note: "dt" or "mobile" can be swapped out for specific breakpoints. ie: xxs, xl
