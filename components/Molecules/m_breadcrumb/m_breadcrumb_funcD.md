# The UVM website employs a cohesive navigation system that works together to help users navigate the different areas of the 400 or so websites that make up the university’s web properties.  This includes the website search, primary navigation navDrawer, the section-specific navigation and the breadcrumb navigation.

The breadcrumb provides users with contextual orientation of the page they are on, rather than conveying the user's browsing history to get there. Breadcrumbs are also useful as a navigation tool to allow users to link 'up the tree' to the parent page and website home page from the page they are on. This is critical on mobile and smaller screen sizes where other orienting content may not be visible due to space limitations and progressive enhancement.

Furthermore, since the section-specific navigation does not include all pages within a site, the breadcrumb can reflect deeper levels of nesting and more specific information to where the user is on the website and more granular clickable navigation within a website. We aim for them to be hardworking and useful and secondarily complementary to the other navigation available.

![1728917208627](image/m_breadcrumb_funcD/1728917208627.png)

Breadcrumb breakdown

![1728917219674](image/m_breadcrumb_funcD/1728917219674.png)

![1728917233287](image/m_breadcrumb_funcD/1728917233287.png)

The website name, separator, and item are set with the p.xSmallUc style.

The breadcrumb Title is the page title, set in p.xSmallBoldUc and is not linked to anything as it identifies the page the user is on.

Do’s and Don’ts

* The breadcrumb is a location path relative to the site the user is visiting- not relative to the uvm.edu root.
* The HOME icon is displayed in lockup with the breadcrumb trail. It is linked to uvm.edu
* Display the complete breadcrumb path- don't use ellipses to truncate any part of the tree.
* When full breadcrumbs path is displayed, it may wrap to 2 or more lines (note: the Home icon hangs)
* Don’t display the breadcrumb trail on the home page
* Don’t display the breadcrumb on a top-level landing page. This would be redundant to the H1 page title and not useful to the user
* The current page *does* get included in the breadcrumb path. However, it *should not* have a link.

Content

* Breadcrumb names are dynamically pulled from the page names in the data
* Page titles are never truncated, do not have "short" names
* Website / group names can have an optional short name. Do not use acronyms in your website name

Breadcrumbs and SEO

* Your page title will populate the breadcrumb dynamically- there will not be a curated "substitute" name for the breadcrumb.
* Incorporate keywords into page titles and breadcrumbs to improve SEO
* Align your page title with words your target audience will use to find content on your website.
* Practice “confidence in the click”. The page title, and keywords should align with the actual content on your page

Functionality

* Users can click on any of the breadcrumb labels to link to pages “up the tree” from the current page.
* Breadcrumb trail should never replace section level or primary navigation.
* Show hierarchy. Not History. Breadcrumb does not show the path that the user took to arrive at the page. The breadcrumb reflects where the user is relevant to the current site they are in

# Display

No display modifications

Links are programmatic.

HOME always will link to uvm.edu

Each breadcrumb is separated by a “/” with a space on either side of the “/”. No substitution.

“/” separator slashes are not linkable to anything. They are just visual dividers.

## Dark version

Q: is this needed?

We do have primary1 as a background for some components used in the top-tier. Facts, CTA teaser, for example. If a variation is needed for on-Dark, here are the colors:

* Default color: #F2FBF3 primary1-faded
* Hover color: #FFF
* Visited color: #FFF
* Current page: #FFF, bold style
* Focus: focus box is primary3 #FFD416
* Dividers: 50% alpha white

Here is what this would look like, along with the options for the visited color and their contrast ratios.

![1728917245605](image/m_breadcrumb_funcD/1728917245605.png)

![1728917258676](image/m_breadcrumb_funcD/1728917258676.png)

![1728917268174](image/m_breadcrumb_funcD/1728917268174.png)

Focus color: site wide, focus color is primary3

Note that 50% alpha white is used for divider pipes and "/" to make them less prominent.

The Visited color is a light tint of primary3.

![1728917280978](image/m_breadcrumb_funcD/1728917280978.png)

![1728917303452](image/m_breadcrumb_funcD/1728917303452.png)

## Behaviors

**Six breadcrumb states**

Default color: #5D646B lightGray

Hover color and active: #26292C darkGray, underlined

Visited color: #5D646B lightGray

Focus: focus box is primary3 #FFD416

Dividers: #5D646B lightGray

Current page: #5D646B lightGray, bold style

![1728917347275](image/m_breadcrumb_funcD/1728917347275.png)

![1728917357810](image/m_breadcrumb_funcD/1728917357810.png)

## Hover-on-tap states

![1728917368723](image/m_breadcrumb_funcD/1728917368723.png)

![1728917378387](image/m_breadcrumb_funcD/1728917378387.png)

# Responsive differences

Breadcrumb fills the container it is in and can wrap. It’s one size fits all- the size of the text or icons do not change.

# Multi-line breadcrumbs

If the text wraps to multiple lines, the vertical divider rule between the home icon and the breadrumb text scales vertically to fill the breadcrumb container. The home icon centers vertically as this rule scales.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOUPOllSAgQ-AxL6f4syTswBpGe64WaHUEEctU-F0Ne6nsQoThf8nS4GzfSe9O4u6UF2qdAjfvyyiUsdWqGlkI8NHoVnxjJSXJWLwr0pzq_jwC5SfqFjubeeT7PLgKLPnPg==?assetKey=university-of-vermont%2Fivy%2Fimg-1679493132006-2-By-NGdtux3.png)

# Don't: Breadcrumb text formatting

![1728917387572](image/m_breadcrumb_funcD/1728917387572.png)

## Overview page UX decision

We have a page called "Overview" on each site- which is really the home page for each site. By naming it, it allows users an easy way to navigate back to the website's home page. However, as it solves one problem, it also creates another.

If we applied the breadcrumb functionality to the overview page for the Rubenstein School, it would look like this:

![1728917396489](image/m_breadcrumb_funcD/1728917396489.png)

❌ The above is wrong for all Overview pages as clicking on the parent in the breadcrumb trail would not navigate to a new page.

**✔  Solution for ALL home page "overview" pages**

Leave off the "Overview" name in the breadcrumb as it is one in the same with the home page and it is indicated in the section navigation. In this scenario, the breadcrumb reflects the name of the website.

Here is how the breadcrumb would appear for the Rubenstein School's home page.

![1728917412920](image/m_breadcrumb_funcD/1728917412920.png)

## Development considerations for screen readers

* **Note: The visual / separators do not need to be text and are not intended to be navigable by keyboard.**
* *I would like to include a table of the keyboard interactions here after you work them out. Developers please provide this information when available.*
* *Will the breadcrumb be implemented as an unordered list so screen readers can provide more context?*
