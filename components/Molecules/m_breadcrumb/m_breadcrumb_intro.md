# **Breadcrumbs not only provide users with contextual orientation within the website, they also allow users to navigate ‘up the tree’ from the page they are on.  This is critical on mobile and smaller screen sizes where other orienting content may not be visible due to space limitations and progressive enhancement. **

![1728917170116](image/m_breadcrumb_intro/1728917170116.png)
