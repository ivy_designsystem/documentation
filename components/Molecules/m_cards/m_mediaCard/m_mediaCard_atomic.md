# Atoms

**Arrow icon**

Ivy / Foundations / Icons / UI / [icn_arrowR-primary2](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

**Image**

16x9 image

**Play Button**

Ivy / Components / Atoms / Buttons / [Play Button](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63dbdc6741981d7ffdb289b9/tab/design?mode=preview)
