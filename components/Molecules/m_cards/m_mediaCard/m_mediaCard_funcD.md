# The Media Card is a single card with an image or video. It is a molecule of the larger mediaCardGroup and is not used as a stand-alone element. It is a hard working, ubiquitous component as it can be used to elevate content and drive users to deeper levels of the site.

![1728918973897](image/m_mediaCard_funcD/1728918973897.png)

**Above ^** *Examples of the mediaCard. The left has a still image and the right is also a still image but representative of a video. *

## Data

### Mandatory

**(1) media element: **

image: 16x9 aspect ratio (aspect ratio is essential to maintain)

Minimum size: 960x540

Maximum size:  1920 x 1080

**Title:  **simple text

**Description:  **simple text

**Link: **Valid link is mandatory.

Can be either

* to launch a video hosted by 3rd party in take-over
* or link that brings user to another page on the website

*Note: Not recommended to use this component to link to a 3rd party website. (the right arrow would not be the correct arrow for external links)*

### Optional

n/a.

## Media holding rule

Media in the Media Card has a modalBlack-05 1px border .. just as a very subtle holding rule in case the media placed there is white/light. It appears nearly invisible until needed to hold the edge.

## Missing Image

If the image is missing for the Media Card, a [16x9 placeholder, transparent image](https://uvmoffice.sharepoint.com/:f:/s/UVMWebsiteRedesign/EqQ-hhEauNlOufkxJqCbGqABHvtALNY4c8LjfYYd-w3trQ?e=kKQUKw) holds the space. The user would see no visible image (background color of the component) and the holding rule.

## Functionality

The mediaCard is a teaser for either a video or a web page.

If the link is a video link, the "Play Video" icon will show over the 16x9 image.

**Hit area**

Entire card is the 'hit' area. On click/tap, takes user to the link provided for the card.

**Link**

The link can be to another web page or to a 3rd party hosted video. If the link is to a video, it opens in a full takeover with 100% modal black overlay. (double check that.. should match prior release)

## Interaction

**Entire card is hit state.**

[Follow the interaction used on the featured story in the News Teaser component.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/functional%20narrative?mode=preview) (image has a saturation shift, arrow at bottom moves 8px to the right, title gets a primary3 underline that animates on, left-right and top-bottom.)

## Design Elements

**Rules **

There is a bounding rule at the bottom on mobile: it is a solid rule, transparent black (gray)

## UI Elements

There is a right arrow, color primary2 at bottom of each card.

There is a "play" button over the image if a video link is provided. It varies in size by breakpoint. It has a shadow.

## Responsive Notes

See the spec. There are some differences by breakpoint to text sizes. Also the width for each is determined by the parent component (Media Card Group)



![1728918985408](image/m_mediaCard_funcD/1728918985408.png)

Above ^ Examples of the mediaCard at the xxs-320 breakpoint.
