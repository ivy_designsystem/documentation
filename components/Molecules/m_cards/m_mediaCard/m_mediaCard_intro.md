# The Media Card is a single card with an image or video. It is a molecule of the larger mediaCardGroup and is not used as a stand-alone element. It is a hard working, ubiquitous component as it can be used to elevate content and drive users to deeper levels of the site.

![1728919006506](image/m_mediaCard_intro/1728919006506.png)
