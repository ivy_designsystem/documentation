# Atoms

**Icon assets**

Downloadable icon assets in SVG format for both individual grid and list icons as well as the pair grouped together.

Ivy / Foundations / Icons / [list and grid ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64f7637ae1135b7d20e4e097?mode=preview)controls

**Documentation for list and grid icons**

Ivy / Components / Atoms / [ListGrid icons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f725e08c7cd8e2ecc98c87/tab/atomic%20elements?mode=preview)
