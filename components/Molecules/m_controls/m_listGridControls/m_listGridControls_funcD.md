# The "listGrid" controls component is a pair of icons that allow the user to select either a grid or list display type.

![1728919168535](image/m_listGridControls_funcD/1728919168535.png)

## Description

The "ListGrid Controls" is a pair of icons that provides a user interface selection for displaying a group of items in either a list form or a grid layout.

## Behavior

The pair of icon buttons function very much like a toggle control. On page load, one is selected and displays a "selected" state with the circle background of the icon button filled-in gray. The modal gray background is the same for both the hover and selected states. The user can select the non-selected icon to change the display of the parent component.

Design

Default: Icons are lightGray with a transparent background

Hover: Icon is darkGray with a modal background

Selected: Same as hover. Icon is darkGray with a modal background

Focus: Same as hover and selected with the addition of a 2px primary3-lighter box around the icon. Border should be "Inside" so button doesn't "jump".

Size:

The size of the control icons are larger on mobile to provide a nice surface area to tap.

Mobile: 56px

Desktop: 48px

# Hover states

There is no "active" hover state. The selected icon should not provide a link to display itself.

The unselected icon has a hover state.

Hover and active states are visually the same.

Focus: 2px primary3-lighter border. If border can be "inside", that will help the icon from "jumping" on-focus.

## Accessibility Notes

Icons have a focus state and should be navigable by keyboard.

## Responsive Notes

Size varies from mobile to desktop.
