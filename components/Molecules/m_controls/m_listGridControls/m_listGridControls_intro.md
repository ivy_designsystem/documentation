# The "listGrid" controls component is a pair of icons that allow the user to select either a grid or list display type.

![1728919155787](image/m_listGridControls_intro/1728919155787.png)
