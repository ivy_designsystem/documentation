# It's important that everything has a name and that we all use the same name to refer to design elements and components.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about something.

# Naming for the ListGridControl icon group:

![1728919143747](image/m_listGridControls_naming/1728919143747.png)
