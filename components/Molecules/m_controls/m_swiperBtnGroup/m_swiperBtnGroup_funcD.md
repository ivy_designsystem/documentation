# The SwiperBtnGroup is used for internal navigation to empower users to view the NEXT or PREVIOUS of something. It is used in the FactsSwiper.

## States

The SwiperBtnGroup's individual arrows have the following states:

* Default (link)
* Hover / on-tap
* Disabled

For example, in a slider:

* When a user gets to the beginning of the facts slider, the PREVIOUS arrow has a disabled state
* When a user gets to the END of the facts slider, the NEXT arrow has a disabled state

![1728919257342](image/m_swiperBtnGroup_funcD/1728919257342.png)
