# The SwiperBtnGroup is a pair of SwiperBtns used to provide internal navigation for a swiper / slider to help the user navigate to the previous and next item in the component.

![1728919274064](image/m_swiperBtnGroup_intro/1728919274064.png)
