# UPDATE: 4/25/23 alignment changed from flush left to centered. Susy to make new screen grabs.

# Single fact consisting of a number and a description. Used in the Facts Swiper.

## CSS Class names

Facts exist on the Drupal site. Since we are working with the same database and this is a redesign of an existing component, we are going to keep the same CSS class names.

## Data

![1728919542473](image/m_fact_funcD/1728919542473.png)

**There are 2 data types that make up a fact. Both are mandatory.**

**Highlight**

The individual fact is composed of a larger number or fact called the "highlight".

**Sub-Highlight**

The sub descriptor text is called the "sub-highlight".

## Color variations

## Facts have 2 color variations:

indFact-onDark

indFact-onLight

The onDark is for facts that go on a dark background.

The onLight is for facts that go on a light background.

Currently we have just primary1 for the onDark color.

And white and neutral for the backgrounds for the onLight color.

## Size Variations

The individual fact comes in two size variations:

* xxs
* M

The xxs size is used in Fact Groups for the xxs-320 to S-768 breakpoints.

The M size is used on FactGroups for the M-1012 to XL-1440. breakpoints.

The type is smaller in the xxs fact to fit the breakpoints at the lower end of the responsive scale.

The type is larger for the Medium fact for breakpoints Medium to XL.

## IMPORTANT DEV NOTES

The InActive state is not applicable in the Medium size as the medium size is not used in the FactsSwiper.

Please see the [FactGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/634ebe016a1a8fbee36bb449/tab/design?mode=preview) for more info about how the Facts are used.
