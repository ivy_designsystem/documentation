# UPDATE: 4/25/23 alignment changed from flush left to centered. Susy to make new screen grabs.

# The FactsGrp is a group of 3 individual facts with each consisting of a number and a description. Used in the Facts Swiper.



![1728921012186](image/m_fact-group_funcD/1728921012186.png)

## Data

The FactsGroup is made up of a group of [IndividualFacts](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/614a3fa0334e545d012de914/tab/design?mode=preview).  The FactsGroup is used in the [FactsSwiper](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/614b48b5b4841ffe20c93cc2/tab/design?mode=preview).

## Color variations

## FactGroups have 2 color variations:

factGroup-onDark

factGroup-onLight

The onDark is for facts that go on a dark background.

The onLight is for facts that go on a light background.

Currently we have just primary1 for the onDark color.

And white and neutral for the backgrounds for the onLight color.

## Size Variations

The individual fact comes in two size variations:

* xxs
* M

The xxs size is used in Fact Groups for the xxs-320 to S-768 breakpoints.

The M size is used on FactGroups for the M-1012 to XL-1440. breakpoints.

The type is smaller in the xxs fact to fit the breakpoints at the lower end of the responsive scale.

The type is larger for the Medium fact for breakpoints Medium to XL.

## IMPORTANT DEV NOTE

The padding in each individual fact changes per fact group to keep the fact text a readable length.
