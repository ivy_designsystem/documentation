# Form Elements are the nuts and bolts of what goes into a form. Our forms are designed to be accessible for all so sighted, visually impaired and motor-impaired users can easily engage with them.

## 1. Form Elements Overview

 Forms on our website provide a crucial role in collecting information, requesting information, performing surveys, user registration and authentication, to name a few applications. They are an important touch point with our website audience. The design, usability and accessibility of forms on our site is critical to engagement. Ultimately, no one likes to fill out a form. But a well-designed form is essential to ensure a positive user experience, increased engagement, and increase completion rate of filling out forms.

Form elements are the different types of input, text, labels, and UI elements that are used in the form interface.

## 2. Design Principles

The design principles followed when designing UVM’s forms and form elements include:

**Consistent design**

Consistency in design is crucial; ensure uniformity in typography, color, and spacing to create a visually harmonious form.

**Effective use of white space **

White space is used to reduce visual clutter and enhance readability.

**Responsiveness**

The form elements and forms are designed for consistency across all breakpoints and to adapt to different screen sizes and orientations. They are designed to perform well across different platforms, devices and viewing scenarios.

![1728921145349](image/m_form-elements_funcD/1728921145349.png)

**Accessibility **

The form design follows accessibility best practices to ensure that individuals with various disabilities, including visual, auditory, motor, and cognitive impairments, can use our forms independently. Key accessible design aspects built into the forms include adequate contrast for all states (with the exception of the disabled state), descriptive label elements above each form element, focus styles to identify where the focus is for keyboard users, keyboard accessibility to ensure that users can successfully interact with all forms using a keyboard, and an error state to support our error handling approach to guide users through successful form completion.

The use of accessible design best practices combined with semantic html, error handling, and aria roles and attributes work together to create an accessible experience of forms on the UVM website.

## 3. Visual Design

* For all breakpoints, text entry fields and drop downs are 48px high
* Label text is the same size for all breakpoints and appears above the form element.
* The form elements have distinct states so user engagement with the form is clear.

### Form inputs: radio buttons

![1728921158798](image/m_form-elements_funcD/1728921158798.png)

### Form inputs: text input

![1728921167665](image/m_form-elements_funcD/1728921167665.png)

^ Note error state has an alert icon to make it accessible for color impaired users.  Team needs to discuss error handling and how it is done. This form element alerts that there is an error with the entry in the form element but the error handling strategy is needed to convey to the user what went wrong and how to fix it.

### Form labels

![1728921179248](image/m_form-elements_funcD/1728921179248.png)

### Form inputs: selections, dropdowns

![1728921187599](image/m_form-elements_funcD/1728921187599.png)

## Form submit button

![1728921201929](image/m_form-elements_funcD/1728921201929.png)

See the [documentation for the solid button](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6397e0ddc887bdbe24eedc1f/tab/design?mode=preview). We are using this for the submit button instead of creating another button specific to the form. The scalable grid button may replace it in the future.  The pros of using this button now are:

* It's available and already developed
* It's recognizable as a button on the website
* It's in the expected lower left location on the form, where users naturally look for it. This makes it easier for screen magnifier useres.

## 4. Layout and spacing

![1728921213577](image/m_form-elements_funcD/1728921213577.png)

## 5. Accessibility

Our forms and form elements follow both design and programming best practices to make them accessible, including:

** Semantic HTML**

Use semantic HTML elements (e.g., `<form>`, `<input>`, `<label>`) to structure the form. Semantic elements provide clear and meaningful information to assistive technologies and help users understand the form's purpose.

**Labels**

Labels associate each form control (input, select, textarea, etc.) with the input element. Labels appear above the form element and should be descriptive and provide context for the associated form element.

**Keyboard Accessibility**

Ensure that users can navigate and interact with the entire form using only a keyboard. This includes using the "Tab" key to move between form elements and the "Enter" or "Space" key to activate buttons or submit the form.

**Focus Styles**

Our forms have clear and visible focus indicators for form elements, so users can easily identify where their keyboard focus is. Avoid relying solely on color changes for focus indication.

*Note: we are aware that our use of primary3-lighter for our focus color achieves WCAG accessible contrast requirements on dark backgrounds but not on white, neutral or light backgrounds. We will address this after the new brand guidelines are available. At present, we do not know what the new palette will be. *

**Color to identify states**

Color is also used to identify form element states. The default border state and type color within the elements are lighter than their respective active states. Primary1 green is used for the border for the active state. It is OK that it is the same as a “success” color as we are assuming success until an error is determined. At that point, the form border color changes to Error Red.

**Error Handling**

The errant form field’s border changes to “Error Red”.  See [Forms documentation](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4ab5829471d06bd2c5e/tab/functional%20narrative?mode=preview) for more information about error handling.

**Aria Roles and Attributes**

Use ARIA (Accessible Rich Internet Applications) roles and attributes when needed to enhance the accessibility of dynamic or interactive form elements, especially in single-page applications (SPAs) or web applications.

**Responsive Design**

Design forms that adapt to different screen sizes and orientations, making them usable on a variety of devices, including mobile phones and tablets.

**Compliance with Accessibility Standards**

Follow recognized accessibility standards and guidelines, such as the Web Content Accessibility Guidelines (WCAG), to ensure your form meets accepted accessibility criteria.

Except for the disabled style and the aforementioned issue with the focus color, all color combinations in the form are WCAG 2.0 AAA contrast for the type and 3:1 contrast or better for border contrast.

Avoid using color as the sole means of conveying information.

Making a form accessible is a crucial step in creating an inclusive web experience, allowing all users, regardless of their abilities, to engage with your website's content and services effectively.

## 7. Responsiveness

Form elements scale horizontally to fill the container they are placed in. They adapt to different screen sizes and devices.

Form elements are all designed for the smallest width- 256px wide as used in the xxs portrait breakpoint. They scale horizontally to fill the container they are placed within.

![1728921248369](image/m_form-elements_funcD/1728921248369.png)
