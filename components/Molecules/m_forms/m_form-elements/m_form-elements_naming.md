# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the form elements that we have so far in our design system:

## Text field inputs



![1728921283193](image/m_form-elements_naming/1728921283193.png)

^ txtFld_mandatoryDefault



![1728921293823](image/m_form-elements_naming/1728921293823.png)

^ txtfld_mandatoryActive



![1728921306656](image/m_form-elements_naming/1728921306656.png)

^txtFld_mandatoryDisabled



![1728921314933](image/m_form-elements_naming/1728921314933.png)

^txtFld_mandatoryError



![1728921321317](image/m_form-elements_naming/1728921321317.png)

^txtFld_focus

## Labels

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOa-PSgWLUJEimuvVwvS1wZvgOeGxPnlIODwtlx76BSBmsZwB_Yh6dHI-qb0Uua4YlovEV26OFteKo8Sl_xFJ8V2L10R0_XWQjSbGJ3ZR_MVZXXGRsFihS7TGYYooh3IBWA==?assetKey=university-of-vermont%2Fivy%2Flabel_mandatory-r1hGusl1p.png)

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmObHUo-a1nzaG8vcvCpr0OJQkDcqmXrTby48yBk0bkFuUoDz2NnWJzijs9bjucvgFBoUEHPdUl4x2YBK5fjQfXVfvI57pnihKNBlcqBsq7MjaeuadykN8mH674j2K4mr0Cw==?assetKey=university-of-vermont%2Fivy%2Flabel_nonMandatory-HkVmdsgyT.png)

Mandatory label has a red asterisk.

Non mandatory label does not have a red asterisk.

label_mandatory

label_nonMandatory

## Radio inputs



![1728921329510](image/m_form-elements_naming/1728921329510.png)

^ radioBtn_default



![1728921336353](image/m_form-elements_naming/1728921336353.png)

^radioBtn_selected

## Dropdown and dropdown options



![1728921346944](image/m_form-elements_naming/1728921346944.png)

^dropdown_mandatoryDefault



![1728921353157](image/m_form-elements_naming/1728921353157.png)

^ dropdown_mandatory_active



![1728921359807](image/m_form-elements_naming/1728921359807.png)

^ dropdown_mandatoryExpanded
