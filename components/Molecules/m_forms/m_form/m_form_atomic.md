# Atoms

## Caret icon in the dropdown

The dropdown element has a caret icon.

Ivy / Foundations / Icons / [icn_caretUp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/635941be31e244b89af9969f?mode=preview)

Ivy / Foundations / Icons / [icn_caretDown](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/635941be31e244b89af9969f?mode=preview)

## Solid button

We are using the solid button in primary1 for the submit button

Ivy / Foundations / Atoms / Buttons / [solid button](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6397e0ddc887bdbe24eedc1f/tab/design?mode=preview)

# Molecules

 Ivy / Components / Molecules / Forms / [Form Elements](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4a793ea317f903050bd/tab/atomic%20elements?mode=preview)
