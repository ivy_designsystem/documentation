# Forms are designed to be accessible for everyone. Sighted, visually impaired and motor-impaired users can easily engage with our forms.  They are straight forward and easy to understand. They are designed so people can complete them easily without fuss or excessive effort.

This form is the result of a mini sprint for a form for the program pages. We will be building upon this over time.



![1728923210588](image/m_form_funcD/1728923210588.png)

## 1. Form Overview

 Forms on our website provide a crucial role in collecting information, requesting information, performing surveys, user registration and authentication, to name a few applications. They are an important touch point with our website audience. The design, usability and accessibility of forms on our site is critical to engagement. Ultimately, no one likes to fill out a form. But a well-designed form is essential to ensure a positive user experience, increased engagement, and increase completion rate of filling out forms.

Please also see the documentation for [form elements](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4a793ea317f903050bd/tab/functional%20narrative?mode=preview), the inputs and elements that make up a form.

## 2. Design Principles

The design principles followed when designing UVM’s forms and form elements include:

**Consistent design**

Consistency in design is crucial; ensure uniformity in typography, color, and spacing to create a visually harmonious form.

**Effective use of white space **

White space is used to reduce visual clutter and enhance readability.

**Responsiveness**

The form elements and forms are designed for consistency across all breakpoints and to adapt to different screen sizes and orientations. They are designed to perform well across different platforms, devices and viewing scenarios.

Form elements are designed for the smallest size- xxs-portrait and are 256px minimum width by default. They scale horizontally to fill the container they are in. The same form elements work for both mobile and desktop.



![1728923220476](image/m_form_funcD/1728923220476.png)

**Accessibility **

The form design follows accessibility best practices to ensure that individuals with various disabilities, including visual, auditory, motor, and cognitive impairments, can use our forms independently. Key accessible design aspects built into the forms include adequate contrast for all states (with the exception of the disabled state), descriptive label elements above each form element, focus styles to identify where the focus is for keyboard users, keyboard accessibility to ensure that users can successfully interact with all forms using a keyboard, and an error state to support our error handling approach to guide users through successful form completion.

The use of accessible design best practices combined with semantic html, error handling, and aria roles and attributes work together to create an accessible experience of forms on the UVM website.

## 3. Usage Guidelines

These form elements have been designed for use on the program pages. We have not done a full form sprint. We’ll see where this takes us. Here are some overarching usage guidelines for when creating forms on the website.

**Simplicity and clarity **

Keep the form as concise as possible. Remember that people do not like to fill out forms so only request essential information and use clear and concise labels. Every extra field you add to your form is an obstacle to user form completion.

**Flow and order **

Maintain a logical and intuitive flow, arranging fields in a natural order and grouping related elements together.

## 4. Visual Design

Text sizes for microcopy varies between mobile and desktop.  On desktop it is p.defaultStrong size and on  mobile p.smallBold.

For all breakpoints, text entry fields and drop downs are 48px high

Label text is the same size for all breakpoints.

The form elements have distinct states so user engagement with the form is clear.

The desktop and mobile versions of the form are nearly identical. They differ only by the size of the microcopy and the heading type size. The component itself has two variations on desktop.



![1728923241361](image/m_form_funcD/1728923241361.png)

^ The desktop version



![1728923252455](image/m_form_funcD/1728923252455.png)

^ The mobile version. On mobile, there is no background, border or elevation.

## 5. Interaction

Forms are not progressive. All fields are displayed at once.

Form fields have a fast fade micro-transtion upon on-click and on-tap interactions between states- ie: default to active.

The user’s interaction with the form should be clear. Active states let users know which field they are engaging in. Error states and error handling helps users successfully complete the form. And a “success” message should be delivered to the user immediately upon successful completion of the form. Completion is after the “SUBMIT” button is clicked/tapped and the submitted data was accepted.

*DISCUSS WITH DEV: Where does the success menu print? Current form tested here gave no success message- not printed to the screen or via email. https://www.uvm.edu/business/macc_master_accountancy *

## 6. Accessibility

Our forms follow both design and programming best practices to make them accessible, including:

** Semantic HTML**

Use semantic HTML elements (e.g., `<form>`, `<input>`, `<label>`) to structure the form. Semantic elements provide clear and meaningful information to assistive technologies and help users understand the form's purpose.

**Labels**

Labels associate each form control (input, select, textarea, etc.) with the input element. Labels appear above the form element and should be descriptive and provide context for the associated form element.

**Keyboard Accessibility**

Ensure that users can navigate and interact with the entire form using only a keyboard. This includes using the "Tab" key to move between form elements and the "Enter" or "Space" key to activate buttons or submit the form.

**Focus Styles**

Our forms have clear and visible focus indicators for form elements, so users can easily identify where their keyboard focus is. Avoid relying solely on color changes for focus indication.

*Note: we are aware that our use of primary3-lighter for our focus color achieves WCAG accessible contrast requirements on dark backgrounds but not on white, neutral or light backgrounds. We will address this after the new brand guidelines are available. At present, we do not know what the new palette will be. *

**Color contrast**

Except for the disabled style and the aforementioned issue with the focus color, all color combinations in the form are WCAG 2.0 AAA contrast for the type and 3:1 contrast or better for border contrast.

**Color to identify states**

Color is also used to identify form element states. The default border state and type color within the elements are lighter than their respective active states. Primary1 green is used for the border for the active state. It is OK that it is the same as a “success” color as we are assuming success until an error is determined. At that point, the form border color changes to Error Red with an error handling message below the form element to alert the user to the issue and help them correct it.

**Error Handling**

Provide clear and descriptive error messages when users make mistakes or omit required information. The errant form field’s border changes to “Error Red” and a "warning" icon appears after the form label for the errant element. The warning icon is to make the error message equally accessible for color-impaired users so as not to solely rely upon the color red to denote an error.

Continue without current practice– put error messages at the top of the form. Placing them below the errant form element can be problematic. Make sure that the errant form inputs / fields display their error states so the user can see all errors with their form completion.

The error message should be prefixed with the word "Error".

## DISCuSS with DEV error handling. Things that I think would not be good:

## a long list of errors to work through which would be overwhelming for the user and a negative experience

## the word "Error" is recommended as a prefix to provide context for screen reader users BUT a list of things beginning with "Error", "Error", "Error", will leave users feeling insulted and bad. What to do here? How do we provide the context of error without it being negative?



![1728923263552](image/m_form_funcD/1728923263552.png)

^ To discuss

TIAA's example here is nearly correct.

**Things that are correct**

* The error message is at top so is read first by screen readers.
* The error message at top has color coding for sighted non color impaired users and the exclamation for color impaired / color blind users
* The error and steps to fix are concise and near the errant step

**Things not best practice **

* The error and how to fix should be before the errant item. The placement below can be problematic in some browser scenarios

**Questionable**

The error message at the very top is recommended to have the prefix "Error" before to give this message context. I am putting this as a questionable practice as it can make the user feel like they did something wrong. Up for discussing ideas about this.

**Aria Roles and Attributes**

Use ARIA (Accessible Rich Internet Applications) roles and attributes when needed to enhance the accessibility of dynamic or interactive form elements, especially in single-page applications (SPAs) or web applications.

**Consistent Layout**

Maintain a consistent layout and ordering of form elements, such as placing labels before input fields. This helps users predict the location of form controls.

**High Contrast**

Ensure there is sufficient contrast between text and background colors to make content readable. Avoid using color as the sole means of conveying information.

**Responsive Design**

Design forms that adapt to different screen sizes and orientations, making them usable on a variety of devices, including mobile phones and tablets.

**Testing with Assistive Technologies**

Conduct usability testing with screen readers, voice recognition software, and other assistive technologies to identify and address accessibility issues.

**Documentation and Help**

Provide clear instructions and help text for users, especially when the form involves complex interactions or when specific input formats are required.

**Progressive Enhancement**

Start with a solid foundation of accessible HTML and then enhance the form's functionality with JavaScript while ensuring that it remains usable without JavaScript.

**Compliance with Accessibility Standards**

Follow recognized accessibility standards and guidelines, such as the Web Content Accessibility Guidelines (WCAG), to ensure your form meets accepted accessibility criteria.

Making a form accessible is a crucial step in creating an inclusive web experience, allowing all users, regardless of their abilities, to engage with your website's content and services effectively.

## 7. Responsiveness

Form elements scale horizontally to fill the container they are placed in. They adapt to different screen sizes and devices.

Form elements are all designed for the smallest width- 256px wide as used in the xxs portrait breakpoint. They scale horizontally to fill the container they are placed within.



![1728923272073](image/m_form_funcD/1728923272073.png)
