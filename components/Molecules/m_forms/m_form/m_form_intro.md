# Forms are designed to be accessible for everyone. Sighted, visually impaired and motor-impaired users can easily engage with our forms.  They are straight forward and easy to understand. They are designed so people can complete them easily without fuss or excessive effort.

![1728923001395](image/m_form_intro/1728923001395.png)
