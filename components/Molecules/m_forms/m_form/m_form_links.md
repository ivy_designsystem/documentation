## Master component in Figma

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2192-35820&mode=dev

You can inspect the one labeled "4Ivy"

## Handoff area in Figma

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2192-35923&mode=dev

There are some notes here but the master component is easier to inspect

## Master components: individual form elements

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2192-9360&mode=dev
