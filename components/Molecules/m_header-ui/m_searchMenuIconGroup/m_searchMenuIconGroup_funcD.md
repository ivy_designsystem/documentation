# The SearchMenuGroup is an interface molecule that appears in the header and the navDrawer.

# Description

This is an icon group consisting of the Search icon and the (hamburger) menu icon. It appears on the far right side of the header, flush right with the 32px right margin left to the right of the icon group.

# Data

None.

# Functionality

Desktop: Default and hover states, 48px size

Mobile: Default and on-tap states, 40px size

**The search icon launches** the TabSearch overlay to allow users to either search ALL of UVM or for someone in the People directory.

**The navigation icon **launches the full take-over NavDrawer.

# Spec

The searchMenu Group comes in both 40 and 48px sizes. Usage varies by breakpoint.

![1728926217378](image/m_searchMenuIconGroup_funcD/1728926217378.png)
