# This component is not a stand-alone component- it is the foundational structure for all the 5050 components. This lockup is essential to all 5050s- no matter the flavor.

## Layout differences by breakpoint

**xxs - xs**

The image and the textGrp span 12 columns for the xxs and xs breakpoints. The 32x L/R margins are respected.



![1728916931080](image/m_5050-contentGrp_funcD/1728916931080.png)

**S-M**

The image and the textGrp each take up 6 columns. The image is 6col + 8px with the extra width hanging into the center gutter. The 32px L/R margins are respected.



![1728916920489](image/m_5050-contentGrp_funcD/1728916920489.png)

**L-xxL**

For the L-XXL breakpoints, the image and textGrp each take up 5 columns. The image is 5col + 8px with the extra width hanging into the center gutter. The 32px L/R margins are respected.

![1728916906308](image/m_5050-contentGrp_funcD/1728916906308.png)

**CTA variation**

The single CTA button can be replaced by a [linkList with the arrow icon after](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393afe4f291f20ef5204390/tab/design?mode=preview).
