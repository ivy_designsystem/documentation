# Accessibility

Value required for the label field

## Placeholder text

The placeholder text in the dropdown is to indicate to the user that there are dropdown options to select from.

# Examples in context



![1728927017313](image/m_form-fields_funcD/1728927017313.png)

## Drop down functionality

We have been wanting to meet a MVP with the launch of the newly designed home page so are working quickly and using MUI framework to cut corners. For that reason, this is what is specified for the drop down.



![1728927025061](image/m_form-fields_funcD/1728927025061.png)

### Future / wish list functionality

If at all possible, the drop down functionality could be visually improved if the drop down form field expanded down to review the drop down elements. something like this:



![1728927034446](image/m_form-fields_funcD/1728927034446.png)
