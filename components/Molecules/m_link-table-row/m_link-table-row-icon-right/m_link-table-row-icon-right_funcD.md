# This component is a text row in a LinkTable. The text has a mandatory link. The icon is on the right and is typically an arrow.

*Please see the *[*LinkTable-IcnR*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63c03b231f9a351229fa169c/tab/design?mode=preview)* which is intrinsically related to this.*

## Sizes

The linkTableRowIcR comes in 3 different text sizes:

* p.small
* p.default
* p.bigSister

**Developer Note**

*We only need p.small and p.default for the LinkTableIcnR component. You can hold off on the bigSister size.*



![1728927557317](image/m_link-table-row-icon-right_funcD/1728927557317.png)

## States

There are 5 states for each

* link
* active
* hover
* press
* focus



![1728927550783](image/m_link-table-row-icon-right_funcD/1728927550783.png)

## Design intention

This component is designed to be in a table with a bunch of links. It is an atomic element that is not designed to be a 'stand-alone' component. It should not appear in the website alone at any time. *Please see the *[*LinkTable-IcnR*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63c03b231f9a351229fa169c/tab/design?mode=preview)* which is the parent table component.*

## Layout

 **Width** : The component scales to fill the width of the container it is placed within.

 **Height** : Outside of the padding and text size, the height of the component scales vertically as dictated by the amount of text added.

**Icon**

Note that the icon hugs the right edge of the row with 16px of space to the right. The icon vertically aligns center in relation to the text. This keeps the arrow center-aligned when this component is in the larger LinkTableIcnR component.

**Border bottom**

The border bottom is a 1px rule that is designed to look like a border-bottom in a Table `<LI>`. It spans the entire width of the container it is put in.

Note that the text is flush left and has an 8px space to the left.

The icon is flush right (hugs the right edge) of the row and has a 16px space to the right of it.

**Multiline rows**

The width of the component fills the container is place in. If the text needs more room, it goes to multiple lines. Note that the icon vertically aligns center.

**Internal padding:** The internal top/bottom padding is smaller for the p.small size than the others.

**p.small:** 8px top/bottom padding

**p.default, p.bigSister: **16px top/bottom padding

![1728927541852](image/m_link-table-row-icon-right_funcD/1728927541852.png)
