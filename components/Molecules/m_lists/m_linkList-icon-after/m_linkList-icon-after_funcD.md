# The linkList is a vertically stacked list of text links, each with an arrow icon. This variation has the icon after the text.

Link lists are Text Buttons stacked.

Text buttons are composed of text with an icon. This button has the icon after but there is a [companion button with the icon before.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d6be2fb0aa8bf6e7b976/tab/design?mode=preview)

This button comes in three sizes (small, default and Big Sister), and 2 colors. Text in the text button can wrap to more than one line.

## Text Sizes

p.small

p.default

p.bigSister

## Color Variations

darkGray: #26292c

white: #FFF

## Icon Variations

The list can have either an arrow icon pointing to the right OR a chevron icon pointing to the right. Here is an example of the list with the chevron icon.

![1728929490583](image/m_linkList-icon-after_funcD/1728929490583.png)

## Alignment

Flush left only

## Line wrapping:

Each link can go to multiple lines. For this variation with the icon after, the icon always hugs the text and follows the last word with a 4px space separation.

![1728929500244](image/m_linkList-icon-after_funcD/1728929500244.png)

If the text in the button goes to multiple lines, the icon follows after the last word in the link.

![1728929508103](image/m_linkList-icon-after_funcD/1728929508103.png)

Above is not correct. Figma cannot place the icon immediately after the last word as desired. It placed the icon bottom aligned, outside the text container.

Please, the icon goes directly after the last word- not outside the text container, like this:

## Width

**Minimum width** of ind. link list and link list group is 256px

**Maximum width: **limited to the parent container and component's margins and padding

## Icon

The linkList uses an arrow icon.

## Hover state

**darkGray text**

text turns black with a primary3 underline.



![1728929525542](image/m_linkList-icon-after_funcD/1728929525542.png)

**White text**

text gets a primary2 underline

## Usage:

The** linklist** is used instead of a Call to Action button when:

* There is more than one call to action
* When the links are of the same importance / level of hierarchy
