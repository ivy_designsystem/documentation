# The linkList is a vertically stacked list of text links, each with an arrow icon. This variation has the icon before.

The linkList is made up of a stack of [Text buttons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d6be2fb0aa8bf6e7b976/tab/design?mode=preview), each composed of text with an icon. There is an alternate [linkLIst where the button follows after each text link.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393afe4f291f20ef5204390/tab/design?mode=preview)

This button comes in three sizes (small, default and Big Sister), and 2 colors. Text in the text button can wrap to more than one line.

## Text Sizes

p.small

p.default

p.bigSister

## Color Variations

darkGray: #26292c

white: #FFF

## Text Alignment

Flush left only

## Line wrapping:

Can go to multiple lines. The arrow icon aligns top.



![1728929644365](image/m_linkList-icon-before_funcD/1728929644365.png)

The icon "hangs" outside the margin of the flush-left text.



![1728929653211](image/m_linkList-icon-before_funcD/1728929653211.png)

## Width

**Minimum width** of ind. link list and link list group is 256px

**Maximum width: **limited to the parent container and component's margins and padding

## Icon

This button has an arrow icon after the text.

## Hover state

**darkGray text**

text turns black with a primary3 underline.



![1728929661588](image/m_linkList-icon-before_funcD/1728929661588.png)

**White text**

text gets a primary2 underline

![1728929669812](image/m_linkList-icon-before_funcD/1728929669812.png)

## Usage:

This is a tertiary button. It has less presence than the solid primary button, secondary outline button, or Flat Icon button which has chunkier text and larger icon in a circle.

It's used as an atom in LinkLists. And it can be useful when you want to have a stand-alone text link that is not a primary call-to-action.
