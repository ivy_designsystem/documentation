# This is a list of links using the default size type. It has no adornments.

## Description

Basic list, bare, with no adornments. It uses the listBare with the difference being that the entire `<LI>` list item is a link. Whereas a listBare i an unadorned list that can have contextual links.

## Usage

Used for collections of links.

## Case

The text should be sentence case where the initial character is the only one capitalized. Exceptions include caps for naming, such as, College of Arts and Sciences or for target user groups, such as Graduate Students.

## Punctuation

None. No ending punctuation, please.

## States



![1728930088664](image/m_linkListBare-default_funcD/1728930088664.png)

* default (this is the link state)
* hover
* visited (state so user knows if they already visited the link)
