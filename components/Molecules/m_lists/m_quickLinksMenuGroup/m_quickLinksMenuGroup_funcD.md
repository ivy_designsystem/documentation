# The link list group is a group of vertically stacked link rows. The links are unadorned simple text. No additional styles can be added. No icons or arrows. Just a stack of links. They are white and hover to primary3 with an underline.



![1728930325056](image/m_quickLinksMenuGroup_funcD/1728930325056.png)

## Data

* Simple text. No extra styling permitted.
* Hyperlink for each row of text
* Source: Custom input by content editor
* Character limits: ?

## Mandatory content

(2) Row of text + accompanying URL hyperlink

## Minimum content

Two rows can make a group

## **Maximum content**

**For the component: **none

**For within the context of the primary menu nav drawer / take-over nav: **probably should limit it... depends on who has editorial control. If editorial is responsible, we can have no limitations but convey maximum recommended in usage guidelines.

## **Width**

Width determined by placement in larger organism

## **Interaction**

Use the same interaction as is used for the primaryNavGroup.

Ivy/Components/Moleculse/Navgation/[PrimaryNavGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61fdad448becb1b0eaf9d3c4/tab/functional%20narrative?mode=preview)
