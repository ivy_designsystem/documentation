# The quickLinksMenuGroup is a group of vertically stacked quickLinksMenu rows. The links are unadorned simple text. No additional styles can be added. No icons or arrows. Just a stack of links. They are white and hover to white with an underline.

![1728930342036](image/m_quickLinksMenuGroup_intro/1728930342036.png)
