# The 'imgCoverShort'  is a 4x6 image asset for a hero with a visual height of 58. By default, the image uses the 'background size-cover" + vertical align=middle. The image scales to fill either height or width- whichever is larger, depending on the breakpoint. Content editors can choose top / middle / bottom vertical alignment options to fine tune the focus of the image in the hero.

![1728931750301](image/m_img-cover-short_funcD/1728931750301.png)

Image Assets

**Background cover image**

**6x4 image**

Asset made to the largest size 1800x550

* One source image for all breakpoints
* Image will be a container with a bg image.
* Container can be set to **58%vh** of the viewport

*Note: We will determine value of the visual height during the QA process. So please make sure that we will be able to adjust that value to achieve the best UX. *

## **Vertical alignment for the image**

For the focal point of the image to display across different breakpoints, we are programming in some defaults as well as providing some content editor controls for the vertical alignment of the image.

**Approach: Background cover image + vertical alignment controls**

By default, the image uses the 'background size-cover" style and scales to fill either height or width- whichever is larger, depending on the breakpoint. For example, the [short hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c2bd0f1579dd6e755f934d/tab/design?mode=preview) is vertical at the mobile breakpoints with the device in portrait position but horizontal and shorter at the desktop breakpoints.

By default the image will be centered vertically and for many 6x4 image assets, this default cover setting will achieve a fine result at all breakpoints. However, we are providing the content editor with additional controls to be able to vertically align the image top, middle, or bottom.



![1728931759551](image/m_img-cover-short_funcD/1728931759551.png)

**Developer note:***We will need to provide controls for top, middle, and bottom vertical alignment even though it will default to middle. This is to ensure that the user can choose vertical-middle alignment again after selecting top or bottom.*



![1728931775691](image/m_img-cover-short_funcD/1728931775691.png)

This example shows top, middle and bottom vertical image alignments for the ' *imgCoverShort* ' component in the '[short hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c2bd0f1579dd6e755f934d/tab/design?mode=preview)'. ^



![1728931792281](image/m_img-cover-short_funcD/1728931792281.png)

Vertical alignment-**top** was the preferred alignment. It is maintaining the image focus across breakpoints.

## Ensuring adequate contrast for type

**Scrim overlays**

Like the [hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/design?mode=preview), [story scroll hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6388bf5d20bb6c9c4a6ce23d/tab/design?mode=preview), and the[ takeover hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6437150906ea1118c0b12e0a/tab/design?mode=preview), the *imgCoverShort* component has a modal scrim overlay on the top and bottom. This is to ensure adequate contrast for the header and text group content that overlay in white when this image is placed in the context of a hero. For the *short hero,* the scrims have some overall transparency to the image can still be seen below while still providing enough contrast for the white overlay type.

The top and bottom scrim appear at all breakpoints.
