# The 'imgCoverShort'  is a 4x6 image asset for a hero with a visual height of 58. By default, the image uses the 'background size-cover" + vertical align=middle. The image scales to fill either height or width- whichever is larger, depending on the breakpoint. Content editors can choose top / middle / bottom vertical alignment options to fine tune the focus of the image in the hero.

![1728931810716](image/m_img-cover-short_intro/1728931810716.png)
