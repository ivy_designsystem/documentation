# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'image cover short' media element.



![1728931730098](image/m_img-cover-short_naming/1728931730098.png)

The image fills the container using the "background cover image" style and then is masked by the container. Content editors have some control over the vertical alignment of the image in the container. By default, the image loads v-align middle. Controls allow the user to vertically align the image top, middle, or bottom.

The image name reflects that along with the breakpoint size.
