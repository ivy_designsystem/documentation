# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'image cover tall' media element

![1728931962562](image/m_img-cover-tall_naming/1728931962562.png)
