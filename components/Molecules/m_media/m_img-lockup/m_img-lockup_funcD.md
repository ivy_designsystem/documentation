# This is a group of 3 square images.  It is used within the 5050imgLockup component.



![1728931551613](image/m_img-lockup_funcD/1728931551613.png)



![1728931562908](image/m_img-lockup_funcD/1728931562908.png)

Above is an example of the imgLockup within the 5050imgLockup component.

## Data

**Mandatory**

3 images, cropped to 1x1 (square) aspect ratio

Drupal will scale images. Original assets should be 1080x1080 each

## **Layout**

The layout consists of 2 larger images and one smaller. All are square. The 2 larger ones are the same size.

**For each breakpoint**

Larger images: each is 4cols+16px

Smaller image: 2 columns + 16px

The image group has two images on the top, one small, one larger.

The 3rd image centers below the the pair.

The 2 larger images are the same size.



![1728931571373](image/m_img-lockup_funcD/1728931571373.png)

# Spacing between images

The space between the images is always equal but varies by breakpoint.

**xxs - L **the space between the images is 8px

**xl, xxl **the space between the images is 16px

Below is the imgLockup for the Small - Large breakpoints. It has 8px space between the images.

The width of the image group for s -xxl is 6col+16px



![1728931579973](image/m_img-lockup_funcD/1728931579973.png)

The imgLockup for the xxs and xs breakpoints also have 8px space between the images.

The width of the image group for xxs and xs is the breakpoint less the 32px L/R margins.

**xxs-320** Width of the imgLockup is 256

**xs-480 **width of the imgLockup is 416



![1728931588114](image/m_img-lockup_funcD/1728931588114.png)
