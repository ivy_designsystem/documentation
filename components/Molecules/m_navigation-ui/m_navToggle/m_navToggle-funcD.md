# The navToggle is a dual button in the navDrawer takeover that allows users to navigate back and forth between 2 different navigation menus.



![1729522790496](image/m_navToggle-funcD/1729522790496.png)

# Functional Description

The dual navToggle that appears within the SectionNavDrawer allows the user to navigate back and forth between 2 different navigation menus:

1. The button on the right is to display the top-tier uvm.edu primary navigation in the navDrawer
2. The button on the left is to display the school/college/unit/section-specific navigation in the navDrawer

The selected button is always on top. User selects the unselected tab to change their selection.

IMPORTANT:

This is for the dual button- the toggle. Please see the [Individual Toggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/643ebacc06ea116210b3002f/tab/functional%20narrative?mode=preview) for more info.

# Data

Please see the [Individual Toggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/643ebacc06ea116210b3002f/tab/functional%20narrative?mode=preview) for more info.

# Design notes and alignments

**Right button **

The Right button links to uvm.edu. It has a "home" icon and a text label in lockup with a fixed space between them. The fixed space width varies depending on the breakpoint.  It's 4px for xxs and 8px for xs and larger.

**Left button**

The button on the left is for school / college / section / unit navigation. It picks up the name of the website.

**Text alignment**

For all breakpoints the text label is center-aligned.

For all breakpoints the text label and icon for the UVM.edu link are in lockup together and are centered as a unit.

For the school/college/section button, the icon and text label are in lockup and centered aligned for all desktop breakpoints.  For mobile, the icon hugs the right edge of the button and the text centers within the text/label content container.

![1729522800046](image/m_navToggle-funcD/1729522800046.png)

## xxs breakpoint

Individual toggles overlap -48px

Combined toggle width is 256px

(this is the xs breakpoint - L/R margins)

The toggle scales UP to the xs breakpoint



![1729522809291](image/m_navToggle-funcD/1729522809291.png)

## xs breakpoint

Individual toggles overlap -48px

Combined toggle width is 416px

(this is the xs breakpoint - L/R margins)

The text is larger than it is for the xxs toggle.

UVM button

The space between the 'home" icon and the text lable is 8px rather than 4px.

The toggle scales UP to the small breakpoint



![1729522815611](image/m_navToggle-funcD/1729522815611.png)

## small-xl breakpoints

Individual toggles overlap -48px

Combined toggle width is 704px

(this is the small breakpoint - L/R margins)

Note that both text labels are in lockup with the icon with an 8px space between. The text-icon lockup is centered.

The toggle does not scale above this width.

Data

The button on the left needs to pick up the name of the school/college/unit/section-specific website that the user is visiting. See the[ individual nav toggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/643ebacc06ea116210b3002f/tab/functional%20narrative?mode=preview) for more detail about data.

# Anatomy

The navToggle dual button comes in 3 sizes:

* xxs
* xs
* s-xl

Here is a summary of the width of this combined dual button by breakpoint.

![1729522823205](image/m_navToggle-funcD/1729522823205.png)



![1729522830909](image/m_navToggle-funcD/1729522830909.png)

![1729522837363](image/m_navToggle-funcD/1729522837363.png)

# States

**not-selected link state:** can hover

**not-selected hover state:** on mouse-in, hover state displays indicate interaction with a button

**active state:** shown on-tap / on-click

**selected state: **indicates which navigation is selected and visible

**Focus states** are available for all except for the "selected" state. It is not a clickable state. The user would have to click / tap on the unselected button to change the selection.

**Developer note**

Selected button always is on top of the non-selected button

# Micro-interactions

Please add a fade to the hover so that there is some tactile quality to the button.
