# The Primary Navigation Group is the main navigation that appears in the take-over nav drawer.  It is flat- there are no subnavigation items.



![1729522055494](image/m_navigation-funcD/1729522055494.png)

The navigation on desktop is flushRight.

The navigation on Mobile, is FlushLeft.

## Functionality

The navigation is a list of top-level navigation categories. There are no subcategories.

**Hover state for a navigation category and category parent**

On-hover of a navigation category, the text stays 100% white while simultaneously, all of the other navigation categories dim to #FFFFFF @64% transparency. There should be a fade transition on this. Try .3s linear for the opacity transition.



![1729522066598](image/m_navigation-funcD/1729522066598.png)

The above ^^ is showing the hover state for a category, in this case, Admissions.

**On-click / on-tap: **on-click / on-tap takes the user to the page.

## Data

Data comes from the main menu in D9, which already exists.

## Mandatory Elements

All as reflected in the information architecture.

## Developer Notes

Please make sure that you look at the sub atomic elements for this component. The individual rows for top level  navigation are defined in the Ivy / Components / Atoms / Navigation / [Primary Navigation ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6202773dcb5b9777c8b518a9/tab/design?mode=preview)area.

**Inspectable Prototype**

The XXS size has been provided as the inspectable prototype for mobile. (flush left on mobile)

The S size has been provided as the inspectable prototype for desktop (flush right on desktop)

## Interaction

On-click or on-tap of the hamburger menu-search icon in the upper right corner of the header, the primary navigation drawer takes over the screen. We are calling this "take-over navigation".

The green background should take-over the screen first swiping on from **right to left** with the type and other elements making a slightly delated and progress appearance. It should happen quickly though. They user should not have to wait for the navigation to appear... just a slight delayed progression much like you can see in [this example](https://www.medienstadt.koeln/) if you click on the left Menu button.

The site linked above is also a good example for the hover state functionality.
