# Molecules

# Atoms

**Primary navigation list items**

Ivy / Components / Atoms / Navigation / [Primary Nav](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6202773dcb5b9777c8b518a9/tab/design?mode=preview)[ items](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6202773dcb5b9777c8b518a9/tab/design?mode=preview)

Please see Ivy / Foundations / [Typography](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f82e4d10fa0e2078276fe8b?mode=preview)

## Interactions

**Fade in Place and Fade Reveal Delay**

Ivy / Foundations / Interactions / [Interactions and motion](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview)

**Swipe reveal from right to left** (background)

**This is new functionality.**

Here is an [example](https://www.medienstadt.koeln/) of this on a website but the action is from the left to right. (click on the Menu button, mid-screen on the left when in desktop after accepting cookies.)
