# Section navigation is navigation specific to the school, college, or unit the user is visiting. It is a panel that displays in the left sidebar. It allows the user to navigate to pages within the website to 2 levels deep.  The section navigation also indicates to the user what subsection or page of the website they are visiting- that is, if the page is included in the navigation. Also see [breadcrumb](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b07aa733f8e6e46266dd5/tab/design?mode=preview) which always shows the page that the user is on relative to the website they are visiting.

![1729522334173](image/m_sectionNav-funcD/1729522334173.png)

# Behaviors

**Mobile**

* Menu is closed on page load
* Menu is expanded manually by tapping on title / menu navigation item (the entire title row is the 'hit' area)
* Expanded menu should remain open until the user has made a selection or manually closes it
* Tapping outside the menu, tapping on the "X" in the title or tapping on the "x" at the bottom should all close the menu
* The sectionNav menu is self-dismissing. Once a selection has been made, the user is taken to the selected page, which on mobile, loads with the section nav panel closed.

**Desktop**

Functionality is designed so that the section navigation is always available to the user and that there are no surprises. The section navigation is open on page load but gracefully collapses as user scrolls out of the menu zone. It remains persistent and sticky in a closed state to be always available to the user.

* Menu is open on page load
* Menu remains open until either:  the user scrolls down the page and top of the sectionNavPanel is 128px from the top of the page or user manually closes the menu by clicking on the close "x" icon at the upper right.
* On page scroll down, the header turns to the sticky header and the page content flows under the sticky header. Once the top of the section nav panel is at Y-128px, the menu will collapse on its own, in a nice ease-out motion and remain closed and sticky at the 32x, 128y (32 px from the left of the breakpoint 0 edge, 128px from the top of the page.)
* The closed menu continues to be readily available to the user as it remains sticky and floats as an overlay at 32x, 128y within the content area of the page and visual browser area. The page content will go UNDER the collapsed sticky section nav panel and the header.
* The user can expand the menu at any time by clicking on the menu (hamburger) icon in the upper right of the closed menu.
* The sectionNav menu is self-dismissing- once a selection has been made, the user is taken to the selected page
* Developer note: the desktop version of the menu does not have a "x" close icon at the bottom of the menu

![1729522348048](image/m_sectionNav-funcD/1729522348048.png)

The above shows the transition from (left) the section nav is in the expanded position on page-load, (middle) the page scrolls *under* the sticky header, sticky header turns to white like we have on the home page now, the section navigation is sticky and stops scrolling when the top of the section navigation panel is 128px below the top of the page. (right) The section navigation collapses on-scroll when the user scrolls 128px below the top of the section navigation after it reached it's sticky location.

![1729522356555](image/m_sectionNav-funcD/1729522356555.png)

^ The section navigation turns sticky when it is 128px from the top of the page. (16x8=128)

## Interaction reference

The interaction of the section navigation has been difficult to explain and break down into a set of directions. Please [take a look at this example ](https://www.essex.ac.uk/research)of section navigation. It is a good reference for the interaction we are looking for.

![1729522366079](image/m_sectionNav-funcD/1729522366079.png)

## Breakpoints and widths of the section navigation

The section navigation is always present on your website. It varies slightly by breakpoint to optimize for mobile and desktop. Here are the differences by breakpoint.

![1729522373500](image/m_sectionNav-funcD/1729522373500.png)

# Anatomy and States

Please make sure you also take a look at the [sectionNavigationItems ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641c5645723c829301b5f5f3/tab/design?mode=preview)for detailed information about the link types in the individual section navigation.

## Titles

Every section navigation has a TITLE.  This is the name of the website / school / college / unit or department as set in the Website Title in Drupal. The title area can scale vertically so longer names can fit. Best practice would be to keep these short and meaningful but DO NOT use acronyms in the title.

**Mobile optional title**

The title in the section nav panel by default is the group name. However, the content admin has the option to have a shorter name for the title that would appear on mobile. This would be a viable option if it improves user experience by optimizing space or if the name is a familiar name.  For example, 'The Dudley H. Davis Center 'versus the more familiar name, 'The Davis Center'. Do not use acronyms in the short name.

**Collapsed Title (showing group name)**

Default state: hamburger menu icon

Hover state: Just a color shift

**Expanded Title**

Default state: close icon

Hover state: Just a color shift

**Data:**

For more information about the individual section navigation items and data sources, see the section on "[section navigation UI elements.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641c5645723c829301b5f5f3/tab/functional%20narrative?mode=preview)"

The Title data source is the name of the website / school / college / unit or department as set in the Website Title in Drupal.

**Optional shorter title name for mobile.**

Simple text

**Important! **No substitute names for the group (website name) that shows up on the TITLE of the section nav.

![1729522381430](image/m_sectionNav-funcD/1729522381430.png)

## Navigation items and states

![1729522389451](image/m_sectionNav-funcD/1729522389451.png)

## Parent and child navigation items and states



![1729522398460](image/m_sectionNav-funcD/1729522398460.png)

The above is a grab from the mobile section navigation. On mobile, the section navigation panel covers up the content / goes over the content when the panel is expanded. The spacer row with the close panel appears at the bottom so the user can close the panel from the bottom and not have to scroll to the top to close the panel. The panel will also close on its own if the user makes a selection and navigates to a page. On page-load on mobile, the section navigation panel is always closed.

The title and the parent navigation items have states for both their collapsed and expanded views.



![1729522405772](image/m_sectionNav-funcD/1729522405772.png)

Above shows the hover state for the expanded parent navigation item. In this view, the user would be on the child page that is showing the active state.

## Focus state

Focus states are a primary3 1px solid rule around the menu item. They are only used with screen readers.



![1729522414127](image/m_sectionNav-funcD/1729522414127.png)



![1729522422172](image/m_sectionNav-funcD/1729522422172.png)

Keep in mind that for the Titles, the focus border should have a 4px border radius for the upper left and upper right corners. Make sure that the 1px outside border for the focus state also has the border radius to match.

# Layout and Spacing

**Spacer-top**

The vertical space between the navigation title and the first nav item is 8px for all breakpoint**s. **

# Some visual "don'ts"

The sectionNav panel should not have a scroll bar.



![1729522428732](image/m_sectionNav-funcD/1729522428732.png)

# Child sites link to parent sites

The section nav panel on children websites have a link to the parent site at the bottom of the panel.

The arrow "up right" icon indicates that the link is going to another website but not opening in a new window.



![1729522437524](image/m_sectionNav-funcD/1729522437524.png)

# Parent and children website guidelines

* Parents can have many child websites.
* Child websites have only one parent website
* Parent / group name is dynamically populated and associated through the data (not user created as a menu item)

# Interactions

Interactions should be kept simple with enough feedback for the user to inform the engagement.

**TItle**

For both the collapsed and expanded title, the default state for the title is the light version, the hover is the dark version.

**Other section navigation elements**

All other navigation items– nav item, parent item, child subnav item– the hover state has a neutral background on-hover / on-tap. If the item has an icon, be aware of the variations for the states of the icon as well.

========================================

# Integrating the section nav into layouts

# Page regions

### Desktop



![1729522446164](image/m_sectionNav-funcD/1729522446164.png)

# Parts of the page

1. The hero area
2. section navigation
3. Upper content
4. Lower, full-width content

## The hero area

The hero area includes the header and the hero banner

## Upper content

For breakpoints Medium-1012 and larger, the upper content area has the section navigation taking up the 3 columns on the left and the upper content container taking up the remaining 9 columns on the right. Follow layout guidelines for left padding for this content area.

## Section navigation and upper content

**Important! **This container overlaps the hero by 48px.

## Lower, full-width content area

The lower part of the screen holds content and components that span 12 columns. This always includes the footer.

# Mobile

Breakpoint size xxs-s have the same content areas as desktop but they  stack vertically.

1. The hero area
2. section navigation
3. Upper content
4. Lower, full-width content

## The hero area

The hero area includes the header and the hero banner.

## Upper content

The upper content is not divided into 1/3 - 2/3rds for the section nav and upper content like on desktop. Instead, it takes up 12 columns.

## Section navigation and upper content

**Important! **This container overlaps the hero by 48px. The content spans 12 columns.

**Dependency: **getting the type location correct on the hero so the baseline of the type is in the right place and type scales from the bottom up. If this is executed as per the design, the section nav will work nicely with it without interference.

## Lower, full-width content area

The lower part of the screen holds content and components that span 12 columns. This always includes the footer.

# **Keyboard interactions**

Follows WCAG 2.0 standards for keyboard accessibility guidelines.

# [REFERENCE TABLE TK]
