# The SectionNavDrawerGroup is a section/unit/school specific navigation list that appears in the take-over nav drawer. It is redundant to the section navigation panel that appears on all websites. A toggle allows the user to toggle between section-specific navigation and top level navigation in the navDrawer.

![1729522622418](image/m_sectionNavDrawerGrp-funcD/1729522622418.png)

Unlike the primary navigation which is flush right on desktop, the section navigation in the navDrawer is flush left for all breakpoints.

## Data

Data comes from the the website in Drupal and what the content admin has defined to be in the menu.

**Things to discuss**

* ~Let's discuss page title and menu titles-- can they be customized?~ Answer is no. They cannot.
* Changes to Drupal UI and functionality in how menus are created?
* Migrating existing navigation over to new. What doesn't match? (ie: [Dept of English](https://www.uvm.edu/cas/english) and [Film and TV studies](https://www.uvm.edu/cas/filmtv) in their navigation panel)
* Content strategy and consistent IA structure to help with "Apples to Apples" comparison 'shopping' when comparing schools/colleges/programs/ areas of study/majors/minors. This becomes particularly important for the Academic Program Finder experience and could be critical to the UX of our users when investigating academic paths.

## Mandatory Elements

All as reflected in the information architecture.

## Accessing the menu in the navDrawer

On-click or on-tap of the hamburger menu-search icon in the upper right corner of the header, the  navigation drawer takes over the screen. We are calling this "take-over navigation".

## The navDrawer on launch always defaults to the UVM.edu top level navigation.

The hamburger menu icon in the upper right of the site launches the navigation drawer. It defaults to the uvm.edu top level navigation globally, no exceptions.

If the user is within a parent / section / unit website, a toggle appears at the top of the nav drawer allowing the user to toggle from the uvm.edu navigation to the section-specific navigation .

* For all section-specific navigation in the navDrawer, the user will have a TOGGLE to be able to toggle back and forth between the primary UVM Nav and the section nav
* If the user is on the uvm.edu home page, the navDrawer shows the primary navigation in the nav drawer without a toggle (launch the navDrawer on [our website](http://www.uvm.edu/) to see an example of this view)

# Anatomy of the Section Nav in the nav drawer

The section navigation in the navDrawer is a vertically stacked list of navigation items navigable to two levels deep. Navigation items include:

**Menu Item**

**Parent Menu item**

* **child subnav item**

.... and states for all which you can see described in the [navDrSectionNavItems.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6425d976c59e39332d5f50a2/tab/design?mode=preview)

![1729522632700](image/m_sectionNavDrawerGrp-funcD/1729522632700.png)

Right click on the image and "open in new browser window" and then zoom in with your browser to read the labels. OR open the Figma handoff file via the link above under "links".

![1729522640073](image/m_sectionNavDrawerGrp-funcD/1729522640073.png)

Right click on the image and "open in new browser window" and then zoom in with your browser to read the labels. OR open the Figma handoff file via the link above under "links".

# States

Each navigation item has four states —

1. default
2. hover/on-tap
3. active (selected) indicating page user is on
4. focus.

# Functionality

**Guidelines**

**The user is in control of the navigation. If they open a panel, they need to manually close it. If they expand another section, both sections will be open. The opening of a 2nd section will not close the other.**

* Menus should persist until the user has made a selection
* On-click / on-tap of a parent menu item expands the menu to reveal the child subnav elements
* Except for Parent menu items, menus are self-dismissing. Once a selection has been made it disappears.
* Expanded menu with child subNav items is collapsed by clicking [-] on the Parent menu item (*note, hit state is the entire Parent menu item, not just the [+] [-] icon)*

### **Sub navigation**

On-tap / on-click of a parent category, the navigation expands to reveal the child sub navigation options. The entire parent row is the hit state.

The control icon for the parent item turns into a [-] when expanded.

On-hover of a child sub navigation item, the hover state shows. The hover state is: the text has a primary3 underline on-hover / on-tap.

The page the user is on is reflected in the active state of the child subnav item.

# Mobile

There is a mobile version and a desktop version. They vary by text size, left padding and space between the yellow bar in the active state and the text.

# Interactions

**Hover state for a navigation category and category parent**

On-hover of a navigation category, the text stays 100% white while simultaneously, all of the other navigation categories dim to #FFFFFF @64% transparency. There should be a fade transition on this. Try .3s linear for the opacity transition.

FOLLOW the same interactions for the section nav in the nav drawer as was built for the [primary nav.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61fdad448becb1b0eaf9d3c4/tab/functional%20narrative?mode=preview)

![1729522647743](image/m_sectionNavDrawerGrp-funcD/1729522647743.png)

![1729522655800](image/m_sectionNavDrawerGrp-funcD/1729522655800.png)

Important! Children navigation items have a hover state with a darker background. The non-focus state does not happen at the subnavigation/child level.
