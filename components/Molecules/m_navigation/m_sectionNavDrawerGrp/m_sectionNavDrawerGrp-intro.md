# The SectionNavDrawerGroup is a section/unit/school specific navigation list that appears in the take-over nav drawer. It is redundant to the section navigation panel that appears on all websites. A toggle allows the user to toggle between section-specific navigation and top level navigation in the navDrawer.

![1729522669695](image/m_sectionNavDrawerGrp-intro/1729522669695.png)
