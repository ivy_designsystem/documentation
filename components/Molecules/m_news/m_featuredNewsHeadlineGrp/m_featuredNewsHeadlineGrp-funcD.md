## Intro

The News Teaser Headline Group is a group of 4 news headlines including one that is elevated as a (larger) featured headline. Each headline links through to the full news story.

## Data

Headlines are plain text. No Wysiwyg.

```
Text [plain] 255 characters max
```

The headlines are pulled from already published news stories. The content editor manually selects the news stories they want to appear in the news teaser headline group.

Number of stories: 4 mandatory

**Stories will appear in the following order:**

1. Headline feature
2. Followed by 3 headlines

They do not need to follow reverse chronological or chronological order. Published dates will not show. There are no "news categories".

## States and Micro-Interactions

![1729523929233](image/m_featuredNewsHeadlineGrp-funcD/1729523929233.png)

## News headline

Turns black with a primary3 underline.

Has a write-on transition.

Inspo: [Invision blog](https://www.invisionapp.com/inside-design/) has an underline that quickly animates on, left to right, top to bottom

![1729523936024](image/m_featuredNewsHeadlineGrp-funcD/1729523936024.png)

## More Button

on hover reverses color and has a styled text label to the right.

On tap/click, button goes to UVM Today for all news stories [https://www.uvm.edu/uvmnews/all-news](https://www.uvm.edu/uvmnews/all-news)

Has a fade transition (linear)

❤️ Would love the fade + text appears Left to right so it looks like it is writing on from behind the [...]
