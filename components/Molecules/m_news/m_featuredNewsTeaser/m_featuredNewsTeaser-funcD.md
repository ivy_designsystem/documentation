## Intro

Featured News Teaser is an elevated news story teaser designed to engage and get users to click/tab through to the full story.

It is displayed as the largest news teaser in the [News Teaser Block](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/design?mode=preview). It is most prominent with a 16x9 image or video a mandatory design element.

## Data

* Image: 16x9 image or video.
* Headline
* Description

**Headline** Headlines are plain text. No Wysiwyg. The headlines are pulled from already published news stories. The content editor manually selects the news stories they want to appear in the news teaser headline group.

**Media in the Featured News Teaser**

* 16x9 image OR static image placeholder for 16x9 video (defined in news tool and pulled from news tool)
* Image size is 1149 x 646, same as what is needed for the Featured Media in the news story
* Image pulled in from the news; whatever is defined as the default in the news tool. It could be the 1st image of a slideshow, a static image, or a video. (if video, it plays in place. YT hosted)
* Video: If the featured story has a video, it will not play in place. User will need to click / tap through to the story.
* If a video is the featured media in the news tool, the story will have to also have a "backup" image. That static image is what will be shown in the featured news teaser.
* If a static "backup" image was not provided, we will show a placeholder that is transparent... so the background will show (white or neutral.. whatever)
* **Teaser text** (plain text). Mandatory. This is the first **~160 character** of the story followed by an ... ellipse. It can be over ridden by the user and customized. Regardless, it's mandatory.
* Right arrow

## Interaction

![1729523864388](image/m_featuredNewsTeaser-funcD/1729523864388.png)

## On tap/hover

* The entire Featured News Teaser is the hit state
* The image increases in contrast a wee bit AND has a 10% white scrim overlay. image: filter: contrast(1.1);
* The headline turns black with a primary3 underline
* The arrow shifts +16px to the right

See [The News Teaser head](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635bef0b1dda98ca89c0fae8/tab/functional%20narrative?mode=preview) hover headline interaction-- needs to match.

## Developer note

Discussion: More typical hover/tap functionality is that each is an independent link to the same place. Ie: image have a hover and link separate from the headline. I am not sure how this is done without an accessibility issue-- so suggested hit of the entire area. I am ok with the arrow not shifting.
