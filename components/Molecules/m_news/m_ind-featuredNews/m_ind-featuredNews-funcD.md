The Individual Featured News Items are hierarchical news cards. They come in Primary and Secondary design tiers.

## Description

This component displays teaser information for news stories using a hierarchical card display. They come in Primary and Secondary design tiers with the Primary being larger.

## Data

Data is pulled from the NEWS TOOL database of news stories.

**News headline:** mandatory. Simple text

**Image:** Optional. 16x9, 1920x1080

**URL:** URL to the source news story

## Two hierarchies

The Individual Featured News Item comes in a primary and secondary design tier.

![1729524254820](image/m_ind-featuredNews-funcD/1729524254820.png)

## Usage

This component is  **never used independently** . It is grouped with other individual Featured News items in the [Featured News component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563672e983f5ea83a7d85e/tab/design?mode=preview).

## Functionality

Each featured news listing links to the full news story. There is a default, hover, and focus state.

## Color modes

The individual Featured News cards only have a light state.

## States

**There are 3 states for this component:**

* **Default**
* **Hover**
* **Focus (primary3 lighter outline)**

![1729524263795](image/m_ind-featuredNews-funcD/1729524263795.png)

![1729524270704](image/m_ind-featuredNews-funcD/1729524270704.png)

### Important note about the HOVER / ON-TAP state:

The hover/on-tap state can't be simulated well in FIGMA since Figma does not allow change of color for underlined text. We also can't save the recipe for the color shift of images that happens on-hover. Here is what is intended to happen on-hover / on-tap of an individual Featured News card:

1. HIT state is the entire card.
2. Text changes from DarkGray to Black and gets a black underline.
3. There is no change to the background fill of the card.
4. Image has a color shift: 115% contrast, 105% brightness, 95% opacity, with white background behind image.
5. The arrow shifts +16px to the right

![1729524277439](image/m_ind-featuredNews-funcD/1729524277439.png)

See [The News Teaser head](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635bef0b1dda98ca89c0fae8/tab/functional%20narrative?mode=preview) hover headline interaction-- needs to match.

## Display Notes

This component is hierarchical. There is a Featured News component with a primary3 background. This is for the prominent, featured news item.  Here is the primary Featured News item by breakpoint:

![1729524286107](image/m_ind-featuredNews-funcD/1729524286107.png)

![1729524292434](image/m_ind-featuredNews-funcD/1729524292434.png)

The secondary news items are smaller, and less prominent. The width is the only change between small and XL. Note that the type size on the mobile xxs and xs varies in size. Here they are by breakpoint:

![1729524298305](image/m_ind-featuredNews-funcD/1729524298305.png)

![1729524304310](image/m_ind-featuredNews-funcD/1729524304310.png)

## Responsive Notes

* The content displayed is the same for all breakpoints.
* The image always displays at the 16x9 aspect ratio.
* The size of the title text varies by breakpoint.
* The padding varies by breakpoint

 **Title text size by breakpoint for the *secondary* featured news** :

xxs: m.H6

xs-s: m.H3

m-xl: H3

**Title text size by breakpoint for the *primary* featured news item:**

xxs: m.H3

xs: m.H2

s-xl: dt.H2

**The padding for the TEXT in the primary Featured News item varies by breakpoint:**

Desktop small-xl: 48px all sides

xs: 40px all sides

xxs: 24 px all sides

The components do not scale above xl.

**Height of individual primary Featured News component**

Medium-xl: Side by side display, has a minimum height of 786

Small: has a minimum height of 786

**Top and Bottom Padding for the individual featured news item**

desktop: padding-top: 0px; padding-bottom: 64px;

mobile: padding-top: 0px; padding-bottom: 48px;

## Minimum heights for the individual Featured News items

**For primary Individual Featured News item (with yellow background)**

Desktop small-xl: 786px

Mobile xs: 396px

Mobile xxs: No minimum height. Relying on the 24px padding for the text container and 48px padding-bottom.

**For secondary Individual Featured News items (in grid)**

Desktop small-xl: 304px

Mobile xxs: 192px

Mobile xs: 256px

## Layout

On desktop, the individual Featured News 2L (secondary) component only varies in width by breakpoint.

![1729524315582](image/m_ind-featuredNews-funcD/1729524315582.png)

On mobile, the text size varies between the xxs-320 and xs-480 breakpoints. The margin-bottom on the text container is the same though.

![1729524322452](image/m_ind-featuredNews-funcD/1729524322452.png)

![1729524329554](image/m_ind-featuredNews-funcD/1729524329554.png)

The news title in the secondary Featured News item varies in size by breakpoint.

**Desktop, small-xl:** dt.H3

**Mobile, xs and xxs-landscape: **m.H3

**Mobile, xxs portrait: **m.H6

# Layout, primary Individual Featured News item

![1729524336471](image/m_ind-featuredNews-funcD/1729524336471.png)

![1729524342355](image/m_ind-featuredNews-funcD/1729524342355.png)

Right click and open each image in a new window to view the paddings.

**Text container Paddings**

Desktop, small-xl: internal padding for text container is 48px on all sides.

xs-480 and xxs-landscape, internal padding is 40px on all sides

xxs-320 portrait: internal padding is 24px on all sides.

## Developer note

Type sizes may scale down during QA and we get to see this component with real data.

## Interaction

The news listing loads with[ fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview).
