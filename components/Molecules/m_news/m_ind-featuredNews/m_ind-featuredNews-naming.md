# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'Individual Featured News' component:



![1729524236014](image/m_ind-featuredNews-naming/1729524236014.png)

Note that there is a* primary* individual Featured News component and a *secondary* Individual Featured News component.

Keeping "ind" for "individual" in the name is important as the larger Featured News component is just called "Featured News".

We don't need to include "primary" in the name for the larger individual Featured News item since we are making it clear that it is the *individual Featured News item* - rather than the larger group.

See documentation for the [Featured News component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563672e983f5ea83a7d85e/tab/design?mode=preview)for comparison. This component is made up of 5 Individual Featured News items.
