# Individual news listing. Used in the [News List component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/design?mode=preview).



![1729524053667](image/m_newsListing-funcD/1729524053667.png)

# Description

This component displays teaser information for news stories. Content varies by breakpoint but at the largest size can include the title of the news story, summary body copy and an image.

# Usage

This component is  **never used independently** . It is grouped with other individual news listings in the [News List Component.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/design?mode=preview)

# Data

Data is pulled from the NEWS TOOL database of news stories.

**News headline:** mandatory. Simple text

**Date: **Date stamp, simple text, format is mm/dd/yyyy

**News summary:** Optional. Simple text. maximum 300 characters. Truncated automatically with an "..." ellipse.

(note: Drupal default is first 300 characters but content admin can manually override with a custom intro description)

**Image:** Optional. 16x9, 1920x1080

**Image usage on mobile: **16x9 image is masked to display as 1x1

**URL:** URL to the source news story

# Functionality

Each news listing links to the full news story. There is a default, hover, and focus state. The width of the component varies by breakpoint. Also, some content is suppressed on mobile.

# Color modes

The news listing was designed for both a light and dark mode. ***We are only developing the light mode for now* **but please take this into consideration  during development as it will impact how you work with variables and filenaming, etc, .



![1729524059917](image/m_newsListing-funcD/1729524059917.png)



![1729524067257](image/m_newsListing-funcD/1729524067257.png)

# States

**There are 3 states for this component:**

* **Default**
* **Hover**
* **Focus (primary3 outline)**

## Important note about the HOVER / on-tap state:

The hover/on-tap state can't be simulated well in FIGMA since Figma does not allow change of color for underlined text. We also can't save the recipe for the color shift of images that happens on-hover. Here is what is intended to happen on-hover / on-tap of an individual news listing:

1. HIT state is the entire card.
2. Text changes from DarkGray to Black and gets a primary3 underline.
3. Card background fill changes to neutral #F7F7F7.
4. Card border changes to 40% alpha black
5. Image has a color shift: 115% contrast, 105% brightness, 95% opacity, with white background behind image.



![1729524073228](image/m_newsListing-funcD/1729524073228.png)



![1729524079333](image/m_newsListing-funcD/1729524079333.png)



![1729524084781](image/m_newsListing-funcD/1729524084781.png)



![1729524091531](image/m_newsListing-funcD/1729524091531.png)

# Anatomy by breakpoint and responsive notes

**Mobile**

Note: 16x9 image is masked to force display to 1x1 for mobile breakpoints



![1729524097776](image/m_newsListing-funcD/1729524097776.png)



![1729524105017](image/m_newsListing-funcD/1729524105017.png)



![1729524110273](image/m_newsListing-funcD/1729524110273.png)



![1729524118262](image/m_newsListing-funcD/1729524118262.png)

 **On mobile** , the news listing is flat and is not displayed as a tile. The summary body copy is suppressed and the optional image is force-masked to display as 1x1.

For the  **desktop small breakpoint** , the news listing is displayed as a tile. The summary body copy is suppresed.

On  **desktop medium-xl** , the summary body is displayed.

**Column spans**

For xxs - medium breakpoints, the news listing spans 12 columns

Medium - xxl: News Listing width matches the WYSIWYG text container width.

# Interaction

The news listing loads with[ fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview).

Hover / on-tap HIT area is the entire listing since there is only one link- link goes to the related news story. Add a very short Fade for the hover state.
