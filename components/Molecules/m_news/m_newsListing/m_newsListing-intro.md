# Individual news listing. Used in the [News List component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/design?mode=preview).

![1729524037576](image/m_newsListing-intro/1729524037576.png)
