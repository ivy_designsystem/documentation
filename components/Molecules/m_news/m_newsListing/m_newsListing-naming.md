# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'NewsList' component:



![1729524022102](image/m_newsListing-naming/1729524022102.png)
