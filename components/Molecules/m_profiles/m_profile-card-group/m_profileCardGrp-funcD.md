# The Profile Card Group is a collection of teasers using the [profile card ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/naming?mode=preview)display style. It’s a common UI element that displays a group of profile teasers that provide quick introductory information about a group of people - faculty, staff, leadership, students, etc.

# **Usage**

The 'Profile Card Group' is a collection of individual 'Profile card items'. It is *never used on it's own* as is only a sub-component to the Profile Queue-card component.

Here is the atomic progression of the profile teaser components in card format. On the left is the individual profile card item followed (center) with the profile card group described here. The [Profile Card Item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/naming?mode=preview) and the Profile Card Group are  *never used independently* . They are sub-elements to the [Profile Queue-Card component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656e0031663f0b838c6d636d/tab/design?mode=preview) (right).



![1729536150274](image/m_profileCardGrp-funcD/1729536150274.png)

# Display types

Profile teasers come in both card and list display types. This documentation is for the card display. Please also take a look at the [profile list group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6508574848713abbe2652046/tab/functional%20narrative?mode=preview) documentation.

## Color variations

Profile teaser groups are available in light and dark modes for both lists and cards. We are only going to work with the light mode at present. **There is no need to develop the dark mode right now.**

![1729536133043](image/m_profileCardGrp-funcD/1729536133043.png)

^ Dark version.



![1729536157588](image/m_profileCardGrp-funcD/1729536157588.png)

^ Light version

# Data

See the [profile teaser documentation](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65085740e1135bd60fe8aebb/tab/functional%20narrative?mode=preview) for the data for the individual profile teasers. This component is a group of profile teasers.

# Interaction and states

Profile teasers can link through to a profile page. The person's name is the link to the full profile. The email is a "mailTo" link and on tap/click will launch the user's email app to draft and email to the profile recipient. The phone number will be turned into a link on mobile depending on the platform and mobile browser functionality.

**Name:** Links to the profile. Gets a green underline on hover

**Email:** Is a MailTo link. Use UVM contextual link style for hover: text turns black, yellow underline wipes on left to right (like on the news teasers)

**Phone:** Is clickable / tapable on mobile, dependent on mobile browser and phone. Use UVM contextual link style for hover.

IMPORTANT: There is no scenario where this hover state will show as the component has various links- the name, email, and on mobile- the phone number.

# Grid gutter and corner radius

Corner radius: 8px all breakpoints

Grid Gutter: vertical and horizontal gutter between cards is 16px, all breakpoints

![1729536168866](image/m_profileCardGrp-funcD/1729536168866.png)

# Display by breakpoint

The Profile Card Group component can be displayed as either a list or a grid. This is true for every breakpoint. There are some differences though. The number of columns that the group spans varies by breakpoint and on mobile, the list view for portrait displays, does not display the image.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.



![1729536175539](image/m_profileCardGrp-funcD/1729536175539.png)

# Breakpoints

The Profile Card teaser item has an image at all breakpoints.

Gutter / internal grid spacing is 16px for all breakpoints.

Here is a visual reference to show the column spans for the card group by breakpoint. Of note:

**xxs portrait:** this is the only breakpoint where there is 1 card / row. They stack vertically with 16px between then.

**xxs landscape:** Cards go to 2 / row

**xs:** Cards are 2/row

**Small and larger: **cards are 3/row. ColSpan of card Group varies by breakpoint. See chart above.

**XL: **card group does not scale above XL.

![1729536182371](image/m_profileCardGrp-funcD/1729536182371.png)
