# Profile Cards display high-level introductory information about people - faculty, staff, leadership, students, etc. in a card format.

![1729536390619](image/m_profileCardItem-funcD/1729536390619.png)

Profile Cards have a light and a dark mode. We are only developing the light mode for now.

# Display Types

Profile teasers come in both card and [list](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65085740e1135bd60fe8aebb/tab/functional%20narrative?mode=preview) display types. Here is  an individual profile list item:

# Data

**Image** 1x1 image, 1080x1080 (scales down)

The profile hero banner and the profile teasers use the same image. Only the one image asset is needed.

**Name **Simple text. Links to the individual's profile page, if they have one. Link should be dynamic- not manual.

**Title **Simple text

**Email **simple text. Is a MailTo link

**Phone **Pickup same format and content from existing. Links on mobile if user's scenario handles that

**What to do if there is no image?**

Unlike the profile hero banner, the profile teaser has a placeholder profile image if none is supplied. If none, use [provided placeholder.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/652ebb6a55d56ae89ed2ff0e?mode=preview)



![1729536397472](image/m_profileCardItem-funcD/1729536397472.png)

# Profile image recommendations and art direction

The profile image is used in many places related to someone's professional bio. It's used in the:

profile hero banner

profile teaser (card and list formats)

These applications all take a 1x1 square image with a minimum size of 800x800.

However, some profile photos, like those for the Deans of our schools and colleges, also get used in 16x9, 1920x1280 size in the hero banner on the Welcome / Letter from the Dean pages for each school. Of course, a different photo is most likely going to be a better choice for this format other than a profile portrait.

Full art direction recommendations for the profile images is forthcoming on rollout of new brand, however, it's important to note that the cropping of the photos needs to be consistent and to be of a similar crop and closeness to the individual so the head and shoulders of each person takes up about the same amount of space. That way when you look at the profile teasers, some won't look like close-ups, extreme closeups, or "day in the lift" verité shots.

## Profile teaser card display with multiple job titles



![1729536403444](image/m_profileCardItem-funcD/1729536403444.png)

Profile bios can have up to 5 job titles. However, the Profile Card display type is limited to a maximum of 2 positions. If your collection of profile teasers include people with more than 2 positions, the List display is recommended.

Commas can be used within job titles, for example:

Vice Provost for Diversity, Equity and Inclusion

Multiple job titles run together and are separated by bullets. The bullets help with readability and help the site visitor discern the individual titles within the list.

In the Drupal admin area, content editors can add up to 5 position titles per bio. Here's what the admin interface looks like.



![1729536409871](image/m_profileCardItem-funcD/1729536409871.png)

Just keep in mind that the maximum positions displayed in the card view is 2.

The List view can display up to 5 positions. See the documentation for the Profile List Item for more information.

# Interaction and states

The cards have a default state. Since there is no "hit" area for the entire card, the card does not have a hover or a focus state. The data within the cards link to different things. So the hover and focus states are for the links within the card.

**Name:** Links to the profile. Gets a green underline on hover

**Email:** Is a MailTo link. Use UVM contextual link style for hover: text turns black, yellow underline wipes on left to right (like on the news teasers)

**Phone:** Is clickable / tapable on mobile, dependent on mobile browser and phone. Use UVM contextual link style for hover.

### A note about hover and focus states

The entire teaser container is not the "hit" area as there are multiple links within the teaser itself. Thus, the individual links within the teaser have their own default, hover, and focus states, as applicable. For text links, the text underlines yellow on focus. For the hover state, the lightGray linked text underlines yellow and the text turns black. For the link for the person's name, the hover and focus state shows the primary3-lighter underline and the text remains primary1 green.

# Responsive Notes

### The Profile Card comes in both mobile and desktop sizes.

**Desktop:** for small - xl breakpoints (scaling stops at xl)

**Mobile: **for xxs and xs breakpoints whether landscape or portrait



![1729536416828](image/m_profileCardItem-funcD/1729536416828.png)

On mobile:

* Text is smaller
* Paddings are smaller

# Layout and Specifications

Please see the specifications in the Figma file.

* The padding for both lists and cards is 24px
* The space between the image and the text content is 24px for both lists and cards



![1729536422507](image/m_profileCardItem-funcD/1729536422507.png)

The padding and spacing varies  between the desktop version (left), and the mobile version (right).

Note that there is 8px between the icon and the label for both desktop and mobile versions.

**Border radius**

The border radius for all cards is 8px.



![1729536433753](image/m_profileCardItem-funcD/1729536433753.png)

For medium 1012 breakpoint and larger, the card with the larger type is used. Unlike the profileTeaserList, just the name and title are larger.

 For small 768, xs 480 portrait and mobile landscape, the card has slightly smaller type for the name and title. Unlike the profileTeaserList, just the name and title are smaller.



![1729536444284](image/m_profileCardItem-funcD/1729536444284.png)

## Border radius

The border radius for both the card and the list is 8px.

For the card, the image also has a border radius on the top left and top right. The bottom of the image does not have a border radius.



![1729536462202](image/m_profileCardItem-funcD/1729536462202.png)



![1729536471069](image/m_profileCardItem-funcD/1729536471069.png)

# Type size differences

### Card view, medium 1012 - xxl breakpoints



![1729536489602](image/m_profileCardItem-funcD/1729536489602.png)

^ For medium 1012 breakpoint and larger, the card with the larger type is used. Unlike the profileTeaserList, just the name and title are larger.

### Card view, small 768 and mobile landscape



![1729536500525](image/m_profileCardItem-funcD/1729536500525.png)

^ For small 768, xs 480 portrait and mobile landscape, the card has slightly smaller type for the name and title. Unlike the profileTeaserList, just the name and title are smaller.
