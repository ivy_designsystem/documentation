# Profile Cards display high-level introductory information about people - faculty, staff, leadership, students, etc. in a card format.  This is documentation for the individual profile card.

![1729536542092](image/m_profileCardItem-intro/1729536542092.png)
