Profile pages on the UVM website have a hero with the name, title and profile image. The Profile Image Group uses the epic shape as both an SVG image mask and a animated shape asset.

![1729534388554](image/m_profileImgGrp-funcD/1729534388554.png)

[See the profile hero banner for the description of this component within the context of the hero.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e37cb348713aebc4593c99/tab/design?mode=preview)
