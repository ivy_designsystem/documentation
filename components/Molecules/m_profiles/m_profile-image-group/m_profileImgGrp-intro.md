Profile pages on the UVM website have a hero with the name, title and profile image. The Profile Image Group uses the epic shape as both an SVG image mask and a animated shape asset.

![1729534403246](image/m_profileImgGrp-intro/1729534403246.png)
