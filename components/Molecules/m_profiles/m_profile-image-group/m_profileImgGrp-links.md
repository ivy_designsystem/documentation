## Profile Image Group Figma handoff file

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-185298&mode=design

This is a link to both the profile image group and the group placed within the hero banner container for all breakpoints.

## Profile image group component on artboards by breakpoint

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-186656&mode=dev

Inspectable artboards: final profile image group design by breakpoints placed on page artboards-- so you can turn on the grid and see the grid columns in relation to the component.

## Profile image group- solid fills

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-186763&mode=dev

Final profile image group by breakpoint but with solid fills-- so more primitive.

## Animation description

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-186244&mode=dev

Animation description for profile image group
