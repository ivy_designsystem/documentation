# The Profile List Group is a collection of profile teasers using the list display format. It’s a common UI element that displays a group of profile teasers that provide quick introductory information  about a group of people - faculty, staff, leadership, students, etc.

# **Usage**

The 'Profile List Group' is a collection of individual '[Profile list items&#39;](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65085740e1135bd60fe8aebb/tab/design?mode=preview). It is *never used on it's own* as is only a sub-component to the Profile Queue-list component.

Here is the atomic progression of the profile teaser components in list format. On the left is the individual profile list item followed (center) with the profile list group described here. Profile List Item and the Profile list Group are  *never used independently* . They are sub-elements to the Profile Queue-list (right).



![1729535312958](image/m_profileListGroup-funcD/1729535312958.png)

---

# Display types

Profile teasers come in both card and list display types. Please also take a look at the [profile card group ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3eb20332e95d12ae5917/tab/functional%20narrative?mode=preview)documentation.

## Color variations

Profile teaser groups are available in light and dark modes for both lists and cards. We are only going to work with the light mode at present. **There is no need to develop the dark mode right now.**

![1729535320392](image/m_profileListGroup-funcD/1729535320392.png)

^ Dark version.



![1729535327054](image/m_profileListGroup-funcD/1729535327054.png)

^ Light version

# Data

See the [profile teaser documentation (List)](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65085740e1135bd60fe8aebb/tab/design?mode=preview) for the data for the individual profile teasers. This component is a group of profile list teasers.

# Interaction and states

Profile teasers can link through to a profile page. The person's name is the link to the full profile. The email is a "mailTo" link and on tap/click will launch the user's email app to draft and email to the profile recipient. The phone number will be turned into a link on mobile depending on the platform and mobile browser functionality.

**Name:** Links to the profile. Gets a green underline on hover

**Email:** Is a MailTo link. Use UVM contextual link style for hover: text turns black, yellow underline wipes on left to right (like on the news teasers)

**Phone:** Is clickable / tapable on mobile, dependent on mobile browser and phone. Use UVM contextual link style for hover.

**Image:** No UX research has been done, but I think that people will tap/click on the bio image also to view the full bio. Would like to have the bio image as a redundant link with the person's name. On desktop, use the hover state with the gamma shift that we have been doing.

IMPORTANT: There is no scenario where this hover state will show as the component has various links- the name, email, and on mobile- the phone number.

# Display by breakpoint

The Profile List component can be displayed as either a list or a grid. This is true for every breakpoint. There are some differences though. The number of columns that the group spans varies by breakpoint and on mobile, the list view for portrait displays, does not display the image.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.



![1729535334330](image/m_profileListGroup-funcD/1729535334330.png)

# Layout spacing

Corner radius: 8px all breakpoints

Grid Gutter: vertical and horizontal gutter between cards is 16px, all breakpoints

Profile teasers have 16px space between them whether cards or lists. Spacing is the same between cards vertically and horizontally.



![1729535340944](image/m_profileListGroup-funcD/1729535340944.png)



![1729535347557](image/m_profileListGroup-funcD/1729535347557.png)

## Border radius

The border radius for both the card and the list is 8px.

For the card, the image also has a border radius on the top left and top right. The bottom of the image does not have a border radius.



![1729535353393](image/m_profileListGroup-funcD/1729535353393.png)

# Display by breakpoint

The Profile Card Group component can be displayed as either a list or a grid. This is true for every breakpoint. There are some differences though. The number of columns that the group spans varies by breakpoint and on mobile, the list view for portrait displays, does not display the image.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.



![1729535359246](image/m_profileListGroup-funcD/1729535359246.png)

The Profile Card teaser item has an image at all breakpoints.

Gutter / internal grid spacing is 16px for all breakpoints.

Here is a visual reference to show the column spans for the card group by breakpoint. Of note:

**xxs portrait:** this is the only breakpoint where there is 1 card / row. They stack vertically with 16px between then.

**xxs landscape:** Cards go to 2 / row

**xs:** Cards are 2/row

**Small and larger: **cards are 3/row. ColSpan of card Group varies by breakpoint. See chart above.

**XL: **card group does not scale above XL.

![1729535365398](image/m_profileListGroup-funcD/1729535365398.png)
