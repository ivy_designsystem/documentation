## Master component in Figma

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-104532&mode=dev

Master component for design and for Ivy

## List group specs

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-105434&mode=dev

## Card group specs

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-108655&mode=dev
