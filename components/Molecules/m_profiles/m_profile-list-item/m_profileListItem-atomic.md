## Atoms

Ivy / Foundations / Icons / Profile / [profile icons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64e5160c7c57dc9aeebda9e0?mode=preview)

Note: the phone and email icons used in the profile hero banner are outlined and the icons used in the profile teasers are solid.

![1729534818912](image/m_profileListItem-atomic/1729534818912.png)

^ This is a screen grab from a profile teaser

![1729534824364](image/m_profileListItem-atomic/1729534824364.png)

^ this is a screen grab from part of the profile hero banner.

**Image**

Ivy / Components / Atoms / Media / [img_1x1](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61390881f9799804f7f9d035/tab/design?mode=preview)

The image for the profile teaser always has a 1x1 ratio.

**Placeholder image**

This is a backup placeholder image to be used if no image was used in the person's bio profile.

Ivy / Foundations / Images / [Placeholder image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/652ebb6a55d56ae89ed2ff0e?mode=preview)

## Molecules
