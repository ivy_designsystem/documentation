Profile Teasers are a common UI element that displays quick introductory information  about people - faculty, staff, leadership, students, etc.  in a visually pleasing way. Profile Teasers exist in both card and list formats. This is the documentation for the individual profile list item.

## Display types

Profile teasers come in both [card](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/functional%20narrative?mode=preview) and list display types. Here is  an individual profile list item:

![1729534695733](image/m_profileListItem-funcD/1729534695733.png)

## Color variations

Profile teasers are available in light and dark modes for both lists and cards. We are only going to work with the light mode at present. **There is no need to develop the dark mode right now.**

![1729534703102](image/m_profileListItem-funcD/1729534703102.png)

## Data

**Image** 1x1 image, 1080x1080 (scales down)

The profile hero banner and the profile teasers use the same image. Only the one image asset is needed.

**Name **Simple text. Links to the individual's profile page, if they have one. Link should be dynamic- not manual.

**Title **Simple text

People can have up to 5 titles. In the profile teaser, the titles appear sequentially separated by a [space][bullet][space].

**Email **simple text. Is a MailTo link

**Phone **Pickup same format and content from existing. Links on mobile if user's scenario handles that

**Image:** No UX research has been done, but I think that people will tap/click on the bio image also to view the full bio. Would like to have the bio image as a redundant link with the person's name. On desktop, use the hover state with the gamma shift that we have been doing.

**What to do if there is no image?**

Unlike the profile hero banner, the profile teaser has a placeholder profile image if none is supplied. If none, use [provided placeholder.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/652ebb6a55d56ae89ed2ff0e?mode=preview)

![1729534712691](image/m_profileListItem-funcD/1729534712691.png)

## Interaction and states

Profile teasers can link through to a profile page. The person's name is the link to the full profile. The email is a "mailTo" link and on tap/click will launch the user's email app to draft and email to the profile recipient. The phone number will be turned into a link on mobile depending on the platform and mobile browser functionality.

**Name:** Links to the profile. Gets a green underline on hover

**Email:** Is a MailTo link. Use UVM contextual link style for hover: text turns black, yellow underline wipes on left to right (like on the news teasers)

**Phone:** Is clickable / tapable on mobile, dependent on mobile browser and phone. Use UVM contextual link style for hover.

![1729534719022](image/m_profileListItem-funcD/1729534719022.png)

### A note about hover and focus states

The entire teaser container is not the "hit" area as there are multiple links within the teaser itself. Thus, the individual links within the teaser have their own default, hover, and focus states, as applicable. For text links, the text underlines yellow on focus. For the hover state, the lightGray linked text underlines yellow and the text turns black. For the link for the person's name, the hover and focus state shows the primary3-lighter underline and the text remains primary1 green.

## Scaling with content

Content and images are aligned top. Content containers scale from the top down with the image aligning top left in the component. The content could exceed the default height of the card. If this happens, the component will scale in height, respecting the bottom padding. Below is an example of how the profile teaser list component scales when a lot of content is added to it. We hope that our content editors and profile owners show good content authoring and editing skills and that this doesn't happen.

NOTE: Areas of Expertise was removed from the profile teaser as people were packing too much content in there.

![1729534727338](image/m_profileListItem-funcD/1729534727338.png)

^ Note. The telephone and email have moved above the Areas of Expertise (starting with "chamber music..." in this example) but animated gif was not remade to show the correct order.

## List display with multiple job titles

![1729534739240](image/m_profileListItem-funcD/1729534739240.png)

Profile bios can have up to 5 job titles. Commas can be used within job titles, for example:

Vice Provost for Diversity, Equity and Inclusion

Multiple job titles run together and are separated by bullets. The bullets help with readability and help the site visitor discern the individual titles within the list.

In the Drupal admin area, content editors can add up to 5 position titles per bio. Here's what the admin interface looks like.

![1729534752327](image/m_profileListItem-funcD/1729534752327.png)

Note: the Profile Card display type it limited to to the card format to a maximum of 2 positions. If your collection of profile teasers have include people with more than 2 positions, the List display is recommended.

## Responsive Notes

### List view, medium 1012 - xxl breakpoints

![1729534758001](image/m_profileListItem-funcD/1729534758001.png)

^ Showing the dark version here. Characteristics:

* The contact information is displayed in 2 columns starting at the medium-1012 breakpoint.
* The list teaser at the medium breakpoint starts at 948px wide and scales up to fill the container for the larger breakpoints.
* The text is a larger size than the mobile version.

*Important: "Areas of Expertise" has since been removed*

### List view, small 768 breakpoint and mobile landscape

![1729534763348](image/m_profileListItem-funcD/1729534763348.png)

^ Showing the dark version here for the small 768 breakpoint and landscape mobile. Characteristics:

* The contact information is stacked
* The text is smaller than the text in the teaser above used for the medium - xxl breakpoints (hard to tell here as InVision DSM scales up the image)

### List view on mobile portrait

![1729534771392](image/m_profileListItem-funcD/1729534771392.png)

^ On mobile portrait the list view:

* Image does not display

![1729534777963](image/m_profileListItem-funcD/1729534777963.png)

List view, sizes medium and larger (scales horizontally)

## Icon Alignment

Figma adds a bit of padding to text containers so some fussiness is required with the layout to get the icon and text to align top.

Desire: icon appears centered to the text to the right of it. Icon aligns top if the text wraps to multiple lines.

![1729534783184](image/m_profileListItem-funcD/1729534783184.png)

## Layout and Specifications

Please see the specifications in the Figma file.

* The padding for both lists and cards is 24px
* The space between the image and the text content is 24px for both lists and cards

![1729534789254](image/m_profileListItem-funcD/1729534789254.png)

![1729534800283](image/m_profileListItem-funcD/1729534800283.png)
