Profile Teaser displays high-level introductory information about people - faculty, staff, leadership, students, etc. in a horizontal card format. This is documentation for the individual profile list item.

![1729534679843](image/m_profileListItem-intro/1729534679843.png)
