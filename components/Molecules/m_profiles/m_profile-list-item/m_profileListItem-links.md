## Figma Layout specifications

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2337-60328&mode=dev

Specs_profileTeaserList_m-xxl

## Figma layout specifications

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2337-60329&mode=dev

Specs_profileTeaserCard-s

## Figma layout specifications

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2337-60331&mode=dev

Specs_profileTeaser-mobilePortrait

## Master component

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-104135&mode=dev

Master Profile Teaser component- both cards and lists, light and dark

## Handoff for profile teaser

https://www.figma.com/file/YdXyU9MEyK4BKhD6a6CbZm/Profiles?type=design&node-id=1-104990&mode=dev
