# The hero progress bar communicates the progress the user is making while viewing a hero that has a sequence with a beginning and an end. It was designed for the home page hero which has 4 slides with an animated transition between each but can be repurposed.

### Elements

Bar indicating progress: uses color primary3

Bar indicating 100%: uses color modalBlack-20

### Parts / breakdown

The "trench" shows 100% and is a modal black overlay @20% opacity.

The "progress" bar lays on top of the trench and fills primary3-yellow to indicate, by %, the progress while viewing the home page hero.

The yellow bar is at 0% on hero load and 100% width at the end of the hero play.

### Replay

If the user gets to the end and then chooses to Replay the home page hero, the progress bar goes resets to zero.

![1729536783190](image/m_heroProgressBar-funcD/1729536783190.png)
