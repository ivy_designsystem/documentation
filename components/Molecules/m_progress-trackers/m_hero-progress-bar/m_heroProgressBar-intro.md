# The hero progress bar communicates the progress the user is making while viewing a hero that has a sequence with a beginning and an end. It was designed for the home page hero which has 4 slides with an animated transition between each but can be repurposed.


![1729536745444](image/m_heroProgressBar-intro/1729536745444.png)
