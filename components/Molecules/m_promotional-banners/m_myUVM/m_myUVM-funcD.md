# The My UVM promotional banner is a link that appears in the take-over navigation drawer. It's purpose is to provide a highly visible, center-stage link to the My UVM portal.

## **Data: **

None

## **Micro Interaction**

Underline write-on left to right with arrow bump to the right.

[Screencast show and tell.](https://uvmoffice-my.sharepoint.com/:v:/g/personal/smasonla_uvm_edu/Eau4V8QOtJ5Cmm8P4-00dgABw53F-xMEa3H3qH0GpkHLVQ?e=54eLcB)

Here is an example of that interaction ([harvard.edu](http://www.harvard.edu/) and then select the hamburger menu. )

**Size and Width**

The promoBanner_myUVM is the same size and 256px wide for all breakpoints

**Hit state**

Preferred: that the entire banner is the hit state and the hover state.

**Hover state**

On-hover:

• The link text has an underline.

• The arrow moves +16px to the right on hover.

![1729609450217](image/m_myUVM-funcD/1729609450217.png)



![1729609460482](image/m_myUVM-funcD/1729609460482.png)

*Note: the primaryDark background was added here just to make these visible. *
