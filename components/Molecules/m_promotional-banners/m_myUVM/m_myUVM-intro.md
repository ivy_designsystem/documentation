# The MyUVM promotional banner is a link that appears in the take-over navigation drawer. It's purpose is to provide a highly visible, center-stage link to the My UVM portal.



![1729609476811](image/m_myUVM-intro/1729609476811.png)
