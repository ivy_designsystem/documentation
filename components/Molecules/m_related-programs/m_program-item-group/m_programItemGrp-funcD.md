# The Program Item Group is a collection of [individual related programs](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf7b58294793d1b5b305/tab/Related%20program%20group?mode=preview). There is a list and grid display variation. This component is not intended to be used on its own. It is integral to the [related programs component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bfa41579dd36a26ce0fb/tab/design?mode=preview).

## Data

The group consists of individual related program items that are grouped together. [See the related program item.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf7b58294793d1b5b305/tab/Related%20program%20group?mode=preview)

## **Criteria for success**

**Important! **The data for the high-level content that is displayed in the program list or card, is intentionally defined to provide a consistent, high-level, take-away view of the program. Essentially, we are providing a shopping experience for the site visitor and their ability to get a quick overview of the program is really important and to compare programs. The programs can easily be compared at-a-glance if we offer high-level information about the program in an apple-to-apples way. Consistency is key with the display of content so the end user experience is optimal and the content it easy to understand, consume and compare.

## Usage

The Related Program Group is a subcomponent of the [Related Program component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bfa41579dd36a26ce0fb/tab/design?mode=preview). It is never used independently.

## Layout

Whether displaying as a card group or a list group, the space between items is 16px horizontally or vertically.

## Width of group by breakpoint

The width of the group is dependent upon the container that it is placed within. The gutter between items regardless of the view is 16px. There is also a 16px vertical space between items when stacked.

After accounting for the 16px space between each item, the individual related programs in the group scale width-wise to fill the container the group is placed in.

Please see the [Related Program Group ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bfa41579dd36a26ce0fb/tab/design?mode=preview)for more information.
