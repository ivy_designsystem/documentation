# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'Program Item Group' component:



![1729610759769](image/m_programItemGrp-naming/1729610759769.png)
