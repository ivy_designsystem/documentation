# The Related Program item displays the information of a single program. It has two display styles: list or card.

![1729609955439](image/m-programItem-funcD/1729609955439.png)

![1729609964664](image/m-programItem-funcD/1729609964664.png)

# Display

The related program element can be displayed in card or list form.

^ This is a Program card



![1729610013707](image/m-programItem-funcD/1729610013707.png)

^ this is a program list item with an image. The image is optional for the list view.



![1729610020591](image/m-programItem-funcD/1729610020591.png)

^ This is a program list item for mobile. The image, ifthere is one, does not display on mobile for the program list.

## Background color options

Both the program list and card views can be on either a white or a neutral background.

**List items **

Individual list items can have a white background with hover to a neutral background. Or a neutral background with hover to a white background.



![1729610027758](image/m-programItem-funcD/1729610027758.png)

**Program Cards can be either on a white background or a neutral background. **



![1729610034081](image/m-programItem-funcD/1729610034081.png)

# Display by breakpoint

The related program component can be displayed as either a list or a grid. This is true for every breakpoint. There are some differences though. The number of columns that the group spans varies by breakpoint and on mobile, the list view for portrait displays, does not display the image.  In landscape mode, however, the image does show for both list and grid displays on mobile.

The image shows in the grid display for all breakpoints.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.



![1729610041462](image/m-programItem-funcD/1729610041462.png)

# Responsive notes



![1729610047635](image/m-programItem-funcD/1729610047635.png)

^ at the small-768 breakpoint, the grid view shows 3 program cards / row



![1729610054010](image/m-programItem-funcD/1729610054010.png)

^ on mobile landscape mode, the grid shows 2 programs / row. Above is the xxs-landscape view (568 wide)



![1729610061274](image/m-programItem-funcD/1729610061274.png)

^ On all mobile breakpoints, portrait or landscape, the image does not show in the  **list view** .

# Layouts and spacing



![1729610072435](image/m-programItem-funcD/1729610072435.png)



![1729610083102](image/m-programItem-funcD/1729610083102.png)

See the FIGMA file.. The sPEC tool is adding a dark outline on the hover state that is not there.

# Accessibility

* Semantic HTML: Use semantic HTML elements to provide meaningful structure to screen reader users
* Keyboard navigation: Users should be able to navigate the card/container component using the keyboard, allowing users to focus on different parts of the content
* Screen reader users should be able to select each program item (the entire container for each individual program is linked) and navigate to the destination link
* Screen reader support: The card/container component is announced to screen reader users and its content is read out loud, making it accessible to users with visual impairments
* Content editors should provide alternative text for images: Describe the image (using the alt attribute) for screen readers to announce to user

# More specs

Make sure you take a look at the layout and spacing specifications on the [Figma page](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bfa41579dd36a26ce0fb/tab/puffyB!k3)@). Between inspecting the components and the Specs, there is a lot information there about padding, spacing, and layout.



![1729610093098](image/m-programItem-funcD/1729610093098.png)

## Image

A 1x1 image is mandatory and displays on desktop for both the list and grid versions, and on mobile for the grid display.

**Mobile**

The image does not display on mobile for the list display.

# Data

**Mandatory data for both card and list views**

* Program title
* Degree type
* School or College
* Class format
* Estimated years to completion
* Credit hours to graduate

The data is the same regardless of the display.

**Mandatory data **

1x1 image, 800x800

# **Composition / Criteria for success**

**Important! **The data for the high-level content that is displayed in the program list or card, is intentionally defined to provide a consistent, high-level, take-away view of the program. Essentially, we are providing a shopping experience for the site visitor and their ability to get a quick overview of the program is really important and to compare programs. The programs can easily be compared at-a-glance if we offer high-level information about the program in an apple-to-apples way. Consistency is key with the display of content so the end user experience is optimal and the content it easy to understand, consume and compare.

# A note about the image

It would be desirable to have the image be a larger square and fill the container like shown below, top. However, this approach falls apart as soon as the container scales in height. If the square image scales proportionally to fill the gap as the container scales vertically, it will quickly eat up the space in the container and squeeze the text. If the image scales only vertically, then the aspect ratio of the image will need to be dynamic.

Either way, it's problematic. Just a note added as to why the square image in the list view floats.

# Focus state

Borders for the Related Program Item for the default and hover states are 1px and are outside borders. The focus border is a 2px centered border with the intention of thickening the border to 2px to meet WCAG2.0 standards while keeping the item from shifting/jumping on focus.

DEV Note: I believe focus state will BOX the tile rather than adding a border to the edge. To discuss on Monday 9/11. I can update.

# Size

The type size is the same for both display types and all breakpoints.

The padding is the same for both list and card views for all breakpoints.

The widths are determined by the container they go in and the padding between elements. Please see "related components" below for links to the larger components that these get nested in.

# Related components

This component is the individual program element which never appears on its own. It is a sub element to the larger related program group and related programs component.  See the description for these components for more information.

* [Related program group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf85194db8298492b6ab/tab/design?mode=preview)
* [Related programs](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bfa41579dd36a26ce0fb/tab/functional%20narrative?mode=preview)

# Layout and Spacing



![1729610105372](image/m-programItem-funcD/1729610105372.png)
