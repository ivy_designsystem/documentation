## Figma file

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2140%3A83970&mode=design&t=70GAQHbOvBCWcd2d-1

Link to the section in the Figma file with the inspectable master component.

## Handoff area in Figma file

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2158-8441&mode=dev

inspectable components for Ivy.
