# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'program item' component:



![1729609902540](image/m-programItem-naming/1729609902540.png)
