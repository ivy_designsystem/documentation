# **The search on the website uses tabs to allow users to switch between a general UVM search and a search for someone via the People directory. These are the individual tabs that when paired together, are used in the tabSwitcher Group.**

## Inputs

None

## States

The tabs come in 2 sizes: one for the xxs breakpoints and the larger for xs-xl breakpoints.  They have active, hover, and inactive (non-selected) states.



![1729621575900](image/m_indSearchTabs-funcD/1729621575900.png)

# Actions

**inactive state:** can hover

**active state: **communicates which search the user is in (website or people)

**Hover state:** Shows this state on-hover/on-tap. On-click / on-tap switches the hover state to an active state

**Developer note**

Active state is always on top of the inactive button.

**xxs**

Tabs are smaller.

Tabs do not have icons

Tab labels are smaller label size text

**xs-xl**

Larger size tab

Tabs have icons

Tab labels are default tab label size text

# Micro-interactions

Please add a fade to the hover so that there is some tactile quality to the tab.
