# Search suggestions are search return recommendations that populate a dropdown dynamically below the search entry box as users type their search query. These recommendations appear beneath the search box and change as users type each letter of their query.

Google’s auto suggestion feature provides real-time returns as the user enters type into the text entry area . We are currently using this and wish to keep it.

The goal is to help users find what they are looking for with as few clicks and as fast as possible. This functionality is in align with our goals.

**Important! ***You can pick up the existing functionality and styling for starters. Make sure that the bullets do not appear. I think the content is being pulled in as an unordered list and the bullets are hanging outside the auto-suggest box. Do not leave it like this. *

On **mobile,** the search suggestions are on white below the search header. The content area



![1729621439295](image/m_searchSuggest-funcD/1729621439295.png)

^ Notice that the width of the Search Suggestion box is 12 columns at the xl-1440 breakpoint and goes full screen width. The text content box is inset 332px on the left and right, respecting the 32px page margins.



![1729621446817](image/m_searchSuggest-funcD/1729621446817.png)

^ Notice that the width of the Search Suggestion box is 6 columns at the xl-1440 breakpoint.

## Width of the Search Suggestion box

To keep a comfortable reading line length, the width of the search suggestion box and text varies by breakpoint.

Here is a reference table showing the widths of the auto suggestion box by breakpoint.

![1729621453686](image/m_searchSuggest-funcD/1729621453686.png)
