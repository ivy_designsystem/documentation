# Search suggestions are search return recommendations that populate a dropdown dynamically below the search entry box as users type their search query. These recommendations appear beneath the search box and change as users type each letter of their query.

![1729621470619](image/m_searchSuggest-intro/1729621470619.png)
