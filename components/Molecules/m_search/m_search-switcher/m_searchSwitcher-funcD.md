# The Search Switcher is a group of two tabs.

# It is compact and concise yet powerful. It allows site visitors to perform either a global site search or a search for a person in one combined user interface.

## Inputs

None

**Search Switcher**

**The search interface uses a **[**Search switcher **](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6340b11574f7cd2dd76e4a7d/tab/design?mode=preview)(tab group) to allow the user to switch between searching all of UVM or the people directory.

**Hit area**

The Search Switcher is comprised of two tabs:

* Website search
* People search

The entire area of each individual tab is the hit area.

**Each button has the following states**

* Inactive Tab
* Hover
* Active Tab

Please see the[ Individual Tab component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6344322e323a737aef79bf2b/tab/design?mode=preview)for everything you want to know about the individual search tabs.

## States

Here are the states for the ** Tab Switcher** . Make sure the active tab is always in front of the inactive tab. (Inactive is perhaps better referred to as 'non-selected', as this tab is a link with a hover state)



![1729621334064](image/m_searchSwitcher-funcD/1729621334064.png)

**Developer Note** We are not changing the search functionality, just combining the existing two separate searches into one with a toggle to switch between them.

# Actions

**inactive state:** can hover

**active state: **communicates which search the user is in (website or people)

**Hover state:** Shows this state on-hover/on-tap. On-click / on-tap switches the hover state to an active state

**Developer note**

Active state is always on top of the inactive button.

**Responsive display differences**

**xxs**

Tabs are smaller.

Tabs do not have icons

Tab labels are smaller size text

**xs**

Larger size tab

Tabs have icons

Tab labels are still the smaller size text

**S+**

Larger size tab

Tabs have icons

Tab labels are default label text

# Micro-interactions

Please add a fade to the hover so that there is some tactile quality to the tab.
