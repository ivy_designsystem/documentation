# The Search Switcher is a group of two tabs.

## It is compact and concise yet powerful. It allows site visitors to perform either a global site search or a search for a person in one combined user interface.
