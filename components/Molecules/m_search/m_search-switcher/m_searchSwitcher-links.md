## Search Switcher

https://www.figma.com/file/wtBdYBBs9T1HOAdJObEy3k/%F0%9F%93%9A--navigation-Ivy?node-id=634%3A149145

Search Tab Group for the Search header

## Desktop prototype

https://www.figma.com/proto/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?page-id=659%3A25745&node-id=1409%3A43268&viewport=252%2C428%2C0.14&scaling=min-zoom&starting-point-node-id=1407%3A45765&show-proto-sidebar=1

Desktop prototype to show how the tabs switch. NOTE: Hover state not shown
