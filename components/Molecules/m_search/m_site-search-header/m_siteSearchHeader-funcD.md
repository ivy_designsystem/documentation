# UVM uses Google to power the search on the site. The search functionality is launched via the search icon in the upper right of the header on all website pages. The search interface is a take-over. Users are able to perform either a people search or a website search.

## Functionality description

Our Search has brevity. It is compact and concise yet powerful. It uses a** tab group** to provide both a global site search and a search for someone in our people directory.

**Dual search with tabs**

On-click / on-tap of the search icon in the upper right corner of the site's header, the search interface does a screen take-over. The search defaults to the Website search with the UVM tab in an active state. Users can click on the tabs to switch between a website search and a people search.

When the People search is active on the toggle, the hint text in the text entry area changes to "Type to search for someone".

When the Website search is active on the toggle, the hint text in the text entry area changes to "Type to search our website"



![1729621106732](image/m_siteSearchHeader-funcD/1729621106732.png)



![1729621160941](image/m_siteSearchHeader-funcD/1729621160941.png)

**Developer Note** We are not changing the search functionality from what is currently on the site. We are improving the user interface by combining the existing two separate searches into one with tabs to switch between them.

# **Inputs**

## **Hint text for keyword search**

**For Website search**

For breakpoints small - xxl: Type to search our website

For xxs, and xs breakpoints: Type to search our website

**For People search**

For breakpoints small - xxl: Type to search for someone

For xxs, and xs breakpoints: Type to search for someone

**Search text**

Simple text. No additional styling.

Users can enter the search text string for what they want to search for.

## M**andatory design elements**

(all)

Search switcher

UVM logo

Close icon

Search icon

**Developer Notes:**

Let's discuss the search returns outside of the auto-suggest. This has not been discussed as a team. There is no functional narrative for the search returns and it is assumed that the existing functionality stays for now.

**Accessibility Notes**

Search interface should be accessible and able to be controlled via the keyboard. The full experience needs to be tested with jaws.

The **hint text** changes when users switch between **website search** and **people search **in the toggle.



![1729621171251](image/m_siteSearchHeader-funcD/1729621171251.png)
