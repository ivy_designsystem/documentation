
# UVM uses Google to power the search on the site. The search functionality is launched via the search icon in the upper right of the header on all website pages. The search interface is a take-over. Users are able to perform either a people search or a website search.

![1729621190347](image/m_siteSearchHeader-intro/1729621190347.png)



![1729621197258](image/m_siteSearchHeader-intro/1729621197258.png)
