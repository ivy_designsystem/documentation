# The tabNavBar is internal navigation to a component. It allows users to consume content by category or grouping. It allows a lot of content to be easily accessible within a limited amount of real estate.

![1729622850598](image/m_tabNavBar-funcD/1729622850598.png)

State for individual tabs

* Active
* Default (link)
* Hover
* Press
* Focus

![1729622858895](image/m_tabNavBar-funcD/1729622858895.png)

## Sizes

* mobile (48px high tab)
* desktop (48px high tab)

Note: they were different sizes when they were square tabs and have since been made uniform pills... exec. level change.

# Width of tabNavBar

The width of the tabNavBar is determined by the number of tabs.

# tabNavBar parameters

* No more than 7 tabs
* No fewer than 2 tabs
* First tab is selected on page-load
* Only one tab can be selected at a time
* Tabs become horizontally scrollable when the length of the tabNavBar exceeds the width of the container
* If tabs exceed width of their container, a gradient is added to the end (right) of tab container
* When scrolled to end of tabs, a gradient is added to the beginning (left) of tab container

# ScrollBar behavior

TabNavBar: No horizontal scroll needed



![1729622872783](image/m_tabNavBar-funcD/1729622872783.png)

TabNavBar: Horizontal Scroll



![1729622878802](image/m_tabNavBar-funcD/1729622878802.png)

If the tab content in the tabNavBar exceeds the breakpoint of the page (including margins), then the swiperBtnGroup appears as internal navigation.  The image above shows both previous and next arrows in their default/link state as the user can access tabs off screen to the right and the left.

# Behavior

on-click of the SwiperBtn prev/next moves the tab content xx px to the left or right.

Important: We are not going to increment the move by tab widths as the width of the tabs change based upon content admin input. The idea is to allow the user to incrementally reveal more tab content either to the left or right.

Distance sweetspot tbd during QA... we don't want the user to work hard with teensy incremental moves but we don't want it to be frustrating to 'zone in' on a tab they wish to review-- ie: the tabs should not whoosh by left and right so the user feels out of control. Start with a 96px move and we will see how that feels.

TabNavBar: Next only



![1729622885230](image/m_tabNavBar-funcD/1729622885230.png)

Only the needed / relevant control is revealed. The disabled state is invisible.

In the example above, the user cannot navigate to a Previous tab as the dark green selected tab is #1 in the tabNavBar. Thus, the Previous SwiperBtn is not visible.

# ScrimWhite

Note that the scrimWhite is overlayed over the navBar when the swiperBtn controls appear.
