# The tabNav is internal navigation to a component. It allows users to consume content by category or grouping. It allows a lot of content to be easily accessible within a limited amount of real estate.

![1729622898072](image/m_tabNavBar-intro/1729622898072.png)
