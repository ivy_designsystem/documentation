
# Intro

This tile has an icon, a title and an arrow. The entire card is linkable.

`br`
![alt text](tile_icn-title.png)



# Data

**Mandatory**

* Icon (SVG only)
* Title: Simple text 68 characters max
* Link: URL

# Icon

* Icon must be an SVG
* Desktop size of icon is 56px
* Mobile size of icon is 48px

# Names of content elements
![alt text](image/02-tile-icn-title.png)


# States

* Default
* Hover

# Interaction

**Desktop**

onLoad:

Tile loads with a fade.

**Micro-interaction: **on-hover of the tile (entire tile is hit area), the background turns to 100% primary3 yellow.

# Display notes

* Icon and title have a fixed space between them.
* Arrow icon is fixed to the bottom of the tile with a margin.
* Padding on tile varies by breakpoint.
* Space between title and arrow varies by the amount of content on the tile.
* Tile has a fixed size for desktop and mobile. This is important as the tile gets used in a group with a grid layout rather than a masonry layout, so the tiles have to be the same size.

![alt text](image/03_tile_icn-title.png)



# Mobile Notes

**There are 2 different mobile tiles:**

* One that is narrower than the mobile content container so it can be used within a swiper with tiles "in the L/R wings."
* One that is the width of the content container (breakpoint less L/R margins)

**Note: Hold off on the SWIPER version**
