# This tile has a title and an arrow. The entire card is linkable.



![1729622303892](image/m_tile-tItle-funcD/1729622303892.png)

# Data

**Mandatory**

* Title: Simple text 68 characters max
* Link: URL

# Names of content elements



![1729622310344](image/m_tile-tItle-funcD/1729622310344.png)

# States

* Default (see above)
* Hover  (see above)

# Interaction

**Desktop**

onLoad:

Tile loads with a fade.

**Micro-interaction: **on-hover of the tile (entire tile is hit area), the background turns to 100% primary3 yellow.

# Display notes

* Title hugs the top of the tile respecting tile padding
* Arrow icon is fixed to the bottom of the tile with a margin.
* Padding on tile varies by breakpoint.
* Space between title and arrow varies by the amount of content on the tile.
* Tile has a fixed size for desktop and mobile. This is important as the tile gets used in a group with a grid layout rather than a masonry layout, so the tiles have to be the same size.

![1729622318975](image/m_tile-tItle-funcD/1729622318975.png)

# Mobile Notes

**There are 2 different mobile tiles:**

* One that is narrower than the mobile content container so it can be used within a swiper with tiles "in the L/R wings."
* One that is the width of the content container (breakpoint less L/R margins)

**NOTE: hold off on the SWIPER version.**
