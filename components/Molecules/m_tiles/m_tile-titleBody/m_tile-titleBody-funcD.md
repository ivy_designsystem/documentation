# This tile has a title, short descriptive paragraph and an arrow. The entire card is linkable.

![1729622543117](image/m_tile-titleBody-funcD/1729622543117.png)

# Data

**Mandatory**

* Title: Simple text 68 characters max
* Short descriptive sentence: simple text ~90 characters max
* Link: URL

# Names of content elements

![1729622552822](image/m_tile-titleBody-funcD/1729622552822.png)

# States

* Default
* Hover (see above)

# Interaction

**Desktop**

onLoad:

Tile loads with a fade.

**Micro-interaction: **on-hover of the tile (entire tile is hit area), the background turns to 100% primary3 yellow.

# Display notes

* Title and descriptive body text are a text group with a fixed space between.
* Arrow icon is fixed to the bottom of the tile with a margin.
* Padding on tile varies by breakpoint.
* Space between text group and arrow varies by the amount of content on the tile.
* Tile has a fixed size for desktop and mobile. This is important as the tile gets used in a group with a grid layout rather than a masonry layout, so the tiles have to be the same size.

![1729622562080](image/m_tile-titleBody-funcD/1729622562080.png)

# Mobile Notes

**There are 2 different mobile tiles:**

* One that is narrower than the mobile content container so it can be used within a swiper with tiles "in the L/R wings."
* One that is the width of the content container (breakpoint less L/R margins)

**NOTE: hold off on the SWIPER version.**
