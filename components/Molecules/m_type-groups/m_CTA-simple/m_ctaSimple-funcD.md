# CTAsimple is a type molecule. It is a simple Call To Action - A single action word or two with an arrow to compel users to take an action.

The choice of words is important. It’s a very small component but needs to be mighty to get users to click on it. The choice of words should also provide confidence in the click and land users where they expect.

![1729623291813](image/m_ctaSimple-funcD/1729623291813.png)

Note: the green background above is just for contrast. It does not appear like that in a component.

# Size and Color Variations



![1729623299607](image/m_ctaSimple-funcD/1729623299607.png)

The CTAsimple comes in 2 sizes and 2 color variations.

**Colors**

white

dark

**Sizes**

mobile

desktop

See the m_imgCTA molecule for more information about this type group. Explaining it in context is easier.

Ivy / Components / Molecules / [m_imgCTA](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6352b5878c01075ed40de926/tab/functional%20narrative?mode=preview)
