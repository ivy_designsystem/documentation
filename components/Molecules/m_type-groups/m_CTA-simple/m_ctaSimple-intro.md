# CTAsimple is a type molecule. It is a simple Call To Action - A single action word or two with an arrow to compel users to take an action.

The choice of words is important. It’s a very small component but needs to be mighty to get users to click on it. The choice of words should also provide confidence in the click and land users where they expect.



![1729623315856](image/m_ctaSimple-intro/1729623315856.png)
