# Type group of head and short body paragraph. Head uses display serif and body is sans-serif.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmORz8wJuTBIe1hW3hDfkgmpZL78gTFsJeLHtGlPKrJ5SfGY_NchyQXW1PJaJuYj_n85QJfrvx-gyWu9-zS7WvvcwU-3MNSQ7IU9-iE8GPNpOZvrrDk7TeVkxtP24KNuVKSA==?assetKey=university-of-vermont%2Fivy%2FheroDisplaySerif-rJ5q6l_vs.png)
