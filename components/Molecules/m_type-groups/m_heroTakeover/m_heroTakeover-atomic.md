# Atoms

## CTA / Text Button icon after

The CTA is an atomic element. It's the text button at different sizes, bold weight, with the icon after.

Ivy / Components / Atoms / Buttons /  [txtBtn_IcnAfter](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393b45fa8eb003116191aa8/tab/design?mode=preview)![1729624613809](image/m_heroTakeover-funcD/1729624613809.png)
