# This type group has an H2head that makes a statement in 1-3 words in lockup with a secondary sentence and CTA.   

# Anatomy of the type lockup



![1729624588245](image/m_heroTakeover-funcD/1729624588245.png)



![1729624595041](image/m_heroTakeover-funcD/1729624595041.png)

# **Data**

**MANDATORY** Data

**H2 head**

Simple text. Up to 24-30 characters, including spaces. 1-3 words only.

Good examples of copy for the H1 head include:

1. Commencement weekend
2. Welcome class of 2026
3. Don’t miss... (secondary tagline and CTA to fill in)
4. Homecoming week
5. March Madness

**Call to Action (CTA) **

The CTA is an atomic element. It uses the txtButtonIcnAfter.

# Non Mandatory data

**.p** short string of text. Simple text.

**Divider: **the divider is a slash "/". It is visible only if the .p string is present.

H1: font

[Playfair Display at 800 weight from Google Fonts](https://fonts.google.com/specimen/Playfair+Display).

Text

Roboto Sans, bold 800

# Character Lengths

**H1:** 1-3 words, simple text, 24-30 characters max

**.p **simple text,

**CTA: **it's the txtBtnIcnR

**Important: **

The .p short string and the CTA are on one line. This combined text should be restricted and not wrap to 2 lines.

# Component sizes

The component comes in a few different sizes to meet the needs of the different breakpoints.

![1729624617953](image/m_heroTakeover-funcD/1729624617953.png)


## H2 and p type size by breakpoint

Type has some type size variation by breakpoint. This table is a reference for the type sizes by breakpoint.

![1729624625391](image/m_heroTakeover-funcD/1729624625391.png)

Note: the CTA button's text size should match the p.text size. The txtButtonIcnAfter comes in bold in all those sizes.

**DESIGN NOTE**

The "/" divider should be 70% #FFF.

It's updated in the FIGMA file but not the grabs.
