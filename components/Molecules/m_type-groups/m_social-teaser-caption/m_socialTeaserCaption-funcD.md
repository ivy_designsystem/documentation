# The Social Teaser caption is a text group with optional social icon and optional social handle.

It is used in the [Curated Social Media block](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c245f9c63da5b335e277d/tab/design?mode=preview).



![1729623428968](image/m_socialTeaserCaption-funcD/1729623428968.png)

Here it is in context, in the Curated Social Media block at a desktop size.

### Social Icon

Content creator should not have to upload the social icon into the Drupal admin area. Social property for post to be shown should have associated icon. Instagram icon to appear with Instagram post teaser. Twitter icon to appear with Twitter post teaser etc.

### No icon

No icon will be shown if we don't have one to display. In that scenario, the caption text only will display. It will go full width of the social post image. This scenario can happen if a new social property is introduced.

### Handle

Handle is optional. If included, it should be to the TOP level of the social property page- not a duplicative, hand-coded visible link to the post.



![1729623436244](image/m_socialTeaserCaption-funcD/1729623436244.png)
