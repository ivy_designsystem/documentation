# The hero text group is designed for usage with the short and tall hero banners. The child hero text group has a variation for placement on a home page and on a lower level website page. The parent hero text group is for the tall hero, home page placement only.



![1729623932845](image/m_txtGrpHero-funcD/1729623932845.png)

# Design and data

The hero **child** type group has two variations:

* one for when appearing in the [short hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64aebed5194db88efe7fc1f1/tab/design?mode=preview) on a home page
* one for when appearing in the short hero in a lower level page.
* both variations have a mobile and desktop size

The hero **parent** type group is for the [tall hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c93f3bef92d31d3e5e0355/tab/design?mode=preview) and can only be placed on a home page.

**Short hero used on the home page**

When the type group appears in the hero on a home page of a website, it has the following text:

**H1 Website name**

Optional tagline

And this data:

H1 website name is the group name for the website: simple text

Optional tagline: simple text



![1729623940622](image/m_txtGrpHero-funcD/1729623940622.png)

### The short hero used on any lower level page has this type group:

Eyebrow: website name

**H1 page title**

**Data**

Eyebrow- is the website/group name. It is dynamically populated: simple text

H1 page title: also dynamically populated from the page title: simple text

The page title can have a mobile-friendly / shorter name that would appear on mobile only so text will fit in the smaller xxs breakpoint. Avoid using acronyms.



![1729623948080](image/m_txtGrpHero-funcD/1729623948080.png)

# Functionality bottom margin

Whether for placement on the home page or a lower level page, the text group as a fixed bottom margin of 112px for all desktop breakpoints and 80px for mobile breakpoints.

The text group can scale in height but to maintain the bottom margin, the text containers scale up from the bottom. It's important that the text containers for this component can increase in height to accommodate longer character counts, however, the margin-bottom has to be maintained. This is true for either the website name or the optional tag line.



![1729623954396](image/m_txtGrpHero-funcD/1729623954396.png)

See the [short hero component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64aebed5194db88efe7fc1f1/tab/functional%20narrative?mode=preview) for contextual relevancy.

# Responsive notes

For xs-small breakpoints, the text group spans 12 columns

For medium-xl breakpoints, the text group spans 7 columns

# Hyphenation, word breaks and line breaks

We do not want to control the line breaks in the text. Text should wrap as needed in its container, without hyphenation.

Please set hyphen control to "none."

hyphens: none

# Anatomy and specifications

The text group for a child home page has the name of the website on top as the H1 followed by an optional paragraph / tagline.

# Text Group Hero Home Page, mobile



![1729623961969](image/m_txtGrpHero-funcD/1729623961969.png)



![1729623970955](image/m_txtGrpHero-funcD/1729623970955.png)

The text group for heroes on lower level (LL) pages has the page title as the H1 and the website/group name as a p.eyebrow above. Both are mandatory.



![1729623989218](image/m_txtGrpHero-funcD/1729623989218.png)



![1729624005405](image/m_txtGrpHero-funcD/1729624005405.png)

## Text group for the parent home page level placed in the tall hero

* parent text group can only appear on a parent home page in the tall hero
* Same as the child text group, the parent text group has a 112px bottom margin
* The H1 is the name of the website / group name
* There is an optional .p tag
* Like all other hero text groups, the text scales up from the bottom so the 112 px margin -bottom is consistently respected.
