# The hero text group is designed for usage with the short and tall hero banners. The child hero text group has a variation for placement on a home page and on a lower level website page. The parent hero text group is for the tall hero, home page placement only.



![1729623925906](image/m_txtGrpHero-intro/1729623925906.png)
