# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.



![1729623845472](image/m_txtGrpHero-naming/1729623845472.png)

## Naming for the text group child hero

The text group child hero (txtGrpChildHero) comes in 2 sizes:

Mobile (for xxs, xs breakpoints)

Desktop (for s-xxl breakpoints)

The abbreviation for mobile is "m"

The abbreviation for desktop is "dt"

The functional narrative provides more detail to the variants of this text grouping, but in addition to the size variations for mobile and desktop, the text varies as to whether it appears at the home page level or on a lower level page.

"LL" is short for 'lower level'

"HP" is short for 'home page'

## Naming for the text group parent hero

The text group for the parent hero can only be placed on the larger, 'tall hero' with home page level placement.
