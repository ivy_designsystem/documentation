# This is the ubiquitous text group, the primary text group, the base of so many variations of the text group. It comes in white, darkGray and boxed on a dark background. It can have a CTA button or a link list.

# Usage guidlines

The **txtGrp **component is designed to deliver quick take-away messages and elevated content teasers that, along with the links and the CTA, is designed to drive users to deeper levels of the site or to call attention high-level content.

If you have a lot of text content to show, this is not the component to use. Go for other text-heavy and text-only components when you need options to layout highly consumable and nicely designed text-heavy pages

# Data

**Mandatory**

**Headline: **simple text

**Paragraph of text:** WYSIWY with a character limit.

**Character limits**

Note: character limits will be specific to each component and will be related to the container height limit. If a user has a lot of llinks in their link list, then they will probably need to have less body text.

**Optional **

**Links**

Single Button (max width of 256 with limited character count for the label)

Or

Text buttons with arrow: up to 8, simple text.

*Each link- whether button or text, needs a link URL.*

# Color options

## Component name:  txtGrp_darkGray

The darkGray text is designed to go over white or neutral background.

![1729623658307](image/m_txtGrp-funcD/1729623658307.png)

## Component name:  txtGrp_white

The white text version is designed to go over a dark green or dark background.

![1729623664195](image/m_txtGrp-funcD/1729623664195.png)

## Component name: txtGrp_boxPrimary1

The txtGrp_boxed has a boxed primary1 background with a primary3 left border. This component is designed as an overlay over an image.

![1729623670330](image/m_txtGrp-funcD/1729623670330.png)

# Link options

ALL txtGrp components and color variations can have a single button or a list of links.

* **Text link with arrow: **maximum of 8
* **Button:** single CTA

Contextual linking

The text paragraph within the txtGrp is WYWIWYG text.  Content editors will be able to embolden or italicize text as well as add a contextual link within the text if that is more appropriate than a button or a list of links

Button: The button has a maximum width of 256 so the button label needs to be short enough to fit.

**Developer Note: **

In Trail Mix, the prior design system, the button would automatically change to the "wordy icon button" when exceeding a certain length. We are not going to do this in IVY. The width of the button has a maximum of 256. The label needs to fit or the user can use the text button with the right arrow.

# Responsive notes

## Border-left on the txtGrp_box molecule

The **txtGrp_box **component has a 4px primar3 border for all breakpoints small-768 and above. The mobile xxs and xs breakpoint do not have the left border due to the stacked layout that happens at the xxs and xs breakpoints.

txtGrp_box-s

![1729623676783](image/m_txtGrp-funcD/1729623676783.png)

txtGrp_box-xxs

![1729623682275](image/m_txtGrp-funcD/1729623682275.png)

# Padding

The padding for the txtGrp varies by breakpoint. Here is a summary.

## Padding xxs  breakpoints

xxs does not have L/R padding. Only 24px top padding. The bottom padding is handled by the parent component the txtGrp is placed within.

![1729623688390](image/m_txtGrp-funcD/1729623688390.png)

## Padding xs breakpoints

The xs breakpoint has 24px padding top and padding-right. There is no padding-left.  The bottom padding is handled by the parent component the txtGrp is placed within.

![1729623698125](image/m_txtGrp-funcD/1729623698125.png)

## Padding small breakpoint

The small-768 breakpoint has 24px padding top, left and right. The bottom padding is handled by the parent component the txtGrp is place within.

## Padding medium- xxl breakpoints

The medium-1012 to xxl breakpoints have 40px padding top, left and right. The bottom padding is handled by the parent component the txtGrp is place within.



![1729623715676](image/m_txtGrp-funcD/1729623715676.png)
