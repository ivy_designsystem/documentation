# This is the ubiquitous text group, the primary text group, the base of so many variations of the text group. It comes in white, darkGray and boxed on a dark background. It can have a CTA button or a link list.



![1729623740620](image/m_txtGrp-intro/1729623740620.png)
