# Slate forms can be included in a web page layout. Forms from other sources can be included using an iFrame.

There are no Drupal forms in D10. They are all generated externally in other applications like Slate, MS Forms, or older Drupal forms.  The approach to embed a form differs depending on the form source.

### Non-Slate forms

1) Content administrators have privileges to add embedded forms using an iFrame. This would be for all forms not originating in Slate.

**Slate forms**

2) Slate forms can be added by someone on the digital team. The form gets added to a block that can be added to certain pages (like the Academic Program page).

See the documentation for “embedded iFrames”  if you want to embed a form into your content that is not a Slate form.

# Embedding and accessibility

UVM Drupal permits the use of “embed code” on web pages. When using embed code not supplied or recommended by the UVM Digital Team, it is the web editor’s responsibility to ensure that the code meets established information security procedures and the UVM accessibility policy. Embedded code often contains iFrames that most automated accessibility tools ignore, testing only the containing page. Therefore, web editors must take particular care to verify that any iFrame content is accessible by using both automated tools (outside of the containing page) and manual testing methods (within the page itself). More information on web accessibility is located in our Drupal Web Guide.

# Form styling

**IMPORTANT**

This documentation is not about the visual style or performance of the Slate form. It is just about the CONTAINER sizes that you can put a Slate form into.

# Usage Guidelines

### When to use

Slate is a comprehensive CRM platform that UVM uses to support admissions. Forms in SLATE can be embedded into layouts by someone with authorization to do so, such as someone on the web team.

# How to choose the best container width for embedded Slate forms?

Slate forms are fluid and scale to fill the container that it is placed in.

Choose a container width from the drop-down styles that is large enough so that your embedded form is readable and does not get cropped in the layout.

# Layout and spacing

Vertical spacing is built into the container for the embedded component.

desktop: top/bottom 64px margins

Mobile: top/bottom margin 48px margins

# Stacking

Embedded forms can be stacked. The top/bottom padding of the component will handle the vertical spacing needed.

**Warning!**

People generally do not like to fill out forms, so it is not recommended that you stack forms in your layout. It will overwhelm users and increase abandonment.

# Scrolling content

The container the embedded Slate form is placed in will scale vertically to the height needed as determined by the embedded content. Users can scroll or swipe vertically as needed using the native browser’s functionality to view content.

# Mobile

Expect Issues with embedded content on mobile. It is highly likely that you will have accessibility issues and mobile issues with embedded content including content going out of the screen view.

# Slate forms: embedded widths by breakpoint

There are 3 container width options for embedded forms:

Medium: the width of the WYSIWYG container

Large: spans 10 columns

Full-width: spans 12 columns.

Not all container widths are supported for every breakpoint. This table references which container widths are available by breakpoint.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOcNFwQvPatvMQiR4fdpQ41ECKFVw1BDX_DwyXLMsTrHMVrjCZYki0wsSiZ7LETjcPzzlg0YFJMZLZ2iNTejAwHkomfDZqIIqQQ1hngX_lmjSTued8AHOLchhj29_dS1vSQ==?assetKey=university-of-vermont%2Fivy%2FForm_SizeKey-SkI4B9t5T.png)

# WYSIWYG embedded forms by breakpoint

![1729627662274](image/wswg_formEmbed-funcD/1729627662274.png)

![1729627675917](image/wswg_formEmbed-funcD/1729627675917.png)

# Large size embedded forms by breakpoint

![1729627684209](image/wswg_formEmbed-funcD/1729627684209.png)

# Full-width embedded forms by breakpoint



![1729627694896](image/wswg_formEmbed-funcD/1729627694896.png)
