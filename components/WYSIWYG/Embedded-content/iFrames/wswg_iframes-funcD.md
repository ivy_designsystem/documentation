# Content and widgets from external sources (eg. Qualtrics and MS Forms, Twitter, Facebook, YouTube) can be included in a web page layout using an iFrame embed.

# Embedding and accessibility

UVM Drupal permits the use of “embed code” on web pages. When using embed code not supplied or recommended by the UVM Digital Team, it is the web editor’s responsibility to ensure that the code meets established [information security procedures](https://www.uvm.edu/sites/default/files/UVM-Policies/policies/infosecurityprocedures.pdf) and the [UVM accessibility policy](https://www.uvm.edu/sites/default/files/UVM-Policies/policies/accessibility.pdf). Embedded code often contains iFrames that most automated accessibility tools ignore, testing only the containing page. Therefore, web editors must take particular care to verify that any iFrame content is accessible by using both automated tools (outside of the containing page) and manual testing methods (within the page itself). More [information on web accessibility](https://www.uvm.edu/drupalwebguide/web_accessibility_drupal) is located in our Drupal Web Guide.

# Usage Guidelines

### When to use

Embedding 3rd party content on our site in an iFrame is a last resort fall back approach when you absolutely need to get specific content displayed on your website and there is no component or display type offered to do so natively within our product offerings.

That said, we aim to meet the content display needs of all our website editors and content administrators. We like to keep an inventory of ‘wish list’ items that our content administrators have. If there is enough need for something to be developed, then we

### When not to use

There already are components to display:

* Curated social media
* Videos from 3rd party hosting services like You Tube and Vimeo.

 It is recommended that you use the UVM branded components to display this content rather than the embed.

# Layout and spacing

How to choose the best container width for embedded content?

iFrame content usually has a fixed width which is determined on export when the embed content was created in the 3rd party app. iFrame content is not fluid and will not scale to fill the container that it is placed in.

Choose a container width from the drop down styles that is largr enough so that your embedded content does not get cropped in the layout.

## Vertical spacing

Vertical spacing is built into the embedded component.

desktop: top/bottom 64px

Mobile: top/bottom margin 48px

## Stacking

Embedded components can be stacked. The top/bottom padding of the component will handle the vertical spacing needed.

## Scrolling content

The iFrame embed container scales vertically to the height needed as determined by the embedded content. Users can scroll or swipe vertically as needed using the native browser’s functionality to view content.

## Mobile

Issues with embedded content on mobile. No handling to view things that don’t fit.

IFrame content, as stated earlier, is a last resort if you absolutely need to place certain content on the site that is not supported natively by either Drupal WYSIWYG or the components we offer.

It is highly likely that you will have accessibility issues and mobile issues with embedded content including content going out of the screen view.

# iFrames: embedded widths by breakpoint

There are 3 container width options for content containers for embedded iFrames:

Medium: the width of the WYSIWYG container

Large: spans 10 columns

Full-width: spans 12 columns.

Not all container widths are supported for every breakpoint. This table references which container widths are available by breakpoint.



![1729627523663](image/wswg_iframes-funcD/1729627523663.png)

# WYSIWYG embedded forms by breakpoint

![1729627533865](image/wswg_iframes-funcD/1729627533865.png)

# Large size embedded forms by breakpoint

![1729627543039](image/wswg_iframes-funcD/1729627543039.png)

# Full-width embedded forms by breakpoint



![1729627558020](image/wswg_iframes-funcD/1729627558020.png)
