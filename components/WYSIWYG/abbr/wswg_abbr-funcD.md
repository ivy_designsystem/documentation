# The `<abbr>` tag in HTML is used to define an abbreviation or acronym for a longer word or phrase. It is typically used to provide a brief explanation or expansion of the abbreviated term.

# Syntax

**Here is the basic syntax for the `<abbr>` tag:**

`<abbr title="full-term">`abbreviation`</abbr>`

The 'title' attribute is required and should be used to provide the full-term or expanded form of the abbreviation. This attribute is used to display a tooltip or additional information when the user hovers over or interacts with the abbreviation.

**Here is an example of how the `<abbr>` tag can be used:**

The University of Vermont’s `<abbr title="College of Arts and Sciences">`CAS`</abbr>` has 4,500 students.

# Display

The abbreviated text will display how a user types it. It will display with a 1px dotted underline to indicate that it is an element that the user can interact with. UVM's link style for body content contextual links is 1px solid lightGray. Links for abbreviations are 1px dotted lightGray. The rationale is that the abbreviation links are different as they display a tooltip with the definition of the acronym -- the full spelling. This would happen on hover on desktop and on-tap on mobile. There is no "on-click" event on desktop for an abbreviation tooltip.



![1729625838271](image/wswg_abbr-funcD/1729625838271.png)

**Underline styling**

The underline for the `<abbr>` text should be   

1px lightGray dotted

(I don’t think we can do .5px so have specified 1px)

Note: The underline color will be over-ridden by some browsers and default to the darkGray text color. That is understood and OK.

![1729625846330](image/wswg_abbr-funcD/1729625846330.png)

Note: We will adjust the vertical offset of the underline during QA. It needs to be tested on acronyms with descenders. Start with the default: border-bottom and we will see.

**Tooltip styling**

Use the default browser tooltip styling. In most cases, it is what is shown in the example above for "College of Arts and Sciences".

# Casing

It's important to note that the abbreviated text is not necessarily all uppercase, so do not include text-transform: uppercase; in the styling for the `<abbr>` tag.

The casing should be dependent upon the user to enter the correct casing of the acronym.   The abbreviated acronym adopts the styling of the paragraph it is in.

![1729625856145](image/wswg_abbr-funcD/1729625856145.png)

**Examples of mixed case acronyms include:**

MoMa

CATCard

It's important to note that the `<abbr>` tag is primarily used for semantic purposes and accessibility. It helps provide additional context and understanding for users, particularly those who may not be familiar with certain abbreviations or acronyms.
