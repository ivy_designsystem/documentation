# The `<abbr>` tag in HTML is used to define an abbreviation or acronym for a longer word or phrase. It is typically used to provide a brief explanation or expansion of the abbreviated term.

![1729625871937](image/wswg_abbr-intro/1729625871937.png)
