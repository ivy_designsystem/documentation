# The `<addr>` tag in HTML is used to define address or contact information.

Here is a code snipped using the `<address>` tag.

# Visual display for the `<address>` tag

The text within the `<address>` tag displays in the default body style + italic.

## Examples

### The text for the above code would display in the browser like this. Note that the `<br>` tag is used to create line breaks between different lines of the address.

![1730389013641](image/addr_funcD/1730389013641.png)

**The content editor could add a +bold style to the name and the phone number, if so desired. It would look like this: **



![1730389022446](image/addr_funcD/1730389022446.png)

# Accessibility

It is a non-semantic tag, meaning it does not convey any specific meaning to search engines or assistive technologies. Its purpose is mainly to style the address or contact information in a specific way using CSS.
