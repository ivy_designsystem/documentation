# The `<addr>` tag in HTML is used to define address or contact information.

![1730389034558](image/addr_intro/1730389034558.png)
