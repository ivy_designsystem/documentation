# The blockquote is a short sentence or paragraph of text that is emphasized.

Blockquotes are typically implemented using HTML `<blockquote>` elements. These elements should encapsulate the quoted content, whether it's a single paragraph or multiple paragraphs.

See also pullquotes.

# Design

This is the visual design of the blockquote at the small-768 desktop size ( *Note: screen grab is scaled by InVision to fit on this page so not the actual size.* )



![1729626525625](image/m_blockquote-funcD/1729626525625.png)

The markup for the above looks like this:

<blockquote>

<p>blockquote content</p>

<p>Attribute</p>

</blockquote>

Note that the text in the blockquote is uniformly styled.

**Semantic structure and recommendations for content editors**

The `<blockquote>` tag when selected in the WYSIWYG can wrap all sorts of content types. Users could, for example, select text and an image in their WYSIWYG content editor and wrap that in a `<blcokquote>`. The text and image would display within the blockquote with a neutral background and the primary3 border-left.

The recommendation is that the blockquote is used to provide an elevated emphasis to text. If an attribute exists for the text that is elevated, content editors could apply the `<cite>` tag for the attribute The `<cite>` for the attribute would help with assistive technologies correctly identify and present the information while still keeping the display of the blockquote text and attribute within the blockquote layout.

# Anatomy

Mobile

![1729626534400](image/m_blockquote-funcD/1729626534400.png)

Desktop



![1729626544022](image/m_blockquote-funcD/1729626544022.png)

# Visual display

The padding, margin and text sizes vary between desktop and mobile.

The blockquote style varies slightly between mobile (xxs-xs) and desktop (s-xxl). Here are some styles of note:

**Common styles**

Background color: #F7F7F7

Type face: Playfair display

Font weight: 700 bold

border-left: 8px primary3

## Desktop blockquote, layout and spacing

Padding desktop: 64.40.64.40

Margin desktop: 48px L/R

Internal spacing: 32px

**Desktop type style:**

color: var(--grayscale-light-gray, #5D646B);

/* support/callout-dt */

font-size: 22px;

font-family: Playfair Display;

font-weight: 700;

line-height: 38px;

letter-spacing: -0.1px;



![1729626553994](image/m_blockquote-funcD/1729626553994.png)

## Mobile blockquote, layout and spacing

Padding mobile: 40.32.40.32

Margin mobile: 0px L/R

Internal mobile: 32px

**mobile type style:**

color: var(--grayscale-light-gray, #5D646B);

/* support/callout-mobile */

font-size: 19px;

font-family: Playfair Display;

font-weight: 700;

line-height: 30px;

letter-spacing: -0.1px;



![1729626561765](image/m_blockquote-funcD/1729626561765.png)

# Accessibility Considerations

When implementing blockquotes, it's essential to ensure accessibility for all users. Consider the following:

Semantic Structure: Use the `<blockquote>` element for the main quoted content and `<cite>` for the citation. This helps assistive technologies correctly identify and present the information.

Keyboard Navigation: Ensure users can navigate to and interact with blockquotes using keyboard-only controls.

ARIA Roles and Attributes: If necessary, use ARIA roles and attributes to provide additional accessibility information or improve screen reader behavior.

# Do's and Don'ts

**Images in the blockquote**

We don't recommend including an image in a blockquote. However, a square image could be used if related to the attribute as it would add value to the user.



![1729626570117](image/m_blockquote-funcD/1729626570117.png)

* Don't include images that are not related to the attribute.
* Image should not be vertical.



![1729626578186](image/m_blockquote-funcD/1729626578186.png)

* Don't embed video in the blockquote.



![1729626605138](image/m_blockquote-funcD/1729626605138.png)

## Developer notes

The container maintains its designated column-span by breakpoint regardless of the text content. The blockquote should not hug the text even for short text scenarios. Here is an example.



![1729626611283](image/m_blockquote-funcD/1729626611283.png)
