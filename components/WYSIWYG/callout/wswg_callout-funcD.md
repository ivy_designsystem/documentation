# Callouts are used to highlight text information by putting it in a shaded box. They come in 3 different type sizes to provide different levels of emphasis.

![1729626850022](image/wswg_callout-funcD/1729626850022.png)

# Description

The "Callout"  is a  container - content in a shaded box - used to draw attention to specific content. Callout boxes are designed to stand out from the surrounding content, making key information easily noticeable. They serve the purpose of highlighting important points, providing additional context, or guiding users' attention to particular details.

Key characteristics of the callout box include:

**Visual Distinction**

Callout boxes have a distinctive visual style that sets them apart from regular content. Out callout box has a unique background color, and a border radius.

**Encapsulation of Information**

Information within the callout box is encapsulated, creating a clear boundary between the highlighted content and the rest of the page or interface.

**Placement**

The Callout box can be strategically placed near relevant content, such as next to a form field, a paragraph of text, or an image that requires additional explanation.

**Emphasis and scale**

The Callout is a Drupal paragraph  element and is styled in the WYSIWYG editor by adding a style to a text paragraph `<p>`. Three callout styles are available:

* Medium
* Large
* xLarge

The names refer to the size of the text in the callout.

Additional WYSIWYG styling can be applied to the text, for example +italic or +bold. However, since the callout style is applied to a single paragraph in the Drupal WYSIWYG editor, multiple styles cannot be applied. For example, even though you can embolden text to make it look like a heading within the Callout, you can't have an actual Heading style within a paragraph.

**Inline Buttons within the Callout**

Inline buttons can be assigned within the callout.

# Data

The "Callout" style will be applied to the `<p>` element. It can have contextual inline links, an inline button, or be assigned bold or italic.

There is no "baked in" or required content. Consequently, the content editor can add whatever content they want in the Callout that is offered within the WYSIWYG editor.

# Variants

The Callout is a fluid component, filling the WYSIWYG content container as the page scales by the user's viewing scenario. Any padding on the parent WYSIWYG container applies to the Callout.

There are 3 size options: Medium, Large, and xLarge with each size having a desktop and a mobile variant.

The following table is a reference for the type sizes that are used for each Callout Size variant for both desktop and mobile.

![1729626859640](image/wswg_callout-funcD/1729626859640.png)

# Size differences between desktop and mobile variants

The 3 sizes have mobile and desktop variants within their scale. Here are the differences by type scale and platform.

![alt text](04.png)


![alt text](05.png)


![alt text](06.png)


# Design and layout

**Color:**

A new color is introduced with this component. It is strictly a background fill color. It's only usage at present is in this Callout.

fills / background fills / neutralWarm #ECEBE2

Corner radius: 16 for both mobile and desktop

# **Responsive layout and spacing differences**

# Usage

## When to use

**Examples of when callout boxes might be used include:**

**Informational Messages:** Providing additional details or explanations about a feature.

Tips and Guidelines: We don't have a specific callout for Tips. This Callout can be used to communicate tips and guidelines.

**Important Notices:** Highlighting critical information within the context of a page and specific content that users need to be aware of.

## When not to use

Don't overuse this component. Callouts should be used sparingly to maintain effectiveness. It is intended to elevate very specific content within the context of a section or a page.

• Do not have more than one Callout boxes per section or topic.

• Do not put low level or irrelevant content in a Callout. Callouts are to elevate important content. The concern with this is a "cry wolf" sort of scenario. If the Callout content is not important, users may disregard other Callouts on the site, deeming them unimportant.

# Accessibility

Callout is a WYSIWYG element with regular text inside. It doesn’t currently have any added functionality for assistive technologies.

# Responsive Notes

The Callout can only be placed within WYSIWYG containers. It is fluid and fills the WYSIWYG container, respecting any padding of the parent container.

For desktop breakpoints M-1012 - xxl,  the WYSIWYG text container has a 40px left padding. The Callout will respect that padding and align with the other WYSIWYG content.

![alt text](07.png)

For the small-768 breakpoint, like all WYSIWYG content, there is no left padding in the WYSIWYG container.

![alt text](08.png)


The WYSIWYG container for xxs-320 to xs-480 spans 12 columns on mobile, as does the Callout.

![alt text](09.png)


## Accessibility

Callouts are essentially a WYSIWYG container with styling inside the WYSIWYG content container. As such,  a heading in the Callout would help users who navigate the site via a keyboard and text reader. Content editors need to be aware of the semantic hierarchy of heads on their page and make sure that the heading assigned within the callout follow the correct semantic order.
![alt text](10.png)