# Callouts are used to highlight text information by putting it in a shaded box. They come in 3 different type sizes to provide different levels of emphasis.



![1729626839986](image/wswg_callout-intro/1729626839986.png)
