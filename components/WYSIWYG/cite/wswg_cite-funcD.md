# The `<cite>` tag is used to highlight the title of work being referenced within text content. Examples include research or creative work, such as a book, movie, song, or other piece of content.

## Attributes:

The `<cite>` tag does not have any specific attributes.

## Markup example

<p>

The Graduate College is pleased to present  `<cite>`"Lucille: A Life Story,"`</cite>` a lecture by Emily Bernard, Ph.D., to be held Monday, April 17, 2023, at 4:30 PM in Waterman's Memorial Lounge (338).

</p>

In the example above, the title "Lucille: A Life Story" is marked as a citation using the `<cite>` tag.

## Styling of the `<cite>` tag

The `<cite>` tag italicizes the text while keeping all other text styling.

## Note:

* The `<cite>` tag is a semantic tag and is used to provide meaning and structure to the content.
* While the `<cite>` tag is often used to represent the title of a work, it can also be used to indicate the name of a person or organization being cited.
* The appearance of the `<cite>` tag is typically italicized by default in most web browsers, although this can be modified using CSS.
* The `<cite>` tag should not be used to create a citation or a bibliographic reference. It is used primarily for indicating the title or name being referenced.
