# The `<cite>` tag is used to highlight the title of work being referenced within text content. Examples include research or creative work, such as a book, movie, song, or other piece of content.
