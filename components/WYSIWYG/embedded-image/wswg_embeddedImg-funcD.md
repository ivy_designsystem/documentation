# Images can be embedded in HTML content using the Drupal CK WYSIWYG editor and have some layout and alignment options.

![1729628266267](image/wswg_embeddedImg-funcD/1729628266267.png)

## Description

Images can be embedded in your HTML content using the Drupal CK WYSIWYG editor. Styling for embedded images include options for size, fluid or fixed width, and alignment.

Embedded images can be styled to be either fluid or fixed widths. When placed into a Drupal HTML WYSIWYG container (paragraph in Drupal terms), as long as the container is not nested within a larger component like an accordion, infoBand or vertical tab, the image can be styled to fit within or extend outside the width of the container.

## Data

**Mandatory**

Image: Any aspect ratio

**Optional**

Caption: simple text. Native Drupal 10 functionality

**Preferred aspect ratios**

* Landscape: 16:9, 3x2

**Less preferred aspect ratios**

* Portrait orientation is used less frequently and not preferred. Aspect ratios for portrait include 1:1, 2:3, 9:16

**Very small images and illustrated icons**

There are 3 fixed width styles to handle things like thumbnail images, logos and illustrated icons. These do not scale responsively and are fixed size for all breakpoints.

* Fixed width medium: 160px wide
* Fixed width small: 64px wie
* Fixed width xSmall: 48px wide (not releasing now)

## Layout and spacing

**Layout and image sizing**

Embedded images have style option to control image size and alignment. There are no cropping features. Content admins must crop and optimize their images to the desired size and appearance prior to uploading to Drupal.

**Aspect ratios**

Users can upload images of any aspect ratio and size. We know they will! All sizing options are width predominant. Images maintain their aspect ratio in the layout and scale proportionally depending on the sizing options selected by the content admin. Fixed-width images do not scale responsively. Fluid width images proportionally scale in size as the viewport size changes.

**Vertical Spacing**

The embedded image has margin on the top and bottom to give some air in the layout with surrounding content. If the image has a caption, the caption is directly under the image on the lower left and the margin-bottom is outside of the caption.

Desktop: 32px margin top and bottom

Mobile: 24px margin top and bottom

**Alignments**

Left and center alignments are available. Drupal didn't seem to have a right alignment option without text wrap so we are disregarding right alignment for now.

**Text wrapping**

We are not offering text wrapping around images. Text wrapping can be very difficult for responsive layouts. It can work for wider widths but almost never for mobile. You end up with a few characters lingering disconnectedly to the side of the image. It's a bother and often ends up looking broken and unreadable. It's something that is better used in print mediums where you have control over line breaks.

## Image Style / Size

Image styles are selected from a drop-down in Drupal. They appear in one list but some are fluid-width and some are fixed-width.

![1729628274436](image/wswg_embeddedImg-funcD/1729628274436.png)

## Fluid image size examples

![1729628279941](image/wswg_embeddedImg-funcD/1729628279941.png)

### Full-width ^

The above image is the full width size. It is a fluid size and spans 12 columns.

![1729628287084](image/wswg_embeddedImg-funcD/1729628287084.png)

### Full width and Large sizes ^

The above image is shows both the full width and large sizes. They are fluid and scale proportionally to the grid as the browser size changes.

Full width spans 12 columns

Large spans 10 columns.

Large is only available on desktop.

![1729628293959](image/wswg_embeddedImg-funcD/1729628293959.png)

### Medium size ^

The medium size image scales proportionally to the width of the WYSIWYG container that it is placed in. It does not extend beyond the container. If the container has padding, it is also applied to the image.

![1729628328406](image/wswg_embeddedImg-funcD/1729628328406.png)

### Small size ^

The small size is 50% of the WYSIWYG container that it is place within. This size option applies to all breakpoints. It is a fluid size and scales with the viewport and grid while always maintaining the aspect ratio of the image.

# Fixed image size examples

![1729628336081](image/wswg_embeddedImg-funcD/1729628336081.png)

*Note: The above comp shows the images side by side but don't think that this can be achieved without a flex box container. *

## Caption

Any embedded image in Drupal can have a caption. The caption is optional. It can be used to provide a photo credit for the photographer or as a caption for the image. We will use Drupal's default functionality for the image caption- which places it below the image on the left. It is simple text and cannot take any additional styling or links.

## Alignment options

Images can be left or center aligned. Take a look at the table above. Alignment will not be visible in scenarios where the image scales to fill the container.

## Behavior / loading style

Fade. Images fade-on with page load.

## Images placed in components with WYSIWYG text area

Some components like the [vertical tab](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64d23fdd1579dd213d638485/tab/design?mode=preview), [infoBand](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64d23fffe1135b4430da59df/tab/design?mode=preview) and [accordion](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview) have WYSIWYG text areas where images can be embedded. Images in these scenarios never extend beyond the container that they are placed in, even if set to Large or Full-width. Their maximum size is the parent container.

Images can be sized smaller by selecting the small, Fixed medium, Fixed small or Fixed xSmall size options.

## Responsive versus fixed

Embedded images can be styled to be either fluid or fixed width.

Fluid width images generally scale proportionally to the width of the container that they are placed in. The width can be a percentage of the container. The container may have padding. A fluid image will resize with the grid between different breakpoints as the container and viewport sizes change.

A fixed-width image does not responsively scale as viewports and screen sizes change. The width is set to a specific value and does not change as the browser resizes. As the name implies, a fixed-width image has a fixed width.

## How to choose between fluid and fixed

### Fluid-width images

When an image is embedded, it will default to it's native size with a fluid width. If the image is wider than the container it is placed in, it will proportionally scale down to fill the container, accounting for any padding that it will inherit from the container.

Fluid-width images scale by breakpoint. They work well in layouts where the image punctuates text content and has room to scale.

### Use fixed-width when:

* For smaller images such as illustrated icons or lower quality thumbnail images so they don't scale up to huge and ridiculous proportions as the grid scales
* To maintain white space in text-heavy layouts

### Fluid images within infoBands / Vertical tabs / and accordion components

Fluid width images when placed in infoBands / Vertical tabs and accordion components scale proportionally to fill the WYSIWYG content container. They respect the internal padding of the container, as applicable. This is really important in the accordion component where the edge of the fluid-width image needs to indent to align with the accordion text content so as not to create hierarchical confusion within the accordion.

## Image Quality

* All images should be of high-quality and sharp. Crop your image to recommended sizes and optimize using tinypng.com to strike a balance between file size and image quality.
* Avoid small files sizes of poor quality that will be blurry or pixelated.
* Avoid unnecessarily large files as they will slow the site down and harm the user experience of the website.

## Image filenames

The image filename should follow proper filenaming conventions.

* Use _ underscores or - dashes in a filename instead of spaces.
* Make sure you put the aspect ratio and perhaps also the image width and height in the filename. You will be happy you did this as it will make image finding and selecting in Drupal easier and you will be less likely to put the wrong size image in the layout.
* Keep the name short but descriptive and recognizable for what it is

Here is an example of a good filename for the image shown below.

![1729628359506](image/wswg_embeddedImg-funcD/1729628359506.png)

![1729628350587](image/wswg_embeddedImg-funcD/1729628350587.png)
