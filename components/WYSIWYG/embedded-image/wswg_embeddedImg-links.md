## Figma handoff

https://www.figma.com/file/03eXjPmVFRvFHS1GhkH1RJ/Ivy--WYSIWYG?type=design&node-id=94-35826&mode=dev

Handoff and spec notes for the embedded image

## Master component with variants

https://www.figma.com/file/03eXjPmVFRvFHS1GhkH1RJ/Ivy--WYSIWYG?type=design&node-id=56-8911&mode=dev

Suggest working with the handoff file. This is the master and the handoff has instances.
