## The `<HR>` divider component is used to visually divide and vertically separate blocks of content on a page.  The divider is horizontal and is useful to mark transitions and topic changes in dense text content.



![1729628640271](image/wswg_hrDivider-funcD/1729628640271.png)

Dividers improve the readability of text-heavy content by providing a visual pause, marking transitions and topic changes. HTML offers an `<hr>` tag out-of-the-box for this purpose. But we have stylized the `<hr>` divider  to better suit the University's brand.

## CSS

stroke-width: 3px;

stroke: var(--grayscale-darkGray, #26292C);

display: flex;

padding: 32px 0px;

flex-direction: column;

align-items: flex-start;

gap: 10px;

align-self: stretch;

color: darkGray #26292C

## When to use

**Separate topic changes in related content**

Placing dividers within text content where there are topic changes.

**Breaking up dense content**

When used appropriately, dividers can improve the readability of text-heavy layouts by visually breaking the text up into more easily consummable sections.

## **When not to use**

Dividers can be useful to separate content into groups or sections, but use them sparingly with intention as excessive use can create a negative user experience.

## Variants

**There are 2 variants of the `<HR>` divider: **

**Default `<HR>`:** The default `<hr>` has been stylized as a dotted rule.



![1729628650643](image/wswg_hrDivider-funcD/1729628650643.png)

**Decorative:**  We are also offering decorative divider which has significant white space and a miniature "campus scape" illustration. The decorative 'campus scape' divider should be used sparingly with lots of text content between each usage.



![1729628657587](image/wswg_hrDivider-funcD/1729628657587.png)

## Layout

Dividers are fluid and fill the width of the WYSIWYG container.

We are not currently offering a full-width `<HR>` divider that would span all 12 columns or a full display divider.

Follows is an example of the **default `<HR>` divider in a layout.**

![1729628667752](image/wswg_hrDivider-funcD/1729628667752.png)

And following here is an example of **the decorative divider in a layout.**



![1729628674193](image/wswg_hrDivider-funcD/1729628674193.png)

# Functionality for the Drupal content editor

Content editors in Drupal select either `<HR>` hRule from the WYSIWYG drop down. The default (dotted) hRule is then inserted into the WYSIWYG container.

When we roll out the decorative one, the admin can pick the "decorative" version from a secondary style menu.

**IMPORTANT: **

We are going to rollout the default `<HR>` element now.

The decorative `<HR>` element's assets are not ready. The decorative `<HR>` will be a Phase 2 release. Please plan for this addition when coding.

## Notes about the decorative divider

**It should be used intentionally and sparingly. **

The decorative 'campus scape' divider is a collection of dividers consisting of a miniature illustration of an iconic campus scene or people in silhouette. The intention is that the Drupal content editor can select either the default (dotted) hRule or the campus scape style from a drop down.

The campus scape style has a randomizer and randomly pulls from the array of pre-designed campus scape dividers. If there are enough of these, then there will be a random appearance where used on the website.

The campus scape hRule divider will change on page refresh, pulling another from the array and displaying it.

**Here are some of the decorative design comps to provide a better sense of the concept. **Dividers are used to visually divide sections of content and create some vertical space so the user can pause. The divider communicates a contextual transition in the content. When used appropriately, dividers can increase the readability of text-heavy pages and improve the consumption rate of content.



![1729628682808](image/wswg_hrDivider-funcD/1729628682808.png)

The decorative dividers are miniature and familiar campus scenes. Shown in silhouette, diversity is less of an issue and easier to be represented in silhouette form. The decorative divider is randomized so each placement and refresh of the screen is going to show a different divider. All of the decorative dividers here are FPO- for position only- and are proof of design concept. We are in the process of creating a list of iconic campus scenes and buildings to use in these decorative dividers and then create the silhouette vector art assets. The decorative dividers shown here are just representative examples. This usage is too tight and the paragraphs are too short for adequate usage of a divider.



![1729628689319](image/wswg_hrDivider-funcD/1729628689319.png)

Iconic campus buildings may be an image source for the silhouettes. The miniature, recognizeable landscapes will give some context and a sense of wonder and place.



![1729628696567](image/wswg_hrDivider-funcD/1729628696567.png)

## Accessibility

**Contrast:** Contrast is not an issue with a decorative element. Even so, the dividers as designed well exceed WCAG 2.1 contrast AAA standards.

**Visual clarity:** The top and bottom margins on the dividers ensure adequate negative space for readability and an accessible and clear user experience.

**Screen readers:** Dividers should not be included in screen readers as they are static structural elements.
