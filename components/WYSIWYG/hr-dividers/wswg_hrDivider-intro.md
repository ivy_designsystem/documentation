## The `<HR>` divider component is used to visually divide and vertically separate blocks of content on a page. The divider is horizontal and is useful to mark transitions and topic changes in dense text content.

Default

![1729628720540](image/wswg_hrDivider-intro/1729628720540.png)

Decorative example

![1729628725401](image/wswg_hrDivider-intro/1729628725401.png)
