# The `<pre>` tag in HTML is used to define preformatted text. It preserves both spaces and line breaks within the content, displaying it exactly as it appears in the HTML code.

# ¯\\ _(ツ)_/¯
