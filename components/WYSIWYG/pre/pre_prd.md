# The `<pre>` tag in HTML is used to define preformatted text. It preserves both spaces and line breaks within the content, displaying it exactly as it appears in the HTML code.

Usage

In textbooks and manuals, the `<pre>` tag is often used to display a code snippet. Here is an example:

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOeA846UO1HcvwA8BsOyURLrcowOXdTyaM71XHwvUxTcTdnYlRjzgsPUtGztFnRhBFganZ2DQOK3Ion8A6XBl7vR2hrKfYFezg0-7oVZo9Dy1j-2Ed4-dB35yExVeiUi2GQ==?assetKey=university-of-vermont%2Fivy%2Fimg-1687529247488-0-SkuXP7mO2.png)

![1735407890818](image/pre_prd/1735407890818.png)


In the above example, the `<code>` tag is nested within a `<pre>` tag to provide additional semantic meaning. However, nesting is not required, and you can use the `<pre>` tag alone as well.

The `<pre>` tag can also be used to control the line breaks and visual display of type. This example shows a forced setting of flush right type around a tennis ball.

![1729708385853](image/pre_prd/1729708385853.png)

The `<pre>` tag is often used in ASCii art, or any text that requires a mono-spaced font and preserved manual formatting. The 'shrug' emoji is an example of ASCii art.

# ¯\_(ツ)_/¯

# Font

Please use mono-space font  for `<pre>` text.   I think courier-new  is the only web-safe monospace font but UVM design specifications call for Roboto mono. This font will be used within data tables for the digits in numerical data to align.
