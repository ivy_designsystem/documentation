# The `<q>` tag in HTML markup is used to enclose a short quotation.



![1729708558306](image/q_prd/1729708558306.png)

**Here is an example of the HTML markup for the `<q>` tag.**



![1729708564652](image/q_prd/1729708564652.png)

The above text would look something like this when viewed in the browser:

## The q element

WWF's goal is to:

“Build a future where people live in harmony with nature”

We hope they succeed.

# Display

The `<q>` tag can be styled with CSS. UVM's style is to italicize the text within the `<q>` tag and add open and close quotes around the text. The  open and close quotation marks are styled as  "curly"in the CSS rather than "straight".

Displaying the quotes as "curly" rather than straight is best practice for typography.  Here are the HTML entities for the open and close quotes, along with the keyboard combinations for Windows and Mac if you are typing them.

Important! The content editor in Drupal will not have to type the quotes themselves. The `<q>` tag will add the curly-styled quotes automatically when the `<q>` tag is applied.

## Cody the quotes to be curly

### “	opening double quote

**Windows:** alt 0147

**Mac:** option + [

 **HTML entity: ** &ldquo;

### ”	closing double quote

**Windows: **alt 0148

**Mac:** option + shift + [

**HTML entity:** &rdquo;



![1729708572836](image/q_prd/1729708572836.png)

# User guideline: don't type in the quote marks yourself



![1729708581436](image/q_prd/1729708581436.png)

# Using the `<q>` tag with the `<cite>` tag

Please also refer to our [documentation about apostrophe&#39;s and quotes.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/641335cff70b3b4c426eac2b?mode=preview)

# The `<q>` tag used with the `<cite>` tag

You can also use the cite attribute with the `<q>` tag to specify the source of the quotation. Here's an example of this markup:

`<q cite="https://example.com/quote-source">`This is a short quotation.`</q>`

In this example, the cite attribute contains the URL or reference to the source of the quotation.
