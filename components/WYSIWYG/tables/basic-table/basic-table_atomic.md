# Atoms

**Hand-swipe icon**

ui/base/[icn_handSwipe-24](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

/* scroll to the bottom to see this icon and download the .svg */

![1729707950712](image/basic-table_atomic/1729707950712.png)
