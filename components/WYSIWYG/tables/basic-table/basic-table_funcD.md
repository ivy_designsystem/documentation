# Tables provide order to display data efficiently in an easy to understand grid. This component is a *basic* table whereas each cell corresponds to only one column header and one row header. It is designed to handle both text and numeric data though is not designed to handle complex data projections with nested data.

# Introduction

The basic table is an all-purpose table. It is designed to look good out of the box, be easy to work with a familiar interface, and be accessible across all breakpoint, on a large screen or small screen or using a screen reader like Jaws for accessing the content audibly or with a keyboard.

# Design notes

Here is a sample table with text and numeric data.



![1729707741339](image/basic-table_funcD/1729707741339.png)

## Over-arching design guidelines for the tables

**Table properties**

* Table column widths will follow the Drupal pattern: hug content + cell padding
* We will offer an override in Table Properties for having equal width columns
* The overall table has a 3px darkGray border-top and a 1px darkGray border bottom
* Table has no background fill.

**Cell height**

* Table cell and row heights are 43px by default with the cell scaling vertically as content demands

**Rules**

* All tables have a 3px margin-top and 1px margin-bottom, darkGray.
* Cells have a 20% black, 1px rule between them (margin-top)

**Zebra striping**

* Zebra striping is optional
* The background fill for zebra striping is 2% black.

**Cell content**

* All cell content is lightGray
* Text content is flush left
* Numeric content is flush right
* Numeric data has a monospace, flush right font styling so number digits align
* Totals are bold, lightGray
* Cell content is top-aligned  (scales down)
* Mixed text and number content will be set in text. Example: July 12 to August 18, 2023



![1729707750147](image/basic-table_funcD/1729707750147.png)

^ Text cell content is flush left



![1729707757302](image/basic-table_funcD/1729707757302.png)

^ Numeric cell content is flush right

**Table headers**

* Headers `<TH>` are bold, darkGray
* By default, table headers are flush left
* Table headers for numeric data have a flush right style which content editors will need to apply
* The Table Header row has a 1px darkGray bottom rule



![1729707764352](image/basic-table_funcD/1729707764352.png)

^ Headers for data columns are flush right. The content editor will need to manually apply the "flush right" style.



![1729707771031](image/basic-table_funcD/1729707771031.png)

^ By default, table headers are flush left.

## Table text styles

For optimal reading and performance, table content has its own text styles. The text size is 15/19 for both text and numbers. Numbers have their own style using a mono-space font and flush right so the digits can all align vertically.

Cell content can be linked. Here is the description for the linked states:

Link: Linked content is underlined with a 1px solid lightGray rule.

Hover: Border bottom turns to primary3. Text color goes to black #000

Focus: Primary3 box (or did we do primary3-faded?)

Disabled: -

Visited: -

Susy worked those out also - tk.



![1729707779172](image/basic-table_funcD/1729707779172.png)

Footnotes: Footnotes within cell content to be set using `<sup>` superscript. Schools/colleges/units/departments may have their own guidelines and specific needs to footnotes and hierarchy of footnotes, which the table will need to meet. Dev team to discuss how this happens in the CK editor.

Footnote descriptions should go below the table, one line per footnote. Use text style p.xsmall.

DEV question: How will content admins assign the footnote styles in CK Editor?

## Accessibility

Our approach to making tables accessible on mobile where content is not viewable without horizontal scroll has these progressive enhancements:

1. Table to responsively condense (OOTB tables are condensed because cell hugs content)
2. Horizontal scroll appears when table is wider than the screen width
3. Affordance for browsers that don't support display of horizontal scroll controls: animated icon of finger swiping. (delivering static SVG initially)
4. Collapsed table with left column anchored (Possible phase 2)

**Tables should be ****accessible:**

* For all breakpoints
* Using a mouse, keyboard, or touch
* Navigable audibly with a screen reader

# Table display variations

1 /   Zebra off



![1729707787513](image/basic-table_funcD/1729707787513.png)

without zebra stripes ^



![1729707793683](image/basic-table_funcD/1729707793683.png)

2 /    Zebra stripes on^

3 /  Table with variable cell width that hugs cell content (Default display)



![1729707799923](image/basic-table_funcD/1729707799923.png)

^ Default display, table cells hug content with added padding. (note: Could not create this view properly in Figma. The headers should align with the columns.)

4 /  Table with equal cell width



![1729707810196](image/basic-table_funcD/1729707810196.png)

^ Tables can also be displayed with equal cells. These are generally easier to read than the default as they are wider and less compressed.

# Table Anatomy

Here is a sample table with text and numeric data.



![1729707817414](image/basic-table_funcD/1729707817414.png)

![1729707832572](image/basic-table_funcD/1729707832572.png)

**All table cells are 43px high by default. The height of table cells scale as needed determined by the content. **

![1729707838822](image/basic-table_funcD/1729707838822.png)

# Developer note:

The numeric header has a specific style in Figma which flush rights it. In Drupal, it is going to be a flush right style in the drop down.

# Usage

Don't use tables as a layout devise. Data tables are for displaying data. UVM's website uses components to provide visual structure to display different types of image and text content. For this reason, we are not going to offer an option to not show the table horizontal row rules.

# Responsive behavior

![1729707847497](image/basic-table_funcD/1729707847497.png)

At all breakpoints, horizontal scroll actives (as supported by browsers) when the table width exceeds the content container. On mobile, as shown above, the table container is 12 columns including the 32px L/R margins.



![1729707855532](image/basic-table_funcD/1729707855532.png)

Some browsers do not display the horizontal scroll control. For affordance to the user, we wish to hint out these scenarios and place an animated icon of a finger swipe to indicate to the user that the table is swipable / scrollable horizontally.

Check the [&#34;Can I use&#34; website](https://caniuse.com/css-scrollbar) to see which browsers support the display of a horizontal scroll bar.



![1729707862667](image/basic-table_funcD/1729707862667.png)

**Display UX issue**

It is more common on mobile, but possible that tables can be taller than the viewport. For the scenario shown above in the screen grab, this would put the horizontal scroll bar or the animated icon below the visible viewport. Although not ideal, this pattern is common and that until there is a better solution, we are expecting that through learned behavior, users are able to navigate table content even with this hindrance.

*TBD during build: we may need to explore an affordance like white gradated overlay top/bottom to also indicate offscreen vertical content on mobile. *

*We also have this issue with the hero issue;; when on mobile and you turn to landscape, the controls get crunched up in landscape mode. *

# Do's and Don'ts

**01 Do: Respect 32px left margin on page load**

**On mobile, the table container is 12 columns + the 32px L/R margins. Howe**ver, on page load, respect the 32px left margin. The table should extend to the left edge after the user swipes to see more of the table off-screen on the right.

![1729707874015](image/basic-table_funcD/1729707874015.png)

**02 Do: Show affordance for horizontal swipe on mobile**

The horizontal scroll bar shows on mobile when the table exceeds the content container width. For browsers that don't display the horizontal scroll, show the swipe icon.

**Do not:** Show both the horizontal scroll bar and the swipe icon at the same time. They are redundant.



![1729707881543](image/basic-table_funcD/1729707881543.png)

**03: Do not show left margin shading on page load**

On page load on mobile, do not show the left margin shading as the table content starts at 32px and until the user scrolls, there is not overflow content on screen left.

**Do: **Show margin shading either left or right when there is overflow content.

# ![1729707890043](image/basic-table_funcD/1729707890043.png)Screen reader behavior

Let's fill in this area together after coding. Here is a helpful link about best practices for coding tables for success with screen readers.

[https://accessibility.oregonstate.edu/tables](https://accessibility.oregonstate.edu/tables)
