# Tables provide order to display data efficiently in an easy to understand grid. This component is a basic table whereas each cell corresponds to only one column header and one row header. It is designed to handle both text and numeric data though is not designed to handle complex data projections with nested data.

![1729707720238](image/basic-table_intro/1729707720238.png)
