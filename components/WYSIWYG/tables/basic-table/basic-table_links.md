## Figma handoff file

https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2007-94399&mode=design

Handoff file for the Basic Table.

## Typograph style library

https://www.figma.com/file/Qy5874w86fN3UHPL87pdGp/typography--Ivy?type=design&node-id=0-1&mode=design

take a look at the art board for Table Type Styles. For optimal viewing, tables have type styles that are specific for tables.
