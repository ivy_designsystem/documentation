# Content editors have styles to specify the widths of tables in layouts.

## Width options for tables

**Medium: **fills the WYSIWYG container (default table size)

**Large:** spans 10 columns (only available for Large-1280 to xl-1440

**Full-width:** spans 12 columns

There are display differences by breakpoint. For example, on mobile, the WYSIWYG container has a 12 column span. Tables only display with 12-col span on mobile.

## This table references the different table sizes by breakpoint



![1729707626859](image/table-widths_funcD/1729707626859.png)

# Medium / WYSIWG table widths by breakpoint

Note that breakpoints medium-1012 and larger, the WYSIWYG container has a 40px left border.



![1729707634868](image/table-widths_funcD/1729707634868.png)

![1729707650501](image/table-widths_funcD/1729707650501.png)

# Large table widths by breakpoint

The large table width spans 10 columns. It is only available between Large-1280 and XL-1440 breakpoints.



![1729707660047](image/table-widths_funcD/1729707660047.png)

# Full-width table widths by breakpoint

The full-width table spans 12 columns. It is available at all breakpoints. Note that the Medium-WYSIWYG and Full-width are the same on mobile.



![1729707669625](image/table-widths_funcD/1729707669625.png)

# Tables placed within the InfoBand, vTab or Accordion components
