# Ivy has a default body type style and then a limited few styles that are larger and smaller for different applications from captions to hero banner description text.  Font sizes impact both readability and comprehension across different platforms and devices.

# Sizes

The "Normal" or default body copy size is p.default 17/24 in darkGray.

![1729688832654](image/t_bodyStyles-funcD/1729688832654.png)

![1729688825580](image/t_bodyStyles-funcD/1729688825580.png)

![1729688842160](image/t_bodyStyles-funcD/1729688842160.png)

There are other body styles to handle the range of sizes needed for all the different applications where body text is called for- from image captions and credits to hero banner description text.

None of these styles are selectable within the WYSIWYG of Drupal. They are for product designers to use wi

thin the design of type groupings and components and then are "baked in" to the design.  ( *Note: size has been scaled by InVision for the screen grab to fit here.* )

![1729690061042](image/t_bodyStyles-funcD/1729690061042.png)

![1729690072682](image/t_bodyStyles-funcD/1729690072682.png)

![1729690082844](image/t_bodyStyles-funcD/1729690082844.png)

![1729690092685](image/t_bodyStyles-funcD/1729690092685.png)

p.xSmall

![1729690103609](image/t_bodyStyles-funcD/1729690103609.png)

p.small

![1729690115613](image/t_bodyStyles-funcD/1729690115613.png)

p.bigSister

![1729690125797](image/t_bodyStyles-funcD/1729690125797.png)

p.large

![1729690133605](image/t_bodyStyles-funcD/1729690133605.png)

# Color

Color is "baked in" to UVM's Drupal content management system (CMS). The default color for all body type is darkGray. LightGray and white are secondary colors that are used by product design within components and are not available to content editors within the WYSIWYG interface.

![1729690141607](image/t_bodyStyles-funcD/1729690141607.png)

![1729690149546](image/t_bodyStyles-funcD/1729690149546.png)

There are also 2 alpha colors in our palette for placing type on dark backgrounds or light backgrounds and having it appear consistent and "blended" . By using an alpha transparency in the text color, it will appear more consistent whether it is on a dark gray, dark green, dark blue etc. for the onDark color (white with .72 alpha) and  for the onLight color (darkGray with .56 alpha) for appearing consistent on neutral, light blue etc. and other light background colors.

These two alpha colors "onDark" and "onLight" can be built into our Drupal palette but do not appear in the WYSIWYG interface. They are for product designers to build into components.

# Weight

All body styles come in the default weight 400.  Bold is 700 weight.

![1729690160996](image/t_bodyStyles-funcD/1729690160996.png)

# Alignment

Body copy is always flush left.

# Casing

Body copy should never but all uppercase. It should be flush left and sentence case.

# Paragraph spacing

All body type styles have paragraph spacing- extra space between paragraphs - to increase readability.

![1729690188395](image/t_bodyStyles-funcD/1729690188395.png)

![1735404671424](image/t_bodyStyles-funcD/1735404671424.png)

# Contextual Links

Note: This is a global pattern within the Ivy design system. It has been used pervasively.[ The News Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/functional%20narrative?mode=preview) is an example of this.

**Link state:**

For darkGray text: text is underlined in darkGray

For lightGray text: text is underline in darkGray

**Hover state:** linked text turns to black, underline changes to primary3.

**Visited:** text is lightGray, underline is lightGray

**Focus:** primary3 focus box around linked text

# Typography Principles

Here are the key principles guiding our typography choices:

1. **Readability:** The typography should be easily readable across different devices and screen sizes.
2. **Consistency:** Consistent typography usage ensures a cohesive and harmonious visual experience.
3. **Hierarchy:** Proper use of heading styles and paragraph styles helps convey information hierarchy.
4. **Accessibility:** Type sizes and colors have been designed to meet a minimum of WCAG AA accessibility standards, ensuring legibility and contrast for all users. The body copy is designed to meet WCAG AAA accessibility standards.
5. **Responsive:** Typography should adapt to different screen sizes and orientations while maintaining legibility.
