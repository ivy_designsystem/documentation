# Ivy has a default body type style and then a limited few styles that are larger and smaller for different applications from captions to hero banner description text. Font sizes impact both readability and comprehension across different platforms and devices.

![1729690227782](image/t_bodyStyles-intro/1729690227782.png)
