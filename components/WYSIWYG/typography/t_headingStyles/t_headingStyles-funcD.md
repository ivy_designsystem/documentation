# Heading styles have a hierarchy of sizes from H1 to H6. Optimal size will vary depending on messaging and application but the range in sizes is available to work with. There are also some specialty display styles.

Typography plays a crucial role in effective communication and visual design. It involves the arrangement and presentation of typefaces, fonts, and text elements to convey information and enhance readability. One essential component of typography is the use of headings, which provide structure and hierarchy to written content.

Headings, represented in a hierarchy of sizes from H1 to H6, are HTML tags used to define the importance and structure of headings in web pages. Each heading level corresponds to a specific hierarchy, with H1 being the highest and H6 being the lowest.

![1729688160596](image/t_headingStyles-funcD/1729688160596.png)



![1729688173044](image/t_headingStyles-funcD/1729688173044.png)



![1729688181348](image/t_headingStyles-funcD/1729688181348.png)

## Heading styles

The headings have desktop and mobile sizes (see[ FIGMA file for exact representation](https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=1657-53297). The screen grab below scales to fit by default so the sizes are not accurate.)



![1729688192284](image/t_headingStyles-funcD/1729688192284.png)

## Fonts used

The primary font used for the headings is Roboto, weight 700.

Playfair display, semi-bold 600, is used for select H1 headings in the hero banners Level 1 to Level 3.

# Do's and Don'ts

### 01 Make sure text has good vertical spacing

![1729688206405](image/t_headingStyles-funcD/1729688206405.png)



![1729688213817](image/t_headingStyles-funcD/1729688213817.png)

### 02 Flush left headings. NEVER justify. Don’t center.



![1729688220917](image/t_headingStyles-funcD/1729688220917.png)



![1729688228095](image/t_headingStyles-funcD/1729688228095.png)

### 03 Headings should not be upper case



![1729688237139](image/t_headingStyles-funcD/1729688237139.png)



![1729688245209](image/t_headingStyles-funcD/1729688245209.png)

### 04 Headings are bold 700



![1729688251416](image/t_headingStyles-funcD/1729688251416.png)



![1729688257438](image/t_headingStyles-funcD/1729688257438.png)

# Design description of the headings

**H1 headings**

These are specialty headings and the largest on the scale. They are used in some banners where the type needs to make a big statement with only one-three words, such as the hero banner.

Playfair display, semi-bold 600, is used for select H1 headings in the hero banners Level 1 to Level 3.

## Semantic description of the headings

1. H1: The H1 heading is used for the main title of a webpage or a significant section. It should appears only once on a page. H1 headings are often larger, bolder, and more visually prominent compared to other headings.
2. H2: The H2 heading is used to denote major sections or subsections within a webpage. It provides a clear visual break and helps readers navigate through the content. H2 headings are usually slightly smaller than H1 headings but still carry significant weight.
3. H3: The H3 heading signifies sub-sections within H2 sections. It further breaks down the content and emphasizes a specific topic or idea. H3 headings are smaller than H2 headings and help maintain a clear hierarchy within the content structure.
4. H4: The H4 heading is employed for sub-sections within H3 sections. It continues the pattern of providing further division and emphasis. H4 headings are smaller than H3 headings and should be used judiciously to prevent overwhelming the reader with too many levels of hierarchy.
5. H5: The H5 heading represents sub-sections within H4 sections. It continues the progression of division and helps maintain clarity and organization in the content structure. H5 headings are smaller than H4 headings and should be used sparingly.
6. H6: The H6 heading serves as the lowest level of hierarchy within the heading system. It is used for sub-sections within H5 sections and provides additional differentiation and emphasis. H6 headings are typically the smallest in size among all the headings.

When utilizing the heading system, it is important to follow best practices:

* Maintain a logical hierarchy: Use headings in a structured manner, ensuring that each level represents a clear subdivision of the previous level.
* Consistency in design: Maintain a consistent visual style for headings throughout the webpage, including font size, weight, and color.
* Accessibility considerations: Ensure that headings are appropriately marked up using HTML tags to enhance accessibility for screen readers and assistive technologies.
* Avoid skipping levels: Do not skip heading levels (e.g., using H3 directly after H1) as it can confuse both readers and search engines.

By using headings effectively, you can create a well-organized and visually appealing structure for your content, enabling readers to navigate and comprehend your information more easily.
