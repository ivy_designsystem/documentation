# Heading styles have a hierarchy of sizes from H1 to H6. Optimal size will vary depending on messaging and application but the range in sizes is available to work with. There are also some specialty display styles.



![1729688277984](image/t_headingStyles-intro/1729688277984.png)
