## Figma link

https://www.figma.com/file/03eXjPmVFRvFHS1GhkH1RJ/Ivy--WYSIWYG?type=design&node-id=44-4638&mode=design

This is the link to the Atoms / Molecules file which has handoff details and specs for WYSIWYG styles.

Styles are linked to, and controlled by, the shared Typography library.
