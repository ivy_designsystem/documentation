# The Big Number list is a styled HTML decimal ordered list (`<ol>`) used to create a list of items that are sequentially ordered with large, stylized numbers.



![1729629628813](image/wswg_bigNumberList-funcD/1729629628813.png)

# Overview

The `<ol>` element is used to represent a list of items where each item has a specific order or sequence. It is commonly employed when presenting steps, procedures, or any content that requires a sequential order.

This component, the  **Big Number List** , is an `<ol>` element that only uses the decimal version and the numbers. This element has large, stylized numbers- hence the name "Big Number List".

**When to use**

When you want a numbered list that is styled and stands out from the rest of the text content in the WYSIWYG content container.

**When not to used**

* When you just need a plain ordered list within the text content.
* When you need more than decimals in your list (ie: uppercase letters, lower case letters, Roman numerals... etc)

The regular `<ol>` element is appropriate for press releases, informative copy, and research papers.

Here's the difference between the Big Number List and a regular `<ol>`

Here is the Big Number List:



![1729629636472](image/wswg_bigNumberList-funcD/1729629636472.png)

Here is an un-styled HTML ordered list using the web-safe Georgia font rather than a sans-serif arial.



![1729629643624](image/wswg_bigNumberList-funcD/1729629643624.png)

# Data

**Mandatory:**

List content: Text

Note: The text content will be full HTML with WYSIWYG so content admins can select this list option within Drupal's CK editor. Be default, the text will be able to be styled with the WYSIWYG editor, so text can be emboldened and links can be added within the text.

**List type**

The Big Number List uses the decimal list type 1 with CSS styles. This is the default list type for the ordered list but we are adding the styles - it's a "special" ordered list with styling.

# Individual ordered list row

There is only one style for the Big Number List with some size and spacing differences between mobile and desktop.



![1729629650250](image/wswg_bigNumberList-funcD/1729629650250.png)

And placed into an ordered list, the desktop and mobile variants look like this:



![1729629657914](image/wswg_bigNumberList-funcD/1729629657914.png)

# States

None.

# Responsive notes

There is a mobile and a desktop version of this component. Here are the significant differences.

**Desktop: **

margin top/bottom 32px

Space between the number and the list is less

Number uses the desktop H3 font size

**Mobile:**

margin top/bottom 24px

Space between the number and the list is less

Number uses the mobile H3 font size

See specs below for layout spacing red lining.



![1729629667909](image/wswg_bigNumberList-funcD/1729629667909.png)

# Alignment

Left alignment only.

# Stacking

Not recommended.

# Interactions

No user engagement except for users that navigate the site with a keyboard.

# WYSIWYG access and default `<ol>` style

Content admins in Drupal will need to be able to choose between:

* the regular unordered list
* the Big Number List

**Important! **

The regular unordered list is the default.

The Big Number List is a style that can be selected.

**Styling in the WYSIWYG editor**



![1729629677166](image/wswg_bigNumberList-funcD/1729629677166.png)

![1729629684268](image/wswg_bigNumberList-funcD/1729629684268.png)

# Anatomy and Specs



![1729629691499](image/wswg_bigNumberList-funcD/1729629691499.png)



![1729629702559](image/wswg_bigNumberList-funcD/1729629702559.png)
