
# The Big Number list is a styled HTML decimal ordered list (`<ol>`) used to create a list of items that are sequentially ordered with large, stylized numbers.

![1729629620192](image/wswg_bigNumberList-intro/1729629620192.png)
