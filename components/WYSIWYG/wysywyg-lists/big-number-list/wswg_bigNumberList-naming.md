# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Common name:

Big number list

# Component naming for the inline button



![1729629612771](image/wswg_bigNumberList-naming/1729629612771.png)
