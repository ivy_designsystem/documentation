# Atoms

## Icons

They are at the bottom of the page here:

**Checkmark icon**

Ivy / Icons / UI / base/ [checkmark](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

**"x" icon**

Ivy / Icons / UI / ui/ 4Ivy/sectionNavUI-icons/ [icn_closeDark32_default](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

Or you can just[ access the SVGs here.](https://uvmoffice.sharepoint.com/:f:/s/CommunicationsTEAM408/EoGERMToeVxAnEaIEnHBNOgBRKipXfK7m49POHfzQkUS4w?e=Gap05J)

![1729629171919](image/wswg_iconList-atomic/1729629171919.png)
