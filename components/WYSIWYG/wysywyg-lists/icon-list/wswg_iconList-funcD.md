# The 'icon list' component is a list type within our design system. It's a list with an icon. The icon can be either a check mark or an "x".

## Purpose

The Icon List component serves to:

* Display a list of items
* Communicate that things have been done or are relevant or appropriate with a check mark icon
* Communicate that things have not been done or are not relevant or appropriate with an "X" icon

The Icon List component is not an interactive component. Compare it to the HTML Check Box component which serves as a task reminder list where the user can check off items as completed.

Checklist:



![1729629268693](image/wswg_iconList-funcD/1729629268693.png)

Checkbox:



![1729629275359](image/wswg_iconList-funcD/1729629275359.png)

## Types of Icon Lists

We have one Icon List with 2 variants and two sizes:

1. Simple Icon List: a list composed of rows with a check icon + body text
2. Simple Icon List: a list composed of rows with a  "x" icon + body text



![1729629284236](image/wswg_iconList-funcD/1729629284236.png)

Both Icon Lists have a desktop and a mobile size. The only difference is the spacing between the icon and the text. The icon is closer to the text on the mobile version.



![1729629291174](image/wswg_iconList-funcD/1729629291174.png)

The individual Icon List items have either an "X" icon or a check icon. Shown here is the mobile size.



![1729629297753](image/wswg_iconList-funcD/1729629297753.png)

And put into an Icon List, these are the two variants.  (showing the desktop variant)



![1729629307475](image/wswg_iconList-funcD/1729629307475.png)

## **Do's and Don'ts**

**No "Mix and Match" of icons**



![1729629314015](image/wswg_iconList-funcD/1729629314015.png)

**Bold styling can be added with the WYSIWYG editor**



![1729629320654](image/wswg_iconList-funcD/1729629320654.png)

**If you add styling with the WYSIWYG editor, be consistent**



![1729629326709](image/wswg_iconList-funcD/1729629326709.png)

**When to use**

Use it when you want to emphasize critical actions or responses.

* The Check symbolizes something critical that is either done or should be done.
* The 'X' symbolizes something critical to not do or is not relevant.

**For example: **



![1729629333141](image/wswg_iconList-funcD/1729629333141.png)

## When not to use

While lists are a versatile and widely used UI element, there are situations where using a list may not be the most appropriate choice. Here are some scenarios where you might consider alternatives to using a list:

**Single Item Display**

If you are presenting a single item without the need for a structured list, using a list might be unnecessary. In such cases, a simple paragraph or a standalone element could be more suitable.

**Rich content**

Lists are text-only components. If your content consists of a mix of media types (text, images, videos), the Icon List will not work for you.  Explore other layout options that support multimedia content.

Always consider the specific requirements of your content and the user experience when deciding whether to use a list or explore alternative approaches. It's essential to choose the UI element that best aligns with the goals and characteristics of the information you are presenting.

## Data

### Mandatory for both checklist variants:

Icon List content: Text, WYWIWYG

Icons: checkmark and "x" icons. Text needs to relate to an icon, one or the other.

Default icon: check mark

### Number of items

There are no minimums or maximums for how many items can be in the Icon List. We are relying on the content admin's judgment here to keep the list short enough to be readable.

### Restrictions

You cannot "mix and match" the Icon List variants.

Icon Lists either all have a "Check" icon or all have an "X" icon.

Jordan: Can there be restrictions so it doesn't become a WYSIWYG style nightmare? ie: Text can't be turned into headlines? Images can't be added within the list rows?

## States

None. The Icon List is not interactive. It is not a task or To Do list.

## Responsive notes

Both Icon List variants are 100% responsive while maintaining internal padding and margins.

Desktop: The top/bottom margin of the Icon List component is 32px

Mobile: The top/bottom margin of the Icon List component is 24px

The icons are fixed in size and position and the body content stretches to fill the container it is placed in. The height scales to accommodate the  amount of text content.

## Alignment

The Icon List is fluid, goes into the WYSIWYG container only, and scales to fill the WYSIWIG container. The list is only flush left.

## Quantity of rows and stacking

Whereas the Icon List has no enforced limit to the number of items it can hold, It is recommended to have not more than 6 items in an Icon List.

Icon Lists can be stacked though and the component margins will provide some vertical white space to help stacked Icon Lists be more readable for the user.

## Interactions

No user engagement except for users that navigate the site with a keyboard.

## Anatomy and Specs

### Individual Icon List row



![1729629343807](image/wswg_iconList-funcD/1729629343807.png)

## Icon List component



![1729629354156](image/wswg_iconList-funcD/1729629354156.png)
