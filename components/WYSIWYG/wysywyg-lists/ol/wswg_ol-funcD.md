# Lists in HTML are used to organize and structure content to make information more readable and understandable for users. The ordered list `<ol>` uses numbers instead of bullets to show list hierarchy.

An *ordered list* is created using the `<ol>` element. It represents a collection of items that *are* in a specific order. Each item in the list is represented by the `<li>` (list item) element. Typically, ordered lists are displayed with ascending numbers.

Here is a code snipped of an unordered list:

<ol>

<li>First item</li>

<li>Second item</li>

<li>Third item</li>

</ol>

# Three text sizes

UVM's default (normal) text style is17/24, however, we have a few different body type sizes to handle different display scenarios.

The ordered list should have the same type size / style as the body copy for the component within which it appears. For most instances, this will be the default (normal) size. However, we are planning also for a p.small and p.bigSister unordered list size for the off-chance that these other body type sizes are used in a component.

Important! Content editors do not have the option to select the text size themselves for the unordered list. The unordered list adopts by default the text size of the parent container.

Here are the 3 text sizes:



![1729629945534](image/wswg_ol-funcD/1729629945534.png)

# Nested lists

You can nest bulleted lists within each other to create hierarchical structures. To nest a list, simply include another `<ol>` element within an `<li>` element. Here's an example:

<ol>

<li>Parent item 1</li>

<li>Parent item 2

<ol>

<li>Child item 1</li>

<li>Child item 2</li>

</ol>

</li>

<li>Parent item 3</li>

</ol>

You an also mixed ordered and unordered lists in nesting. The following would generate a numbers list with some bulleted items listed under "Parent item 2".

<ol>

<li>Parent item 1</li>

<li>Parent item 2

<ul>

<li>Child item 1</li>

<li>Child item 2</li>

</ul>

</li>

<li>Parent item 3</li>

</ol>

# Three levels deep and numbering style

UVM's ordered lists are numeric and use decimal numbering to communicate the hierarchy of items in the list. Our ordered list style has planned for 3 levels deep in list hierarchy. To create three levels of nesting, you can add another `<ol>` element inside the nested `<ol>`. Here's a code snippet for an ordered decimal list with 3 levels of content hierarchy:

<ol>

<li>List item 1</li>

<li>List item 2

<ol>

<li>List item 2.1</li>

<li>List item 2.2</li>

<li>List item 2.3</li>

    `<ol>`

    `<li>`List item 2.3,1`</li>`

    `<li>`List item 2.3,2`</li>`

    `<li>`List item 2.3,3`</li>`

    `</ol>`

</ol>

</li>

<li>List item 3</li>

</ol>

In this example, the second level is indented and prefixed with a decimal number followed by a period. The third level is further indented and prefixed with a decimal number followed by a period, based on the nesting of `<ol>` elements.

# Visual display

### **Numbers align and hang**

Decimal numbers and text align with the other decimal numbers and text at the same level.

Each inferior level indents so the decimal number aligns with the text of the superior level above.

This structure is logical and tidy and shows the hierarchy of the items in the list.



![1729629955155](image/wswg_ol-funcD/1729629955155.png)

### List fills the text container that it is placed in

The unordered list fills the container that it is in without left or right margins. Text container widths are defined as a percentage of the page width and relative to each breakpoint. This is important to maintain a comfortable and readable line-length for body copy. The target number of characters per line for readability is 60 with the max being 90. This example shows 89 characters on the first line.

# ![1729629963991](image/wswg_ol-funcD/1729629963991.png)Layout and spacing

The indents for the L2 and L3 bullets and text are so that things visually align with the items above as shown here:



![1729629980322](image/wswg_ol-funcD/1729629980322.png)

And here are some layout and spacing guidelines for the p.small, p.default and p.bigSister type sizes. Note: that the indents between the decimals and text are consistent and prescriptive but the indents for each level is determined by what is needed to align with the text of the superior item above.



![1729629987642](image/wswg_ol-funcD/1729629987642.png)

![1729629996100](image/wswg_ol-funcD/1729629996100.png)



![1729630003625](image/wswg_ol-funcD/1729630003625.png)
