# Lists in HTML are used to organize and structure content to make information more readable and understandable for users. The ordered list `<ol>` uses numbers instead of bullets to show list hierarchy.



![1729629916690](image/wswg_ol-intro/1729629916690.png)
