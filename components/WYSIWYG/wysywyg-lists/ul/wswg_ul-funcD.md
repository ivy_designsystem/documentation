# Lists in HTML are used to organize and structure content to make information more readable and understandable for users. The unordered list `<ul>` uses bullets instead of numbers and is used to display a list of items where the order is less important.

An unordered list is created using the `<ul>` element. It represents a collection of items that are not in a specific order. Each item in the list is represented by the `<li>` (list item) element. By default, unordered lists are displayed with bullet points preceding each item.

Here is a code snipped of an unordered list:

<ul>

<li>Item 1</li>

<li>Item 2</li>

<li>Item 3</li>

</ul>

# Three text sizes

UVM's default (normal) text style is17/24, however, we have a few different body type sizes to handle different display scenarios.

The unordered list should have the same type size / style as the body copy for the component within which it appears. For most instances, this will be the default (normal) size. However, we are planning also for a p.small and p.bigSister unordered list size for the off-chance that these other body type sizes are used in a component.

Important! Content editors do not have the option to select the text size themselves for the unordered list. The unordered list adopts by default the text size of the parent container.

Here are the 3 text sizes:



![1729630092901](image/wswg_ul-funcD/1729630092901.png)

# Nested lists

You can nest bulleted lists within each other to create hierarchical structures. To nest a list, simply include another `<ul>` element within an `<li>` element. Here's an example:

<ul>

<li>Parent item 1</li>

<li>Parent item 2

<ul>

<li>Child item 1</li>

<li>Child item 2</li>

</ul>

</li>

<li>Parent item 3</li>

</ul>

## We've styled for 3 levels of nested content

Our unordered list style has planned for 3 levels deep in list hierarchy. To create three levels of nesting, you can add another `<ul>` element inside the nested `<ul>`. Here's a code snippet for 3 levels of unordered list hierarchy:

<ul>

<li>Level 1</li>

<li>Level 1

<ul>

<li>Level 2</li>

<li>Level 2

    `<ul>`

    `<li>`Level 3`</li>`

    `<li>`Level 3`</li>`

    `</ul>`

</li>

</ul>

</li>

<li>Level 1</li>

</ul>

# Bullet styles

The bullet style changes for each level to ensure communication of visual hierarchy.  The bullet styles by level are:

* L1: disc (default): filled circle
* L2: dash (en dash)
* L3: hollow circle

/* Circle style */

ul.circle {

 list-style-type: circle;

}

/* Disc style */

ul.disc {

 list-style-type: disc;

}

/* Dash style */

ul.dash {

 list-style-type: dash;

}

Note: Though not uncommon, the "dash" bullet style is not a traditional HTML bullet style.  It is going to need some CSS using the :before pseudo-element.

CSS

ul.dash-list {

 list-style-type: none;

 padding-left: 16px;

}

ul.dash-list li::before {

 content: "–";

}

Note: the dash is an "en" dash.

Mac: opt + -

HTML entity: &ndash;

HTML

<ul class="dash">

<li>Item 1</li>

<li>Item 2</li>

<li>Item 3</li>

</ul>

# Visual display

### **Bullets align and hang**

The bullets align with the other bullets at the same level.  For all levels, the bullets *hang* outside the margin of the type.



![1729630102507](image/wswg_ul-funcD/1729630102507.png)

### List fills the text container that it is placed in

The unordered list fills the container that it is in without left or right margins. Text container widths are defined as a percentage of the page width and relative to each breakpoint. This is important to maintain a comfortable and readable line-length for body copy. The target number of characters per line for readability is 60 with the max being 90. This example shows 89 characters on the first line.

# Layout and spacing

The indents for the L2 and L3 bullets and text are so that things visually align with the items above as shown here:



![1729630118770](image/wswg_ul-funcD/1729630118770.png)

And here are some layout and spacing guidelines for the p.small, p.default and p.bigSister type sizes.



![1729630126407](image/wswg_ul-funcD/1729630126407.png)



![1729630136592](image/wswg_ul-funcD/1729630136592.png)




![1729630146713](image/wswg_ul-funcD/1729630146713.png)
