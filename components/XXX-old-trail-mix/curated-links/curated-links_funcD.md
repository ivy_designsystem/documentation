# This component is for manually curated links for individual news stories.

 It's used to provide relationships between content outside of a relational database and helps users follow the information scent related to the news story.

## Title

It has a fixed head entitled " **Of Related Interest** ".

## Data

* Recommend 3 -5 links max
* Component can have unlimited number links
* Not dynamic, links manually entered
* Text cannot be inline styled. Links entered in Drupal UI
* Character count for each link- ?

![1730468437291](image/curated-links_funcD/1730468437291.png)
