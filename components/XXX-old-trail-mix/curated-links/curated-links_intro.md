This component is for manually curated links.  It's used to provide relationships between content outside of a relational database and helps users follow the information scent related to the news story. It has a fixed head entitled "Of Related Interest".

![1730468418510](image/curated-links_intro/1730468418510.png)![1730468432593](image/curated-links_funcD/1730468432593.png)
