# The Gallery LightBox displays gallery images larger in a modal lightBox overlay. User can navigate bu clicking the "previous" and "next" arrows in the LightBox navigation to click through the larger gallery images one at a time.

## Launching the LightBox Slideshow

Clicking on any image will expand the image to a modal slideshow with a larger view of the image clicked on.

**On desktop,** user will be able to click forward and backwards through the gallery at a single image, larger view.

 **On mobile** , slideshow navigation arrows will stay but moving through the slides will happen either on tap of the previous/next arrows or via swiping which is a native mobile gesture.

## Aspect Ratio of Images

Images within the slideshow can be horizontal or vertical and have any aspect ratio.

**Mandatory Exception**

Note: The Featured Media component is to be 16x9 so the representational image for a slideshow that is in the slider, needs to be cropped specifically for the slider and be 16x9.

## Caption and Credit

All images to have optional caption and optional credit. Hyperlinks are possible within the caption/credit and are hard-coded.  There is not text limit to the caption.

## Counter

Lightbox slideshow has a counter in the upper left so the user knows which slide they are on out of a given total. For example:

**14/86 **

Current / Total

## LightBox Control Panel and Navigation

![1730468455602](image/lightbox-slideshow_funcD/1730468455602.png)

The lightBox implementation came with some tools and controls. The controls above appear in the upper right corner of the lightBox slider. They appear on-load of the slide but fade-out after xx seconds.

## **Navigation Between Single Images**

**Desktop: **Slideshow has arrows for navigation

**Tablet and Mobile:** Slides in slideshow view change both on swipe and on-tap. Navigation arrows are not persistent.


![1730468472865](image/lightbox-slideshow_funcD/1730468472865.png)

## Back to Masonry Gallery

The grid icon in the control panel will take the user back to the masonry grid view.

## Zoom and Scale

User can click / tap the [+] and [-] zoom icons in the control panel to zoom in our out on the image. Zoom functionality can also be controlled with a wheel on a mouse, if present.

**Keyboard commands to scale**

Shift-+: will scale the image larger via keyboard

Ctrl-  (control key + minus) will scale the image smaller with the keyboard

## Auto-Play Slideshow

The triangle shaped button in the slideShow controls will auto play the slideshow with a fade transition between each slide and optional caption. The user can always stop and override the auto-play if so desired.

## Full Screen

The expand icon will make the lightBox go full screen. Once in full screen mode, user can use the "ESCAPE" key on the keyboard to exit full screen mode.

## Closing the LightBox

The [X] key on the lightBox control panel will close the modal overlay and take the user back to the news story that launched the gallery.

## Meta Data

Each image in a gallery has a mandatory `<ALT>` tag and an optional caption.
