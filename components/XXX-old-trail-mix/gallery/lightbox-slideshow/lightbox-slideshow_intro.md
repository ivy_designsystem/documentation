# The Gallery LightBox displays gallery images larger in a modal lightBox overlay. User can navigate bu clicking the "previous" and "next" arrows in the LightBox navigation to click through the larger gallery images one at a time.


![1730468488076](image/lightbox-slideshow_intro/1730468488076.png)
