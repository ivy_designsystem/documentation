# Gallery Functionality

The **Gallery** was created as a part of the News Tool redesign and these designs were released to help define desired functionality and inform styles for an **out-of-the-box solution programming** that was implemented. That said, the Gallery layouts here are for *style guidelines and layout reference to inform the customization of the visual display. The gallery on the website is a bit different as the functionality was built in.*

## Galleries are related to Individual News Stories

**Galleries are related to news stories and are not available outside of News. They are created from within the News Tool Editor.** Each news story can have 1 gallery associated with it.

The gallery functionality described here is dependent than the Featured Media component. You cannot have a link to a gallery elsewhere—the link will be within the Featured Media component– with either a single image, slider, or video. **Regardless, there is only one gallery per new story.**

## Viewing a Gallery

If a news story has a related Gallery, the Gallery Icon will appear in the caption of the Featured Media component. This icon has a hover state on desktop. The user can launch the Gallery from a news story via on-click or on-tap of the gallery icon in the Featured Media caption. The Gallery will open to the masonry grid view of thumbnails in a modal overlay over the news page.

## Closing a Gallery

Closing the gallery via the "X" in the upper right will bring the user back to the individual news story that they were viewing.

## Masonry Grid View

The masonry thumbnail view of gallery is the default view when the user opens a gallery. It is a modal overlay over the news story page. The Gallery does not load in a new Drupal page or template. The masonry grid of thumbnails stack vertically over modal background. Users can scroll or swipe to view all the thumbnails. Masonry grid loads rows first and then columns. For example, in a gallery with 7 images viewed on desktop, 4 images would load in the first row across before the remaining 3 fill in left to right below in the 2nd row.

## Alignment and Responsive Scaling

The masonry grid responsively scales on the page and centers as a group on the page. At the mobile breakpoints, the 32px L/R margins are always respected.  At the desktop sizes, the masonry grid group does not scale outside of the page and also respects the 32px L/R margins. The grid of images does not scale full browser width on desktop.

## Spacing between image in the Masonry Grid

• Make Horizontal and vertical space between each image in the grid the same- (16px for 768+ and 8px for 0-767). *

• Grid does not scale to fit full browser width at the largest breakpoints (keep L/R page margins 32px)*

• grid responsively scales

**Desktop 768+**

Images are 16px apart

**Mobile 0-767**

Images are 8 pixels apart

## **Number of Columns in the Grid by Breakpoint**

**This chart shows the number of columns in the masonry grid at each breakpoint.**



![1730468518585](image/masonry-grid_funcD/1730468518585.png)

## Image Sizes

The Gallery accepts all image aspect ratios and displays them scaled to size without cropping. Image aspect ratio is determined by what the user uploads.  The masonry grid accommodates all sizes and the grid flows with equal space between images both vertically and horizontally.

**Mandatory Exceptions**

However, since the user is linking to the gallery from the Featured Media component in a News story, the first xx images of the gallery will be the same images that compose the slider… if there are (5) 16x9 images in the slider, the first (5) images in the gallery will be the 16x9 images that the user already was offered in the slider. That said, those images will HAVE to be 16x9.

## Captions

There are No captions with the thumbnail, masonry view.
