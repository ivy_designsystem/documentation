# The masonry grid is the default / on-load view of the Gallery.  It's a collection of thumbnail sized images that the user can quickly scroll or swipe through.

User can view any thumbnail image larger by on-click / on-tap to bring up the individual image in a modal lightBox slideshow view.

![1730468528538](image/masonry-grid_intro/1730468528538.png)
