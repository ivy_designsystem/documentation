# The Card Tiles are used in a grid with 16px of space dividing the Tiles with 32px L/R margins.

The aspect ratio for the cards is 9x16 but the size and how many appear in a row vary by breakpoint.

![1730468547984](image/image-tile_funcD/1730468547984.png)

## Type Size Variations

Larger cards have larger body type on the front.

The Large (1280) breakpoint uses cards are 292px wide. All cards 292px wide and larger use 18/24 roboto bold for the body.

Smaller than that, the body is 16/22 roboto bold.

If you inspect, you can see...

XXS, XS, and S breakpoints use the smaller type.

M, L, XL, and XXL use the larger size.

## Hover States



![1730468558560](image/image-tile_funcD/1730468558560.png)

Image Tile Card  and its hover states. ^



![1730468567846](image/image-tile_funcD/1730468567846.png)

Solid cards hover to their opposite--

light cards hover to dark

dark cards hover to light.
