# Image Tile

The Image Tile card is used in a grid and can be used to display elevated information or they can be a gateway marquee teasers that links to deeper site content. The hover state flips the card and on-click/on-tap go send user to (optional) link. The entire card is the link.


![1730468583139](image/image-tile_intro/1730468583139.png)
