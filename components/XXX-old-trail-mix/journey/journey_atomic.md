## Molecules

**Progress Navigation Dots**

Trail Mix / Components / Molecules / Controls / [Progress Navigation](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60817c8cc8bde9fb842f2afa/tab/design?mode=preview)

**Text pairing is a molecule:**

Trail Mix / Components / Molecules/Text / Heading Groups / [m_textGroup-xtra_sub-light--dt](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit),

[scroll_m_textGroup-xtra_sub-light--dt](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit)[](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit)and [txtGroup-mobile](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit)

## Atoms

**Buttons**

Trail Mix / Components / Atoms / Buttons /

* [Wordy icon button](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6d5222fb0aa03eae7b972/tab/design?mode=preview)  (btn__textStrongWhite)

**Media**

* 1x1 image (1200x675), [FPO image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/61390881f9799804f7f9d035/tab/placeholder%20images?mode=preview)
* 16x9 3rd party hosted video, [FPO video image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/61390943a171d471ec7275b8/tab/design?mode=preview)

**Slider Control icons**

Trail Mix / Foundations / UI / [slider-control-light--right and slider-control-light--left](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix)

**Box**

* [Solid ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/613bb2674b7d36359f8085fe/tab/design?mode=edit) (box-primary1)
* top bg
