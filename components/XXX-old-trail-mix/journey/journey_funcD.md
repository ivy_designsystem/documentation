# **Overview**

The **journey** component is essentially a story-telling content slider that moves horizontally from off screen.

Left side of the component at desktop resolutions has an overlay containing background, text group, and optional button.  On mobile the component stacks with swipable full width media.

As the content slides, the headline remains in place.

## **Inputs**

**Name: Media**

**Type: image or video**

**Required/#: 2,3,4,5 / or 6?**

**Description: n/a**

**Name: Text Group**

**Type: heading, sub heading and description**

**Required/#: 1 for each media - required**

**Description: n/a**

**Name: Wordy icon button**

**Type: text and link**

**Required: NO**

## M**andatory design elements**

**Images: **

Images are mandatory. Images should be cropped to 1x1.

Recommended original asset size: 550x550

### Text content:

This text pairing is a molecule:

Head , Secondary text are mandatory

Sub-head enables optimal story telling on mobile

## Character Counts

**Head: shortText, 32** characters (2 lines)

**Sub-head: shortText, 28** characters (1 line)

**Body: longText, max 486** characters, plainText

 **Read More Threshold** , **208 characters**

** Heading counts could be reversed and work as designed. 1 line heading and 2 lines subheading*

***18 head and 56  sub-head***

## **Variable or optional design elements**

**Media**

A **16x9** media video is optional.

Videos should be hosted by 3rd party video host such as YouTube or Vimeo. The input is source URL and they use the video skin and controls native to the source.

**Optional CTA**

The CTA for this component is optional. You can a wordy icon button.

Options for the button include:

btn__textStrongWhite



![1730468605137](image/journey_funcD/1730468605137.png)

*See the usage guidelines for the buttons for more information about how the icons work for the buttons. Basically, the FlatIcon button does not have an icon option. It just comes with the one right arrow.*

**Accessibility Notes**

Relying on widely adopted swipe pattern for mobile.  Should this present challenges, can add slider navigation arrows.

## **Interactions **

Click arrows to slide media across screen. Right arrow slides the content to the left, revealing the next image on the right. The content moving left moves behind the overlay. Left arrow slides the content to the right, revealing the next image on the left. On mobile, swipe

Reveal more text enabled for description text.

## M**obile**

Regardless of the position of the image on desktop, the mobile view always has the image on the top.

## B**reakpoint notes**

Left type grouping and backgrounds  change from 4 column at XL and L to 5 columns at M and S
