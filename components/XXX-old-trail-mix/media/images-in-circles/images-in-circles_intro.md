Images in circles

Some samples of how we use a circle mask for images and some style variations. Images display 135x135px generally so should not have much detail or complexity in the image content.



![1730468626240](image/images-in-circles_intro/1730468626240.png)
