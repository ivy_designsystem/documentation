# Molecules

**This text pairing is a molecule:**

Trail Mix / Components / Molecules/Text / Heading Groups /[](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit)[txtGroup-dt ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit)[and ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=edit)[txtGroup-mobile](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6cfd89a0d4e39ff2b6c3d/tab/design?mode=preview)

* type/molecules/lockup/dt/m__TextGroupBigSister-light--dt
* type/molecules/lockup/mobile/m__TextGroupBigSister-light--m

**Player List Group**

Trail Mix / Components / Molecules/ Lists / [Player List](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/6143dbe65f53ac50bbcaa003/tab/design?mode=preview)

# Atoms

**Buttons**

Trail Mix / Components / Atoms / Buttons /

* [Text with icon](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/60b6d6be2fb0aa8bf6e7b976/tab/design?mode=edit) (White)

**Media**

* 16x9 image, [FPO image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/61390729d0c2b60c3871d76d/tab/placeholder%20images?mode=preview)
* 16x9 3rd party hosted video, [FPO video image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/61390943a171d471ec7275b8/tab/design?mode=preview)

**Solid Box**

* [Dark - Primary 1](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/asset/613bb2674b7d36359f8085fe/tab/design?mode=preview)
