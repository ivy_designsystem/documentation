# **Overview**

Component of 2, 3, or 4 images, videos, or both that tell a story.  Each piece contains a short title and short description.

# **When to Use This Component**

A flexible utility component, the player is used when there are multiple multi-media items that can be grouped together in a related topic but have different media associated with each.

# **Inputs**



![1730468647650](image/player_funcD/1730468647650.png)

# **Functionality **

**Media + Title: **

* Each media has a thumbnail consisting of a 1x1 image and a title on color background.
* The title and color background use the player list moecule.** The hover and active state also behave the same with an additional opacity layer of the 1x1 image that fades in/out.**
* The large media container is 16:9 horizontal
* On mobile the items become a stacked link list similar to how YouTube works.

**Text Group: **

* This is the same text group molecule as featured media.
* The caption has the added feature of being able to contain a text link button.

## **Mobile**

* Playlist list link includes anchor that scrolls the user up to the top of the media for viewing

# Character Counts

* Optional title: up 28-33 characters is ideal. **Max 46 max** characters (keep at 3 lines @ XXS)
* Headings: 41 (keeps at 3 lines @ XXS) characters is ideal. **Max 53** characters
* Descriptions:  At least 75 characters and up to** 138  Max**

# **Mandatory Design Elements**

Optional Title*, Headings, Short description

# **Optional Design Elements **

Text link

# **Accessibility Alerts (for dev team)**

tk

# **Interactions**

Fade as default, hover and active Player list items are selected

# **Other Notes**

Optional Title is the name of the atom. It is not optional in this case.
