The Player component provides an interface to play a collection of 4 manually curated videos along with their respective captions.

# This is a media player. It can feature 2, 3, or 4 images, videos, or both. Each story has a caption consisting of a short title, short description and optional link.

![1730468705777](image/player_intro/1730468705777.png)
