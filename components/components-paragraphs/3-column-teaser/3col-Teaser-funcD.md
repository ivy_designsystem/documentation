## Intro

**The Media Card Group is made up 3 [Media Cards](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63da902b70516c62891cdd04/tab/design?mode=preview). Media cards are little teasers with ~either~ a still image  ~or a video~ . They elevate a small amount of content and help drive users to other areas on the site.**

**Image rendering with markdown**
![1726066376401](image/3col-contentTeaser-funcD/1726066376401.png)
![alt text](01.png)

**Attempt to render and scale image with HTML**
`<img src="image/3col-contentTeaser-funcD/1726066376401.png" width=200 height=64>`

**Attempt to render the image with HTML using ZH absolute URL to the image**
`<img src="https://ivy.zeroheight.com/api/integration/uploads/698/ad45aa064532850f3e8cbbc18acc8f6d71b17f11/e586aa32d619d3a2ef44fb45e9de9e30.png">`

<img src="https://ivy.zeroheight.com/api/integration/uploads/698/02e0699fc73e71953920abe9946e392213c272d4/96efc653ff477faf9120f53d61fe2db4.png" alt="1726066428431">

/* this renders the html in a toast-like display*/

```html
<img src="image/3col-contentTeaser-funcD/1726066376401.png" width="200"/>
```

/* attempt that is working as is native image placement */
![1726066376401](image/3col-contentTeaser-funcD/1726066376401.png)

### Data

The Media Card Group consists of 3 media cards in a group.

**Mandatory: **3 cards

Since the Media Card Group is a collection of Media Cards, please see the [Media Card](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63da902b70516c62891cdd04/tab/design?mode=preview) (molecule) section for information about data, functionality and requirements for each card.

### Optional Data

**Component head: **simple text

### Media holding rule

Media in the Media Card has a modalBlack-05 1px border .. just as a very subtle holding rule in case the media placed there is white/light. It appears nearly invisible until needed to hold the edge.

### Missing Image

If the image is missing for the Media Card, a [16x9 placeholder, transparent image](https://uvmoffice.sharepoint.com/:f:/s/UVMWebsiteRedesign/EqQ-hhEauNlOufkxJqCbGqABHvtALNY4c8LjfYYd-w3trQ?e=kKQUKw) holds the space. The user would see no visible image (background color of the component) and the holding rule.

### Functionality

The mediaCard is a teaser ~for either a video or a web page~ for a link to other web content.

~If the link is a video link, the "Play Video" icon will show over the 16x9 image. The Play Button has a lot of states, but since the entire card is the "hit" area, they will not all be used. ~

**Desired on hover/tap **

Pickup the same micro-interaction that is used in the [Featured News Teaser ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63b5cfefc75ee24a9e574458/tab/functional%20narrative?mode=preview) ~with the addition of the Play Button~ .

* ~Play button shows hover state~
* Saturation increase on image
* Arrow-right at the bottom shifts +8px to the right
* Title gets a yellow underline and turns black

### **Hit area**

Entire card is the 'hit' area. On click/tap, takes user to the link provided for the card.

### **Links**

Each card can have only 1 link since the entire card is the "hit" area. The link ~can be to either a video or~ is to another web page. ~If the link is exclusively to a 3rd part hosted video, the video Play Button appears and o~~n-click / on-tap of the video play button, the video launches in a full take-over with a 100% black background and uses the video skin native of the 3rd party where the video is hosted (YouTube, Vimeo, etc) to allow the user to control the video. ~[~There is an example of this on the Stio website's~](https://www.stio.com/blogs/snow/port-to-peak)~ story about a guy who biked from Portland to Mt. Washington with his skis.~

The mediaCard itself is designed for either a still image or a video. However, we are rolling out the **Media Card Group** with only still images- no videos.

~If the link is to a page, the video icon should not appear - even if the linked page ~*~has~*~ a video. The Play Button over the image conveys that by clicking on it, a video will play. ~

### ~Video option~

~The media element can be a still image or an image with a video play button. If a link is provided, the video play button will appear over the still image. ~

**~Mixed-and-match is ok~**

~There is not prescribed mandate for video or still image. It's up to the content editor and content needs which are video and which are still media elements. They can be uniform or mixed-and-match. ~

### Interaction

**Entire card is hit state.**

[Follow the interaction used on the featured story in the News Teaser component.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/functional%20narrative?mode=preview) (image has a saturation shift, arrow at bottom moves 8px to the right, title gets a primary3 underline that animates on, left-right and top-bottom.)

### Design Elements

**Rules **

There is a bounding rule at the bottom on mobile: it is a solid rule, transparent black (gray)

**Width of the group of card group**

xs-480 to xl-1440, the card group spans 10 columns central on the grid.  The xxs breakpoint spans all 12 columns.

### Backgrounds

Component comes on either a white or neutral #F7F7F7 background.

### UI Elements

There is a right arrow, color primary1 at bottom of each card.

~Cards that have a link to a 3rd party-hosted video get a Play Button over the image.~

### Responsive Notes

**Stacked on mobile**

The display of the Media Card Group stacks in 2 columns for the xxs-320 and xs-480 breakpoints.

![1726066428431](image/3col-contentTeaser-funcD/1726066428431.png)
![alt text](02-responsiveNotes.png)

**Group in one row on desktop**

The Media Card Group shows the 3-cards in a row for all desktop breakpoints, s-780 and larger .

![1726066461753](image/3col-contentTeaser-funcD/1726066461753.png)
![alt text](03_1row-on-desktop.png)

The width for each is determined by the parent component (Media Card Group)

**Content lengths and alignment**

Cards will have fixed heights that vary by breakpoint. The length of the content may vary but cards for each breakpoint will be the same height. It will be up to the user to write to fit.

**Developer Note** *Users will have to write to fit since the cards will not scale in height. I am not providing a maximum character count because the length of the text is a combined length of the title and description. Thus, users will rely on visual feedback to write to fit and the programming of the component will need to just hide overflow text.*

For non-stacking displays S-768 and larger, the arrows should line up at the bottom of the cards in the group.

The space between the bottom arrow and the text content will vary depending on the amount of content in the card.
