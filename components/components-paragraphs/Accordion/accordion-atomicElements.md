## Molecules

**Accordion Toggle**

This is the individual toggle for the accordion.

Ivy / Components / Molecules / [Accordion Toggle](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd989c1579dd2e9962a603/tab/design?mode=preview)
