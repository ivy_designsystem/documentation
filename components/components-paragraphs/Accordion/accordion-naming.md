# Naming

**It's important that everything has a name and that we all use the same name to refer to the component.**

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

## Naming for the 'Accordion' component

![1727706756027](image/accordion-naming/1727706756027.png)

Only 2 master components were made, one for mobile and one for desktop, as the accordion scales horizontally to fill the container it is placed within. The text stays flush left and the icon stays flush right. Just the text width scales as the accordion containers scale.
