# Intro

**An accordion component is a user interface element in extensive use on the UVM website. It can organize and manage a lot of content in a small amount of screen real estate and make it more consumable by displaying it as a  list of collapsible sections.**

![1727706922561](image/accordion/1727706922561.png)

## Usage

The Accordion component is suitable for various scenarios where hierarchical information needs to be presented, such as FAQs, step-by-step instructions, and more. It is a valuable addition to the design system as it enhances the user experience by reducing clutter and improving content discoverability.

## Structure

The Accordion component is composed of the following elements:

- Accordion toggle: A clickable interface element at the top of each section. It contains the category head for the section and is the control to expand or collapse the section.
- Content panel / Accordion Content: The area that expands and collapses, displaying the detailed information or content.
- Accordion group: The entire accordion is a group of accordion toggles that can expand/collapse to reveal/hide content

## Data

Each accordion section consists of an accordion toggle with a category header and a content area, allowing users to toggle the visibility of the content by clicking on the toggle.

﻿

**Title**: The title is in the accordion toggle. It is manually entered content. Simple text. xxx characters max

**Content:** WYSIWYG content.

**Important exclusion**: Accordions cannot be nested. Content editors should not be allowed to nest an accordion within an accordion

## Display

Accordions scale horizontally to fill the container they are put into.

**Desktop**

accordion toggle: is 88px fixed height. Toggle title should fit without scaling toggle height

**Mobile**

accordion toggle: Minimum accordion toggle height is 88px. Accordion toggle can scale in height to fit the title while preserving the toggle padding.

### Important note for content editors

If you don't like the way your content displays in the vertical tab, you can display the same content as an accordion (across all breakpoints) or an infoBand instead. Each has its individual strengths and they are interchangeable and do not require re-entry of the content and assets into Drupal to change the display.

## Vertical spacing

**desktop:** 96px padding top and bottom

**mobile:** 64px padding top and bottom

## Responsiveness

The Accordion component is responsive and adapts well to various screen sizes and devices. It gracefully adjust its layout to provide an optimal user experience across desktop, tablet, and mobile devices. Maximum column spans for accordions have been defined for each breakpoint to ensure that the line length of the text held in the accordion has an accessible character length for reading.

## Functionality

- On page-load, by default all accordion sections are collapsed, displaying only their headers.
- Content editor can choose to have the initial accordion expanded if so desired
- When a user clicks on an accordion header, the associated content area expands, revealing the detailed information.
- Clicking on an already expanded accordion header collapses the associated content area, hiding the detailed information.
- ~~Only one accordion section can be expanded at a time (single-open behavior). Opening a new section closes any previously opened section.~~
- 11/20/23: Functionality for the build is different: accordion not self-closing on selection of another panel. User is in control to open and close panels at will

## Usage Guidelines

**When to use the accordion**

- **Long Lists of Information:** If you have a long list of items, such as FAQs, product features, or blog posts, an accordion can help users quickly find the information they're interested in without overwhelming them with a wall of text.
- **Detailed Content:** When you want to provide users with detailed information but don't want to overwhelm them with all the content at once, using an accordion can allow users to expand sections they're interested in while keeping other sections collapsed.
- **Step-by-Step Processes:** Accordion components can be helpful for breaking down complex processes into manageable steps. Each step can be a collapsible section, guiding users through a series of actions or decisions.

**Best practices for content organization**

- **Prioritize Content:** Place the most important and relevant information at the top of the accordion. Users should be able to find what they need without having to scroll through multiple sections.
- **Limited Sections:** Keep the number of accordion sections manageable. Too many sections can confuse users and defeat the purpose of a compact interface. Consider grouping related content together.
- **Clear Section Titles:** Use clear and concise section titles that accurately describe the content within. Users should have a good idea of what to expect when they click on a section.

**Content tips for accordion toggle titles**

- Use clear and concise header titles that give users a preview of the content within each section.

**Nested content**

Just because you can stuff content within a container, doesn't mean that is the best place for it. For example, a complex data table should probably not go into an accordion as a complex table will need to span 10-12 columns to be effective. Accordions should never be flowed into 12 columns at the desktop breakpoints as the line length of text is too long and not accessible.

## Interaction

- **Visible Cues:** The accordion toggle uses a "+" and "x" to indicate that a section is expanded or collapsed.  When a section is expanded, the symbol change to "x" to indicate that it can be collapsed. When it is open, it shows a "+" to indicate that it can be expanded
- **Animation of the icon:** The "+" icon becomes the "x" icon with a 45 degree rotation. However, we wish to rotate the icon on hover 360+45 degrees to provide extra affordance and a micro-interaction for the user interface
- **Single-Open Accordion:** By default, only one section can be open at a time. This prevents the interface from becoming cluttered and makes it easier for users to focus on one piece of content at a time.
- **Smooth Transitions:** Use smooth animations when expanding sections. Abrupt changes can be disorienting for users.
- **Abrupt close:** accordions snap shut instantly on-click /* tbd during dev. they may need to open and close at the same rate */
- **Default State:** All accordion panels are collapsed on page load.
- **Optional first accordion panel open:** Consider allowing the content editor to be able to set the first section of the accordion to be open by default. This helps users immediately see the content without having to click and would be the preferred state for introductory content such as content for the Programs pages.

## Accessibility

- **Keyboard Navigation** Users should be able to navigate and interact with the accordion using keyboard controls (e.g., Tab, Enter, Arrow keys).
- **Screen Reader Compatibility**  Accordion toggle headers and content sections are should be appropriately labeled for screen readers to ensure the content is accessible to users with disabilities.

## **Managing page weight and load times**

**Prevent Overloading:** Avoid loading all the *heavy* content within an accordion section at once, especially if the content is heavy (e.g., images, videos). Load content dynamically as the user opens a section to prevent slow performance. Text should load with the page- we are not doing skeleton load, we will really on OOTB Drupal behavior.
