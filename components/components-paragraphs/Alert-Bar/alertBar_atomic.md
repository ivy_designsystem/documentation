## Atoms

**Foundations / Text**

The text for xs-xl breakpoints is p.small and p.smallBold

[Token for p.small](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c0120001836112a/folder/6185b525ee411a3bbb978863?mode=preview)

The text for xxs breakpoint is p.xsmall and p.xsmallBold

[p.xsmall](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f9321382a1828526c9c5381?mode=preview) (scroll to the bottom)

**Foundations / UI **

Ivy / Foundations / Icons  / ui / [b](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)[tn_close24-default](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

Ivy / Foundations / Icons / ui / [b](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6185888cee411a88889767e0/tab/Ivy%20/%20Foundations%20/%20Icons%20%20/%20ui%20/%20btn_close24-default)[tn_close24-h](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6185888cee411a88889767e0/tab/Ivy%20/%20Foundations%20/%20Icons%20%20/%20ui%20/%20btn_close24-default)[over](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6185888cee411a88889767e0/tab/Ivy%20/%20Foundations%20/%20Icons%20%20/%20ui%20/%20btn_close24-default)

![1727710746951](image/alertBar_atomic/1727710746951.png)
