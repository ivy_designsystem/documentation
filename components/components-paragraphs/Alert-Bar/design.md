## Intro

**The Alert Bar is a message bar that appears above the header as needed to communicate an urgent or timely message to the University community and site visitors.**

![1727710658109](image/design/1727710658109.png)
