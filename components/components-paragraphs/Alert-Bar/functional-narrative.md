## Functional Description

**The Alert Bar is a message bar that appears above the header as needed to communicate an urgent or timely message to the University community and site visitors.**

Examples of timely or urgent messages include impending storm, the report of hazardous conditions, etc.

### Status

By design, it was not desired to communicate status or degrees of emergency. Ie: emergency, urgent, resolved etc. Thus, there is no color coding or other indications of status level.

There is a **mobile version** and a  **desktop version** . Mobile is shown from 0-767. Desktop is shown from 768-S and larger.

### Data

**Mandatory data types**

 **Alert topic: ** (This is the 'as short as possible' nature of the alert. It should be **inlined-bold 800** weight)

**Contextual link** (link for more information. Has an internal link style. See Contextual Link below)

**Optional data type**

**Message/optional description:** (regular 400 weight, the body of the announcement)

This is optional because it could potentially be covered by the contextual link and Alert Topic.

![1727710388119](image/functional-narrative/1727710388119.png)

**Potential future implementation**

**Type:** This is in the data. It's an emergency level. We will keep this for future possible implementation where a degree of alert gets communicated but for now all Alert Bars will have the same level of urgency.

### Contextual link and states

Since the Alert Bar should communicate the nature of an alert in as few words as possible, it is essential that a link is provided where site visitors can read more and get the full story. This is handled by a contextual link, usually with the label:

More information

**Link State**

The link state is the same darkGray color as the rest of the text but with a border-bottom / underline. The rule should be 1px.

**Hover / on-tap state**

The text and link changes in color to black. It's subtle but on Discovery, many Alert Bars and (similar) GDPR consent components do not have hover states.

![1727710407664](image/functional-narrative/1727710407664.png)

### Close icon

On-click / on-tap of the Close Icon, the Alert Bar closes.

The Close icon has a link and a hover/on-tap state.

![1727710434295](image/functional-narrative/1727710434295.png)

### Visibility

The Alert Bar stays open until the user closes it. If the user scrolls down the page and the sticky header gets triggered, the Alert Bar remains as a sticky Alert Bar above the sticky header until the user manually closes the Alert Bar by clicking on the Close button.

### Arrow icon

Note: The Arrow icon which in an earlier iteration followed the link text, **has been removed. **The reasons being:

* If the link is contextual and not at the end of the text string, it should not have an arrow.
* If the link is contextual but we left the arrow at the end of the text string, then it would make sense to also link the arrow.
* For the above reasons, the UX could not be clear and the production is fussy for something that should not be fussy.
* If there is an emergency, this alert bar needs to go online without an fuss!

### Text

**Text size**

Note that the xxs breakpoint uses p.xsmall and the other breakpoints use p.small.

**Line length **

To keep the text comfortably readable, the width of the text block adapts responsively. Here is a table that shows the width of the text block by columns on the grid / breakpoint.

![1727710451204](image/functional-narrative/1727710451204.png)

Text alignment

Text is aligned flush left for all breakpoints.

Text centers vertically within the Alert Bar.

Text itself has a center aligned text setting.

![1727710467995](image/functional-narrative/1727710467995.png)

This center-vertical alignment is important for the scalability of this component.

### Scalability

**Background**

The background fill extends the width of the viewport outside of the breakpoint width

**Content**

The component and its content stays within the fixed page width including the margins built into the Alert Bar component.

### Desktop Version

![1727710484527](image/functional-narrative/1727710484527.png)

### Mobile Version

![1727710498007](image/functional-narrative/1727710498007.png)
