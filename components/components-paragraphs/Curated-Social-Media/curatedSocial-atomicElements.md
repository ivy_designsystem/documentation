## Atoms

**Social property icons**

Ivy / Foundations / Icons / social


**Kicker**

Ivy / Components / Atoms / kicker


**Button**

Ivy / Components / Atoms / Buttons / Buttons / button neutral


## Molecules

**Swiper button**

Ivy / Foundations / Icons / UI-controls / swiperBtnPrimaryDarkOnWhite



**You need these:**

swiperBtnPrimaryDarkOnWhitePrev-default

swiperBtnPrimaryDarkOnWhiteNext-default

swiperBtnPrimaryDarkOnWhitePrev-hover

swiperBtnPrimaryDarkOnWhiteNext-hover



**Social Teaser Caption**

Ivy / Components / Type / social teaser caption
