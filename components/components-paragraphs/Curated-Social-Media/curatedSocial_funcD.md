## Overview

**The Curated Social Media component's purpose is to raise awareness about our social presence and tease users via curated social content to click-through to visit a select short list of UVM's social properties.**

![1727711421615](image/curatedSocial_funcD/1727711421615.png)

### Data

**01 Kicker**

Categorical title for this component. Semantically, it should be a headline.

Editable text. Plain Text. ~60 characters, not enforced.

**02 Social icon group**

Curated group of social icons. Each links to their respective property. Content admin should be able to:

Choose from a list of properties. Content admin adds property URL and handle for each.

Properties will be intentionally curated. Ie: We may be linking to the College of Education and Social Studies instagram OR UVM's instagram. It depends on desired content, what's relevant and content strategy and where the Curated Social Media component is being placed contextually within the universe of UVM's websites.

**03 Post image**

Manually curated. 1080x1080 with Drupal scaling down depending on breakpoint.

* Image links to the related POST.
* Link and image manually curated.

**04 Caption group**

Caption group is composed of:

1. Social icon
2. Teaser caption
3. Social property handle

*The social icon* does not link. It is a property identifier.

*The Social handle* (not mandatory) is the handle for the social property related to the post teaser shown. It links to the top level landing page for that social property. Link to be added by the editor.

*The caption* is plain text. It is related to the image and links to the same post as the image. No wysiwyg. Maximum character count should be ~100 including spaces. (we don't want it to go more than 3 lines @xxs-320 breakpoint)

**Note about the social handle**

Is there are way to store the social handles so the content admin doesn't have to hard code the handle every time a teaser is added? Functionality and need to be discussed.

![1727711438203](image/curatedSocial_funcD/1727711438203.png)

**05 swiper**

The component appears as a swiper on mobile.

The Btn_swiperNext shows when there is something to swipe to see next.

The Btn_swiperPrev shows when there is something to swipe to see previous.

Both buttons show if there is something to see next and previous.

It's a fluid, user controlled swipe. No attempt will be made to "snap" a post into place.

On mobile, the user can "scrub" by swiping with finger or tapping the arrow button.

**06 button**

Link: is manual input.

For the home page launch, the Button links to the the comprehensive social media directory for the University of Vermont.

[https://www.uvm.edu/uvmnews/social-media-directory](https://www.uvm.edu/uvmnews/social-media-directory)

This button is optional.

### Quantity

Mandatory quantity of 3 post teasers.

They have to link to different posts but there is no restrictions as to properties. They could all link to different Instagram posts, for example.

### Caption Group

**Caption Content Tips**

The caption is a shorter, skillfully crafted teaser to encourage users to click through to read the full story. It should not be the same text that is in the post--

* It needs to tease
* It should deliver 'confidence in the click/tap' in that it should be relevant to the post the user is clicking through to.
* Clicking through should deliver extra value to the user-- it should be worth the effort to click.

### Variations of the caption

![1727711452476](image/curatedSocial_funcD/1727711452476.png)

### Breakdown and description of caption elements

**Social Icon**

Content creator should not have to upload the social icon into the Drupal admin area. Social property for post to be shown should have associated icon. Instagram icon to appear with Instagram post teaser. Twitter icon to appear with Twitter post teaser etc.

**No icon**

No icon will be shown if we don't have one to display. In that scenario, the caption text only will display. It will go full width of the social post image. This scenario can happen if a new social property is introduced.

**Handle**

Handle is optional. If included, it should be to the TOP level of the social property page- not a duplicative, hand-coded visible link to the post.

Here it is in context, in the Curated Social Media block at a desktop size.

### Responsive notes

xxs-320  Swiper

xs-480.    Swiper

s-768  No swiper, all 3 visible at once

m-1012 No swiper, all 3 visible at once

L-1280 No swiper, all 3 visible at once

XL-1440 No swiper, all 3 visible at once
