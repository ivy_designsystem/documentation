## Intro

**The Curated Social Media component's purpose is to raise awareness about our social presence and tease users via curated social content to click-through to visit a select short list of UVM's social properties.**

![1727711201098](image/curatedSocial_intro/1727711201098.png)
