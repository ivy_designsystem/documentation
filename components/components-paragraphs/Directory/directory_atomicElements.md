## Atoms

**Chevron icon**

Ivy / Foundations / Icons /

**Text link with chevron**

Ivy / Components / Atoms / [text button with chevron right](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393b45fa8eb003116191aa8/tab/functional%20narrative?mode=preview)  (scroll to the bottom of the page)

## Molecules

**Directory row**

Ivy / Components / Molecules / [directoryRow](https://www.figma.com/design/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=4185-12674&m=dev)

**Link Group**

Ivy / Components / Molecules / [Link list icon after](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393afe4f291f20ef5204390/tab/functional%20narrative?mode=preview)

The link group with the chevron icon at the end is the same at the link group with the right arrow icon at the end-- just swap out the arrow icon for the chevron.

*Also see the Links tab above.*
