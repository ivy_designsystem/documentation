# Overview

**The Directory component is used to display categorized lists of links. Directory lists can be used for a collection of downloadable assets or as orienting links to help users navigate to specific content elsewhere on the site.**

## Data

The Directory component can have either a link list or a wee paragraph with an optional link. The content types can be mixed and match across columns.

**Mandatory if using the LinkGroup**

* Link text: Simple text for each link with an associated URL
* Character count for link text: within reason, recommending not to exceed 11 words or 56 characters.
* Maximum number of links per each link group (column): 8
* Link group title: Simple text, editor's discretion. Recommending not to exceed 11 words or 56 characters.

**Mandatory if using the text description and optional link**

* Single paragraph: Simple text
* Optional link text: Simple text for each link with an associated URL
* Character count for link text: within reason, recommending not to exceed 11 words or 56 characters.
* Maximum number of links per each link group (column): 8
* Link group title: Simple text, editor's discretion. Recommending not to exceed 11 words or 56 characters.

**Optional**

* Directory title: Simple text, editor's discretion. Recommending not to exceed 11 words or 56 characters.

## **When to use**

* Use the Directory component when you have many links with categorical groupings.
* Compare to the [Tile group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63befc25c887bd4d53f5e64c/tab/design?mode=preview) which is an elevated link with one link per tile, or the [Link Table](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63c03b231f9a351229fa169c/tab/design?mode=preview), which holds a lot of links but with no ability to categorize into subgroups.

## **Variants and states**

* Single row with 3 link groups, 3 descriptive paragraphs with optional link, or any combination thereof.
* Up to 3 rows with 3 link groups
* Note: the Directory title (H2) is optional but the link group title is mandatory for each link grouping.

### 01 Image scale test

I am not able to scale the image in the markdown the way one could possibly do this in GitHub. The University works with GitLab so the repo is in GitLab and it doesn't work the same way.

This image is wider than the ZeroHeight content container and placed in the markdown.

![1726064397092](image/directory_funcD/1726064397092.png)

^ Directory, single row, showing the hover state for the first link on the left.

### Image 02 
I scaled this image down prior to uploading into the MarkDown.
![alt text](image/directory_funcD/1726064397092-copy.png)

### 03 Image scale test
Scale image with regular HTML image src.

### 04 Jordan's idea
As soon as I add teh {width =""} at the end, the image does loads but does not scale. It prints the {image... } code instead as text.
![1726064397092](image/directory_funcD/1726064397092.png "alt text") {width="300"}



**Background options**

The Directory component can have either a neutral or a white background.

There can be no zebra striping within a single Directory component. The background of the component is uniform whether there is a single Directory row or multi.

**Anatomy**

![1726065351490](image/directory_funcD/1726065351490.png)

![1726065372946](image/directory_funcD/1726065372946.png)

![1726065656693](image/directory_funcD/1726065656693.png)

![1726065614633](image/directory_funcD/1726065614633.png)

# Component with multiple Directory rows

The Directory component can have multiple Directory rows. It is important to note that there can always only be one component title (the H2) and that if multiple Directory rows are used, they do not get their own title- although each column- linkList - does have a mandatory heading.

Drupal content admin will need to place multiple Directory components on the page if they require the H2 title to head the component.

## Spacing between multiple directory rows

**Desktop: **48px

**Mobile: **32px

![1726065586184](image/directory_funcD/1726065586184.png)

![1726065527830](image/directory_funcD/1726065527830.png)

**Accessibility**

* Follow semantic hierarchy for applying heading styles to titles. The Directory title will most likely be an H2 and the LinkGroupTitle will most likely be an H3.
* The default link state is underlined to communicate to users that it is a link.

## Responsive notes

For all desktop views, the Directory component has 3 equal columns with a 32px gutter between each.

For all mobile view, the Directory component has just one column- the columns responsively stack on mobile.
