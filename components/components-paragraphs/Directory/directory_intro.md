## Intro

**The Directory component is used to display categorized lists of links or a short sentence with an optional link. Directory lists can be used for a collection of downloadable assets or as orienting links to help users navigate to specific content elsewhere on the site.**

![1727712066203](image/directory_intro/1727712066203.png)
