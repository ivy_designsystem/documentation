### [Handoff, directory row](https://www.figma.com/design/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=4185-12674&m=dev)

Master, row of link groups

https://www.figma.com/design/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=4185-12674&m=dev


### [Handoff, Directory](https://www.figma.com/design/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=3690-120&m=dev)

Directory component master

https://www.figma.com/design/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=3690-120&m=dev


### [Individual Link List item](https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3118-2658&m=dev')

This is a text button with an icon right: a text string with a chevron icon at the end, inline after the last character

https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3118-2658&m=dev


### [Link List Group](https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3118-4049&m=dev)

This is a collection of text links with the chevron icon at the end, inline after the last character in the text string

https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3118-4049&m=dev
