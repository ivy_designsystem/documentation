## Molecules

### **Facts Swiper Group**

**factSwiperGroup-onDark**

**factSwiperGroup-onLight**

Ivy / Components / Molecules/ [Fact Swiper Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/634ebe016a1a8fbee36bb449/tab/design?mode=preview)


### **Swiper Button Group**

**SwiperBtnGroup**

Ivy / Foundations / UI / Controls / [SwiperBtnGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/634ed1d3e86ff664e9c25b05/tab/design?mode=preview)


## Atoms

### Swiper Button

**SwiperBtn**

Ivy / Foundations / UI / Controls / [SwiperBtn](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/634ecc7906533d0b1d6f0137?mode=preview)
