## **Intro**

**The "Fact Bar" displays a group of 3+ short facts in a band the width of the page- the background color extends full browser viewport.  It provides a "UVM by the numbers" perspective by elevating fact bites consisting of a number and a short description. The content is dynamic and served from the shared file server.**

![1726066779341](image/factBar_cover/1726066779341.png)

![1726066784510](image/factBar_cover/1726066784510.png)
