## *Intro*

**The "Fact Bar" displays a group of 3+ short facts in a band the width of the page- the background color extends full browser viewport.  It provides a "UVM by the numbers" perspective by elevating fact bites consisting of a number and a short description. The content is dynamic and served from the shared file server.**

NOTE: only building out for 3 facts at this time- swiping only on mobile.

![1726066779341](image/factBar_cover/1726066779341.png)

Above: the 3 color options for the Facts Swiper component. Functionality is the exact same for each- just the color varies.  Note: the shadow is added for this document only so the bounding box of the component is visible.

**Description**

This component can display an unlimited number of facts pulled from the shared file server facts database by category. The user navigates manually through the slider. The slider does not have an auto-play mode.

**Desktop**

When there are more than 3 facts being displayed, on desktop the SwiperBtnGroup navigation displays. The user clicks the previous / next navigation arrows to swipe the slider left/right to view all of the facts

**Mobile**

The SwiperBtnGroup navigation does not appear on mobile with users using a swipe interaction to view the facts in the slider. The 'swipe' interaction is a highly adopted pattern on mobile. However, the SwiperBtnGroup navigation arrows can be added later if analytics show low user engagement on mobile.

### When to Use this Component

Use this component when  you want:

* to display facts about the university

## **Inputs**

*See the Facts database on the shared content server.*

**Highlight:** number or very short short fact, 2-3 words, plain text

**Sub-highlight:** Long text, plain text, less than 122 characters.

**Fact Category:** This is not visible in the display of the table. The Fact Category is selected by the content editor to determine the feed to populate the table.

## **Functionality 768+**

Facts are displayed in single row with a slider interface.  Arrow navigation is visible for S-768+ when there are more than 3 facts. If only 3 facts, slider navigation arrows do not show.

**Maximum facts displayed:** none, determined by the number by category in the database.

**Minimum facts displayed:** 3

**Maximum recommended facts: **9

**Fact Swiper interaction **

* Clicking the left arrow  moves the slider to the right and reveals the next fact.
* Clicking the right arrow moves the slider to the left and reveals the next fact.
* ~The [current] number in the counter changes to reflect the active fact shown in the center.  ~ counter deleted 1/27/22
* The Swiper is not an infinite loop. The user will know the beginning and end limits of the collection of facts.
* When a user gets to the beginning of the facts slider, the PREVIOUS arrow is not visible
* When a user gets to the END of the facts slider, the NEXT arrow is not visible
* The Swiper does not have an auto-play, it requires user interaction to navigate
* SwiperBtnGroup navigation arrows have default and hover states.

### Functionality 320-767

Facts are displayed in slider/swiper.

**Maximum facts displayed:**none, determined by the number by category in the database.

NOTE: only building out for 3 facts at this time- swiping only on mobile.

**Minimum facts displayed: **3

The current fact is centered on the component with the previous and next facts “waiting in the wings” to the left and the right.

**Fact Band Swiper interaction **

* Swiping to the left moves the slider to the left and reveals the next fact.
* Swiping to the right moves the slider to the right and reveals the next fact.
* ~The [current] number in the counter changes to reflect the active fact shown in the center. ~ counter deleted 1/27/22
* The Swiper is not an infinite loop. The user will know the beginning and end limits of the collection of facts.
* The Swiper does not have an auto-play, it requires user interaction to navigate
* For resolutions above 768, SwiperBtnGroup navigation arrows are present. Swiper navigation arrows have default and on-tap states. (for mobile)

### Height of Swiper Component

The height of the Swiper component is determined by how much content is in the individual facts + the internal margins. The internal spacing is consistent (varies for desktop and mobile) but the content height is not. The bottom margin below the fact group is measured from the bottom of the longest fact.

### Design Elements

**Fact number**

**Fact description**

 **~Slider counter ~** ~(1011 and below) showing [active] | [total].~ counter deleted 1/27/22

**SwiperBtnGroup navigation arrows** (if more than 3 facts, 768-1011)

### Eyebrow heading

**Eyebrow**

Head that precedes the table but within the bounding box of the component and sharing the same background.

**Eyebrow width:**

The column width of the head varies by breakpoint to ensure comfortable reading length.

**Eyebrow Width on Mobile**

320-767 12 columns wide

**Eyebrow Width on Desktop**

768 S 12 columns wide

1012 M 8 columns wide

1280 L 7 columns wide

1440 XL 6 columns wide

### Color options

* Primary1 background
* White background
* Neutral background

Note: the Facts swiper is built already for the home page to be released in December, 2022. The additional swipers have no functional change, just a palette change.

Accessibility Alerts (for dev team)

Relying on widely adopted swipe pattern for mobile.  Should this present challenges, can add Swiper navigation arrows.

For the SwiperBtn arrows, the interface should be controllable by a keyboard and screen reader.

### Responsive Notes

Each Fact consists of a Fact Number on the top and the Fact Description below it. Type is flush left.

For more than 3 facts:

Mobile: The active fact is in the center and the previous and next facts are “waiting in the wings” on the left and right.

**For 3 facts:**

Starting at the S-768+ breakpoint, if there are only 3 facts to display, all 3 facts are visible at once. Swiper navigation arrows do not appear as are not needed to view all the facts.

 **For all resolutions 767 and below** :

The active fact is in the center and the previous and next facts are “waiting in the wings” and partially revealed, to the left and right.

Mobile does not have arrow navigation. Swiping on mobile is an intuitive, adopted navigation pattern. The interface has partial reveal of the facts "in the wings" on the left and the right... an indicator of the additional content the user will be delivered on swipe.

Here's a table that shows where the facts are partially revealed or not by breakpoint and when the SwiperButtonGroup controls appear.

### **Interactions / Transitions**

**mobile**

Looking for something short, smooth, perhaps smooth + ease out. Looking for a sweet gliding action- not a "snap into place". Quick but to give a pleasing interaction.

**Desktop**

On-click of the control arrow **slides to **the next FactGroup. Note that the next factGroup could have 1,2, or 3 facts. The initial factGroup on page load ALWAYS has, at minimum, 3 facts.

Motion on Desktop:

[Example. ](https://www.wundermanthompson.com/)Click no the arrows for the slide transition. It is near linear.

### Getting to the 'beginning' or 'end' of the facts

* The Swiper is not an infinite loop. The user will know the beginning and end limits of the collection of facts.
* The Swiper does not have an auto-play, it requires user interaction to navigate
* On page load, the swiper loads at the beginning.
* At the beginning, the PREVIOUS swiperBtn would be in and disabled state as the user would not be able to navigate to a previous fact.
* When the user gets to the END of the slider (last fact), the NEXT swiperBtn would be in an disabled state as the user would not be able to navigate to a NEXT fact.
