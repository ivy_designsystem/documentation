## Naming

**It's important that everyone knows what things are called and agree on the naming of things so we are all speaking the same language. That said, there are some slight variations in the file naming nomenclature depending on whether the component is in our design app Sketch or whether it is developed and implemented on the site.**

### Friendly Name

Fact Bar

### Design name

**Fact Bar**

factBar-OnLight--XXS, XS, S, M, L, XL

factBar-OnDark--XXS, XS, S, M, L, XL
