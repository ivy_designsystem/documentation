## Molecules

**I Think None Believe it or not!**

## Atoms

**Social Icons**

Ivy / Foundations / Icons / [Social Icons ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/6154d445848435131e2d512f?mode=preview)(

*Bottom of the page, you can download SVGs for both default and hover states*

**Map Marker icon**

Ivy /Foundations / Icons / Maps-Location/ [Map marker icon](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/6348766be86ff68e1dc126d4?mode=preview)

## Components

**SubFooter**

Ivy / Components / Organisms / Footer / [SubFooter](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63480a7e6a1a8f2b7c69f9f5/tab/design?mode=preview)
