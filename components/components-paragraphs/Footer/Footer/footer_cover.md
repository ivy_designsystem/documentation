# Intro

**Nothing fancy here. Single footer throughout the website (rather than multiple footers). Contains valuable evergreen information. The subFooter is a mandatory part of the website Footer and appears sandwiched at the bottom.**

![1726069572884](image/footer_cover/1726069572884.png)
