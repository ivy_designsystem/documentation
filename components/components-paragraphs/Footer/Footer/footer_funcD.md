# **Overview**

Single footer throughout the website (rather than multiple footers). Contains valuable ever green information that is not used for prominent in primary navigation (Diversity, Equity and Inclusion, for example)

**When to Use This Component**

A Footer appears on every page of the website. This component is the University's default footer.

## **Parts of the Footer**

![1726069416295](image/footer_funcD/1726069416295.png)

The Footer is composed of the upper footer and the [subFooter](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63480a7e6a1a8f2b7c69f9f5/tab/design?mode=preview)-- the entire organism is called "Footer". The subFooter can be used on its own, but the Footer *always* has the subFooter.

## **Inputs**

**Name: Heading**

**Type: Text**

**Required#: 3 (showing Location, Explore, Connect)**

**Description: Used to organize and group footer links and information**

**Name: Text links**

**Type: Text + Link**

**Required/#: One required for section to show, max within discretion**

**Description: n/a**

**Name: Social Icons**

**Type: Icons + Link**

**Required: Yes, at least 3, max 8. Let Analytics and social goals inform which elevate here.**

## **States, hover states**

Link text is #FFF @ 90% alpha. On hover the text goes to #FFF solid with a 1px solid white rule underline.

Gif is a little rough, but you can also look at the live footer at the bottom of the [UVM website.](http://www.uvm.edu/)

![1726069433295](image/footer_funcD/1726069433295.png)

**Map and Tours link**

The "Maps an Tours" text as well as the Map Marker icon link: https://www.uvm.edu/map/

## Functionality

**Links and Icons: **

* Link to respective pages, location map or social profiles

## **Differences by breakpoint**

**xxs-m**

* Content area is divided into thirds
* Social links are on row 02, c01 of the footer content

**L-XL**

* Content area is divided into quarters
* Footer content all fits on one row

**Social icons**

Desktop: icons are 32px

mobile: icons are 40px

## Character Counts

* More info links: up 20 characters is ideal. **Max 29 max** characters (keep at 1 lines)
* Headings: 1 or 2 words max

## **Mandatory Design Elements**

The FOOTER organism is comprised of 2, mandatory components:

* the upperFooter
* the subFooter

Location content

Social Links

All content in the subFooter is mandatory

Explore section (minimum 1, manually curated link)

## Optional Design Elements

Number of manually curated links under the "Explore" section is TBD. Only one link is required.

## **Accessibility Alerts (for dev team)**

Keyboard navigable.

## Anatomy with specs

Mobile: 320-767

Desktop: 768 - 1440 (background only scales above 1440)

![1726069488537](image/footer_funcD/1726069488537.png)

## Spacing

![1726069502080](image/footer_funcD/1726069502080.png)

![1726069520623](image/footer_funcD/1726069520623.png)
