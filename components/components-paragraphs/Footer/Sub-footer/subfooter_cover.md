# Intro

**The subFooter is a mandatory, global footer that is the base of all footers on the website. It can also be used as the sole footer on a page. It has the UVM logo which provides some brand identity, along with links to important legal content about accessibility, privacy, and terms of use.**

![1726069867117](image/subfooter_cover/1726069867117.png)
