# Overview

**The subFooter is a mandatory, global footer that is the base of all footers on the website. It can also be used as the sole footer on a page. It has the UVM logo which provides some brand identity, along with links to important legal content about accessibility, privacy, and terms of use.**

The subFooter is used globally on all UVM websites. It is a mandatory component in the Footer, can appear alone as a slim footer, or can be paired as a subFooter to unit/school - specific footers.

![1726069768568](image/subfooter_funcD/1726069768568.png)

**When to Use This Component**

This component is used at the foot of every page.

# **Parts of the Footer**

![1726069778973](image/subfooter_funcD/1726069778973.png)

The Footer is composed of the upper footer and the subFooter-- the entire organism is called "Footer".

# Logo

The logo is 40px high. It links back to the home page.

# **Data and Inputs**

**There is critical text content and links in the subFooter, including the Accessibility and Privacy / Terms of Use links. **

**Name: **Accessibility

**Type: **Text link

**Required#: **yes

**Description: **Links to the University's Accessibility policy

**Name: **Privacy / Terms of Use

**Type: **Text link

**Required#: **yes

**Description: **Links to the University's privacy / terms of use policy

**Name: **Copyright

**Type: **Text

**Required#: **yes

**Description: **Copyright indicia to indicate the website's content is under copyright.

**Functionality: **Date should not be hard-coded. Date should be coded so it automatically updates globally at the start of each new year.

# **Differences by breakpoint**

**xxs-s**

* Content is divided into 2 rows with the logo on one row and the text links below.

**M-XL**

* SubFooter content is centered vertically in the subFooter and is on one line.

# **Accessibility Alerts (for dev team)**

Keyboard navigable.

![1726069794546](image/subfooter_funcD/1726069794546.png)

![1726069805967](image/subfooter_funcD/1726069805967.png)

![1726069831341](image/subfooter_funcD/1726069831341.png)

![1726069838984](image/subfooter_funcD/1726069838984.png)

![1726069844651](image/subfooter_funcD/1726069844651.png)

Medium 1012 - xl 1440

The subfooter scales with the logo hugging left and the text group hugging right. For all breakpoints, there is a 32px margin L/R.

xxl

The subfooter, footer, header do not scale above 1440. Just the background scales full browser width with the subfooter centered.
