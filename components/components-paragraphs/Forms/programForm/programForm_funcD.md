# The Program Form component was designed as a request info form for the program pages.

Please see the [form](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4ab5829471d06bd2c5e/tab/design?mode=preview) and [form elements ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4a793ea317f903050bd/tab/functional%20narrative?mode=preview)documentation where the design and functionality of our forms are discussed more thoroughly.


# Purpose

The program form component was designed for users visiting the program pages to request more information about the program of interest. People hate filling out forms and studies show that every extra field or click decreases the likelihood of the form being completed. The goal of this form is to collect the interested user's essential information to be able to fulfill their request.

## Data

The Program Form was designed to collect basic information from users interested in getting more information about our programs. On

ly a handful of form elements were designed to fill this need. The form content is not prescriptive. Content administrators are free to work with the limited form elements that we are providing for their form.

Please see the [form](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4ab5829471d06bd2c5e/tab/design?mode=preview) and [form elements ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6501d4a793ea317f903050bd/tab/functional%20narrative?mode=preview)documentation where the design and functionality of our forms are discussed more thoroughly.

**Form elements include: **

* Form input: radio button
* Form input: text fields
* Form input: submit button

States are available for all to communicate status of engagement with the user.

## Visual form header

The form has a header area with an optional logo and a form title.

**Optional logo:** logo asset should be 256px wide or smaller. Logo should scale proportionally to 256px wide if content editor uploads something larger.

**Form heading:** simple text. Should be concise and short. No character limit provided.

## Visual design

The desktop and mobile versions of the actual form embedded in this component are nearly identical. They differ only by the size of the microcopy and the heading type size. The component itself has two variations on desktop.

**Visual overview**

![1726072606654](image/programForm_funcD/1726072606654.png)

^ on desktop, the form can have an optional background. For this variation, for form has an elevation.

![1726072615531](image/programForm_funcD/1726072615531.png)

^ The desktop version without the optional background. For this variation, the form does not have an elevation.

![1726072646276](image/programForm_funcD/1726072646276.png)

^ on mobile, there is no background, border or elevation.

And when it is placed within the component, there is a spacer to the left so the form itself spans the same column width as all the text. Here the different backgrounds are tinted to show the spacings and column spans. There is a 40px padding-left on the container of the form for desktop sizes medium and larger. This matches the padding that we have on all of our text containers at these breakpoints. That keeps the form matching the text width.


![1726072706044](image/programForm_funcD/1726072706044.png)

The alternate style of this component on desktop allows for a background image behind a 40% black scrim. Here is an example:

![1726072712622](image/programForm_funcD/1726072712622.png)

It is exactly the same as the solo form but has the additional background image and scrim. This gives our content editors the ability to add some brand interest to forms, to elevate them, and to give an option to include a form within a text-heavy page that draws some attention and disrupts the density on a text-heavy page.

This presents a measurable opportunity in analytics to see if the background increases form engagement and form completion.

Note that the form has an elevation when overlayed on an image.

## Form component spacing on desktop

For desktop breakpoints medium and larger, the text content container has a 40px left-padding. Ao as to align with the text, the form does also. Below shows how the intro paragraph, text in the vertical tab component and the form all have the same 40px left-padding.

![1726072770803](image/programForm_funcD/1726072770803.png)

## Mobile

The mobile Program Form component is just the form. There is no background fill, border or elevation.

![1726072792535](image/programForm_funcD/1726072792535.png)

## Program Form component in the context of a layout

Please see the [Program Form component in the layout comps for the program page](https://www.figma.com/file/PWTg6C6AQ9nvEuEBauEdIz/layouts?type=design&node-id=178-26391&mode=dev) and the documentation for the [program page layout.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64fb8c171579dd46a06fb142/tab/design?mode=preview)

Here are some examples.

![1726072843332](image/programForm_funcD/1726072843332.png)

^ This desktop-xl example has the Program Form with a background image appearing after the facts. Notice how the form spans the same width as the text-only content higher up on the page.


![1726072884236](image/programForm_funcD/1726072884236.png)

^ This desktop-xl example has the Program Form with NO background image also appearing after the facts. Notice how the form spans the same width as the text-only content higher up on the page.

![1726072905915](image/programForm_funcD/1726072905915.png)

^ This example shows the Program Form on a mobile-xs program page comp.

The mobile version of the Program Form is just the form- no background fill, border or elevation.
