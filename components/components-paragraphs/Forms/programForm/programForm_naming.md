# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'program form' component:

![1726073016445](image/programForm_naming/1726073016445.png)

The naming for the desktop breakpoints has a variant as the desktop component can have an optional image background.

There are no variants for the mobile version of the Program Form.
