# Atoms

**Rule**

1px dashed primaryLight

Ivy / Components / Atoms /[ Rules](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61894259a3371065670d2f07/tab/design?mode=preview) /

**Button**

Ivy / Components / Atoms / Buttons / Secondary Outline

[btn_secondaryOutline-light](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61ba3cea85ca0019a05c151a/tab/design?mode=preview)

**Shadow/Elevation**

Ivy / Components / Atoms / [Shadows](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61898459dc1e244502a50190/tab/design?mode=preview)
