# The GDPR Consent pop-up appears on-load of the UVM.edu website. It notifies site visitors that we use cookies to store user data.

![1726073151570](image/gdpr_cover/1726073151570.png)
