# The GDPR Consent pop-up appears on-load of the UVM.edu website. It is at the bottom of the viewport on every page until the user accepts it by clicking on the "I understand" button. It notifies site visitors that we use cookies to store user data.

![1726073186937](image/gdpr_funcD/1726073186937.png)

The following table shows the column span for the text in the GDPR Consent overlay by breakpoint. The larger the screen width, the less columns the text spans. This is to ensure a comfortable reading line length for the user at different viewing scenarios.

*Ignore the 2 right columns in the table, it's just character count notes that the designer used to figure this out.*

![1726073196860](image/gdpr_funcD/1726073196860.png)

## Data

**GDPR consent body copy: **

Simple text. Can have an inline contextual link. Our GDPR statement has a contextual link  to  UVM's Privacy Policy. In the example the link to the Privacy Policy is at the end of the statement, but it can be anywhere within the text.

Is not rich text.

## **Alignments**

M-XL: text is flush left. Button is flush right

xxs , xs: text and button are aligned at the 32px Left margin flush left

**Functionality**

Like a sticky footer, the page flows UNDER the GDPR consent. On click/tap of "I Understand" button, the GDPR closes. [Example](https://www.utas.edu.au/).

**Interactions**

**On Page Load**

The GDPR appears on-page load. It fades in 300-400ms and stays as a sticky footer until the user closes it.

To close the form, the user needs to consent to the notification that the site uses cookies.  There is no other option.

On-click of the consent button, the form instantly goes away. No dissolve.

**Privacy Policy Link**

On-click or on-tap of the privacy policy brings the user to UVM's Privacy Policy page.

## States

**Contextual link**

The GDPR text is white with an alpha, so it is a transparent, off-white. The contextual text link has a primary3 underline and on hover / tap, the text becomes 100% #FFF. Underline persists.

**Button**

The button is a white outline button on default/link state. The hover/tap state is primaryDark background with 100% #FFF white text label.

![1726073213415](image/gdpr_funcD/1726073213415.png)
