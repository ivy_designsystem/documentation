# The White Sticky Header is the slimmer, 'sticky' version of the White Overlay Header. This slimmer header replaces the header on-page-scroll below the fold. It is designed to take up less space while keeping the logo branding, and more importantly, the navigation and search icons, visible for users even after they scroll down the page.

## Description

The sticky header is an important navigation feature as it keeps the the search and navigation links visible as the user scrolls down the page. Because of the sticky header, users will not have to scroll back to the top of the page to navigate to launch the NavDrawer / Primary navigation.

## Functionality

On Scroll down, the rest of the page scrolls under the sticky navigation. The overlay header in its entirety shows when the user scrolls back up to the top and reveals "above the fold" content.

* On-click / on-tap of the search icon launches a search take-over with a tabbed search switcher.
* On-click / on-tap of the hamburger icon, launches a take-over navigation drawer.

## Data

None.

## Accessibility

Some of our website users may use a keyboard to navigate our site. This header is "bare bones" and just has the UVM logo, a search button to launch a site search, and a hamburger icon to launch the main navDrawer.

Future versions of the header may have secondary CTAs, section-specific navigation or other UI elements. When coding, please ensure that the header is accessible and keyboard navigable. Please also take into consideration future scalability to that the addition of titling or other UI elements will not get in the way of keyboard users being able to "skip to navigation" without having to wade through obstacles.

## Responsive Notes

**Vertical positioning**

For all breakpoints, the header is a 0 on the y-axis unless the temporary site-wide alert is activated on the site.

**Horizontal scaling**

The header itself fits within the fixed page with just the background, as applicable, scaling to the full width of the  browser viewport.  The left edge of the logo is at 32px of the breakpoint width and the right edge of the SearchMenuIconGroup is at the right edge of the breakpoint width, leaving the 32px right margin to the right.

### Usage on the home page

Desktop:

On desktop, the sticky header overlays on top of the home page hero. The background is transparent revealing the hero below and the green background, which extends full browser width.

Mobile:

On mobile, this header appears above the home page hero and has a primaryDark background.

In order to provide an optimal experience, the logo and the searchMenuIconGroup vary in size, depending on breakpoint.

# **Design Notes**

The sticky header is slimmer in height than the website header. That said, the logo is a bit smaller than the logo in the website header. The search/menu icon group are the same size for ALL headers and should register in location.

## **Size variations**

The height of the sticky header is the same for all breakpoints: 56px high

XL, L, M, S breakpoints

* logo is on a single line
* logo height is 40px

xxs and xs breakpoints

* logo is stacked
* logo height is 44px

Note: The padding top/bottom is different for desktop and mobile breakpoints to maintain the uniform height of the sticky header. The logo height varies depending on whether it is stacked or not, so the padding must be different from desktop to mobile for the component height to remain consistent.

## Icon size differences

**SearchMenuIconGroup variation**

S,M,L,XL: 48px icon group

xxs,xs: 40px icon group

![1726081833601](image/header-sticky_funcD/1726081833601.png)
