# Atoms

**UVM Logo, white**

Components/Atoms/Logo/[White logo for overlay header](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/633739664b0bafb6a3af17c9/tab/design?mode=preview)

# Molecules

**SearchMenu icon group**

Components/Molecules/Header UI/[searchMenuGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/633af939133ea027a956cb9f/tab/functional%20narrative?mode=preview)
