# The White Overlay Header is intended to overlay on a dark background or image(s). It has the UVM logo and some UI elements to launch a search or the main navigation drawer.

## Description

The header is an important branding and navigation component for the site. The logo immediately identifies the website as belonging to the University of Vermont and the icon SearchMenu icon group on the right of the header gives user access to either search the site or launch the take-over main navigation drawer.

## Functionality

The UI element in the White Overlay Header is an icon group consisting of a search icon and a hamburger menu icon. Both have hover/ on-tap states.

On-click / on-tap of the search icon launches a search take-over with a tabbed search switcher.

On-click / on-tap of the hamburger icon, launches a take-over navigation drawer.

## Data

None.

## Accessibility

Some of our website users may use a keyboard to navigate our site. This header is "bare bones" and just has the UVM logo, a search button to launch a site search, and a hamburger icon to launch the main navDrawer.

Future versions of the header may have secondary CTAs, section-specific navigation or other UI elements. When coding, please ensure that the header is accessible and keyboard navigable. Please also take into consideration future scalability to that the addition of titling or other UI elements will not get in the way of keyboard users being able to "skip to navigation" without having to wade through obstacles.

## Responsive Notes

**Vertical positioning**

For all breakpoints, the header is a 0 on the y-axis unless the temporary site-wide alert is activated on the site.

**Horizontal scaling**

The header itself fits within the fixed page with just the background, as applicable, scaling to the full width of the  browser viewport.  The left edge of the logo is at 32px of the breakpoint width and the right edge of the SearchMenuIconGroup is at the right edge of the breakpoint width, leaving the 32px right margin to the right.

### Usage on the home page

Desktop:

On desktop, the header overlays on top of the home page hero. The background is transparent revealing the hero below and the green background, which extends full browser width.

Mobile:

On mobile, this header appears above the home page hero and has a primaryDark background.

In order to provide an optimal experience, the logo and the searchMenuIconGroup vary in size, depending on breakpoint.

**Logo variations**

XL, L, M breakpoints have a larger, single line logo

S breakpoint has a slightly smaller, single line logo.

xxs and xs breakpoints have a stacked logo.

*Note: The Medium - XL breakpoints use the same header. The only difference is it scales horizontally with the logo anchored flush against the left margin and the icon group anchored flush right against the right margin.*

**SearchMenuIconGroup variation**

S,M,L,XL: 48px icon group

xxs,xs: 40px icon group

![1726074460947](image/headerOverlay_funcD/1726074460947.png)

![1726074480038](image/headerOverlay_funcD/1726074480038.png)


![1726074496904](image/headerOverlay_funcD/1726074496904.png)
