# Atoms

Logo

Ivy / Components / Logos / [horizontal](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/627410102eebc0467530078d?mode=preview)

Ivy / Components / Logos / [stacked](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/627410f36561544014b28c48?mode=preview)

## Molecules

**SearchMenu icon group**

Components/Molecules/Header UI/[searchMenuGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/633af939133ea027a956cb9f/tab/functional%20narrative?mode=preview)
