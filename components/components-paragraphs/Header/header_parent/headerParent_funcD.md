# The header for schools, colleges, units, and departments is the same as the [overlay header](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6337385a6ac3f2c303dae1eb/tab/design?mode=preview) but with the addition of the name of the parent in the lockup.

# This header (headerParent) has a white version to overlay on a dark background or image(s) and a  primary1 version to go on white, nearly-white or neutral background. It has the UVM logo in lockup with the Parent name and some UI elements that launch a search or the main navigation drawer. Variations are to provide optimal sizing by breakpoint.

# **Anatomy of the header**

![1726074597658](image/headerParent_funcD/1726074597658.png)

![1726074610920](image/headerParent_funcD/1726074610920.png)

# Responsive notes

**All breakpoints**

* The logo is flush left with the content container respecting the 32px left margin
* The search/menu icon group hugs the right edge of the content container respecting the 32px right margin

**xxs, xs**

* The logo and school name are stacked.
* The search/menu icon group is smaller (40px high)
* The school/college name can wrap to multiple lines as needed based on character length

**s-m**

* Logo and school are not stacked
* The school or college name can go to 2 lines if needed
* The search/menu icon group are larger (48px high)
* The space around the vertical divider rule between the UVM logo and the school/college name is less than it is for the L / XL breakpoints

**s - xl**

* Logo and school are not stacked
* The search/menu icon group are larger (48px high)

**xxl**

* The header does not scale above the xl width

![1726074624927](image/headerParent_funcD/1726074624927.png)

# Data  /  Content

Logo: stacked and not stacked

Name of school or college: This can be database driven as it will be set in web-safe Times font, regular weight 400.

Note: we are trying for it to resemble the Arno used in our logo by using the closest web-safe font. It may need to be a 500 weight after it translates to the Drupal webspace. TBD during QA

## Design note about the search and menu icons

The size of the search and menu icons as well as the spacing between differs between mobile and desktop. Please take note and inspect the [search-menu icon molecule ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/633af939133ea027a956cb9f/tab/design?mode=preview)to get this right.

The grabs below show both icons in their hover states just to make the size of the icons with their bounding box noticeable. The icons, of course, would never show hover states simultaneously.

![1726074651479](image/headerParent_funcD/1726074651479.png)

![1726074658661](image/headerParent_funcD/1726074658661.png)
