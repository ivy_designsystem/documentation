# **The Hero Takeover is a hero banner that can override the home page hero to elevate timely content and drive visitors to more info about a timely event.**

**It has a big background that can either be a still image or a video spanning the full breakpoint width. Graduated dark scrims on the top and bottom assure adequate contrast for the header at the top and an H2 headline and secondary CTA tag at the bottom of the component.**

![1726084189041](image/hero-takeover_funcD/1726084189041.png)

**Functionality and purpose**

The purpose of this hero is to make a splash and communicate something timely in a big way. The text has a big H2 head with 1-3 words- nothing more- and a Call to Action. The hero-takeover's purpose is to:

1. Inform site visitors of something timely in 1-3 words
2. Drive users to a page on the site to find out more information about the event

# **Anatomy** of the text lockup

![1726084204678](image/hero-takeover_funcD/1726084204678.png)![1726084209263](image/hero-takeover_funcD/1726084209263.png)


**Data**

**MANDATORY** Data

**Background media**

Background media can be either a 6x4 image or UVM-hosted MP4 video. (Note: do we have more latitude with the image asset aspect ratio since we are using [Drupal&#39;s Focal Point Module](https://www.drupal.org/project/focal_point)?

6x4 Image: Image asset made to the largest size 1920x1280

Video: optimized and saved in MP4 video format. Expected behavior: Video to loop and rely upon pause / play icon for user control.

**Cropping around focal point**

User to define focal point of media. The Drupal container will mask the media dynamically keeping the focal point in the center while filling the component container.

**Editing needs within the Drupal UI**

* Content editor needs to select the focal point of the image.
* https://www.specbee.com/blogs/focal-point-module-in-drupal-8

[Drupal Focal Point Focus](https://www.drupal.org/project/focal_point_focus)

[Demo video ](https://www.youtube.com/watch?v=VomfkogYOjc)from Evolving Web, Montreal

**H2 head**

Simple text. Up to 24-30 characters, including spaces. 1-3 words only.

Good examples of copy for the H1 head include:

1. Commencement weekend
2. Welcome class of 2026
3. Don’t miss...  (secondary tagline and CTA to fill in)
4. Homecoming week
5. March Madness

**Call to Action (CTA) text**

The call to action text is simple text with mandatory URL.

It is the only think that links in the takeover hero. The **hit state** is the entire button- not just the text or the arrow.

**OPTIONAL** Data

**p.short string of text**

The short string of text preceding the mandatory CTA is optional. It is intended to be a date or a few words.

Date format planned for is

Month DD, YEAR

Month DD-DD, YEAR

September 22, 2923

September 22-31, 2023

The secondary text group (short string + CTA with right arrow icon) should not wrap to 2 lines in any breakpoint so a container / character constraint will need to be in place.

**Divider**

The divider slash '/' is mandatory if the p.short string of text is present.

If not, then it should not show.

# Type faces used

H2: font used is [Playfair Display at 800 weight from Google Fonts](https://fonts.google.com/specimen/Playfair+Display).

p.Text is Roboto Sans, bold 800

All type is flush left.

Type has some type size variation by breakpoint. This table is a reference for the type sizes by breakpoint.

![1726084234247](image/hero-takeover_funcD/1726084234247.png)

Type scales from the bottom up. The distance of the base of the text group to the bottom of the component varies by breakpoint as follows.

![1726084247143](image/hero-takeover_funcD/1726084247143.png)

**Pause / Play video button**

The background. If video is used, there is a pause/play button in the lower right for desktop breakpoints and lower left for mobile breakpoints. See components in Figma.

^ desktop: pause icon to the bottom right

![1726084258775](image/hero-takeover_funcD/1726084258775.png)

^ Mobile: pause icon below the text

The larger component above is the heroTakeover small breakpoint with video. The smaller grab above is the heroTakeover with video at the xxs breakpoint. Note that the pause icon is to the bottom right for all sizes Small and larger and below the text lockup for mobile breakpoints.

# **Image Assets **

**Background image**

**6x4 image**

Asset made to the largest size 1920x1280

* One source image for all breakpoints
* Container can have a background mage
* Or, an MP4 video
* Media element is masked by the hero container

**Background Video**

**MP4 video, optimized**

* Video is 16x9 but will be masked by the hero container

**Ensuring adequate contrast for type**

Two graduated dark scrim bands overlay the image to ensure the type and logo overlays meet WCAG 2.0 accessibility guidelines. One is on the top so the logo contrast is good and the other is on the bottom so the H1 head has adequate contrast. These scrim overlays vary in height by breakpoint though the top one hugs the top at y-0 and the bottom one hugs the bottom of the image container. They are added programmatically and appear at all breakpoints.

**Engineer note**

Unlike the hero, the scrim on the bottom goes to 90% black rather than 100% black.

Shown here is the xxs-320 breakpoint

![1726084269417](image/hero-takeover_funcD/1726084269417.png)

with a static image, xxs

# **Dimensions of Hero**

**The container holds the background image. ***It’s a takeover of the home page hero. The width and height (bounding box or footprint) of the hero takeover matches the home page hero for all desktop breakpoints. It overlays – or takes over – the home page hero.*

**width:** 100% width of the breakpoint. Does not extend to full viewport.

**height: **Is percentage of the viewport.

The design files in Figma are getting reverse engineered now to match what happened on the dev end and got pushed out to the live site. Here’s how it has been coded:

Match what is used in the home page hero. It’s closely based on the following:

![1726084281080](image/hero-takeover_funcD/1726084281080.png)

The heroTakeover is 84% of visual height at the xxs and xs mobile breakpoints. The goal is for the H2 headline to be above the GDPR when visible.

he background extends full browser width and is primary1.

Engineer note

It is designed for the home page hero takeover. It is based on the HERO component with a few differences. This component has a CTA that the Hero doesn’t. Also, the text component is 32 px from the breakpoint edge on the left, so the text alignment is different than the hero.

# **Transition and interaction**

On page load:

* Media instant load
* H2 title in lockup with p. [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)
