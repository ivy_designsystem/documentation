# Atoms

**Image**

6x4 static image

Ivy / Components / Atoms / Media / [6x4 image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61391f9a5c82e474e3192fff/tab/design?mode=preview)

**Pause/Play button for video version, GHOST flavor**

Ivy / Foundations / Icons / UI / [pause play ghost](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

# Molecules

 **Type Group: heroDisplaySerif**

Ivy / Components / Molecules / Type Groups / [Hero display serif](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/638a595a1efecb2204f21da8/tab/design?mode=preview)

**Important developer note! **

*The paragraph of text in the "hero display Serif" text group molecule is not appearing in this component. I have the visibility of the paragraph text turned off. *
