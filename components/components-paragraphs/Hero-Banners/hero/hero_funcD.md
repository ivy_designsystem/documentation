# The Hero component is a utilitarian hero with a big media element and some text. Gradated dark scrims on the top and bottom assure adequate contrast for an H1 headline or page title as well as the website header. It can have a still image or a video.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOUVwcmgM_OAaDrJ0agko13DyW3gG_Bo8CiLmNyDtd_tWTxMHfRfIdH_mzqSmZYPTSAb7tKSbAbGbN0WoLZbdh01RUxuO2dgv3aFwA-tNnx6VyPrpl17LK81DRfaD_TRZ4w==?assetKey=university-of-vermont%2Fivy%2FheroImg-xxl-HyTMOiD9R.png)

![1726083976624](image/hero_funcD/1726083976624.png)

***Engineer Note**** This component is closely related to the hero story scroll. It is simpler. It has less content types is simpler and doesn't have a parallax scroll interaction.*

# Functionality and purpose

The purpose of this hero is to communicate with the large background image or video and H1 title what the page is about. The desired action is for the users to consume more content by scrolling down the page to read more.

# Data

**MANDATORY**

**6x4 image or UVM-hosted MP4 video**

6x4 Image: Image asset made to the largest size 1920x1280

Video: optimized and saved in MP4 video format. The Drupal container will mask.

**H1 head**

simple text. Up to 24 characters, including spaces. 1-3 words only. (is this realistic?)

# Type

H1: font used is [Playfair Display at 800 weight from Google Fonts](https://fonts.google.com/specimen/Playfair+Display).

Note: 800 is the bold weight for all fonts.

All type is flush left.

Type has some type size variation by breakpoint.

![1726083998693](image/hero_funcD/1726083998693.png)

![1726084010846](image/hero_funcD/1726084010846.png)

*Note: all breakpoints can accommodate video media. If video is used, there is a pause/play button in the lower right. See components in Figma. *

# Transition and interaction

On page load:

* Media fades on
* H1 title [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

# Image Assets

**Background image**

**6x4 image**

Asset made to the largest size 1920x1280

* One source image for all breakpoints
* Image will be a container with a bg image.
* Container can be set to **88%vh** with the width set to the full breakpoint (not viewport)

*Note: We will determine value of the visual height during the QA process. So please make sure that we will be able to adjust that value to achieve the best UX. *

**Background Video**

**MP4 video, optimized**

* Video is 16x9 but will be masked by the background container

# Ensuring adequate contrast for type

Two graduated dark scrim bands overlay the image to ensure the type and logo overlays meet WCAG 2.0 accessibility guidelines. One is on the top so the logo contrast is good and the other is on the bottom so the H1 head has adequate contrast. These scrim overlays vary in height by breakpoint though the top one hugs the top at y-0 and the bottom one hugs the bottom of the image container. They are added programmatically and appear at all breakpoints.

The scrim on the bottom goes to 100% black.

### This is the CSS for the overlay scrim that goes on the top and the bottom:

background: linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(38,41,44,0.804359243697479) 34%, rgba(255,255,255,0) 100%);

Shown here is the xxs-320 breakpoint

# Dimensions of Hero

**The container holds the background image. **

**width:** 100% width of the breakpoint. Does not extend to full viewport.

**height: **will be a percentage of the viewport. Start with 88%. We want the user to see a bit of what is below rather than a takeover of the  entire viewport.

# Editing needs within the Drupal UI

* Content editor needs to select the focal point of the image.
