# Atoms

### Pause and Play buttons

Foundations/Icons/[UI](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

You can download SVGs of the pause and play buttons for both desktop and mobile and the different states.

Note: The Home Page Hero uses the WHITE versions.

# Molecules

### Header

The header is placed over the home page here and registers at the top of the page, centered over the hero.

Ivy/Components/Header/[**03 header white overlay**](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6337385a6ac3f2c303dae1eb/tab/design?mode=preview)

### Progress Bar Tracker

Components/molecules/progress trackers/[hero progress bar](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63347fb84b0bafd84fae0e61/tab/design?mode=preview)
