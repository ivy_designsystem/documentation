# The animated Home Page Hero component is singular to the home page. It is designed to emphasize UVMs strategic imperatives through highly stylized and emotive photography supported by short messaging, text statements.

It presents a passive animated experience with the user able to stop it at any time or replay when reaching the end. The Hero is comprised of 4 screens with an animated transition between each. Positioning statements for each slide form the take-away messages.


![1726084357893](image/hp-hero_cvr/1726084357893.png)

![1726084371400](image/hp-hero_cvr/1726084371400.png)
