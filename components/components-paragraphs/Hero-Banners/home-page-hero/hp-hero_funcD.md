## The animated Home Page Hero component is singular to the home page. It is designed to emphasize UVMs strategic imperatives through highly stylized and emotive photography supported by short messaging text statements.

![1726084412303](image/hp-hero_funcD/1726084412303.png)

## Description

The home page hero has 4 "slides" with an animated transition between each. Positioning statements for each slide form key take-away messages. .

Three of the slides have different image layouts consisting of the same number of images with equal aspect ratios. The last slide has only one image that fills the entire bounding box (fixed page width including margins) of the hero. All slides have a type lockup with a large, serif word (or two) and a smaller, sans serif, uppercase, supporting subhead below.

The hero presents a passive animated experience with the user able to stop it at any time by clicking/tapping on the Pause button in the lower right. The Pause button changes to a Play button on slide #4 and the user can choose to replay the Hero experience when reaching the end.

On desktop, the Header overlays the hero.

On mobile, the Header appears above the hero on a primaryDark background, matching the background color of the hero itself.

## Scalability

The background goes full view-port and is primaryDark. The Hero image layouts are all within the fixed page width.

## When to use this component

This component is reserved for use on the home page only. It is mandatory content for the UVM.edu home page.

## Approved Prototypes

[desktop](https://www.figma.com/proto/Dg1JZAnpaag5eSblXwpdPy/websiteRedesign-2.0?page-id=659%3A25745&node-id=1017%3A40403&viewport=698%2C268%2C0.02&scaling=min-zoom&starting-point-node-id=1017%3A40403&show-proto-sidebar=1)

(Hero to be self-playing but just for the prototype, click on the pause button to advance to the next slide)

[mobile ](https://www.figma.com/proto/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?page-id=659%3A25745&node-id=810%3A32905&viewport=392%2C650%2C0.32&scaling=min-zoom&starting-point-node-id=810%3A32905&show-proto-sidebar=1)

(Hero to be self-playing but just for the prototype, click on the right edge of the hero to advance to the next slide)

*Note: In FIGMA, option+/ will toggle the Figma UI on/off.*

# Mandatory Design Elements

**Desktop**

* Positioning Word
* Positioning Body
* (1) Larger vertical image
* (1) Medium square image
* (3) small square images
* (1) large, breakpoint size image for final “slide”
* UI: Pause / Play button
* UI: Progress bar

**Mobile**

* Positioning Word
* Positioning Body
* (4) small square images
* (1) large, breakpoint size image for final “slide”
* UI: Pause / Play button
* UI: Progress bar

### Non-Mandatory Design Elements

None

# Data

There are (4) slides to transition between.

Additional slides: We are not planning for scalability here- 4 slides. None more. None less.

**DESKTOP Data Each slide had the following UI and design elements**

* Positioning Word: one or two words maximum
* Positioning Body: Not more than 21 characters including spaces.
* (1) Larger vertical image (1/3 x2/3 of breakpoint/fixed page width)
* (1) Medium square image (size of 4 smaller square images combined)
* (3) small square images
* (1) single, large, breakpoint size image for final “slide” on 4th screen
* (1) pause/play button

**MOBILE Each slide had the following UI and design elements**

* Positioning Word: single word* (same content as desktop)*
* Positioning Body: Not more than 21 characters including spaces.* (same content as desktop)*
* (4) small square images
* (1) pause/play button

## Images

### Image sizes

Images will be prepped at the largest size needed and Drupal will scale the images down responsively. The entire hero grid scales proportionally.

**Square images**

*The largest square image* is 384x384 at the XL-1440 breakpoint.

*The smaller square images* are each 192x192 @ XL-1440

*The large vertical image *is 480x960 @ XL-1440.

1440-XL is the maximum breakpoint.

### Image assets

Image assets are named by slide, column and row. The large vertical image is always 1/3 the width of the breakpoint. This one is not considered in the column/row naming at all. The remaining 2/3rd of the hero is divided up on into 5 equal columns and 5 equal rows. That division forms the 5x5 grid that the naming of the square images reflect.

*Note: The columns get disrupted by the large vertical image. Even so, just omit the larger vertical image and count the columns left to right. *

![1726084447423](image/hp-hero_funcD/1726084447423.png)

![1726084451773](image/hp-hero_funcD/1726084451773.png)

![1726084456284](image/hp-hero_funcD/1726084456284.png)



The square image assets have a naming convention to help you identify which slide, column and row the image belongs to.

## Ensuring adequate contrast for type

Content creators will be responsible to work with the Scrim-dark to prep the images for the hero to add a darker gradation overlay on the images in areas where type or UI elements appear on top of the images. **The scrim overlay will not be added programmatically. **It will be up to the content creator to ensure that there is adequate contrast for type and UI to meet WCAG 2.0 accessibility guidelines.

Components/Atoms/Box/[scrim-dark](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6335aaec861ffd0dbfca8f90/tab/design?mode=preview)

## Editing Needs within the Drupal UI

Content editors need to be able to change out individual images at will.

The desktop version has 5 images- 4 square and a single, larger vertical image. The vertical image does not show on the mobile versions (below 768pixels). Just the 4 square images will show at all breakpoints.

Images scale proportionally between breakpoints so editor can upload the largest size for each aspect ratio and let Drupal scale.

LAST Slide / Slide #4

The last slide always only has a single image. The height of this image is 2/3 of the breakpoint width. For example, if the breakpoint width is 1440, the height of the hero is (1440/3) x2 = 960px

## Hero Grid

The images in the hero are placed in a 2-part grid. You can inspect the slides in the FIGMA file to get the image sizes, but it's worth breaking down the "Math" of the hero for a better understanding of how it works.

Step 1: determine the size of the large vertical image

The large vertical image is 1/3 of the full breakpoint width. To determine the height of the large image, divide the breakpoint by 2/3.

Large vertical image =  1/3 of the fixed page width

Step 2: determine the size of the remaining space

The remaining 2/3rds space is divided into fifths. At the 1440 breakpoint, the remaining 2/3rds of the breakpoint is made up of (5) 192px wide columns.

Here’s how the images layout for each screen:

small square image = 1/5 of (2/3rds of fixed page width)

**Hero layout on mobile**

![1726084475744](image/hp-hero_funcD/1726084475744.png)

**Hero layout on desktop**

![1726084498828](image/hp-hero_funcD/1726084498828.png)

![1726084504350](image/hp-hero_funcD/1726084504350.png)


Even though the large vertical image changed its location on the x-axis, it is still taking up 1/3 of the screen and leaving 2/3rds for the remaining 5 columns.

## Type

Font:

Big Words: font used is [Playfair Display at 700 weight from Google Fonts](https://fonts.google.com/specimen/Playfair+Display).

Supporting head: Roboto bold, 800

Note: Design team OK with 800 weight for all. Design approved with the head at 700 and subhead @800.

**Type classes**

hpHeroHeading: big word(s)

hpHeroSub: supportive subtext

**desktop: **text is flush left and starts at the 3rd column

**mobile:** text is flush left and starts at 32px, leaving a 32px margin to the left.

**Type positioning: **Type is flush left starting at column 3 for all desktop breakpoints.  On mobile, the type is flush left within the content box (breakpoint - 64px for margins)

## **UI Elements**

Since the home page hero has an auto transition between slides, a pause button is provided to meet accessibility requirements to allow the user to pause  the hero. The pause button changes to a Play button when the end is reached at the 4th slide. Icons for all breakpoints are 40px.

Pause button-40px

Play button-40px

![1726084520771](image/hp-hero_funcD/1726084520771.png)

## Responsive Notes:

* Although the content of the hero itself stays within the fixed page width at each breakpoint, the primaryDark background extends full browser view width.
* Type scales proportionally down in size along with the rest of the hero content (use the type sizes though provided for each breakpoint as a target)
* On mobile, the large, vertical image does not appear and the layout changes to the 4-up square images.

**Transition animation between slides**

**Image transitions**

Each image has a vertical wipe. [This animation on Tympanus](https://www.tympanus.net/Development/LayerMotionSlideshow/) is a reference for the image transitions (not the type transitions). The [article about this animation is here](https://tympanus.net/codrops/2019/01/24/layer-motion-slideshow/) with a link to the sample files on GitHub.

Transition to slide 4 (single image), is one vertical wipe, right to left.

**Type transition**

Inspo: Liking the progressive character write-on of the example #3 (red type on black) [https://tympanus.net/Development/LetterEffects/](https://tympanus.net/Development/LetterEffects/) for the LARGE WORDS.

Subhead/uppercase, would just fade in underneath.
