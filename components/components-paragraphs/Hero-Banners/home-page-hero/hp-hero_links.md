# Links

## Figma file

[Link](https://www.figma.com/file/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?node-id=738%3A33613)

Figma file with home page hero for release. See "Handoff"" / home page hero in the left sidebar.

## Flow: desktop prototype

[Link](https://www.figma.com/proto/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?page-id=659%3A25745&node-id=1017%3A40403&viewport=40%2C-295%2C0.25&scaling=min-zoom&starting-point-node-id=1017%3A40403&show-proto-sidebar=1)

Approved prototype for desktop. Note: just for the prototype, you will need to self-navigate by clicking on the pause button in the bottom right.

The search and nav are included in the prototype.

## Flow: mobile prototype

[Link](https://www.figma.com/proto/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?page-id=659%3A25745&node-id=810%3A32905&viewport=105%2C249%2C0.16&scaling=min-zoom&starting-point-node-id=810%3A32905&show-proto-sidebar=1)

Approved prototype for mobile. Note: just for the prototype, you will need to self-navigate by clicking on the pause button in the bottom right.

The search and nav are included in the prototype.

## Images for hero slides

[Link](https://uvmoffice.sharepoint.com/:f:/s/UVMWebsiteRedesign/Eq-sw2zkBd9Lpojbb0g1x7UByPtfAiShBoJJS1ubZeE1YA?e=3tGPer)

PNGs for the images for each slide provided at the largest size for the XL-1440 breakpoint. Images scale down responsively. Image names reflect their respective column and row position.
