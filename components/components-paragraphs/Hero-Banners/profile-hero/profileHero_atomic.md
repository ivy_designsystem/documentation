# Atoms

**Epic shape**

Base shape used for both the animated "mountain" shape and the mask for the profile image.

Ivy / Components / Atoms / Shapes / [epic shape](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e370265829471934b0e719/tab/design?mode=preview)

# Molecules

 **Profile image group**

Ivy / Components / Molecules / [Profile Image Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e3747293ea31b3a8266dc7/tab/design?mode=preview)
