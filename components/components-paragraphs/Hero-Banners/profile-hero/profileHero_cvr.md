# The Profile Hero appears at the top of all profile pages. It introduces the person with key information and an optional profile image.

![1726088391212](image/profileHero_cvr/1726088391212.png)
