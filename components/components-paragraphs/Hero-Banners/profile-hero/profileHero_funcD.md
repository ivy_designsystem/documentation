# The Profile Hero appears at the top of all profile pages. It introduces the person with key information and an optional profile image.

![1726088458872](image/profileHero_funcD/1726088458872.png)

![1726088485074](image/profileHero_funcD/1726088485074.png)

# Introduction

# Data

**Profile image: **1x1 profile image, 1080x1080 (optional but recommended)

**Profile Name:** Simple text, Fullname, see data for bios

**Title: **Simple text, see data for bios

**Telephone number:** simple text (device to manage whether it is linkable or not)

**eMail:** simple text with link to email, on-click opens client email app.

**Profile icons:** profile icons appear with the key info in the hero banner on desktop breakpoints. You can [download the SVGs here](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64e5160c7c57dc9aeebda9e0?mode=preview). The default color is lightGray and they hover to darkGray.

# Component width

In general, the component scales full width of the viewport. The background only extends full width.

**background:** darkGray background of the component is 100% viewport.

**overlay text and header:** type content and header is within the breakpoint content area for all breakpoints

**Profile image group:**

The profile image group always extends past the right edge of the breakpoint. On mobile, the profile image group gets cut off on the right by the devise viewport edge. For larger breakpoints, the profile image group can be seen extended on the right if the browser is open enough so the area to the right of the breakpoint is visible.

# Animation

On page load, the profile image group animates on in the background of the hero. There are 2 different events happening:

1. the profile image dissolves-on in place (0% opacity to 100% opacity)
2. the epic shape animates onto the hero stage

**Profile image**

On page load, the background of the profile hero is darkGray.

If profile image exists for the bio, the profile image fades on in place from 0% opacity to 100% opacity.

If there is no profile image for the bio, do not show anything- there will be no placeholder image or substitute image if the profile does not have a bio picture.

**Epic shape "mountain"**

The epic shape is also used as a shape asset on every profile hero- regardless of the status of the profile image.

The epic shape resembles a mountain and is duplicated. The persistent mountain shape is in primary1 and the secondary mountain starts out as primary1Darker-80 and as it animates on, it changes in opacity to 100%.

Both shapes animate on paths with a rotation to their respective angles at their final resting places at the end of the animation. The secondary shape animates on and off the hero stage swiping underneath the persistent epic shape.

![1735572322332](image/profileHero_funcD/1735572322332.png)

The top sequence above has a profile image that fades on.

The bottom sequence does not.

The 2 "epic" shapes are shown here off the hero stage and would be masked out by the hero container.  They rotate in and animate into place on the bottom of the hero. The secondary darker shape animates in under the partner shape until is is off-stage.

[The animation sequence is outline here.](https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2046%3A102764&mode=design&t=Kj0AHachrejAaU38-1)

# Animation assets

![alt text](asset.png)

^ This is the [shape asset.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e370265829471934b0e719/tab/design?mode=preview) We call it the "epic" shape for short as it is in the family of shapes. It is used in the profile image group component and is used as different sizes and rotations in the profile hero.

![1726088504362](image/profileHero_funcD/1726088504362.png)

The epic shape is used to mask the profile image. Above are the assets used for the profile image mask - to be combined with a 1x1 (800x800px) profile image asset.  Not sure if this is needed now that the profile image is not going to have a mask animation. [Here is the link to the Figma artboard ](https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2046%3A102929&mode=design&t=BQoWMOPFbMUZ6YWR-1)for these 3 masks and you can [download the individual SVGs here](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64e372e98c7cd860bec31746?mode=preview).

# Profile image group

The [Profile Image Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e3747293ea31b3a8266dc7/tab/design?mode=preview) is the shape and profile image grouped together in their final placement within the profile hero container.

**Scrim**

There is a scrim at the top of the hero to ensure that the overlay items in the header are readable. The scrim goes on top of the profile image lockup and under everything else.

# Responsive Notes

## Mobile breakpoints

**Text content:** The mobile breakpoints show the profile image group animation, name and title. The additional contact information moves below into the key contact area for mobile.

**Profile Image group animation: **The profile image group has the same animation across all breakpoints although the starting and end coordinates vary, obviously, due to the varying widths. The size of the profile image group does not vary for all mobile breakpoints.

## Desktop breakpoints

**Text content:** The desktop breakpoints show the profile image group animation, name, title, telephone number, email address, pronouns, and name pronunciation in the hero area.

**Profile Image group animation: **The profile image group has the same animation across all breakpoints although the starting and end coordinates vary, obviously, due to the varying widths. The size of the profile image group does not vary for all desktop breakpoints.

The type sizes vary between mobile and desktop.

The profile image group size varies between desktop and mobile.

The profile image is aligned left with column 6 for breakpoints L - xxl. (spans right 6 columns of breakpoint + overhang on the right)

The profile image is aligned left with column 6  for the  medium breakpoints. (spans right 7 columns of breakpoint + overhang on the right)

The profile image is aligned left with column 5 for the small breakpoint and  spans right 8 columns of breakpoint + overhangs on the right)

# Layouts by breakpoint

![1726088518766](image/profileHero_funcD/1726088518766.png)

Here are the mobile versions of the profile Hero. The landscape mode is a bit special-- the layout is side by side instead of stacked.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOYKBtWdd79YGatpQpzHmH7bs2_vQ6UELY6-VRdH61HYMSWxOzBDzCJsb5u-s3l6v_cHu_PQ7-uCALiEbg9D_DQMHdY15IHXNPwM_gkaEtTtgZeoaOQhRvPadX_Vbai44Pg==?assetKey=university-of-vermont%2Fivy%2Fspecs_profileHero-xx-rkAPVRPcR.png)

Working our way down from the largest size... When the browser window is open larger than the breakpoint on non-mobile devices, the right edge of the profile image will be visible outside of the page grid.

![1726088532116](image/profileHero_funcD/1726088532116.png)

The xl and the xxl are the same- just the background extends further.

![1726088543041](image/profileHero_funcD/1726088543041.png)

![1726088563154](image/profileHero_funcD/1726088563154.png)

Note: The profile image spans more columns at this breakpoint- otherwise the person's face would be cut off.

![1726088577039](image/profileHero_funcD/1726088577039.png)

Note: the small breakpoint is unique. The profile image spans 8 columns.

# Anatomy

![1726088591729](image/profileHero_funcD/1726088591729.png)
