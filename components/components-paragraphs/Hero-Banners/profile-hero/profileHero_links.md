# Links

**Animation description**
[Link](https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2046%3A102764&mode=design&t=D2cfMHR1IjMMEESF-1): The profile image is masked by the "epic" shape and dissolves on in place from 0% opacity to 100% opacity.

**Figma file for the profileImgGroup**
[Link](https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?type=design&node-id=2046-102723&mode=dev)
profileImgGroup components in their final static, ending position. (last frame of the animation)

**Figma file fo rthe profileImgGroup with solid fills**
[Link
]()This would be the last frame of the animation. Provided here with solid fills rather than an image fill.Image group asset

**Figma file for the profile hero**
[Link](https://www.figma.com/file/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?type=design&node-id=2960-51&mode=design)

**Handoff artboard for the profile hero**
[Link](https://www.figma.com/file/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?type=design&node-id=2960%3A142250&mode=design&t=vcSvm4gKtWK0Irdq-1)

**Figma artboard with master profile hero component**
[Link](https://www.figma.com/file/l7Wvp8J7TyF5PxAgZl79TC/Ivy_Banners?type=design&node-id=2960%3A142226&mode=design&t=vcSvm4gKtWK0Irdq-1)
