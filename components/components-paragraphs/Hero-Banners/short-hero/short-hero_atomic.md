# Atoms

**Image**

6x4 static image

Ivy / Components / Atoms / Media / [6x4 image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61391f9a5c82e474e3192fff/tab/design?mode=preview)

1x1 static image

Ivy / Components / Atoms / Media / [1x1 image](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61390881f9799804f7f9d035/tab/design?mode=preview)

# Molecules

 **Type Group: **

Ivy / Components / Molecules / Type Groups / [tGrp_hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c0174348713a131a4ff302/tab/functional%20narrative?mode=preview)

Note: the tall hero uses the tGrp_childHero component

Ivy / Components / Molecules / Media / [imgCoverShort](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c2bd0f1579dd6e755f934d/tab/design?mode=preview)
