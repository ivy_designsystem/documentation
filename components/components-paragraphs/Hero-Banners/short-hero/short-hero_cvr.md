# The banner is the first visual component that users see when they visit a page. The ‘short hero’ banner fills around half of the visual height of the viewport and has variations for either home page placement or lower level (LL) pages.

![1726089683092](image/short-hero_cvr/1726089683092.png)

![1726089688823](image/short-hero_cvr/1726089688823.png)
