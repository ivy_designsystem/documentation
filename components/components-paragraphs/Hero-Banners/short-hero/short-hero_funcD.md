# The banner is the first visual component that users see when they visit a page. The ‘short hero’ banner fills around half of the visual height of the viewport and has variations for either home page placement or lower level (LL) pages.

![1726089709949](image/short-hero_funcD/1726089709949.png)

# Introduction

The Short Hero is designed as a progressively enhanced component so content editors can go public with their pages with just a page title in the hero. Image content can be added later for progressive display variations ranging from a single square image, a grouping of two square images, or a larger background image.

# Usage

To better understand the usage of the short hero component, here is a usage comparison with the [*hero*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/functional%20narrative?mode=preview)component that is a deeper (taller) hero banner.

**Short Hero (shorter, 58% vh)**

* Can be used on any page (Homepage/Non Homepage)
* Is the default hero for the home page of all non-academic websites
* Is the default hero for all academic *child* websites
* Is the default hero for all lower level, non-home pages

**Hero (larger, 88% vh)**

* Used for the home page for Academic *parent* websites
* Can be used for any other site that has permission from the web team
* Cannot be used on a lower level page. It is reserved for the home page level.

** Note**

*We could swap out the text lockup in **the *[*hero*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/functional%20narrative?mode=preview)* component with the text lockup we are using on the short hero. It would be more utilitarian. *

# Visual Display: Progressively enhanced design

The short hero and all its variations can be displayed either on a home page or on a lower level page. Here are the progressive design variations available, shown below in left => right progressive order:

* No image, solid background
* Single thumbnail image (single square image)
* Thumbnail group (2 square images)
* Background image

![1726090342393](image/short-hero_funcD/1726090342393.png)

**Visual display progression and options for the short hero ^**

* The top row is the short hero for a home page. Notice that the website/group name is the H1 for the hero at that tier.
* The bottom row is the short hero for a lower level (LL) page. For lower level placement, the H1 is the page title.

# **Exceptions where websites can have a larger hero**

Groups that have a short hero by default, can request the larger hero instead for their home page. There are a few scenarios envisioned that would make this a reasonable request, including, a desire to show video. For example, by default the Office of the President will have the short hero on the home page. However, if this group wishes to display a looping video on their home page, then the web team could make the larger hero show up on their home page instead of the short hero.

# Data for the 'short hero'

**MANDATORY DATA**

* Website group name (simple text)
* Page title (simple text)

**OPTIONAL DATA**

* 1-2 square1x1 images (recommend 1080x1080 square at minimum)
* For cover image version: 1800x550 image
* Optional tag line (simple text)

# Text groups for the short hero

The short hero has a type group that varies depending on whether the hero is a home page hero or used on a lower level page.

**Short hero used on the homage page**

**H1 Website name**

Optional tagline

![1726090358096](image/short-hero_funcD/1726090358096.png)

### The short hero used on any lower level page has this type group:

Eyebrow: website name

**H1 page title**

![1726090375054](image/short-hero_funcD/1726090375054.png)

# **Functionality description for the text groups**

**Type scales from the bottom up**

Both child hero text groups scale from the bottom up. This is a critical feature as in context, the baseline of the type group needs to remain fixed. See also the [child hero txtGroup component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c0174348713a131a4ff302/tab/functional%20narrative?mode=preview).

![1726090392586](image/short-hero_funcD/1726090392586.png)

**Fixed bottom margin of 112 px**

Whether for placement on the home page or a lower level page, the text group as a fixed bottom margin of 112px for all breakpoints. This is to allow space for the section navigation panel that overlays the hero at the bottom.

The text group can scale in height but to maintain the bottom margin, the text containers scale up from the bottom. It's important that the text containers for this component can increase in height to accommodate longer character counts, however, the 112px margin-bottom has to be maintained. This is true for either the website name or the optional tag line.

# Header for the short hero

The parent header is used for the short hero. The parent header has the UVM logo in lockup with a website name following these guidelines:

* **The name of the parent website**

It doesn't matter whether the child website is academic or not, the parent name would still show in the    header lockup. for example:

    Child website: English Department // Parent: College of Arts and Sciences

* **Child websites could choose to not have a parent in the header**

  For example, the School of the Arts (SotA) could opt to not have the College of Arts and Sciences (CAS) name in their header and use the larger hero banner for their page - which we would put in for them.
* **The name of the website itself, if there is no parent .** For example:

  Residential Life

  Student Financial Services

  Office of the President

  Gund Institute for the Environment
* **Child websites with multiple parents**

  Child websites that have multiple parents, must choose which parent they wish to show in the header of their website. They also have the option of not showing a parent at all in their website.

## **Website name**

The website name appears as the H1 title on the home page of the school/college/section/unit website in the short Hero.

For non-home page pages of the school/college/section/unit website, the website name appears as a kicker before the H1 page title.

## **Page title**

The page title displays as the H1 header on all pages except the home page for the of the school / college / section / unit website.

# Transition and interaction

Follow what has already been created for the [*hero*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/functional%20narrative?mode=preview)* component.*

On page load:

* Media fades on
* Type group: [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

# Image Assets

**Background image**

**6x4 image**

Asset made to the largest size 1920x1280

* One source image for all breakpoints
* Image will be a container with a bg image.
* Container can be set to **58%vh** of the viewport

*Note: We will determine value of the visual height during the QA process. So please make sure that we will be able to adjust that value to achieve the best UX. *

## **Image alignment for the short hero with a full background image**

For the success of the component with the full background image, we are providing some controls over the position of the image in the hero so the area of focus is held.

**Approach: Background cover image + vertical alignment controls**

By default, the image uses the 'background size-cover" style and scales to fill either height or width- whichever is larger, depending on the breakpoint. For example, the short hero is vertical at the mobile breakpoints with the device in portrait position but horizontal and shorter at the desktop breakpoints.

By default the image will be centered vertically and for many 6x4 image assets, this default cover setting will achieve a fine result at all breakpoints. However, we are providing the content editor with additional controls to be able to vertically align the image top, middle, or bottom.

![1726090405638](image/short-hero_funcD/1726090405638.png)

**Developer note:***We will need to provide controls for top, middle, and bottom vertical alignment even though it will default to middle. This is to ensure that the user can choose vertical-middle alignment again after selecting top or bottom.*

![1726090417371](image/short-hero_funcD/1726090417371.png)

This example shows the vertical alignment options: vertical align top, centered, and bottom. ^

![1726090430732](image/short-hero_funcD/1726090430732.png)

Vertical alignment-**top** was the preferred alignment. It is maintaining the image focus across breakpoints.

# Ensuring adequate contrast for type

**Scrim overlays**

Like the [hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/design?mode=preview), [story scroll hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6388bf5d20bb6c9c4a6ce23d/tab/design?mode=preview), and the[ takeover hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6437150906ea1118c0b12e0a/tab/design?mode=preview), the shortHero has a modal scrim overlay on the top and bottom of the hero. This is to ensure adequate contrast for the header and text group content that overlay in white. For the *short hero,* the scrims have some overall transparency to the image can still be seen below while still providing enough contrast for the white overlay type.

The top and bottom scrim appear at all breakpoints.

**Guardrails with the xxs mobile**

If the combined text content for the textGroup is too long, on mobile xxs, the text will extend into an area without a scrim and not be readable.

![1726090442971](image/short-hero_funcD/1726090442971.png)

# Dimensions of Hero

**width:** Background extends the full width of the viewport

**height: **will be a percentage of the viewport. (initial spec is 58% but exact percentage to be decided during QA)

This table shows the visual heights that we are using for design purposes at the different breakpoints. For working purposes, the visual heights of heros are either 100vh, 88vh, or 58vh.

![1726090453657](image/short-hero_funcD/1726090453657.png)

Short heroes are 58% vh.

Short heroes can be elevated to the home page. For the short hero with a cover image, it can be either 88vh or 58vh for the home page. Heroes with thumbnails or no image (green background) are always 58vh due to the leadership committee's abhorance of negative space.

# Responsive notes

The short hero component which includes one or two thumbnail images has a variation on mobile. Rather than responsively stacking the images, which would cause the page content to be pushed further down, the mobile version defaults to a text-only version with a solid background. This behavior ensures a better user experience on mobile devices and reduces vertical scrolling / swiping to access the page content.

# Editing needs within the Drupal UI

* Content editor needs to select the focal point of the image for the version that has a full background image.

# Display anatomy and differences by breakpoint

## 01 Child hero with text

**childHeroTxtLL**

*(child hero, text only, for a lower level page)*

This text-only child hero appears on a lower level page if no image is uploaded.

xxs, xs ^

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s, the hero text group spans 12 columns
* The group name appears as the eyebrow in the text group

![1726090464706](image/short-hero_funcD/1726090464706.png)

small ^

* @xxs - s, the hero text group spans 12 columns
* The header container has a fixed height of 88px

![1726090474783](image/short-hero_funcD/1726090474783.png)

medium ^

* @medium - xxl breakpoints, the text group spans 7 columns
* The header container has a fixed height of 88px

**childHeroTxtHP**

*(child hero, text only, for a home page)*

This text-only child hero appears on a child website's home page if no image is uploaded.

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s the hero text group spans 12 columns
* The group name appears as the H1 in the text group
* There is an optional tag line

![1726090484586](image/short-hero_funcD/1726090484586.png)

xxs ^

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s the hero text group spans 12 columns
* The group name appears as the H1 in the text group
* There is an optional tag line

![1726090493848](image/short-hero_funcD/1726090493848.png)

small ^

* @xxs - s the hero text group spans 12 columns
* The group name appears as the H1 in the text group
* There is an optional tag line
* The header container has a fixed height of 88px

![1726090506823](image/short-hero_funcD/1726090506823.png)

medium ^

* @medium - xxl breakpoints, the text group spans 7 columns
* The group name appears as the H1 in the text group
* There is an optional tag line
* The header container has a fixed height of 88px

## 02 Child hero with single image

**childHeroImgHP**

*(child hero, thumbnail image, for a home page)*

This child hero appears on a child website's home page if a single 1x1 square image is uploaded.  Note that the image does not appear on the xxs, xs, and small breakpoints.

![1726090514973](image/short-hero_funcD/1726090514973.png)

xxs breakpoint ^

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s the hero text group spans 12 columns
* The group name appears as the H1 in the text group
* There is an optional tag line
* @xxs-s the image does not appear, hero displays a text-only version

![1726090524987](image/short-hero_funcD/1726090524987.png)

small breakpoint ^

* The hero text group spans 12 columns
* The group name appears as the H1 in the text group
* There is an optional tag line
* The header container has a fixed height of 88px
* @xxs-s the image does not appear, hero displays a text-only version

![1726090539304](image/short-hero_funcD/1726090539304.png)

Medium - xxl breakpoints  ^

* The hero text group spans 7 columns
* The group name appears as the H1 in the text group
* There is an optional tag line
* The square thumbnail appears
* The header container has a fixed height of 88px

**childHeroImgLL**

*(child hero, thumbnail image, for a lower level page)*

This child hero appears on a child website's lower level page if a single 1x1 square image is uploaded. Note that the image does not appear on the xxs, xs, and small breakpoints.

﻿

![1726090547797](image/short-hero_funcD/1726090547797.png)

childHeroImgLL-xxs ^

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s the hero text group spans 12 columns
* @xxs-s the image does not appear, hero displays a text-only version
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group

![1726090560430](image/short-hero_funcD/1726090560430.png)

childHeroImgLL-small  ^^

* @xxs - s the hero text group spans 12 columns
* @xxs-s the image does not appear, hero displays a text-only version
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group
* The header container has a fixed height of 88px

![1726090570762](image/short-hero_funcD/1726090570762.png)

childHeroImgLL-medium +    ^

* The medium breakpoint and larger shows the square image.
* The text group spans 7 columns
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group
* The header container has a fixed height of 88px

**childHeroImgHP**

*(child hero with a single square image that appears on a child or non-academic home page)*

This child hero appears on the home page of a non-academic site or a child website of any kind if a single 1x1 square image is uploaded. Note that the image does not appear on the xxs, xs, and small breakpoints.

﻿

![1726090577363](image/short-hero_funcD/1726090577363.png)

childHeroImgHP-xxs ^

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s the hero text group spans 12 columns
* For the xxs-s breakpoints, the square image does not appear
* The website / group name is the H1 in the text group. It dynamically populates
* There is an optional tag line

![1726090588013](image/short-hero_funcD/1726090588013.png)

childHeroImgHP-small  ^

* @xxs - s the hero text group spans 12 columns
* For the xxs-s breakpoints, the square image does not appear
* The website / group name is the H1 in the text group. It dynamically populates
* There is an optional tag line
* The header container has a fixed height of 88px

![1726090598556](image/short-hero_funcD/1726090598556.png)

childHeroImgHP-M +     ^

* For the medium - xxl breakpoints, the square image appears.
* The text group spans 7 columns
* The website / group name is the H1 in the text group. It dynamically populates
* There is an optional tag line
* The header container has a fixed height of 88px

## 03 Child Hero with an image group

**childHeroImgGroupHP**

*(child hero with a group of square images.  Placement is for a child or non-academic home page)*

This child hero appears on the home page of a non-academic site or a child website of any kind if 2 square 1x1 images are uploaded. Note that the image group does not appear on the xxs, xs, and small breakpoints.

﻿

![1726090609677](image/short-hero_funcD/1726090609677.png)

childHeroImgGroupHP-xxs ^

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s the hero text group spans 12 columns
* @xxs-s the image does not appear, hero displays a text-only version
* The website / group name is the H1 in the text group. It dynamically populates
* There is an optional tag line

![1726090619371](image/short-hero_funcD/1726090619371.png)

childHeroImgGroupHP-s ^

* The header container does not scale. It is fixed at 88px high.
* The image group does not show at the small breakpoint.
* The text group spans 12 columns

![1726090635871](image/short-hero_funcD/1726090635871.png)

childHeroImgGroupHP-medium+ ^

* The header container has a fixed height of 88px
* For the medium breakpoint and larger, the image group displays.
* The text group spans 7 columns
* Like all child heroes that appear on a home page, the website/group name is the H1 head.
* There is an optional tagline

**childHeroImgGroupLL**

*(child hero with a group of square images. Placement is for a lower level page on a child website or non-academic website)*

This child hero appears on a lower level page of a non-academic site or a child website of any kind if 2 square 1x1 images are uploaded. Note that the image group does not appear on the xxs, xs, and small breakpoints.

﻿

![1726090648689](image/short-hero_funcD/1726090648689.png)

childHeroImgGroupLL-xxs ^

* @xxs, the parent name in the header is stacked
* @xxs - s the hero text group spans 12 columns
* @xxs-s the image does not appear, hero displays a text-only version
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group

![1726090660047](image/short-hero_funcD/1726090660047.png)

childHeroImgGroupLL-s ^

* The header container has a fixed height of 88px
* The image group does not display from the xxs-s breakpoints
* The text group spans 12 columns
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group

![1726090670316](image/short-hero_funcD/1726090670316.png)

childHeroImgGroupLL-medium+ ^

* The header container has a fixed height of 88px
* From the medium breakpoint and larger, the image group displays.
* The text group spans 7 columns
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group

## 04 Child Hero with background image

**childHeroBGimgHP**

*(child hero with a background image filling the hero container. Placement is for a child or non-academic home page)*

This child hero appears on the home page of a non-academic site or a child website of any kind if an image is uploaded for the background.

﻿

![1726090680042](image/short-hero_funcD/1726090680042.png)

childHeroBGimgHP-xxs ^

* @xxs, the parent name in the header is stacked
* @xxs - s the hero text group spans 12 columns
* Like all child heroes that appear on a home page, the website/group name is the H1 head.
* There is an optional tagline
* Background image has 'cover' style
* User to establish focal center of image OR be able to assign image alignment

![1726090701565](image/short-hero_funcD/1726090701565.png)

childHeroBGimgHP-s ^

* The header container has a fixed height of 88px
* The text group spans 12 columns
* Like all child heroes that appear on a home page, the website/group name is the H1 head.
* There is an optional tagline
* Background image has 'cover' style
* User to establish focal center of image OR be able to assign image alignment

![1726090713053](image/short-hero_funcD/1726090713053.png)

childHeroBGimgHP-medium+ ^

* The header container has a fixed height of 88px
* The text group spans 7 columns
* Like all child heroes that appear on a home page, the website/group name is the H1 head.
* There is an optional tagline
* Background image has 'cover' style
* User to establish focal center of image OR be able to assign image alignment

**childHeroBGimgLL**

*(child hero with a group of square images. Placement is for a lower level page on a child website or non-academic website)*

This child hero appears on a lower level page of a non-academic site or a child website of any kind if an image is uploaded for the background.

\

![1726090723682](image/short-hero_funcD/1726090723682.png)

childHeroBGimgLL-xxs ^

* @xxs, the parent name in the header is stacked
* @xxs - s the hero text group spans 12 columns
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group
* Background image has 'cover' style
* User to establish focal center of image OR be able to assign image alignment

![1726090733690](image/short-hero_funcD/1726090733690.png)

childHeroBGimgLL-s ^

* The header container has a fixed height of 88px
* @xxs - s the hero text group spans 12 columns
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group
* Background image has 'cover' style
* User to establish focal center of image OR be able to assign image alignment

![1726090744981](image/short-hero_funcD/1726090744981.png)

childHeroBGimgLL-medium+ ^

The header container has a fixed height of 88px

* The text group spans 7 columns
* The page title appears as the H1 in the text group
* The website / group name dynamically populates and appears in the eyebrow in the text group
* Background image has 'cover' style
* User to establish focal center of image OR be able to assign image alignment

![1726090766752](image/short-hero_funcD/1726090766752.png)
