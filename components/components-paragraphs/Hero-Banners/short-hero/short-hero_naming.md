# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the short hero

This component is called the " **short hero** ".

The short hero is, well, a shorter hero- a hero that is less deep and does not fill the above the fold viewport on page load.

It is a progressive component. The naming is in camel case and the structure consists of :

componentName_variantTier-size

 **LL= Lower level** . The lower level short Hero uses this type group:

 **HP= Home page** . The short hero when placed on the home page uses this type group:

![1726095929835](image/short-hero_naming/1726095929835.png)

**And the progressive variants of the short hero with their names:**

shortHero_txtLL

shortHero_txtHp

shortHero_imgLL

shortHero_imgHp

shortHero_imgGrpLL

shortHero_imgGrpHp

shortHero_BgImgLL

shortHero_BgImgHp

## Follows are visual examples with their associated name below.

![1726095943782](image/short-hero_naming/1726095943782.png)

shortHero_txtHp ^

![1726095959407](image/short-hero_naming/1726095959407.png)

shortHero_textLL ^

![1726095974119](image/short-hero_naming/1726095974119.png)

shortHero_imgHP ^

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmORofMAGhc63BPmDqy_ovXZFwa2GMiW9xQLavt9FbFLiMJmj2XT05OtOm7lkRJjDAk1BcftEH_GbuJXeFshbgPNNNVHk5i1F9oQySPI7-IE_ySK_CEI5cm4c_bluZKFK1-Q==?assetKey=university-of-vermont%2Fivy%2FshortHero_imgLL--Bkw1rUbi3.png)

shortHero_imgLL  ^

![1726095985472](image/short-hero_naming/1726095985472.png)

shortHero_imgGrpHP. ^

![1726095998657](image/short-hero_naming/1726095998657.png)

shortHero_imgGrpLL  ^

![1726096022446](image/short-hero_naming/1726096022446.png)

shortHero_bgImgHP.  ^

![1726096035098](image/short-hero_naming/1726096035098.png)

shortHero_BgImgLL. ^
