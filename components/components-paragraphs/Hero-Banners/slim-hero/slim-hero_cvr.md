# The "Slim Hero" is a hero-header lockup with a minimum height. It takes up the minimum amount of vertical space to accommodate the header and an H1 page title for pages without a unit level navigation.

![1726148345300](image/slim-hero_cvr/1726148345300.png)
