# The "Slim Hero" is a hero-header lockup with a minimum height. It takes up the minimum amount of vertical space to accommodate the header and an H1 page title for pages without a unit level navigation.

![1726148444718](image/slim-hero_funcD/1726148444718.png)

# IMPORTANT

## Do not change the header, header padding from the live. Your live header is the source of truth.

## Functionality

Slim Hero has a minimum height. The header is fixed and the hero scales vertically as needed if the H1 page title wraps to multiple lines.

## Usage destination

The Slim Hero can only be used on websites that do not have a unit navigation.

The Slim Hero is used on the generic Header / Footer UVM branded page wrapper template.

## Variants

The Slim Hero variants are:

• with UVM header

• with UVM Parent header

Mobile and desktop minimum widths also vary for each variant above.

![1726148455806](image/slim-hero_funcD/1726148455806.png)

![1726148466267](image/slim-hero_funcD/1726148466267.png)

## Data for the 'Slim Hero'

**MANDATORY DATA**

* Website group name (simple text)
* Page title (simple text)

## H1 page title

Desktop: uses dt_H1 display serif

Mobile: uses m_H1 display serif

Note: Most Heros use H1 display serif Grande, which is a much larger heading

## **Functionality description for the the H1 page title**

**Type scales down**

The H1 page title can wrap to multiple lines if needed. If this happens, the container for the entire Slim Hero will scale vertically down.

Note that there is a fixed space between the header and the H1 page title for all breakpoints AND a fixed margin below the H1 page title to the bottom of the component's container.

![1726148478680](image/slim-hero_funcD/1726148478680.png)

## Transition and interaction

Follow what has already been created for the [*hero*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/functional%20narrative?mode=preview)* component.*

On page load:

* Media fades on
* Type group: [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

## Image Assets

**None. N/A**

## Dimensions of Hero

**width:** Background extends the full width of the viewport

**HEIGHT**

**desktop: **min-height 264px, scales vertically if H1 page title wraps to multiple lines

**Mobile:**

For UVM header (no parent) min-height 224, scales vertically if H1 page title wraps to multiple lines

For Parent Header min-height260 , scales vertically if H1 page title wraps to multiple lines

# Display anatomy and differences by breakpoint

![1726148525437](image/slim-hero_funcD/1726148525437.png)

![1726148555174](image/slim-hero_funcD/1726148555174.png)
