# The hero_storyScroll component is a hard-working hero with a full-width image with additional text and thumbnail images that animate in on page scroll.

![1726096166944](image/story-scroll-hero_cvr/1726096166944.png)

![1726096171430](image/story-scroll-hero_cvr/1726096171430.png)
