# The hero_storyScroll component is a hard-working hero with a full-width image with additional text and thumbnail images that animate in on page scroll.

![1726096246316](image/story-scroll-hero_funcD/1726096246316.png)

# Design elements

**Desktop on page load**

* full breakpoint width background image
* Dark graduated scrim overlay bands under the logo at the top and the H1 type at the base of the image container.
* H1 title

# **Responsive Notes**

**On Parallax scroll, the following appears**

**XXL, XL, L**

* Dark scrim overlay (fades-in over the entire background image)

**Parallax Layer**

* H1 head
* Paragraph of descriptive text
* Group of 2 small square thumbnail images
* OR 1 square image

**M**

* Dark scrim overlay (fades-in over the entire background image)

**Parallax Layer**

* Paragraph of descriptive text

**S, XS, XXS**

* No parallax or on-scroll event. Everything visible on-load.
* Scrim just under text and logo area appears on-load

---

**Mobile on page load**

**XXS, XS S**

* full-width background image
* H1 title
* Dark graduated scrim overlay bands under the logo at the top and the H1 type at the base of the image container.
* Paragraph of descriptive text butting below the image on a black backroung

*Note: On mobile, the thumbnails do not appear, the parallax FX has been dropped- everything is visible on page load.*

# Data

**MANDATORY**

**6x4 image**

Asset made to the largest size 1920x1280

**H1 head**

simple text. Up to 24 characters, including spaces. 1-3 words only.

**1 Square thumbnail image**

1x1 aspect ratio. Largest asset needed is 1080x1080, drupal to scale.

**NOT MANDATORY**

**p. body text**

Up to 272 characters, simple text.

**2 (total) Square thumbnail images**

1x1 aspect ratio. Largest asset needed is 1080x1080, drupal to scale.

# Image Assets

**Background image**

**6x4 image**

Asset made to the largest size 1920x1280

* One source image for all breakpoints
* Image will be a container with a bg image.
* Container can be set to **88%vh** with the width set to the full breakpoint (not viewport)
* *Note: We will determine value of the visual height during the QA process. So please make sure that we will be able to adjust that value to achieve the best UX. *

**Square thumbnail images**

One square image is mandatory with an option to have a total of two.

* They add value to the user's experience
* They help "tell the story" that is being conveyed by the hero component
* They have a bigger value return for the user on-scroll

**Thumbnail options**

Design accommodates from 1-2 thumbnail images. Images should be 1x1 aspect ratio with the largest target needed @ 436x436.

Images should be close to, or larger than, target size. Retina is not needed.

Below are prototypes of the on-scroll view showing a group of 2 images or the variation with a single image.

![1726096269739](image/story-scroll-hero_funcD/1726096269739.png)

![1726096277870](image/story-scroll-hero_funcD/1726096277870.png)

# Ensuring adequate contrast for type

## Over image on page load

Two graduated dark scrim bands overlay the image to ensure the type and logo overlays meet WCAG 2.0 accessibility guidelines. One is on the top so the logo contrast is good and the other is on the bottom so the H1 head has adequate contrast. These scrim overlays vary in height by breakpoint though the top one hugs the top at y-0 and the bottom one hugs the bottom of the image container. They are added programmatically and appear at all breakpoints.

The scrim on the bottom goes to 100% black. This is because on mobile, the p.body type is visable on page load rather than on-page scroll with parallax. The type is on black and the scrim blends into it.
![alt text](image/story-scroll-hero_funcD/mobile.png)

# Dimensions of Hero

**The container holds the background image. **

**width:** 100% width of the breakpoint. Does not extend to full viewport.

**height: **will be a percentage of the viewport. Start with 88%. We want the user to see a bit of what is below rather than a takeover of the  entire viewport.

The hero dimensions do not include the body type which only shows up on parallax on page scroll.

# Editing needs within the Drupal UI

* Content editor needs to select the focal point of the image.
* Content editor needs to upload 0-2 thumbnail images.
* If 1 is loaded, one larger square is shown.
* If 2 are loaded, 2 are shown in the staggered group as shown in the design.

# Type group

H1: font used is [Playfair Display at 800 weight from Google Fonts](https://fonts.google.com/specimen/Playfair+Display).

Note: 800 is the bold weight for all fonts.

p.onScroll

This is the paragraph of text that shows up as parallax on page scroll. It is Roboto and varies in size by desktop/mobile.

All type is flush left.

Type has some type size variation by breakpoint.

# Transition and interaction

How does the Story Scroll Header work? What happens as the user scrolls down the page? What happens when the user scrolls back up the page? [ Check out this video walk through ](https://uvmoffice.sharepoint.com/:v:/s/CommunicationsTEAM408/EXKwkWJtw6VJljjqVEe-pyoBtL19X5yi1n-nrMf2jC3QMA?e=1W4fMS)of the intended functionality.

![1726096311361](image/story-scroll-hero_funcD/1726096311361.png)

Video talk-through of the functionality of this hero and possible interactions. [A WaPo article](https://www.washingtonpost.com/investigations/interactive/2022/bodybuilding-extreme-training/?itid=hp-top-table-main_p001_f004) is reference for an 'on-scroll parallax' transition example.

**Hero on page load:**

Image, graduated overlay and H1 head shows up on page-load. Either instant or quick ease-in fade.

**Transition 01: overlay content becomes visible on scroll.**

Load thumbnail and optional additional text.

User scrolls to load the overlay content on-parallax. User not able to scroll away from the hero area until the overlay content has reached its final position via parallax scroll.

On-initial-scroll, the text and the thumbnails scroll in over the hero background image while the modal overlay simultaneously fades on underneath to ensure readability.

Overlay elements stop scrolling when they arrive at their zenith.  (image group and text group each center vertically in the hero container)

* Scrim fades on
* Parallax layer simultaneously moves up to y=0 (text group and thumbnails would be centered vertically over the hero image at zenith of transition

**Transition 02: overlay content goes away as user scroll down**

After the overlay content has reached its zenith, the user will be able to scroll down the page away form the hero.

*Alert: Parallax scroll can be annoying if the user feels controlled or force-fed content or feel they can't scroll away from the hero. Let's find the sweet spot where there is value and no annoyance.*

**Transition 03: **user scrolls back UP the page and the hero becomes visible.

As the user scrolls back UP the page, the modal overlay goes away to 0% opacity leaving the partial graduated modal scrims top and bottom. The entire parallax layer moves down the page on the Y-axis from it's zenith / registered position to the original position scene on page-load. It basically resets itself.

The parallax layer moves in lock-step with the user's scroll action. After the parallax layer reaches its zenith, the user is able to scroll down away from the hero.  If the user UP to the hero, parallax process reverses itself:

* Scrim fades away
* Parallax layer simultaneously moves back to it's original position

**Parallax in lock-step with user scroll- rocking the parallax layer**

![1726096323826](image/story-scroll-hero_funcD/1726096323826.png)

This video walkthrough attempt to show how the user's scroll controls the movement of the parallax layer up and down while the modal scrim would fade in our out depending on the direction the user is scrolling.

**On scroll DOWN in hero**

* Scrim fades on
* Parallax layer simultaneously moves up to y=0 (text group and thumbnails would be centered vertically over the hero image at zenith of transition
* 

![1726096336886](image/story-scroll-hero_funcD/1726096336886.png)

**On scroll down away from hero**

After Parallax layer reaches apex (y=0), user can scroll down the page away from the hero.

**On scroll back UP within the hero**

Parallax transition reverses itself

* Scrim fades away
* Parallax layer simultaneously moves back to it's original position

**On scroll back up TO the hero**

The user would find the hero reset and the text would scroll back up into the hero area. Exact position and transition details TBD with dev team.

**On page-refresh**

Story Scroll Hero resets itself

The user would see them animate away in lock-step with the scroll down the page only IF a portion of the hero is visible.

## Image layout in rapport to the text group

L-1280 and larger has the extra thumbnail images that appear on scroll. They do not appear for breakpoints smaller than Large.

Note that on page-load, only the H1 head shows over the large image.

The final position of the type and image group on-parallax scroll, has the textGroup and imageGroup centered vertically over the image container.

![1726096380431](image/story-scroll-hero_funcD/1726096380431.png)

However, on page load, the thumbnail images CANNOT be in the same spatial layout on the parallax layer with the H1 head- otherwise the top of the image/image thumb group would be visible on page-load-- and we DO NOT want that.

**This is wrong-- don't do this for the on-page-load view.**

![1726096401594](image/story-scroll-hero_funcD/1726096401594.png)

## UI elements

There are none. Transition of the parallax layer happens organically on scroll.
