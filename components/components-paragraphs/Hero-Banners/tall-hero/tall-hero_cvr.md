# The banner is the first visual component that users see when they visit a page. The ‘tall hero’ banner fills around 88% of the visual height and can only appear on a home page of a website. It can have a full background image or a looping video.

![1726083819864](image/tall-hero_cvr/1726083819864.png)

![1726083825565](image/tall-hero_cvr/1726083825565.png)
