# The banner is the first visual component that users see when they visit a page. The ‘tall hero’ banner fills 88% of the visual height and can only appear on a home page of a website. It can have a full background image or a looping video.

# Introduction

The 'tall hero' is designed for home page placement. It has a deeper, larger media element. It can display either a background image or a looping MP4 video.

# What you need to know:

* The Tall banner is for home pages only.
* It can be for a child or a parent.
* It is the same as the short hero except taller
* It can have either a static image or a looping MP4 video
![alt text](image/tall-hero_funcD/01.png)
![alt text](image/tall-hero_funcD/02.png)

# Usage

* Used for the home page for all academic *parent* websites
* Can be used for any other site that has permission from the web team to use the taller hero instead of the [short hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64aebed5194db88efe7fc1f1/tab/design?mode=preview)
* Cannot be used on a lower level page. It is reserved for the home page level
* It needs a strong image of good quality to display at the larger resolutions
* It needs an image that will work for all breakpoints with the area of interest staying within the viewport area
* OR it needs a looping MP4 video of good quality / small file size

# Visual Display

The tall hero is exclusively for placement on the home page of a website.

**Type group**

There is a type group consisting of:

H1: Name of the website

.p- optional short tagline

## Controls

The version of this hero with video has a pause button so the user can stop the video and control when to play it. For desktop breakpoints, the pause button is in the lower right of the hero and 80px from the bottom and 32px to the left of the breakpoint on the right. For mobile breakpoints, the pause/play button is in the lower right and is 32px from both the bottom and the right edge.

# **Exceptions where non academic websites and academic child websites can have a larger hero**

Groups that have a short hero by default, can request the larger hero instead for their home page. There are a few scenarios envisioned that would make this a reasonable request, including, a desire to show video. For example, by default the Office of the President will have the short hero on the home page. However, if this group wishes to display a looping video on their home page, then the web team could make the larger hero show up on their home page instead of the short hero.

# Data for the 'tall hero'

**MANDATORY DATA**

* Website group name (simple text)
* 1800x850 image *or* a looping MP4 video (for the background)

Note: you can have one or the other in your hero. There is only one background to fill.

# Text group for the short hero

See the documentation for the [tGrp_hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c0174348713a131a4ff302/tab/design?mode=preview).

The tall hero has a type group that consists of:

**H1 Website name**

Optional tagline


![alt text](image/tall-hero_funcD/03.png)

# **Functionality description for the text group**

**Type scales from the bottom up**

The text group scale from the bottom up. This is a critical feature as in context, the baseline of the type group needs to remain fixed.

![1726096666583](image/tall-hero_funcD/1726096666583.png)

**Fixed bottom margin**

Whether for placement on the home page or a lower level page, the text group as a fixed bottom margin of 112px for desktop breakpoints and 80px for mobile breakpoints. This is to allow space for the section navigation panel that overlays the hero at the bottom.

The text group can scale in height but to maintain the bottom margin, the text containers scale up from the bottom. It's important that the text containers for this component can increase in height to accommodate longer character counts, however, the margin-bottom has to be maintained. This is true for either the website name or the optional tag line.

**height: **will be a percentage of the viewport. (initial spec is 58% but exact percentage to be decided during QA)

This table shows the visual heights that we are using for design purposes at the different breakpoints. For working purposes, the visual heights of heros are either 100vh, 88vh, or 58vh.


![alt text](image/tall-hero_funcD/05.png)



The Tall hero is 88% vh ^^

# Header for the tall hero

The parent header is used for the tall hero. The parent header has the UVM logo in lockup with a website name following these guidelines:

* **The home page for a school or college website**

Entity logo in the header: College of Engineering and Mathematical Sciences

H1: College of Engineering and Mathematical Sciences

.p: Optional tagline

* **A child website that is using the tall hero**

It doesn't matter whether the website is academic or not, the parent name would still show in the header lockup. for example:

Entity logo in the header: College of Arts and Sciences

H1: Department of English

.p: Optional tagline

* **Child websites could choose to not have a parent in the header**

For example, the School of the Arts (SotA) could opt to not have the College of Arts and Sciences (CAS) name in their header and use the larger hero banner for their page - which we would put in for them.

* **The name of the website itself, if there is no parent .** For example:

H1: Residential Life

.p: Optional tagline

H1: Student Financial Services

.p: Optional tagline

H1: Office of the President

.p: Optional tagline

H1: Gund Institute for the Environment

.p: Optional tagline

# Transition and interaction

Follow what has already been created for the [*hero*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/functional%20narrative?mode=preview)* component.*

On page load:

* Media fades on
* Type group: [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

# Image Assets

**Background image**

**6x4 image**

Asset made to the largest size 1920x1280

* One source image for all breakpoints
* Image will be a container with a bg image.
* Container can be set to **88%vh** of the viewport

*Note: We will determine value of the visual height during the QA process. So please make sure that we will be able to adjust that value to achieve the best UX. *

## **TBD: Image alignment for the tall hero with a full background image**

The short hero has controls to vertically align the image top/middle/bottom.

We only truly need this sort of control for the landscape mobile view.

Let's discuss the utility of this.

This is how we are doing it for the short hero:


![alt text](image/tall-hero_funcD/06.png)

**Developer note:***We will need to provide controls for top, middle, and bottom vertical alignment even though it will default to middle. This is to ensure that the user can choose vertical-middle alignment again after selecting top or bottom.*


![alt text](image/tall-hero_funcD/07.png)

# Ensuring adequate contrast for type

**Scrim overlays**

Like the [hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/design?mode=preview), [story scroll hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6388bf5d20bb6c9c4a6ce23d/tab/design?mode=preview), and the[ takeover hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6437150906ea1118c0b12e0a/tab/design?mode=preview), and the [shortHero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64aebed5194db88efe7fc1f1/tab/functional%20narrative?mode=preview), the tallHero has a modal scrim overlay on the top and bottom of the hero. This is to ensure adequate contrast for the header and text group content that overlay in white. For the *short hero,* the scrims have some overall transparency to the image can still be seen below while still providing enough contrast for the white overlay type.

The top and bottom scrim appear at all breakpoints.

**Guardrails with the xxs mobile**

If the combined text content for the textGroup is too long, on mobile xxs, the text will extend into an area without a scrim and not be readable.

![1726096710199](image/tall-hero_funcD/1726096710199.png)

# Dimensions of Hero

**width:** Background extends the full width of the viewport

**height: **will be a percentage of the viewport. (initial spec is 88% but exact percentage to be decided during QA)

# Responsive notes

The short hero component which includes one or two thumbnail images has a variation on mobile. Rather than responsively stacking the images, which would cause the page content to be pushed further down, the mobile version defaults to a text-only version with a solid background. This behavior ensures a better user experience on mobile devices and reduces vertical scrolling / swiping to access the page content.

**Landscape mode on mobile**

Landscape mode on mobile is challenging as there is less room available for the type to fit into the hero. We are using the 693x320 screen size as the example to solve for landscape mobile. Website analytics for the week of July 7th - August 3rd place this resolution at number 25 in the Technology list.

**XXS Landscape**

693x320

100% vh

xxs-landscape header is used

For this breakpoint, the visual height of the image needs to go to 100% or we just won't have room for the type to overlay the hero - otherwise we'd have to look at responsive stacking of the media and text overlay- which we don't want to do for just the landscape on mobile.

The hero does not need to stack at this breakpoint. You will find an example of the Parent Header used in the xxs-landscape component.

^ xxs-landscape mode with video for the background media (top) and a static image (bottom).

# Display anatomy and differences by breakpoint

## XXS-XS

* @xxs, the parent name in the header is stacked
* The header scales vertically to fit the parent website name (maximum 3 lines @xxs breakpoint)
* @xxs - s, the hero text group spans 12 columns
* The text group has a 80px margin-bottom
* For the video version, the pause/play icon has an 80px margin-bottom (icon to the base of the hero)

xxs-landscape

* @xxs -landscape, the hero text group spans 12 columns
* The header container has a fixed height of 88px
* The text group has a 80px margin-bottom
* For the video version, the pause/play icon has an 80px margin-bottom (icon to the base of the hero)

## Small

* @xxs - s, the hero text group spans 12 columns
* The header container has a fixed height of 88px
* The text group has a 112 px margin bottom
* For the video version, the pause/play icon has an 80px margin-bottom (icon to the base of the hero)

## Medium-XXL

* @medium - xxl breakpoints, the text group spans 7 columns
* The header container has a fixed height of 88px

# 'Tall hero' compared to the 'hero'

NOTE about the tall hero compared to the [hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bc3e201f9a354d48f89c57/tab/design?mode=preview)

The hero component (on the right below) is designed for the top tier. The type is a display face and has an alignment that related to the story scroll hero. The H1 is 1-3 words, for example, 'About UVM', 'Athletics'.



![alt text](image/tall-hero_funcD/11.png)

By comparison, the tall hero (shown on the left above) uses the [tGrp_hero ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64c0174348713a131a4ff302/tab/design?mode=preview)text lockup with the name of the website/group as the H1 with an optional .p tag line.
