# Atoms

**1x1 image**

# Molecules

**5050-ContentGroup**

Ivy / Components / Molecules / [5050-contentGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639a145541981d7b31a51ec7/tab/design?mode=preview)

**Image Group**

Ivy / Components / Molecules / Media / [imgLockup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639b408c9a2534b5d12059b3/tab/design?mode=preview)

(square image in all places would be replaced by the imgLockup of 3 square images)

**Text Group**

Ivy / Components / Molecules / Type Groups / [txtGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6392084441981d35d9a34fc8/tab/design?mode=preview)
