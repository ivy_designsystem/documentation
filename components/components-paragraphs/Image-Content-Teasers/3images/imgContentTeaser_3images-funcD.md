# Introducing the "3 Images" Image Content Teaser component. It is closely related to the [Basic Image Content Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview), with the difference being that the single square image of the Basic component is replaced by a grouping of 3 square images.

![1727447971615](image/imgContentTeaser_3images-funcD/1727447971615.png)

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOdDvbs8WFedUfbUiSsmriJiihxI_3PFXhr3-ZF5lcGNN9DcLz0VkDoZsaEVG-BFnCpv43naNw7wRLf0OYTt8WEKFE3ceLkIk-f-uiBdtg8eKAR-6R2mCuQ0xvkHMR7QnTg==?assetKey=university-of-vermont%2Fivy%2Fbtv_example-SyT2jai4h.png)

This component displays a grouping of 3 square images, no caption. It is available with a white background only.

This component uses the same text group and text group options found in the Basic Image Content Teaser component. The text group consists of a headline and a paragraph of text.

The text group is mandatory. A call-to-action is not mandatory. Options for the CTA are a single button or a link list.

## **Usage**

Content editors would choose this component if they have a multifaceted subject they wish to elevate that would be better described with a few images rather than just one.

The example above is about the location of the university. This component works well as our location in Burlington, VT on the lake, near the green mountains, is better told with a few images rather than one.

Just like all Image Content Teaser components, it provides a "quick take" of high-level content and with the link options,  it's a component that can also drive users to deeper levels of the website. The 1x1 format for each image is really flexible and with cropping accommodates either vertical or horizontal images.

# Data

**Mandatory content**

(3) 1x1 images 1080x1080 each

headline: simple text

paragraph of text, simple text

**Optional content**

Call to action link(s), choose one:

Button (single button, style varies depending upon background color of the component)

Link List with arrow icon after

## Character Counts

**Heading: shortText, **60 characters

**Body: short text, **max 200-300 characters, plainText (this may evolve to become WYSIWIG but we are going to roll it out with plain text)

**Optional button:** button width is maximum 256

**Optional Link List:** if selected for the link, the minimum is 1, maximum links 8

Developer note:

need to figure out how to let users select one or the other- the button or an individual link or link list. And how do we limit and notify that the button text is too long

**Perhaps:**

* user to decide if button or link list
* If they pick the button, deliver a Toast error if they are over the character count and direct them to select link.
* Have a tool tip with recommended character count.

## M**andatory design elements**

**Images:  **3 images are mandatory. the aspect ratio for each is 1x1 with original asset size of 1012x1012.

### Text content:

This text pairing is a molecule:

name: txtGroup

**Variable or optional design elements**

Background colors

white #FFF

*NOTES*

*Note: The background color determines the palette for the components. A content admin can choose, for example, which background color they want the component to have, but they cannot choose the color of the text or the button.*

***(Dev question: ****How easy is it to have a background option? If we also offered neutral #F7F7F7, it would extend the usage of this component as it could better pair with neighboring components AND no change would be needed to the styling of any of the content elements- text or image)*

# Image lockup

![1727448011832](image/imgContentTeaser_3images-funcD/1727448011832.png)

For more information about the image group, see[ imgLockup ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639b408c9a2534b5d12059b3/tab/design?mode=preview)→

# Layout Variations

**Mirror**

The entire component can be mirrored with the image group appearing either on the left or the right. Note that image lockup configuration does not change whether the text is to the right of left of it. The small image is always on the top right.

*Developer notes for admin UI*

 Ideally, mirroring and background color selection would happen in a live-edit view while looking at the layout .

**Optional Call to Action (CTA)**

The CTA for the Image Content Teaser component is optional. You can have either a single button or a Link List. The example below has a single button. The button for the Image Content Teaser components with the white or neutral background is a solid, primary1 button.  *Note maximum width of the CTA button is 256.*


![1727448031565](image/imgContentTeaser_3images-funcD/1727448031565.png)


Alternatively, instead of a single button, you can have a link list. Link lists are comprised of anywhere from 1-8 links (this is our target for launch but TBD where the "sweet spot" is in limiting or not limiting this number)


![1727448046390](image/imgContentTeaser_3images-funcD/1727448046390.png)


**Interactions **

All are on scroll:

* image fades on
* text shift-fade [example](https://www.bucknell.edu/)

## M**obile**

Regardless of the position of the image on desktop, the mobile view always has the image on the top.

## B**reakpoint notes**

**xxs, xs: **Stacked view

**s-xxl: **side by side view

*Note: The largest size of the component is 1920 but the content itself doesn't scale above the 1440 breakpoint, just the background scales.  *

# Layout difference by breakpoint

### **xxs and xs**

Both the imageLockup and text content span all 12 columns at the xxs and xs breakpoints. The space between images is 4px.

![1727448058219](image/imgContentTeaser_3images-funcD/1727448058219.png)


### **small to large breakpoint**

Both the imageLockup and text content span 6 columns at the small breakpoint. The space between the images is 8px. Notice how the image group is on the centerline of the grid, hanging into the center gutter by 16x.

**s-768-xxl-1920 **image lockup spans 6columns + 16px into the 32px center gutter. Measurements are the same even if mirrored with the image group on the right.

![1727448073946](image/imgContentTeaser_3images-funcD/1727448073946.png)


![1727448098925](image/imgContentTeaser_3images-funcD/1727448098925.png)


### **xl and xxl breakpoints (they are the same)**

The exact same layout is used for anything xl and larger. Although the background scales to the full browser width in all instances, the actual content of the component does not scale above 1440.

Both the imageLockup and text content span 6 columns at the xl and xxl breakpoints. The space between the images is 16px. Notice how the image group is on the centerline of the grid, hanging into the center gutter by 16x.

![1727448114536](image/imgContentTeaser_3images-funcD/1727448114536.png)
