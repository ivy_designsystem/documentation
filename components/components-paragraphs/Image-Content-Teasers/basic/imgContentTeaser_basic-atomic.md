# Atoms

**Secondary button, outline**

Ivy / components / Atoms / Buttons / [Secondary outline](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393afe4f291f20ef5204390/tab/functional%20narrative?mode=preview)

**Solid button**

Ivy / Components / Atoms / Buttons / [Solid](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6397e0ddc887bdbe24eedc1f/tab/design?mode=preview)

# Molecules

 **LinkList icon after**

Ivy / Components /Molecules / LinkLists / [Link List icon after](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393afe4f291f20ef5204390/tab/functional%20narrative?mode=preview)

**5050-ContentGroup**

Ivy / Components / Molecules / [5050-contentGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639a145541981d7b31a51ec7/tab/design?mode=preview)

**Text Group**

Ivy / Components / Molecules / Type Groups / [txtGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6392084441981d35d9a34fc8/tab/design?mode=preview)
