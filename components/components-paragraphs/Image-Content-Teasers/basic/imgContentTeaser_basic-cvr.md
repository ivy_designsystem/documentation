# The Basic Image Content Teaser is a foundational component- a real workhorse. It has a text group and a square image.

# It is designed to elevate snippets of content and provide links to drive users to deeper levels of the website. It is often used in pairs with the image alternating left and right.

![1726150474229](image/imgContentTeaser_basic-cvr/1726150474229.png)
