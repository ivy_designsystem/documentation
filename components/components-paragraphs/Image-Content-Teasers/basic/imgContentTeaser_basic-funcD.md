```

```

# Introducing the Basic Image Content Teaser.

![1726150517052](image/imgContentTeaser_basic-funcD/1726150517052.png)

This component displays a 1x1 image on desktop and 16x9 on mobile, no caption. It is available with either a white, neutral or primary1 background.

The text group consists of a headline and a paragraph of text.

A call-to-action is not mandatory. Options for the CTA are a single button or a link list.

## **Usage**

Content editors would choose this component if they have a message they want to elevate. This component is for a quick take-away message or, with the CTA link options, it's a component that drives users to deeper levels of the website. The 1x1 format is really flexible and can with cropping accommodates either vertical or horizontal images.

# Data

**Mandatory content**

Single image in 2 crops: 1x1 and 16x9 (16x9 is used on mobile) 1920x1080 and 1080x1080

headline: simple text

paragraph of text, simple text

**Optional content**

Call to action link(s), choose one:

* Button (single button, style varies depending upon background color of the component)
* Link List with arrow icon after

## Character Counts

**Heading: shortText, **60 characters

**Body: short text, **max 200-300 characters, plainText (this may evolve to become WYSIWIG but we are going to roll it out with plain text)

**Optional button:** button width is maximum 256

**Optional Link List:** if selected for the link, the minimum is 1, maximum links 8

Developer note:

need to figure out how to let users select one or the other- the button or an individual link or link list. And how do we limit and notify that the button text is too long

**Perhaps:**

* user to decide if button or link list
* If they pick the button, deliver a Toast error if they are over the character count and direct them to select link.
* Have a tool tip with recommended character count.

## M**andatory design elements**

**Image: **An image asset is mandatory. Two aspect ratios are used. Two images will need to get uploaded by the content admin.

1. 1x1 for desktop with original asset size of 1400x1400 *(larger than we need but is the target asset size for the *[*Big Image Content Teaser component*](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639bcb751efecba0faf6daed/tab/design?mode=preview)*, so if users change their mind and want to have a component with a BIGGER impact, they can switch without worry of degrading their image asset)*
2. 16x9 for mobile with an original asset size of 1920x1080

### Text content:

This text pairing is a molecule:

name: txtGroup

## **Variable or optional design elements**

Background colors

white #FFF

neutral #F7F7F7

Primary1-dark #154734 (Catamount Green)

Note: The background color determines the palette for the components. A content admin can choose, for example, which background color they want the component to have, but they cannot choose the color of the text or the button.

# Layout detail

Text group is centered vertically as a group with image.

# Layout Variations

**Mirror**

The entire component can be mirrored with the image appearing either on the left or the right.

*Developer notes for admin UI*

 Ideally, mirroring and background color selection would happen in a live-edit view while looking at the layout .

**Optional CTA**

The CTA for the Image Content Teaser component is optional. You can have either a single button or a Link List. The 5050 below has a single button. The button for the Image Content Teaser components with the white or neutral background is a solid, primary1 button.  *Note maximum width of the CTA button is 256.*

![1726150533418](image/imgContentTeaser_basic-funcD/1726150533418.png)

The button style for the Basic Image Content Teaser with a primary1 background is an outline button, as shown here.

![1726150553353](image/imgContentTeaser_basic-funcD/1726150553353.png)

Alternatively, instead of a single button, you can have a link list. Link lists are comprised of anywhere from 1-8 links (this is our target for launch but TBD where the "sweet spot" is in limiting or not limiting this number)

![1726150567759](image/imgContentTeaser_basic-funcD/1726150567759.png)

**Interactions **

All are on scroll:

* image fades on
* text shift-fade [example](https://www.bucknell.edu/)

## M**obile**

Regardless of the position of the image on desktop, the mobile view always has the image on the top.

![1726150578601](image/imgContentTeaser_basic-funcD/1726150578601.png)

```

```

## B**reakpoint notes**

**xxs, xs: **Stacked view

**s-xxl: **side by side view

The Image Content Teaser scales UP to 1920, the maximum size.

Component does not scale up form 1920.
