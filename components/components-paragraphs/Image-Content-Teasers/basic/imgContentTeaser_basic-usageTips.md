# Introducing the Basic Image Content Teaser.

![1726150630225](image/imgContentTeaser_basic-usageTips/1726150630225.png)

This component displays a 1x1 image on desktop and 16x9 on mobile, no caption. It is available with either a white, neutral or primary1 background.

The text group consists of a headline and a paragraph of text.

A call-to-action is not mandatory. Options for the CTA are a single button or a link list.

## **Usage**

## Content editors would choose this component if they have a message they want to elevate. This component is for a quick take-away message or, with the CTA link options, it's a component that drives users to deeper levels of the website. The 1x1 format is really flexible and can with cropping accommodates either vertical or horizontal images. he Basic Image Content Teaser is for a small amount of high level content and links

The Basic Image Content Teaser is designed for short, elevated messaging. With the optional CTA, the Basic Image Content Teaser is a powerful tool to start users on a conversion path, information scent, and to drive them to deeper levels of the site.

It is for a SMALL amount of copy. It will visually break if too much copy is added so a character limit has been set to ensure that the intention is maintained and there is focus to the content.

If too many CTAs or list Link items are added, it will also visually break.

# Data

**Mandatory content**

Single image in 2 crops: 1x1 and 16x9 (16x9 is used on mobile)

headline: simple text, 60 characters max

body: max 200-300 characters, plainText

**Optional content**

Call to action link(s), choose one:

* Button single button, button width is maximum 256

Link List with arrow icon after: the minimum is 1, maximum links 8

## Resources and tips for text-heavy pages

If you have a lot of copy, use a text-heavy component. The Basic Image Content Teaser should not be used if you have a lot of copy!

See [this page](https://uvmd10.drup2.uvm.edu/cas/student-opportunities) for inspiration and resources for creating text-dense pages that are beautifully layed-out and maintain a good flow and high level of consumeability despite the density of content. This page breaks the "don't have more than 4 in a row" rule, but there are some full-width image banners and green background fills to disrupt and re-set the rhythm so it works.

## Layout tips

## 01 DON'T

* No more than 4 in a row
* Do not have the image in the same place for each. Make sure you mirror the layout when stacked. It is designed for the layout to mirror and alternate.

*Note: There may be an application for a layout like this where the images are all flush left and we want more of a "list" view, but this is not the original intention. Avoid this scenario until usage guidelines change.*

![1726150642161](image/imgContentTeaser_basic-usageTips/1726150642161.png)

## 02 DO

* The full-width bannerWithOverlay works great in the middle to disrupt the predictable rhythm.
* Make sure you mirror the layout when stacked

![1726150655254](image/imgContentTeaser_basic-usageTips/1726150655254.png)

## 03 DON'T

* Don’t mix and match multiple backgrounds. Be consistent.

The issue with this example is that it goes from neutral to green to white to neutral. In this case, the green is OK to disrupt the rhythm, but the others should all be white or neutral.

![1726150686576](image/imgContentTeaser_basic-usageTips/1726150686576.png)

## 04 DO

* You can disrupt with one color. If you do this, maintain the alternate mirroring of the image position.![1726150699804](image/imgContentTeaser_basic-usageTips/1726150699804.png)

## 05 Don't pack with tons of copy or links

* **Elevate a snippet of content** Do not put more than a short paragraph of text
* **Don't make them think or figure out, TLDR** Do not have tons of links in your link list. Keep it to 4 or 5 maximum.
* **Confidence in the click** Link text should be short and related to the content you are linking to

![1726150732519](image/imgContentTeaser_basic-usageTips/1726150732519.png)

The last 2 panels above show good usage and good target copy lengths for the text paragraph and the link list.
