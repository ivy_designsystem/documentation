#  Atoms

**image /* size dependent upon development approach... if we can set a focal point or if we need to upload breakpoint specific image croppings */**

**Solid button**

Ivy / Components / Atoms / Buttons / [secondary outline button ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61ba3cea85ca0019a05c151a/tab/design?mode=preview)

 **Image** , cropped to 1x1, 1400x1400

# Molecules

**5050-ContentGroup**

Ivy / Components / Molecules / [5050-contentGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639a145541981d7b31a51ec7/tab/design?version=63a09f80836f090f00fb3ed0&mode=preview)

**Text Group**

Ivy / Components / Molecules / Type Groups / [txtGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6392084441981d35d9a34fc8/tab/design?mode=preview)

[](https://uvmedu.invisionapp.com/dsm/university-of-vermont)
