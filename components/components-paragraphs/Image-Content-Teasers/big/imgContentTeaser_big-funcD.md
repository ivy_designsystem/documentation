# Meet the Big Image Content Teaser.  It is closely related to the [Basic Image Content Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview), with the difference being that instead of a square image, the image takes up 6 columns on desktop and 12columns on mobile. It makes a bigger impact than the regular 5050.

![1727448392716](image/imgContentTeaser_big-funcD/1727448392716.png)

**Usage**

Content editors would choose this component if they have a message they want to elevate with a big impact.  The image is quite large and needs to be a really special image to be used at this size. There are 2 options for linking- content editors can choose either a button with a single link or a linkList.

# **Data**

**Mandatory content**

(1) 1x1 image, 1400x1400px (Drupal to scale)

 **headline** : simple text

 **paragraph of simple text** , not WYSIWYG

**Optional content**

Call to action link(s), choose one:

* Button (single button, style varies depending upon background color of the component)
* Link List with arrow icon after

## Character Counts

**Heading: shortText, **60 characters

**Body: short text, **max 200-300 characters, plainText (this may evolve to become WYSIWIG but we are going to roll it out with plain text)

**Optional button:** button width is maximum 256

**Optional Link List:** if selected for the link, the minimum is 1, maximum links 8

*Developer note: *

*need to figure out how to let users select one or the other- the button or an individual link or link list. And how do we limit and notify that the button text is too long*

**Perhaps:**

* user to decide if button or link list
* If they pick the button, deliver a Toast error if they are over the character count and direct them to select link.
* Have a tool tip with recommended character count.

# Component dimensions

* Entire component scales to full breakpoint width
* On desktop, image is 50% width and the text container is 50% width
* Image is 64vh* (code this to be flexible since my math for the vh is a guestimate. We are going to tweak the vh number during QA and maybe again after people start working with it and we have a better idea of how much content they want to work with)*
* The *container* with the text has a primary1 background is also 50% of the breakpoint width and the height of it always seamlessly matches the height of the image (64vh)

# Image asset tips and prep

* Content creator should crop the image to 1x1 keeping the focus of the image in the center
* Image scales to 64% vh (visual height) and then centers horizontally in the container
* Image is cropped by the image container

![1727448403337](image/imgContentTeaser_big-funcD/1727448403337.png)

The above table shows the best guestimate for the visual height (vh) for each image by breakpoint. For starters, our target height is 64vh.

 ![1727448417393](image/imgContentTeaser_big-funcD/1727448417393.png)Make sure you build in the flexibility to edit the vh value as we won't really get a feel of it until we can view it in our browsers.

![1727448505538](image/imgContentTeaser_big-funcD/1727448505538.png)

The [Video](https://uvmoffice.sharepoint.com/:v:/s/CommunicationsTEAM408/ESJv4wL12xRFvnbjeMN4DhsBYVfFgUy7OmQullMN_kPoYA?e=dcPKxX) above is for *content creators* and provides tips for cropping your image to a 1x1 aspect ratio with the focal point being in the center.

# Layout Variations

**Stacking multiple components**

For all breakpoints, this component can be stacked vertically with no margin between.

**Mirror**

For all breakpoints xs-480 and larger, the component can be mirrored where the text overlay block can be mirrored so as to appear either on the left or the right.

![1727448452802](image/imgContentTeaser_big-funcD/1727448452802.png)

*Developer notes for admin UI*

Ideally, mirroring and background color selection would happen in a live-edit view while looking at the layout so decisions on which side the text block should appear is made within the context of the layout and the rhythm of surrounding components.

**Optional CTA**

The CTA for the 5050big component is optional. You can have either a single button or a Link LIst. The examples shown have a single button.

## **Interactions **

All are on scroll:

* image fades on
* text overlay box [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

## B**reakpoint notes**

## Text Group

There are a few display variables for the text group by breakpoint. The markup in the hand-off file will help bring clarity to this table.

![1727448466625](image/imgContentTeaser_big-funcD/1727448466625.png)

## Text Group alignment

The text group centers vertically in it's container.

## M**obile**

On mobile, the image and text group stack with the image on the top. The text is a smaller body size.

![1727448477381](image/imgContentTeaser_big-funcD/1727448477381.png)


**Desktop**

**s-xxl: **side by side view

*Note: The largest size of the component is 1920 and the component extends fully to fill this larger breakpoint. 1920 is the largest width- it does not scale bigger than that.*/
