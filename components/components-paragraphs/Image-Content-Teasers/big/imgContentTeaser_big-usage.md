# Meet the Big Image Content Teaser. It is closely related to the [Basic Image Content Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview), with the difference being that instead of a square image, the image takes up 6 columns on desktop and 12columns on mobile. It makes a bigger impact than the regular 5050.

![1727448566143](image/imgContentTeaser_big-usage/1727448566143.png)

**Usage**

Content editors would choose this component if they have a message they want to elevate with a big impact. The image is quite large and needs to be a really special image to be used at this size. There are 2 options for linking- content editors can choose either a button with a single link or a linkList.

# **Data**

**Mandatory content**

(1) 1x1 image, 1400x1400px (Drupal to scale)

*Note: the 800x800 image used for the Basic and 3 Image Content Teasers may work fine*

 **Headline** : simple text, 60 characters or less

**Body: short text, **max 200-300 characters, plainText

**Optional content**

Call to action link(s), choose one:

* Button: single button, maximum width of 256px
* Link List with arrow icon after. Minimum is 1, maximum links 8

# Image asset tips and prep

* Content creator should crop the image to 1x1 keeping the focus of the image in the center
* Image scales to 64% vh (visual height) and then centers horizontally in the container
* Image is cropped by the image container

![1727448575716](image/imgContentTeaser_big-usage/1727448575716.png)

The above table shows the best guestimate for the visual height (vh) for each image by breakpoint. For starters, our target height is 64vh.

![1727448589184](image/imgContentTeaser_big-usage/1727448589184.png)Make sure you build in the flexibility to edit the vh value as we won't really get a feel of it until we can view it in our browsers.

The [Video](https://uvmoffice.sharepoint.com/:v:/s/CommunicationsTEAM408/ESJv4wL12xRFvnbjeMN4DhsBYVfFgUy7OmQullMN_kPoYA?e=dcPKxX) above is for *content creators* and provides tips for cropping your image to a 1x1 aspect ratio with the focal point being in the center.

# Layout Variations

**Stacking multiple components**

For all breakpoints, this component can be stacked vertically with no margin between.

**Mirror**

For all breakpoints xs-480 and larger, the component can be mirrored where the text overlay block can be mirrored so as to appear either on the left or the right.

![1727448600103](image/imgContentTeaser_big-usage/1727448600103.png)
