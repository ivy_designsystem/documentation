# Atoms

**16x9 image**

**Secondary outline button**

Ivy / Components / Atoms / Buttons / [Secondary outline button](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61ba3cea85ca0019a05c151a/tab/design?mode=preview)

# Molecules

**5050-ContentGroup**

Ivy / Components / Molecules / [5050-contentGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639a145541981d7b31a51ec7/tab/design?version=63a09f80836f090f00fb3ed0&mode=preview)

**Text Group**

Ivy / Components / Molecules / Type Groups / [txtGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6392084441981d35d9a34fc8/tab/design?mode=preview)
