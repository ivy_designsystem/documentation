# Meet the 5050_overlayCaption component.  It is closely related to the [5050 component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/functional%20narrative?mode=preview), with the difference being that the image is a background image filling the entire container that spans the breakpoint width AND the text group is boxed in with a background.

![1727448994148](image/imgContentTeaser_overlay-cvr/1727448994148.png)
