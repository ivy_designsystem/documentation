# Meet the Overlay Image Content Teaser component. It is closely related to the [Basic Image Content Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview), with the difference being that the image is a background image filling the entire container that spans the breakpoint width AND the text group is boxed in with a background.

![1727448826036](image/imgContentTeaser_overlay-funcD/1727448826036.png)

## **Usage**

Content editors would choose this component if:

1. They have a subject or message they wish to elevate with a quick takeaway message
2. They want to call users attention and lead them to a deeper level of the site via the CTA button link
3. They have a really nice 16x9 horizontal image for the background

Just like all 5050 components, it provides a "quick take" of high-level content. It's a component that can also drive users to deeper levels of the website.

Note: That this component  has character limits built in. The overlay caption can only scale so big- especially at the Small-768 breakpoint. If you have more copy, need a list of links, you may consider the [5050](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview) or [5050big](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639bcb751efecba0faf6daed/tab/design?mode=preview) components.

# Data

**Mandatory content**

(1) 16x9 image 1920x1080

 **headline** : simple text

 **paragraph of simple text** , not WYSIWYG

**Optional content**

**Button** (single button)

## Character Counts

**Headine: shortText, **40-48 characters, 9 words

 **Body:** max 200-269 characters, plainText

## Size of overlay box

*Note: Overlay box is positioned to the bottom of the image maintaining the component's bottom padding. Scales UP as content is added. The Overlay box has it's own internal padding.*

![1727448861926](image/imgContentTeaser_overlay-funcD/1727448861926.png)

** s-768 breakpoint has an exception, the head is a mobile H3 and the body text is .small, even though the small breakpoint is the start of the desktop breakpoints.*

# Layout Variations

**Mirror**

This is a mirrored component where the text overlay block can be mirrored so as to appear either on the left or the right. All breakpoints mirror except the xxs-320 breakpoint.

![1727448879999](image/imgContentTeaser_overlay-funcD/1727448879999.png)

*Developer notes for admin UI*

Ideally, mirroring would happen in a live-edit view while looking at the layout  so decisions on which side the text block should appear is made within the context of the layout and the rhythm of surrounding components.

**Optional CTA**

The CTA for the 5050imgLockup component is optional. It is a single button with a maximum width of 256. Text cannot wrap to 2 lines.

## **Interactions **

All are on scroll:

* image fades on
* text overlay box [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

**Background color for lazy visual loading sequence: **

Neutral #F7F7F7 may work well for this as the UX will feel like a skeleton load for this component. Give that a try first and we will look at it in QA.

## Image size

For all breakpoints, the image is 16x9 and scales to the full breakpoint width while maintaining aspect ratio.

# B**reakpoint notes**

## M**obile**

On mobile, the component is stacked with the image on the top and the text box an offset overlay. (only a small portion of the text box overlays the image on mobile, the rest hangs below the image)

**xxs**

* The text overlay box is 12 columns for xxs. It remains centered.
* There is not a Left/Right variation for the xxs breakpoint as the text box remains centered.
* If 2 component are stacked, there is a total of 96px margin between the two

( margin-top: 32px + margin-bottom: 64px = 96px)

![1727448895011](image/imgContentTeaser_overlay-funcD/1727448895011.png)

**xs** The text overlay box is 8 columns for xs.

* There is a Left and Right variation starting at the xs breakpoint.
* If 2 component are stacked, there is a total of 96px margin between the two

( margin-top: 32px + margin-bottom: 64px = 96px)

![1727448903533](image/imgContentTeaser_overlay-funcD/1727448903533.png)

## Holding the image edge

Please add a 5% black border to the background image, 1px, just as a safeguard to hold the image edge if the image is very light. Most of the time it will not be seen.

/* modal/black/modalBlack-05 */

border: 1px solid rgba(0, 0, 0, 0.05);

## Margins on mobile

* If 2 component are stacked, there is a total of 96px margin between the two

( margin-top: 32px + margin-bottom: 64px = 96px)

Below is a grab of the xs-480 artboard with spacers to indicate the top and bottom margins.  The spacers in are units of 8.

![1727448921670](image/imgContentTeaser_overlay-funcD/1727448921670.png)

# **Margins on desktop**

**Desktop: Stacking margins and limits**

There are no top or bottom margins for this component on desktop (small-768 and larger).

We will rely upon the margins of the adjacent components.

This will allow the 5050-overlayCaption to butt when and if they are stacked on mobile.

**Guardrail**

Note: A stack of 2 is the limit. Below is a screen grab of a stack of 2 sandwiched between 5050's.

![1727448938575](image/imgContentTeaser_overlay-funcD/1727448938575.png)

The above grab shows a group of 2 overlayCaption components disrupting a layout of 5050s. Notice how they butt without vertical space between them. The margins (white space) in the layout example above is provided by the 5050's.

**Padding on Desktop**

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOQ_O59_j55euRfN2kfS2BTKOmEfYuwvhWh5slYxo7mJpUsnCKQrh-2_ak3hZHRrr_rek3qcRgnCX21EHBis8xzISa52VB6bYX9dnfpgHRNIqaR44SOXA27rwpZGImvErbg==?assetKey=university-of-vermont%2Fivy%2FoverlayCapstacked-ryz3i79h6.png)

Above are two small-768 size 5050-overlayCaption components stacked. The bottom one shows the edge case with the maximum amount of text content. The upper one has less content. The caption is bottom positioned over the image and has a 40px space to the bottom of the image. As content increases, the caption box scales upwards. At its maximum size, it has a 40px space to the top of the image.

**Desktop**

**s-xxl: **overlay caption is on top of the image. Image fills the container with the width going to the full breakpoint.

*Note: The largest size of the component is 1920 but only the image container and bg image scale to 1920. The 1440-xl grid centers and the txtGroup box doesn't scale above the 1440 breakpoint.*

*Here is the 1920 image with the 1440-xl grid centered below it.*

![1727448962822](image/imgContentTeaser_overlay-funcD/1727448962822.png)
