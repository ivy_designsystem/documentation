# Meet the Overlay Image Content Teaser component. It is closely related to the [Basic Image Content Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview), with the difference being that the image is a background image filling the entire container that spans the breakpoint width AND the text group is boxed in with a background.

![1727448784796](image/imgContentTeaser_overlay-usage/1727448784796.png)

## **Usage**

Content editors would choose this component if:

1. They have a subject or message they wish to elevate with a quick takeaway message
2. They want to call users attention and lead them to a deeper level of the site via the CTA button link
3. They have a really nice 16x9 horizontal image for the background

Just like all 5050 components, it provides a "quick take" of high-level content. It's a component that can also drive users to deeper levels of the website.

Note: That this component has character limits built in. The overlay caption can only scale so big- especially at the Small-768 breakpoint. If you have more copy, need a list of links, you may consider the [5050](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63979f5a9a253401e61effb5/tab/design?mode=preview) or [5050big](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/639bcb751efecba0faf6daed/tab/design?mode=preview) components.

# Data

**Mandatory content**

(1) 16x9 image

**Headine: shortText, **40-48 characters, 9 words

 **Body:** max 200-269 characters, plainText, not WYSIWYG

**Optional content**

**Button** (single button)

# Layout Variations

**Mirror**

This is a mirrored component where the text overlay block can be mirrored so as to appear either on the left or the right. All breakpoints mirror except the xxs-320 breakpoint.

![1727448805248](image/imgContentTeaser_overlay-usage/1727448805248.png)
