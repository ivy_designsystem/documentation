# The Image CTA component  is a group of 3 Call to Action links, elevated by using an attention grabbing, full color image, in lockup with a styled text link.

# The purpose of the Image CTA is to increase engagement and drive users to specific areas on the site where they can take action.

![1727450855696](image/imgLinkGrp-cvr/1727450855696.png)
