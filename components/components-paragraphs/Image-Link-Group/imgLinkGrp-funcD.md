# The Image CTA component  is a group of 3 Call to Action links, elevated by using an attention grabbing, full color image, in lockup with a styled text link.

# The purpose of the Image CTA is to increase engagement and drive users to specific areas on the site where they can take action.

## Data

 **Images** : (3) cropped to squares. Should be 1080x1080 and Drupal will scale as needed.

**Text:** Manually entered text for the CTAsimple text group. Target is 2 words but the text can wrap to 2 lines. (not recommended)

## Design Elements

**Scrim overlay**

The scrim is designed to shade the bottom part of the image to ensure adequate contrast for the the CTA type and overlay.

/* gradations/gradation-scrim */

background: linear-gradient(180deg, rgba(38, 41, 44, 0.0001) 26.14%, rgba(38, 41, 44, 0.848063) 100%);

mix-blend-mode: multiply;

**Image**

Square. Drupal to scale for display and optimize.

**CTA text** (1 or 2 action-oriented words)

**Right arrow SVG**

## Display

**Desktop**

The ImageCTA component is horizontal on desktop (s-480 to xl-1440). The images are square, butt with no space between them, and take up 1/3 of the page content area not including the left and right margins.

**xxl-1920:** Could it be used @ the XXL-1920 breakpoint at some point? Perhaps. It just scales up so plan for this too.

UGH: SUS the type size changed somehow in the file. Which is it? It's also not right on the website as it is 32px everywhere- all breakpoints.

![1727450908243](image/imgLinkGrp-funcD/1727450908243.png)



![1727450977354](image/imgLinkGrp-funcD/1727450977354.png)


# **order of CTAs**

**Inquire **

**Visit**

**Apply**

**Mobile**

The ImageCTA component is vertical on mobile (xxs320-xs480). The images are square, butt to each other with no space between them. They are the size of the page - spanning 12 columns- not including the left and right margins.

The CTA text on mobile is smaller as well as the padding.

**Background**

size: The background extends full browser widdth. Take note of the internal top and bottom padding for all breakpoints.

 *color* : This component has a solid background. Plan for it to be able to be changed by the content admin during a page edit. For the December 2022 launch of the updated home page, we only need the primaryDark background color shown in the handoff. For future releases, plan for the 2 light background colors to be added.

* white #FFF
* neutral #F7F7F7

## Image Guidelines

**Image selection and style**

Photography should feel natural, engaged and 'in the moment'. It should not be self-conscious or posed. Avoid images where people are looking at the camera. It should feel like we are looking in. Experiential, hands-on learning that shows an engaged academic experience is preferred even if taking place within our classrooms and facilities. Photos are used in full color- not black and white, duotone or tinted.

**Diversity and Inclusion**

Make every effort to select photos that are diverse and inclusive without tokenizing. UVM and Vermont are not richly diverse but are trying. Be thoughtful and deliberate in the selection of photos to portray UVM as inclusive. The selection should feel natural and not forced.

**Select images to increase engagement**

Each image in this component is an integral part of a Call to Action. It's to get users to do something- to click on a link and take an action. The image needs to support the action that needs to be taken and be relevant.

Since 3 images are used in this component, and they butt without any space, it's important to also consider how the 3 images all work together. Even though they butt, the boundaries for each image should be visible. They should not appear to blend into one.

**Type Contrast**

A dark scrim is added programmatically to each image. The scrim is designed to shade the bottom part of the image. In a natural and smooth gradation, it goes from 100% transparency of our darkGray to 26% opacity of our darkGray at the bottom, to ensure adequate contrast for the white CTA type that overlays the image at the bottom.

**Image sizing**

Images should be cropped to square- recommend 600x600 size @72dpi, PNG or JPG 100% quality. Drupal will optimize and scale.

## Interaction

**Desktop**

onLoad:

background loads first and then each image

Images load with [fade d](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview)[elay](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview), consecutively and rapidly, left to right on dt and top to bottom on mobile.

**Micro-interaction: **on-hover of the CTA (entire image is hit area), the image turns to 100% primary3 yellow and the text turns to primary1-darkest

![1727450956753](image/imgLinkGrp-funcD/1727450956753.png)


**Mobile**

Same as above. Image background change, if possible, is on-tap.

## CTA Text Guidelines

Manually entered text for the CTAsimple text group. Target is 2 words but the text can wrap to 2 lines. (not recommended)

The following examples show why 1 or 2 words are recommended for this component for each CTA. In this example, "explore our academic ecosystem" is the edge case for a long CTA. It covers the image completely as a few breakpoints - it's too long.

The example also shows why all 3 CTAs should all have the same amount of text, or nearly the same amount of text.

**Going to 3 lines of text is just bad.** Don't do it. The image is obliterated in many scenarios. Text should be kept to not more than 2 lines at all breakpoints.

**Text Case:**

Text is sentence case: the initial character is capitalized and the rest is lower case.

**Exceptions to Text Casing**

Words can have initial caps if they are official titles- ie: The Davis Center

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOf34JBNA0c8daUAEBhjXTZfZqTlg9wkDDPxE0W2Hlap8UhqqMUcuCVi0iN3jAHkvel9vdgdUJkDgcMcXqVI8nR1IKHt7sm5xcu14CWSZ37EqoNCbFNNt2sMy_TeryXcxTPqNHnWcUjskhbIH7SceYBI=?assetKey=university-of-vermont%2Fivy%2FScreen+Shot+2022-10-20+at+15.14.14-B1OUAfkVs.png)

## Developer's Note

Text is anchored at the bottom and scales UP from the bottom as text is added. The arrow and text block never shifts down.
