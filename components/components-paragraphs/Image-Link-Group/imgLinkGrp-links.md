# Links

**Text Molecule: CTA simple**

https://www.figma.com/file/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=879%3A40198

The CTA text and arrow lockup


**Figma Handoff doc**

https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=430%3A6224

Figma handoff page for the Image CTA
