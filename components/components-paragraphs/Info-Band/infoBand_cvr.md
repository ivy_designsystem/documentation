# Intro

**The infoBand displays sections of content with short, categorical titles or headings. On desktop, we have a side by side layout with headings to the left and the content panel to the right. On mobile, all content stacks vertically.**

The infoBand is related to the accordion and the vertical tab components. All 3 components off different display options for identical content types. Unlike the Vertical Tab and the Accordion components, all content in the infoBand is displayed. User engagement is not needed to see all of the content.

![1727451465352](image/infoBand_cvr/1727451465352.png)
