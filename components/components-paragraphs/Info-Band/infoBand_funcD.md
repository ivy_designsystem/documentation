# Overview

**The infoBand displays sections of content with short, categorical titles or headings. On desktop, we have a side by side layout with headings to the left and the content panel to the right. On mobile, all content stacks vertically.**

The infoBand is related to the accordion and the vertical tab components. All 3 components off different display options for identical content types. Unlike the Vertical Tab and the Accordion components, all content in the infoBand is displayed. User engagement is not needed to see all of the content.

![1727451635506](image/infoBand_funcD/1727451635506.png)

## Related components and display options

The infoBand, vertical tab and the accordion are  related components. Content editors view the same content either as an infoBand., accordion, or vertical tab. Each has its strengths.

## When to use the infoBand

* To organize related information.
* To make lots of text heavy content more readable with the title hanging in the left sidebar
* To disrupt the rhythm on text-heavy pages to make lots of text more readable
* To display content that is visible without user engagement

## When not to use the infoBand

* Use vertical tabs or accordions instead if you need to get a lot of content into a small space and it's OK that it's not all revealed on page load

## Data

**infoBand title:** Title for the infoBand section. Simple text. 4-10 words (not more than 100 characters recommended)

**content panel: **contains WYSIWG content*

** Media by default scales to fill container. Over-arching rules for Drupal's handling media and for options for content editors to control image scaling need to be discussed and defined. For example, if an editor places an icon in the content panel, it is not desired behavior to have the icon scale to fit the container.*

All content is manually entered by the user.

## Scrolling content

Content flows vertically on the page. When content is longer than the viewport the page should vertically scroll. Content should not scroll inside of an individual panel. Content should never scroll horizontally in a panel.

## Component widths by placement

The infoBand component has a 12-column span. The infoBand Titles have the same width as the section navigation panel. Text in the infoBand Content Panel has the recommended colum-span width of the WYSIWYG content. This is to ensure that the text is readable and has an accessible character count for the line length.

Please take a look at the [layout plan in Figma](https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2216%3A14882&mode=design&t=xVv0FBTHFalYfaZ6-1) where you can see the infoBand layed-out by breakpoint. Turn the layout grid on to see how the component spans columns.

Note: The infoBand container stacks on mobile.

### Vertical spacing

**desktop:** 64px padding top and bottom

**mobile:** 48px padding top and bottom

## Functionality

* On desktop, the infoBand component is displayed side x side with the infoBand title in the left sidebar and the content panel to the right.
* On mobile, the infoBand content stacks.

## Display

### Backgrounds

This component can have a neutral or a white background. The background color extends full viewport. The component stays within it's content container.

### Interaction

Content loads with fade on scroll. This is in place now on the home page and top tier content pages.

### Responsive notes

* On mobile, the infoBand content stacks.
* See the chart above and the [layout plan in Figma](https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2216%3A14882&mode=design&t=xVv0FBTHFalYfaZ6-1) for column spans by breakpoint
