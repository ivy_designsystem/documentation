# The 'Key Program Info' component has a text list with high-level program take-away information.

![1727452455629](image/keyProgramInfo-funcD/1727452455629.png)

# Description

The 'Key Program Info' component displays high-level, at-a-glance information about a program. It is a "one-off" component in the sense that it's usage is very specific. It only appears on the program pages directly below and abutting the hero. For the desktop breakpoints, it has a spacer on the left to leave room for the unit-specific navigation that displays in an expanded state on page load for the desktop breakpoints.

There are no contextual links in this component. No user interaction is required to consume this content. It's purpose is strictly informative.

**Consistency is critical**

It is critical that the content, its presentation and order are consistent for all programs. The University is essentially providing a shopping experience of our course offerings and this high-level information about the programs will facilitate quick comprehension and comparison of our offerings.

This data and component is a harbinger to the Academic Program Finder application that we are planning for in the future. We are setting ourselves up for success by honing this data and creating a consistent display that will allow future, side by side, apples-to-apples comparison of programs.

# Data

* Degree type(s)* **REQ**
* School or College **REQ**
* Class format **REQ**
* Estimated time to complete **REQ**
* Credit hours to graduate **REQ**

Call to action buttons (optional)

* URL for 'Apply >' button
* URL for 'Inquire >' button

Button labels, for now, are fixed: 'Apply' and 'Inquire'.

**Credit hours:** text

Number of credit hours. Required. Can be a range.

Q: excel sheet says: Multiple lines when encompassing multiple program types. Can have multiple fields.

Need examples. To discuss

**Class format: **text

In-person, 100% remote, hybrid ??

**Estimated time to complete:** text

/* I think this is not being used in actual implementation */

**Degree type(s):** [separate from name, BA, BS, MA, PhD etc.]  WHAT IS etc?? Accelerated degrees? What?

Degrees pulled from database, will be all spelled out in full, comma delimited between degrees.

**School or College:** text ( selected from drop down)

* College of Agriculture and Life Sciences
* College of Arts and Sciences
* Grossman School of Business
* College of Education and Social Services
* College of Engineering and Mathematical Sciences
* Rubenstein School of Environment and Natural Resources
* College of Nursing and Health Sciences
* The Patrick Leahy Honors College
* The Graduate College
* Larner College of Medicine
* Professional and Continuing Education
* UVM Extension

School of the Arts? Anything else? ^^

Note to dev: I don't have the data info for all of these. Let's review

# Functionality

This component is informative. User engagement not applicable. There are no links. within the Key Program Info.

The only links are the optional CTAs:

"Apply" and "Inquire"

## FlexCTA buttons

There are 2 button options:

"Apply"

"Inquire"

Both are optional. You can have none, one, or two.

They have min and max widths. If in pairs, they both should have the same width.

They never stack. They always appear side by side.

[See the Figma handoff page for more details. ](https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3424-8720&m=dev)

# Background color

The background color is primary1-darker. It is intentionally a darker green as it may be butted against the text-only hero if a program does not have a hero image. In this scenario, it is preferable to have a backround color difference between the hero and the 'Key Program Info' area.

# Responsive notes

**Column spans **

The text content in the 'Key Program Info' component has the same column span as the text content for each respective breakpoint. There is a spacer to the left as the unit level navigation overlays the left columns for desktop breakpoints on page-load. The Key Program Info component accounts for that.

Here is a table that breakdowns the number of columns by breakpoint for the left-spacer, the key program info content, and any remaining spacer columns on the right. All of the mobile portrait text goes to one column and 2 columns on mobile landscape.

![1727452471895](image/keyProgramInfo-funcD/1727452471895.png)

![1727452481457](image/keyProgramInfo-funcD/1727452481457.png)^ The desktop small breakpoint has all the text in one column

![1727452492915](image/keyProgramInfo-funcD/1727452492915.png)

^ All mobile portrait displays have the text in one single column

![1727452501623](image/keyProgramInfo-funcD/1727452501623.png)

^^ For mobile landscape mode, the text goes to 2 columns

![1727452510086](image/keyProgramInfo-funcD/1727452510086.png)

![1727452528221](image/keyProgramInfo-funcD/1727452528221.png)

^ On desktop, the medium and larger breakpoints have the text content displayed in 2 columns. Shown above is without, and with, the CTAs.

**Padding top and bottom**

Padding-top for all breakpoints is 64px

Padding-bottom: for desktop it's 64px, for mobile it's 48px

# Type and icon sizing differences: mobile vs. desktop

**Icon size**

Desktop breakpoints: 24px (icon bounding box)

Mobile breakpoints: 16px

**Text sizes**

Text sizes vary between mobile and desktop.

![1727452553654](image/keyProgramInfo-funcD/1727452553654.png)

^ desktop


![1727452564738](image/keyProgramInfo-funcD/1727452564738.png)

^ mobile

**Space between icon and text**

Desktop: icons are 24px and space between text and icon is 24px

Mobile: icons are 16px and space between text and icon is 16px

# Accessibility

**Semantic HTML:** Use semantic HTML elements to provide meaningful structure to screen reader users

**Keyboard navigation:** Users should be able to navigate the content within the component using the keyboard, allowing users to focus on different parts of the content

**Screen reader support: **The content of the component can be announced to screen reader users and its content can be read out loud, making it accessible to users with visual impairments

There are no links in the Key Program Info component. No user interaction is needed to consume this content.

# Layout spacing

^ xxs-portrait view

![1727452600477](image/keyProgramInfo-funcD/1727452600477.png)

^ Buttons, button labels.


![1727452613239](image/keyProgramInfo-funcD/1727452613239.png)



![1727452656009](image/keyProgramInfo-funcD/1727452656009.png)




^xxs portrait

![1727452681146](image/keyProgramInfo-funcD/1727452681146.png)

^ xxs landscape


![1727452708040](image/keyProgramInfo-funcD/1727452708040.png)


^ small. First breakpoint in desktop



![1727452721229](image/keyProgramInfo-funcD/1727452721229.png)

^ medium breakpoint

![1727452729325](image/keyProgramInfo-funcD/1727452729325.png)

^ large breakpoint

Please also see the SPECS in the Figma file on the '[Key Program Info&#39; component&#39;s page](https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2410-100458&mode=design).

![1727452753758](image/keyProgramInfo-funcD/1727452753758.png)

![1727452767710](image/keyProgramInfo-funcD/1727452767710.png)

# The 'Key Program Info' component in context

To see the 'Key Program Info' component in a layout, see the program page layout documentation.

![1727452777593](image/keyProgramInfo-funcD/1727452777593.png)
