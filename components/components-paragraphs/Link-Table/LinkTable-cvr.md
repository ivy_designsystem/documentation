# This component is a table with a bunch of text links. Each text link has an icon on the right. The icon is typically an arrow.

![1727453261077](image/LinkTable-cvr/1727453261077.png)
