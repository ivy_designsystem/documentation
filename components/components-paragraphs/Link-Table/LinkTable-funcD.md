# Intro

**This component is a tidy table with a bunch of text links. Each text link has an icon on the right. The icon is typically an arrow.**


![1727453118260](image/LinkTable-funcD/1727453118260.png)

## Data

**Link collection name: **This is a short title for the collections of links.  It shows up as a Kicker.  Maximum 1. Visibility not mandatory in the display. Content admin should be able to toggle on/off.

**Link label:** simple text. Entire text string is linked.

*Q: Should we limit the length? By nature of what it is, don't you think people would keep this reasonable?*

**URL link for the text: **Should be a valid URL. It is not visible. It is the link that gets applied to the text string.

**Icon in SVG format: **We are rolling this out with an arrow icon only.

## Quantity of Links

**1 link collection**

minimum of 4 items.

maximum of 8 items ( *we are not sure about this. Let's let our users kick this around first* )

## Background color options

This component has two light-color background options:

* white
* neutral

## Text size

### Link Text

The text size in the individual LinkTableRow varies depending on the breakpoint.

M-1012 to xxl-1920: the text size is p.default

xxs-320 to s-768: the text size is p.small

### Kicker text

The text size for the Kicker varies depending on the breakpoint.

M-1012 to xxl-1920: the Kicker text size if "desktop"

xxs-320 to s-768: the Kicker text size if "mobile"

*Note: the s-768 breakpoint usually uses desktop sizes. Note the exception.*

## Padding

The top/bottom padding for the individual links varies.

xxs-320 - s-768 uses the p.small link text with a top/bottom padding of 12px.

M-1012 - xxl-1920 uses the p.default link text with a top/bottom padding of 16px.

## Component padding

The vertical spacing of components is handled by the internal top and bottom padding of the components. The Link Table has the following top and bottom padding:

desktop: 64px

mobile: 48px

## Functionality States

[Each link](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63c03b5b70516c03be16db9f/tab/functional%20narrative?mode=preview) has 5 states. The entire row is the hit state for the link. Note that the hover and focus states visually impact the entire row and not just the text.

There are 5 states for each

* link
* active
* hover
* press
* focus

![1727453131899](image/LinkTable-funcD/1727453131899.png){width=836}

## Edge Cases

There are a number of edge cases to point out as they will impact how this component is built and how content is administered in Drupal.

### 01

**Maximum content in single component**
![alt text](image/LinkTable-funcD/01-edge.png)
The example above has 8 links- the maximum links you can have with this component. On desktop, they appear in 2 separate columns. They stack on mobile.

### 02

**Minimum amount of links**

The example above shows the minimum number of links for this component- 4. On desktop they are divided between 2 columns and stack on mobile. Note that one has really long text that wraps to 3 lines in this desktop view and many more on the mobile view. This amount of text shows that the rows from left to right do not have to be of equal size and line up across (desktop). In a perfectly ordered design world, they would! But it is not realistic. 😉

![alt text](image/LinkTable-funcD/02-edge.png)

### 03
![alt text](image/LinkTable-funcD/03-edge.png)

**Two components stacked**

The example above shows two linkTables stacked on top of one another. This is a plausible scenario.

Never stack more than 2 of these components on top of each other. If you need to have more than 2, divide the content with something else- and put the 3rd component further down on the page.

### Responsive Notes

This component has 2 columns of links for all breakpoints S-768 and larger.

Note gutter increased from 32px to 64px on 12/19/23.

For xxs and xs mobile breakpoints, the links stack vertically.

![1727453212591](image/LinkTable-funcD/1727453212591.png){width=320}
