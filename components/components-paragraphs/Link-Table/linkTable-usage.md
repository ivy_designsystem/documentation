Never stack more than 2 of these components on top of each other. If you need to have more than 2, divide the content with something else- and put the 3rd component further down on the page.

Make good decisions within the context of all the content in your layout regarding background color choice.

Even though text can wrap to multiple lines, don't make the text for your links really long.
