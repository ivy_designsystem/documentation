# Atoms

**TitleBar oNDark**

Ivy / Components / Atoms / Type / [TitleBar-onDark](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/60b6d13b2fb0aa4d2ee7b64c/tab/design?mode=preview)

**QuickLinkMenuRow**

Ivy / Components / Atoms / Lists / [quickLinkMenuRow](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/633ef101133ea0a5b9586d8c/tab/design?mode=preview)

Please see Ivy / Foundations / [Typography](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f82e4d10fa0e2078276fe8b?mode=preview)

# Molecules

**White overlay header**

**Ivy / Organisms-Components / header / **[**0**3 white overlay](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6337385a6ac3f2c303dae1eb/tab/design?mode=preview)

**Site Search**

Ivy / Components / Molecules / Search / Site search

/* not released yet. tk Lots of subcomponents in this */

**Primary Nav group**

Ivy / Components /Molecules / Navigation / [Primary nav group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61fdad448becb1b0eaf9d3c4/tab/design?mode=preview)

**My UVM login promo banner**

Ivy / Components / Molecules / promotional banners / [My UVM](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6202db33e579eb4f14ce7cfe/tab/design?mode=preview)

**Quick Links**

Ivy / Components / Molecules / Lists / [QuickLinkMenuGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61fd85428becb10a56f9c40f/tab/design?mode=preview)
