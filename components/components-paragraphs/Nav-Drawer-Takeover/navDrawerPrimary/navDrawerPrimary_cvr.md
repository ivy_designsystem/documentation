# The Primary Navigation for all UVM websites is universally accessible via a Menu icon in the upper right corner of the header. It launches a full screen take-over navigation drawer with the primary navigation, secondary/quickLinks navigation, and a link to the myUVM portal.

![1727459743716](image/navDrawerPrimary_cvr/1727459743716.png)
