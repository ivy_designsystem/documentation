# description

![1727459828963](image/navDrawerPrimary_funcD/1727459828963.png)

## Functionality

On load of the take-over navigation drawer, the primary navigation is in its default/link state. The UVM primary navigation is flat and there are no submenu items.

**Data**

Since the take-over drawer is just a layout collection of components, please follow the links on the Atomic Breakdown tab to each components' page in the Ivy design system.

## Mandatory Elements

The mandatory elements in the take-over drawer are:

White overlay header

Primary navigation

QuickLinks (manually curated)

My UVM promo banner

## Optional Elements

For future release: Promotional banners (up to 2), appearing below quickLinks

## Developer Note

The nav drawer is really a full screen take-over that has a layout of embedded components. Outside of describing the interaction of how the take-over either writes on or closes, you will need to look at each of the sub components to understand the functionality of the things on this page.

### **Please make sure that you also look at the sub atomic elements for this component.  **

The naDrawer is a layout made up of su-atomic components. Each sub component has a spec, functionality narrative, and links to its sub-atomic elements.

 See the LINKS tab above.

## Interaction

On-click or on-tap of the hamburger menu-search icon in the upper right corner of the header, the primary navigation drawer takes over the screen. We are calling this "take-over navigation".

The green background should take-over the screen first with the type and other elements making a slightly delayed and progressive appearance. It should happen quickly though. They user should not have wait for the navigation to appear... just a slight delayed progression much like you can see in [this example](https://www.medienstadt.koeln/) if you click on the left Menu button.

**Here is the order of write-on. It all should be very quick. Any fades or progressive delays are micro-seconds (.3 fade)**

01 Background Writes on from right to left

02 white overlay header

03 Primary navigation progressive fade

04 Link list group, and myUVM promo banners

## Responsive notes

The navigation stacks on mobile.

On desktop, there are 2 columns.

The base breakpoints are xxs-320 and s-768

The xxs-320 scales up.

The s-768 breakpoint scales up.

Note that the headers are different at the xxs, s, medium breakpoints.
