# Intro to the unit level Nav Drawer Takeover

**The sectionNavDrawer provides users with both section-specific navigation and the main UVM navigation via a toggle. Users can switch back and forth between the 2 navigation lists via a toggle.**

It is just like the navDrawer but it has a toggle to allow users to switch back and forth between section nav and top level UVM nav.

![alt text](../image/navDr-unit_cover.png)
