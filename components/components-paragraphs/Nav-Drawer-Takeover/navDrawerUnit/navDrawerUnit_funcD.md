# Intro

**The sectionNavDrawer provides users with both section-specific navigation and the main UVM navigation. Users can switch back and forth between the 2 navigation lists via a toggle.**

It is just like the navDrawer but it has a toggle to allow users to switch back and forth between section nav and top level UVM nav.
![alt text](01-A_MGFjZjlkZDY2YjhlM2JmOTFZAbSzZ46dTT-QULsbYTIH9JLn71W7nAbszUJN9Kht5UubR6lpmfQItOWsc5Kzz1gBOMhexMH-Seb7T08ZumkiF1eU9Pi9oXvIilmxHXinLMBrUiXj0mOJRHYytLPOocIHTWkpiYTdCel6AD3nq7A=.png)

## Purpose

The sectionNavDrawer is launched via the menu icon in the top right. Colleges / schools / units / departments etc. All have persistent, section-specific navigation in the left sidebar on their sites but they also have section-specific navigation in the  *section nav drawer* . The sectionNavDrawer is an important, albeit redundant, location for the section nav. If users click on the menu icon to locate the website navigation, their expectation will be met. They will be delivered the section specific navigation by default but also be able to access the top level uvm.edu navigation via on click/tap of the navDrawer toggle.

## Functionality

The functionality is the same as that for the Primary Nav Drawer takeover with these differences:

* The section navigation is not flat, it uses expand/collapse to show subnavigation 1 level deep (parent + child subnav items)
* There is a toggle to allow users to toggle back and forth between the primary nav drawer and the section nav drawer

User clicks on the menu icon in the upper right to launch the takeover nav drawer. On load of the take-over navigation drawer, the section level navigation is the default. Users also have the option to switch to the UVM.edu top level navigation via the sectionNavToggle at the top of the navDrawer between the header and the nav content.

The navDrawer display is relative to the site that user is on. For example, top-tier UVM pages will display the primary navigation takeover drawer, but if the user is accessing the nav drawer from The Luse Center, for example, the drawer will open to display the Luse Center's navigation with a toggle to switch to the primary nav drawer.

# Data

**SectionNavDrawerGroup**

See https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b097614ba64626145360a/tab/design?mode=preview

**Secondary Content**

*Right column on desktop. Stacked lower content on mobile. *

Dynamic content. Not WYSIWYG or user controlled.

We have discussed address / contact info and curated social links (social icon group with links going to the school/college/unit's social properties) would be great here as a default starter.

## Mandatory Elements

The mandatory elements in the section nav take-over drawer are:

* White overlay header
* SectionNavDrawerGroup
* Secondary content for the SectionNavDrawer (not defined yet)
* Primary navigation
* QuickLinks (manually curated)
* navToggle

# Optional Elements / Future

We have promotional banners as future components that could optionally appear in the nav Takeover drawer.

# Responsive design differences

**Mobile:** Navigation is in one column with secondary and optional content stacked below

**Desktop:** 2 column layout. Navigation content is in the left column and secondary optional content on the right.

## Content container width

The vertical top of the content container that holds the navigation is at 224px on the Y-axis in mobile and 260px for desktop small-xl desktop breakpoints.

The following table summarizes the content on the section navigation content, the number of columns it spans, width and alignment details.

![alt text](02-A_MGFjZjlkZDY2YjhlM2JmOffr0yAGmy8fFKdcNMRq2kv5CE_5nUzhMvqoweUxfAtqJmi2VttU1F514G5AuPmryQrDxywiaeSEucG6U8PNEXA7RCvL8hiwqrUNddYtkYRjx3Jq8GeDfcfxvJ52-SsB4g==.png)

## SectionNavToggle Width (combined dual button)

The sectionNavToggle varies in size at the mobile breakpoints and is one, consistent size from the small to the xl breakpoints. For all breakpoints, it centers vertically between the base of the header and the top of the content container and horizontally on the page.

See the [navToggle page](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b09936ed8d40b85855ac9/tab/design?mode=preview%27) for full specs.
![alt text](03-A_MGFjZjlkZDY2YjhlM2JmORe1YjY5-gG66p02QUZMRhRi-Dh-jyXiqYhJ4WGQnfX1Q8U61Lzq0UbyJ5hnnMUo4et6uqY9aV7i0UPVpsOwPgkAYhHUydJprUufWOqf6wuY3zlo8OUn17pGeahFGfdNmg==.png)

![alt text](04_A_MGFjZjlkZDY2YjhlM2JmOXEbyQxjZnA8IAScMJrrgj0Uzju1hpFXnZdBHDWuG9BPNU2zMpVDPlmFQgXAtLNslhDZhy6xSKrfKGf9BiYIKlB4dGkTxs82YjUtH4ziqSRp29nXIH5WvCM5Wi44pjrDHLph2pazk7v_kp4zfbJy72w=.png)

At the xxs and xs breakpoints, the nav toggle spans 12 columns and leaving the 32px L/R margins. It centers vertically between the header and the navigation content with 40px above and below the toggle.
![alt text](05_A_MGFjZjlkZDY2YjhlM2JmOdTJbkQDMvMN6-6vDVcuvVy_ZEMb68LYst9YEkMnaNIIcd9T2jhEJVV5IdiUJ2DXK025dcxcPeBGEMkthq3928wutMgz5RgU_d3dBw4A6s36im_fr56QcBa_Llld3Q7WB86MTLRg_CzS5a6WSj-q_Kg=.png)

At the s-xl breakpoints, the nav toggle is 704px wide and centers, leaving the 32px L/R margins. (small breakpoint 768 - 64 LR/Margins = 704).  It centers vertically between the header and the navigation content with 64px above and below the toggle.
![alt text](06-_MGFjZjlkZDY2YjhlM2JmObkY670y-GO5WMMHfNE1GXSFXp7OlatIeAvTri4ZWr9USQjLYKty96Pb-siNRSB7tE_9XLJvB9oBaV-eQ91wqVw--CVbanfocH-DDRa4vcECdQ-okAWcMmFmSYUA6SKj0HBEAdbiGAaQ3gvUtpd_w8Q=.png)

Both the content and the toggle stay a fixed 704px size from the small to the xl breakpoint. The toggle centers.

# Interaction

The precedent for this is established already with the Nav Takeover Drawer that is live. This is the same thing but has the toggle so the user can toggle between the section-specific nav and the uvm.edu top level nav.

On-click or on-tap of the hamburger menu-search icon in the upper right corner of the header, the primary navigation drawer takes over the screen. We are calling this "take-over navigation". Follow what we have on the uvm.edu website for the main navigation takeover drawer.

## Hover state

When the user hovers over a navigation item (individual item or parent) like on the primary nav drawer, that item stays 100% white while the other items display the non-focus state and become transparent white. This only happens on-hover.
![alt text](07-A_MGFjZjlkZDY2YjhlM2JmOYcuqXbzsbIYJq0gNGTMNRFJQcDpfMKIORJSz2U8SLLkSe00745-tLhHAWUzjTvGnoki7785X-di3mHeCQ_sCFZv_sLPHZLMd46CHeUAWpUqMRCTSFmKw4_ZIaSCLbu5KdjpctgTd12rG9KFlDA1mFU=.png)

![alt text](08A_MGFjZjlkZDY2YjhlM2JmOVQ8sdW8_A5f_2sZ0NuH8FRnsVb_4LA52WAkZnTWYMZg7nFmX6jiugP9GG51SqwT9bsvxm5nYu9lc0xgb-ygG3AqItQjpR3-Ial-jl0N7KTuXp_bdckPY4dC6KFR-LbR9kXadybhqW6sKOPXiCbG49o=-1.png)

For an expanded menu with children, the background color of the child navigation item turns darker on hover. (shown below)
