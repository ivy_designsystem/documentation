# The Featured News- Cards component consists of 5 news headlines elevated into a card format with a primary (larger) featured news item and 4 secondary news items displayed in a grid.

![1727460778735](image/featuredNewsCards_cvr/1727460778735.png)

![1727460784755](image/featuredNewsCards_cvr/1727460784755.png)
