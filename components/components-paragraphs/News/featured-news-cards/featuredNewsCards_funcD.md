# The Featured News- Cards component consists of 5 news headlines elevated into a card format with a primary (larger) featured news item and 4 secondary news items displayed in a grid.

![1727460879934](image/featuredNewsCards_funcD/1727460879934.png)

![1727460885910](image/featuredNewsCards_funcD/1727460885910.png)

# Description

This component is a group of 5 featured news teasers in a hierarchical card display. Each links to its respective related news story.

See the [Individual Featured News component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655636139c106256759238c6/tab/design?mode=preview) for more information about the primary and secondary featured news cards which combined into a group of 5, make up this component.

# Data

This component is made up of 5 [I](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655636139c106256759238c6/tab/design?mode=preview)[ndividual Featured News items](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563672e983f5ea83a7d85e/tab/ndividual%20Featured%20News%20items). Please see the documentation for the Individual Featured News for full information.

Data is pulled from the NEWS TOOL database of news stories.

**Number of news teasers in component: **

5 total, all required as follows:

* 1 Individual Featured News item
* 4 secondary Individual Featured News items

# Usage

This component is used to display and elevate news headlines. Compare it to the [news listing component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/design?mode=preview) and the [News Teaser Block component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/design?mode=preview).

**Use the Featured News component when you:**

* want to feature news headlines
* want to elevate a few news headlines (this component is limited to FIVE news features)
* your news stories tend to have an accompanying featured image

**Do not use the Featured News component:**

If most of your news stories in your news feed do not have a featured image. For teasers with no images, the [News List component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/design?mode=preview) is recommended.

This component can be combined with the [News List component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/design?mode=preview)so more news headlines can be displayed along with this component.  In this scenario, the News List component is subordinate to the Featured News and News Teaser Block components and should appear below.

The Featured News component has a more prominent, elevated display and works better when the stories that it elevates have a 16x9 accompanying featured image.  The News List component has no hierarchy- all of the individual news listings have the same layout and a less dominant display than either the Featured News or Featured Teaser Block components.

# Functionality

Each individual news teaser links to the full news story. There is a default, hover, and focus state for each individual news teaser. The hit state is the entire individual Featured News card.

# Color modes

The individual Featured News items (cards) only have a light state.

# States

**There are 3 states for the individual Featured News items within this component:**

* **Default**
* **Hover**
* **Focus (primary3 outline)**

Here they are for both the Individual Featured News item and the Individual Secondary Featured News Item. But please see the documentation for the [Individual Featured News Items.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655636139c106256759238c6/tab/design?mode=preview)

![1727460902256](image/featuredNewsCards_funcD/1727460902256.png)

![1727460907655](image/featuredNewsCards_funcD/1727460907655.png)


## Important note about the HOVER / on-tap state:

The hover/on-tap state can't be simulated well in FIGMA since Figma does not allow change of color for underlined text. We also can't save the recipe for the color shift of images that happens on-hover. Here is what is intended to happen on-hover / on-tap of an individual Featured News card:

1. HIT state is the entire card.
2. News headline text changes from DarkGray to Black and gets a primary3 underline.
3. There is no change to the background fill of the card.
4. Image has a color shift: 115% contrast, 105% brightness, 95% opacity, with white background behind image.

# Layout

This Featured News component has hierarchical elements. It is comprised of 5 elevated news teasers in card format. The single primary featured news item is larger and the 4 secondary featured news items are smaller and always appear in a grid lockup.

For medium-1012 and larger breakpoints, the Featured News component has a side by side layout, as shown on the left below.

*Important: for the side by side layouts, the Featured news item with the yellow background has a minimum height but this column should scale to fill the height of the grid of items on the right, if those are taller. *

For xxs-320 to small-768 (up to 1011px), the layout is vertically stacked, with the primary featured news item on the top and the grid of secondary featured news items below.
![alt text](image/featuredNewsCards_funcD/layout_skeleton.png)

# Internal spacing

The internal spacing between individual featured news items is 32px for all breakpoints except the xxs-320 portrait, which has a 16px spacing between internal elements.

# Layout handling of optional image

News stories may not have an accompanying featured image, so not all Individual Featured News cards will have images. This is OK and planned for, however, content administrators need to know that if the news teasers they wish to display generally do not have images, then the content would be better displayed using the News List component instead of the Featured News or News Teaser Block components.

Example: A Featured News component showing the Individual Featured News item, with and without an image; and the individual Secondary Featured News (smaller on the right), with and without an image:

![1727460921652](image/featuredNewsCards_funcD/1727460921652.png)

**Primary, larger Featured News Item**

The primary Featured News Item (with the primary3 yellow background), has a minimum height on desktop of 786px. This way when there is no image, the container does not collapse.

**Secondary Featured News Items**

The secondary Featured News Items on the right, are also designed for the image to be optional as not all news stories have a featured image. Whether the grouping of secondary Featured News Items are to the right of the Featured News Item or below, they also have a minimum container height AND they are interdepenent- the height of the item is controlled by both the minimum height and the height of the item next to it. This keeps the 2nd row top aligning. It is not a masonry grid format.

![1727460929763](image/featuredNewsCards_funcD/1727460929763.png)

# Minimum heights for the individual Featured News items

**For primary Individual Featured News item (with yellow background)**

Desktop small-xl: 786px

Mobile xs: 396px

Mobile xxs: No minimum height. Relying on the 24px padding for the text container and 48px padding-bottom.

**For secondary Individual Featured News items (in grid)**

Desktop small-xl: 304px

Mobile xxs: 192px

Mobile xs: 256px

# Responsive Notes

* The content displayed is the same for all breakpoints.
* The image always displays at the 16x9 aspect ratio.
* The size of the title text varies by breakpoint.

### Title text size by breakpoint for the ***secondary*** featured news:

xxs: m.H6

xs-s: m.H3

m-xl: H3

### Title text size by breakpoint for the ***primary*** featured news item:

xxs: m.H2

xs-s: m.H1

m-xl: H1

# Interaction

The news listing loads with[ fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview).

Hover / on-tap HIT area is the entire listing since there is only one link- link goes to the related news story. Add a very short Fade for the hover state.
