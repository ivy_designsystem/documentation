# Atoms

news headline (see [News Teaser Headline Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635bef0b1dda98ca89c0fae8/tab/design?mode=preview))

featured news headline (see [News Teaser Headline Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635bef0b1dda98ca89c0fae8/tab/design?mode=preview))

btn_moreNews

Ivy / Foundations / Icons / UI /[ arrow](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

Ivy / Components / Atoms / Type / [kicker](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c4ae69c63da73435e2e55/tab/design?mode=preview)

**Rules**

Ivy / Components / atoms / Rules / [rule styles](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61894259a3371065670d2f07/tab/functional%20description?mode=preview)

## Molecules

**News Teaser Card Group**

Ivy / Components / Molecules / News Teasers / [News Teaser Card Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635beefd9c63da78de5e1a44/tab/design?mode=preview)

**Featured News Teaser**

Ivy / Components / Molecules / News Teasers / [Featured News Teaser](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635beeef1dda989298c0fae2/tab/design?mode=preview)

News Teaser Headline Group

Ivy / Components / Molecules / News Teasers / [News Teaser Headline Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635bef0b1dda98ca89c0fae8/tab/design?mode=preview)
