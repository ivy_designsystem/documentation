# The newsTeaserBlock elevates select news headlines. There is always a larger, featured story teaser with an image or video and secondary, text-only headlines.

![1727461194391](image/featuredNewsList_funcD/1727461194391.png)


# Description

![1727461209982](image/featuredNewsList_funcD/1727461209982.png)

## Version A: Featured + Text-only News Headlines

# Data

These are news teasers that are selected from existing, published news stories. This component elevates them as news teasers. The content is picked up from the published news stories in UVM's news tool.

**Kicker**

**Featured news story teaser (same for both versions)**

* **Media in the Featured News Teaser:**
* 16x9 image OR static image placeholder for 16x9 video (defined in news tool and pulled from news tool)
* Image size is 1149 x 646, same as what is needed for the Featured Media in the news story.
* Image pulled in from the news; whatever is defined as the default in the news tool. It could be the 1st image of a slideshow, a static image, or a video. (if video, it plays in place. YT hosted)
* Video: If the featured story has a video, it will not play in place. User will need to click / tap through to the story.
* If a video is the featured media in the news tool, the story will have to also have a "backup" image. That static image is what will be shown in the featured news teaser.
* If no one has provided that static "backup" image, we will show a placeholder that is transparent... so the background will show (white or neutral.. whatever)
* **Headline** (pulled in from the news story)
* **Teaser text** (plain text). Mandatory. This is the first **~160 character** of the story followed by an ... ellipse. It can be over ridden by the user and customized. Regardless, it's mandatory.
* Right arrow

**News story headlines group**

* 4 headlines total. (Headline pulled in from the news story)
* First is elevated larger style
* More button (for the home page launch, it links to UVM Today[ https://www.uvm.edu/uvmnews/all-news](https://www.uvm.edu/uvmnews/all-news))

## Version B: Featured + News Teaser Cards

Not developing now.

see Ivy / Foundations / Components / News Teaser Block / [News Teaser Card Block](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63603a80b8f74099eaec1646/tab/functional%20narrative?mode=preview)

# Media holding rule

Media in the Featured News Teaser has a modalBlack-05 1px border .. just as a very subtle holding rule in case the media placed there is white/light.

![alt text](image/featuredNewsList_funcD/1727461221851.png)

# Missing Image

If the image is missing for the Featured News Teaser, a [16x9 placeholder, transparent image](https://uvmoffice.sharepoint.com/:f:/s/UVMWebsiteRedesign/EqQ-hhEauNlOufkxJqCbGqABHvtALNY4c8LjfYYd-w3trQ?e=kKQUKw) holds the space. The user would see no visible image (background color of the component) and the holding rule.

![alt text](image/featuredNewsList_funcD/missing-image.png)

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOQcFLtBoUe90zuYNd1YRtnvT_bkXtKGSnuBAMRuZoXp9blOg3lqrgb9VIRCnmjqlxuddxFXrj2CL-Fm7fP_ODxUTlVtNmIwuLy3yC-aH9ejK7Ky7xbnB6lq4V_IpvppdQA==?assetKey=university-of-vermont%2Fivy%2FnoImage-featuredNews-Hkvr9byro.png)

# Functionality

Featured News Teaser

Entire teaser is the 'hit' state. On click/tap, takes user to the related story.

News Headline

Each headline is the link. On click/tap, takes user to the related story.

More Button

* More button (for the home page launch, it links to UVM Today[ https://www.uvm.edu/uvmnews/all-news](https://www.uvm.edu/uvmnews/all-news))

# Interactions

### News Headlines

All news headlines link. On hover/tap, text turns #000 black and primary3 underline progressively underlines text- top to bottom and left to right. ([Inspo example on blog sidebar headlines here](https://www.invisionapp.com/inside-design/))



![1727461236203](image/featuredNewsList_funcD/1727461236203.png)

### More Button:

The MORE button, has a hover state and links to the UVM Today landing age. (link mentioned above under "Data" section)

On hover/tap, icon circle bg fills solid and revere-video.

* "MORE NEWS" text also shows on hover/tap for accessiblity and clarity.
* Micro-interaction: on fade.
* Wish list/not for MVP: "MORE NEWS" shifts right on during fade (translate on x axes during fade)



![1727461246287](image/featuredNewsList_funcD/1727461246287.png)

* See the Atomic Elements list as the hover/tap states and micro-interactions are related to the sub design elements. *

### Featured News Teaser

Entire featured news teaser is the hit state

**On hover/tap:**

* Headline turns black with progressive primary3 underline
* Arrow moves +16px to the right
* Image contrast changes and has a white scrim overlay (10%)

![1727461254033](image/featuredNewsList_funcD/1727461254033.png)

# Design Elements

**Rules **

Bounding rule at the top: is a solid rule, transparent black (gray)

Internal rules that define the grid: dotted, transparent black (gray)

# UI Elements

Arrow at bottom of featured news story.

More button at the bottom of the news headlines

# Responsive Notes

xxs-320

xs-480

s-768 is a unique configuration

M-1012 scales up to XL-1440

![1727461268797](image/featuredNewsList_funcD/1727461268797.png)

Note:: top component above is the News Teaser Block and the bottom is News Teaser Card Block.

At xxs-320 breakpoint, everything stacks vertically in one column that spans all 12 in the grid.

![1727461280027](image/featuredNewsList_funcD/1727461280027.png)

Note:: top component above is the News Teaser Block and the bottom is News Teaser Card Block.

At the xs-480 breakpoint, the NewsTeaserHeadlines and NewsTeaserCards take up 2 columns. Note that for the text-only headlines, the elevated headline spans all 12 colums.

![1727461287370](image/featuredNewsList_funcD/1727461287370.png)

Note:: top component above is the News Teaser Block and the bottom is News Teaser Card Block.

The s-768 breakpoint starts a new pattern for the text-only headlines. They appear to the right of the featuredNewsTeaser.

For the version with Image Cards, the Featured News Teaser has a horizontal layout so the cards can stack below in a row.

![1727461296303](image/featuredNewsList_funcD/1727461296303.png)

Note:: top component above is the News Teaser Block and the bottom is News Teaser Card Block.

The Medium-1012 breakpoint scales up to XL-1440 with the Featured News Teaser taking up 9 columns and the News Headlines taking up 3 columns.

# Developers Notes

No need to build out the NewsTeaserCard Block now. (the news headlines with images-version)
