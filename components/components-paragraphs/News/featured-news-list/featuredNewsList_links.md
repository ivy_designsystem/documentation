# Links

**Figma file**

https://www.figma.com/file/0PH6Y6dHxPVTEzU2o0t89R/News?type=design&node-id=46-6791&mode=dev



**Featured News Teaser placeholder / fallback image**

There is a 16x9 white and trans image here to be used as the fallback / placeholder image. This is in case the news story to be featured does not have a 16x9 image to pull in for some reason.

https://uvmoffice.sharepoint.com/:f:/s/UVMWebsiteRedesign/EqQ-hhEauNlOufkxJqCbGqABHvtALNY4c8LjfYYd-w3trQ?e=kKQUKw
