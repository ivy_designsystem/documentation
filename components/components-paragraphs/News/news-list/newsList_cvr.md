# The News List component is a group of [News Listings](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655635903a1e277b4133921b/tab/design?mode=preview) with a section identifier head.

![1727460400211](image/newsList_cvr/1727460400211.png)
