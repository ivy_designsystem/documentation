# The News List component is a group of [News Listings](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655635903a1e277b4133921b/tab/design?mode=preview) with a section identifier head.

# ![1727460444358](image/newsList_funcD/1727460444358.png)Description

This component is a reverse chronological list of news headlines. On-click / on-tap of the headline listing, the user is taken to the related news story.

# Usage

Use this component when you want to display a categorical collection of news teasers of equal importance in a list format. It can be used independently or with the [News Feature Component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563672e983f5ea83a7d85e/tab/design?mode=preview) or [News Teaser Block](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/design?mode=preview) component.

*Note: if used with the News Feature component which displays 5 news teasers, you will need to offset the data that the News Listing pulls in by 5 so you don't display the same data twice! See "Functionality" below for more info.*

# Data

Data is pulled from the NEWS TOOL database of news stories.

**News headline: **mandatory. Simple text

**News summary:** Mandatory. Simple text. Drupal pulls in a maximum of 300 characters. Truncated automatically with an "..." ellipse. Display is supressed xxs- small breakpoints.

**Image: Optional.** 16x9, 1920x1080

**Image usage on mobile:** 16x9 image is masked to display as 1x1

**URL:** URL to the source news story

**Mandatory Section Title:** Generally an H2 head. Simple text. 1 or 2 word title to set a context for the news content. For example: News Headlines. SoA News. etc. Manually editable by content admin.

# Functionality

This component is a reverse chronological list of news headlines. On-click / on-tap of the headline listing, the user is taken to the related news story.

**Number of news listings displayed: **The number of news listings displayed by default is 10, with a 5 minimum, and  a maximum display of 25. Drupal content editors can change the number of news listings displayed in the admin area. News listings are always displayed in reverse chronological order.

**When displayed on a page with the Featured News or News Teaser components:**

This component can be combined with the Featured News or News Teaser components on a page. Those components also elevate news stories. Drupal content administrators need to know that they could inadvertently display duplicative news teasers and need to offset the initial number in the news list by 5 to avoid that scenario. ***Here is how that is done: ***

**Offsetting News Feed**

Content admin to define in Drupal the number of stories displayed in the Featured News Block or Teaser when used in combination with the News List, ie: Feature +5, so the News List continues the reverse chronological sequence of news teasers without duplicative content.

**Future functionality**

Once UVM moves all website content over to Drupal 10, we will have more options for the user to view all news or 'more news' using pagination, infinity scroll or other interface. Until then, we have decided to not include a button to "view all news."

# Layout

H2.section Title and News Headings layout varies by breakpoint.

**xxs-320 to medium-1012**

They stack vertically for xxs - small breakpoints with the news listings spanning 12 columns.

For mobile xxs-xs breakpoints, there is not margin between each news listing as the spacing is handled by the top and bottom padding for each news listing item.

**Small 768**

From small-768 + the news listings are displayed as cards with 32px vertical space between each.

**Medium-1012 to xl-1440**

The H2.sectionHead and the news listings span 12 columns and stack vertically. The news listings have 32px space between each.

**Large-xl**

For 1012-Medium to 1440-xl, the H2 section head spans the left 3 columns. The news headings group stack vertically with 32px space between and span the right 9 columns with a 40px left padding. (40px left padding is important to align with other components).

**xxl-1920**

The News List component stops scaling at 1440-xl. It should just center on the page for all screen display above 1440.

# Vertical spacing

**Vertical spacing between each news listing:**

From small-768 and larger, the news listings are displayed as cards with 32px vertical space between each.

For mobile xxs-xs breakpoints, there is not margin between each news listing as the spacing is handled by the top and bottom padding for each news listing item.

## Spacing and vertical stacking of components in page layouts

**Padding-top and padding-bottom for the entire component**

Desktop: 64px

Mobile: 48px

# Color modes

The news listing was designed for both a light and dark mode. We are only developing the light mode for now but please take this into consideration during development as it will impact how you work with variables and file naming, etc, .

![1727460471334](image/newsList_funcD/1727460471334.png)

# States

**Default**

Each news listing links to the news story it is related to. Links are per news listing so states are per news listing also.

Since there is only one link destination per news listing, the entire News Listing is the HIT state. Again, each listing links to its related individual news story. See the News Listing component to find out more about the states for the individual news listing.

# Sizes of the newsList component by breakpoint

![1727460480952](image/newsList_funcD/1727460480952.png)

# Anatomy by breakpoint and responsive notes

**Mobile**

Note: 16x9 image is masked to force display to 1x1 for mobile breakpoints

![1727460506397](image/newsList_funcD/1727460506397.png)

![1727460513573](image/newsList_funcD/1727460513573.png)

![1727460522406](image/newsList_funcD/1727460522406.png)

![1727460530380](image/newsList_funcD/1727460530380.png)

 **On mobile** , the news listing is flat and is not displayed as a tile. The summary body copy is suppressed and the optional image is force-masked to display as 1x1.

For the  **desktop small breakpoint** , the news listing is displayed as a tile. The summary body copy is suppresed.

On  **desktop medium-xl** , the summary body is displayed.

**Column spans**

For xxs - medium breakpoints, the news listing spans 12 columns

Medium - xxl: News Listing width matches the WYSIWYG text container width.

# Interaction

The news listing loads with[ fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview).

Hover / on-tap HIT area is the entire listing since there is only one link- link goes to the related news story. Add a very short Fade for the hover state-- see the hover state description for the individual [news listing](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655635903a1e277b4133921b/tab/design?mode=preview).
