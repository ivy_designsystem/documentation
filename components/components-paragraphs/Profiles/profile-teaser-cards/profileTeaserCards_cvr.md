# The Profile Teaser Cards is the profile teaser component using the [profile card ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/naming?mode=preview)display style. It’s a common UI element that displays a group of profile teasers that provide quick introductory information about a group of people - faculty, staff, leadership, students, etc.

![1727462208090](image/profileTeaserCards_cvr/1727462208090.png)
