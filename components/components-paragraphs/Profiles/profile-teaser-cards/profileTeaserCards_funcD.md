# The Profile Teaser Cards is the profile teaser component using the [profile card ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/naming?mode=preview)display style. It’s a common UI element that displays a group of profile teasers that provide quick introductory information about a group of people - faculty, staff, leadership, students, etc.

It is composed of the [Profile Card Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3eb20332e95d12ae5917/tab/functional%20narrative?mode=edit) and an H2 section head.

Here is the atomic progression of the profile teaser components in card format. On the left is the individual profile card item followed (center) with the profile card group described here. The [Profile Card Item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/naming?mode=preview) and the Profile Card Group are  *never used independently* . They are sub-elements to the [Profile Queue-Card component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656e0031663f0b838c6d636d/tab/design?mode=preview) (right).

![alt text](image/profileTeaserCards_funcD/1727462288481.png)

# Display types

Profile teasers come in both card and list display types. This documentation is for the card display. Please also take a look at the documentation for the [Profile Queue-List component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/650adb5b08c5150461fb1439/tab/functional%20narrative?mode=preview).

# Data

See the "[profile card item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/design?mode=preview)" for the data for the individual profile teasers and the [Profile Card Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3eb20332e95d12ae5917/tab/functional%20narrative?mode=edit) for information about spacing for the grid of cards.

# **Usage**

The 'Profile Teaser Cards' component is used to display a collection of profile teasers. Profiles are typically displayed in collections by position at the University, for example, faculty profiles. But other categories can be made for leadership, staff, etc.

## Card or list display- how to choose?

The card and list profile teaser display variations both have data display differences. The main visual difference is that the list is a horizontal display and the cards display in a grid. Also, the list type can display up to 5 job titles / positions and the card can display a maximum of 2 positions.

The list display is more suitable when there are a lot of profile teasers or the bios have instances with more than 2 positions. If the content is on the heavy side, go with the list display.  It is also the preferred display if many bios are missing a profile photo.

Use the Card display when you have strong and uniform bio images and less text content. The Card view can hold up to 2 job titles / positions.

## When to use the card display

Profile Teasers work well as cards when the profile portraits in the collection are of good quality and are reasonably uniform in terms of head size, art direction, framing etc. Keep in mind that the card version is limited to a maximum of 2 positions. Above that, they will be suppressed from the display.

![1727462299735](image/profileTeaserCards_funcD/1727462299735.png)

## Combining Cards and Lists on a page

The ProfileQueue-Card and Profile Queue-List components can be stacked to show groupings of profile teasers. For example, a Profile Queue with Cards could be followed by a Profile Queue with the list prototype.

If you do stack Profile Queue-lists, there is enough padding built into the component so they can stack nicely without extra spacing.

# Color variations

Profile Queue-cards are available in light and dark modes. We are only going to work with the light mode at present. **There is no need to develop the dark mode right now.**

![alt text](image/profileTeaserCards_funcD/darkVersion.png)
^ Dark version.

![alt text](image/profileTeaserCards_funcD/lightVersion.png)
^ Light version

# Background options

Background options and usage

This component can has two background options with white #FFF as the default background:

* Neutral #F7F7F7
* White #FFF

Until we build a filterable profile teaser component, the two different background options are. useful when stacking this component alternating by categories. For example, a stack may consist of:

* Leadership (white background)
* Faculty (neutral background)
* Staff (white background)

This will give a visual striping to the groups of profile teasers making it easier for the user to swipe or scroll quickly through a long collection of teasers and be able to easily quickly where one category stops and another begins. It adds to the "at-a-glance" consumption ease of this content.

# Interaction and states

Each individual Profile teaser  links through to a profile page. The person's name is the link to the full profile. The email is a "mailTo" link and on tap/click will launch the user's email app to draft and email to the profile recipient. The phone number will be turned into a link on mobile depending on the platform and mobile browser functionality.

**Name:** Links to the profile. Gets a green underline on hover

**Email:** Is a MailTo link. Use UVM contextual link style for hover: text turns black, yellow underline wipes on left to right (like on the news teasers)

**Phone:** Is clickable / tapable on mobile, dependent on mobile browser and phone. Use UVM contextual link style for hover.

**Image:** No UX research has been done, but I think that people will tap/click on the bio image also to view the full bio. Would like to have the bio image as a redundant link with the person's name. On desktop, use the hover state with the gamma shift that we have been doing.

IMPORTANT: There is no "hit" area for the entire card as the component has various links- the name, email, and on mobile- the phone number.

## Spacing

Vertical and horizontal spacing between cards is 16px.
![alt text](image/profileTeaserCards_funcD/spacing.png)

## Image

The image has a 1x1 aspect ratio, 800x800px. It is used in the profile hero banner and the profile teasers. Only the one image asset is needed and it is shared between the profile hero banner and the profile teaser.

# Display by breakpoint

The component displays a group of profile cards and an H2 section head.

Mobile:

On mobile, the component spans the browser display with 32px L/R margins. The background extends to the full width.

Desktop

 On desktop, the component's content stays within the breakpoints leaving 32px L/R margins. The background for the component extends full display width.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.

![alt text](image/profileTeaserCards_funcD/table.png)

![1727462311835](image/profileTeaserCards_funcD/1727462311835.png)

^ open image in a new window to get a closer look

![alt text](image/profileTeaserCards_funcD/breakpoints.png)

* For small-768 and larger, there are 3 cards / row
* For mobile landscape and xs-480, there are 2 cards / row
* The card group stops scaling at xl-1440. Just the background scales to fill the viewport

# Do's and Don'ts

### Don't display cards as a masonry grid

![alt text](image/profileTeaserCards_funcD/no-masonry.png)

### Instead, display a card group in a tidy grid with uniform card sizes
![alt text](image/profileTeaserCards_funcD/yes-uniform-card-size.png)

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOeGT6vu_muO1iSG5a8sq2N-11sqs7r9w9Zkw-1g74CiTUT7Rmvkop1C8EP1ZknTZw3JZpkX-V-UKmosoCdWF3ZvKiLcdJy-hxjG2jS0lKBveN5kaB_hA1cuAZ2JdirmA78tGjf8UOwr49LoDLynSfcU=?assetKey=university-of-vermont%2Fivy%2FProfileCardGroup_yes_tidyGrid-rkyoNP1Lp.png)

### Edge case: long content

Some people will have very long names or a few job titles. In this case, the height of the card scales vertically and the other cards in the grid scale to fill and match in height to the longest card.

![1727462334517](image/profileTeaserCards_funcD/1727462334517.png)

### Do: Crop images consistently

![1727462360220](image/profileTeaserCards_funcD/1727462360220.png)

### Miscellaneous mage issues

![1727462369988](image/profileTeaserCards_funcD/1727462369988.png)
