# The Profile Teaser List is the profile teaser component using the profile list display style. It’s a common UI element that displays a group of profile teasers that provide quick introductory information about a group of people - faculty, staff, leadership, students, etc.

It is composed of the profile list group and an H2 section head.

Here is the atomic progression of the profile teaser components in list format. On the left is the individual [profile list item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65085740e1135bd60fe8aebb/tab/design?mode=preview) followed (center) with the [profile list group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6508574848713abbe2652046/tab/design?mode=preview). The Profile List Item and the Profile List Group are  *never used independently* . They are sub-elements to the Profile Queue-List component (right) that is documented on this page.
![alt text](image/profileTeaserList_funcD/1727461958472.png)

# Display types

Profile teasers come in both card and list display types. This documentation is for the list display. Please also take a look at the documentation for the [Profile Queue-Card component](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656e0031663f0b838c6d636d/tab/functional%20narrative?mode=preview).

# Data

See the "[profile card item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3ec3663f0bfaef6cb0fd/tab/design?mode=preview)" for the data for the individual profile teasers and the [Profile Card Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/656a3eb20332e95d12ae5917/tab/functional%20narrative?mode=edit) for information about spacing for the grid of cards.

# **Usage**

The 'Profile Teaeser List' component is used to display a collection of profile teasers in list view. Profiles are typically displayed in collections by position at the University, for example, faculty profiles. But other categories can be made for leadership, staff, etc.

## Card or list display- how to choose?

The card and list profile teaser display variations both have the same data, so the only discerning difference is the formatting. The list is a horizontal display and the cards display in a grid.

The list display is more suitable when there are a lot of profile teasers. It is also the preferred display if many bios are missing a profile photo.

Use the Card display when you have strong and uniform bio images.

## When to use the list display

The image is shown a bit smaller overall in the list view, otherwise the content is the same. Choose the list view when the profile images are less consistent, poorer quality, or non-existent. Profiles without a bio image will display a placeholder avatar. Lists work well when there are A LOT of teasers to display as they will take up less real estate when stacked than the card variation.

![1727461970971](image/profileTeaserList_funcD/1727461970971.png)
![alt text](image/profileTeaserList_funcD/darkView.png)

## Combining Cards and Lists on a page

The ProfileQueue-Card and Profile Queue-List components can be stacked to show groupings of profile teasers. For example,  a Profile Queue with Cards could be followed by a Profile Queue with the list prototype.

If you do stack Profile Queue-lists, there is enough padding built into the component so they can stack nicely without extra spacing.

# Color

Profile Queue-cards are available in light and dark modes. We are only going to work with the light mode at present. **There is no need to develop the dark mode right now.**

# Background options and usage

This component can has two background options with white #FFF as the default background:

* Neutral #F7F7F7
* White #FFF

Until we build a filterable profile teaser component, the two different background options are. useful when stacking this component alternating by categories. For example, a stack may consist of:

* Leadership (white background)
* Faculty (neutral background)
* Staff (white background)

This will give a visual striping to the groups of profile teasers making it easier for the user to swipe or scroll quickly through a long collection of teasers and be able to easily quickly where one category stops and another begins. It adds to the "at-a-glance" consumption ease of this content.

# Interaction and states

Each individual Profile teaser  links through to a profile page. The person's name is the link to the full profile. The email is a "mailTo" link and on tap/click will launch the user's email app to draft and email to the profile recipient. The phone number will be turned into a link on mobile depending on the platform and mobile browser functionality.

**Name:** Links to the profile. Gets a green underline on hover

**Email:** Is a MailTo link. Use UVM contextual link style for hover: text turns black, yellow underline wipes on left to right (like on the news teasers)

**Phone:** Is clickable / tapable on mobile, dependent on mobile browser and phone. Use UVM contextual link style for hover.

**Image:** No UX research has been done, but I think that people will tap/click on the bio image also to view the full bio. Would like to have the bio image as a redundant link with the person's name. On desktop, use the hover state with the gamma shift that we have been doing.

IMPORTANT: There is no "hit" area for the entire card as the component has various links- the name, email, and on mobile- the phone number.

## Layout and spacing

Vertical and horizontal spacing between cards and tiles is 16px.

# Image

The image has a 1x1 aspect ratio, 800x800px. It is used in the profile hero banner and the profile teasers. Only the one image asset is needed and it is shared between the profile hero banner and the profile teaser.

# Display by breakpoint

The component displays a group of profile cards and an H2 section head.

Mobile:

On mobile, the component spans the browser display with 32px L/R margins. The background extends to the full width.

Desktop

 On desktop, the component's content stays within the breakpoints leaving 32px L/R margins. The background for the component extends full display width.

The number of columns that the group spans varies by breakpoint and on mobile, the list view for portrait displays, does not display the image.  In landscape mode, however, the image does show for both list and grid displays on mobile.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.

![1727461983413](image/profileTeaserList_funcD/1727461983413.png)
![alt text](image/profileTeaserList_funcD/all-breakpoints.png)
^ open image in a new window to get a closer look

## Content layout differences by breakpoint

![1727461993345](image/profileTeaserList_funcD/1727461993345.png)

![1727462010785](image/profileTeaserList_funcD/1727462010785.png)

^ For the 1012-Medium breakpoint and larger, the contact info is displayed in 2 columns.

![1727462021775](image/profileTeaserList_funcD/1727462021775.png)

For the small breakpoint, the content is in 1 column.

^ The list component does not have an image on mobile xxs/xs portrait.

![1727462035216](image/profileTeaserList_funcD/1727462035216.png)

^ The profile teaser group for mobile landscape has an image plus 1 column of content.

# Layout spacing

Profile teasers have 16px space between them whether cards or lists. Spacing is the same between cards vertically and horizontally.

![1727462056187](image/profileTeaserList_funcD/1727462056187.png)


## The profile list component in a layout.

Here is the profile list component in a layout. The card view is used on the top followed by a list view. It's a good example of how these can be combined on one page with each meeting different content or hierarchy display needs.

See the [Profile List Page documentation](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/650f6375ef92d3df0571d4d7/tab/design?mode=preview) for more information about Profile List pages.

![1727462069286](image/profileTeaserList_funcD/1727462069286.png)
