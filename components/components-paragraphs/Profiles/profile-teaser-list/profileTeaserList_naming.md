# It's important that everything has a name and that we all use the same name to refer to the component.

If development varies from these names, it creates a disconnect with the design system and a communication problem as we no longer speak the same language when talking about a component.

# Naming for the 'profile list' component:

![1727462086828](image/profileTeaserList_naming/1727462086828.png)
