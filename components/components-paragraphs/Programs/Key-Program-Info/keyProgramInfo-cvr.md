# The 'Key Program Info' component has a text list with high-level program take-away information.

![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOSMOzWg9oSL3lSEerYyxPddOsbf5GEJqKhQMr1BQNap1xbgUSzPudR2VZ9L2Q4xfySE-l_VHcL2S1fzzjFNqZKa3jZetXxIrk-LqFjwSTW4MbMz6E-CkH3QSj7sn4B1rXFVeI1v6JATRMfGdtBkYD80=?assetKey=university-of-vermont%2Fivy%2Fds_card-16x9-keyProgramInfo-rkIKll3u0.png)
