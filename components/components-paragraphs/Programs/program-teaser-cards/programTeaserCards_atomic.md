# Atoms

Ivy / Foundations / Icons / [Related Program Icons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64f2386c7c57dce191c2a23d?mode=preview)

# Molecules

 **Individual program in card or list view**

Ivy / components / molecules / related programs / [m_relatedProgram](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf7b58294793d1b5b305/tab/design?mode=preview)Item

**Program group**

Group of programs in card or list view

Ivy / components / molecules / related programs / [m_relatedProgramsGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf85194db8298492b6ab/tab/design?mode=preview)
