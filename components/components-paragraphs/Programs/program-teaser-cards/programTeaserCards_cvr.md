# The Related Programs component is a group of high-level related program information that can be displayed in either a list or a grid format.

![1727463204853](image/programTeaserCards_cvr/1727463204853.png)

![1727463209142](image/programTeaserCards_cvr/1727463209142.png)
