# The Programs component is a group of high-level related program information that can be displayed in either a list or a grid format.

# ![1727463265672](image/programTeaserCards_funcD/1727463265672.png)Functionality

The Program component displays a group of related programs in either a grid or a list display. Each program - whether in list or card/grid format, links to a single destination- each program's respective overview page. The entire container is the "hit" area, individual contextual links will not be provided for the list elements, text or image. The hover state darkens the border of the container with an elevation to "lift" the individual container up from the page on-hover / on-tap.

Content administrators can choose which display type they want to default to. On page load, the display will reflect what the content editor chose as the default display.

Users visiting the page that the Programs component is on can toggle between both displays by clicking / tapping on either the grid or list icons above the component.

* Containers - whether list or card- have a white background
* The component always has a neutral #F7F7F7 background so the containers resemble physical cards in that they have a defined area and visually site above the neutral backgrond
* Cards are interactive and on-hover / on-tap respond by "lifting" off of the page background with added elevation and darker border

![1727463276496](image/programTeaserCards_funcD/1727463276496.png)

^ hover state shown above for the grid view on desktop

![1727463283693](image/programTeaserCards_funcD/1727463283693.png)

^ hover state shown above for the list view on desktop

# Data

Content administrators choose which individual programs are related. The group of programs then get displayed as a related program group.

See the following components to find out more about the data types to be included on each program "container" whether in list or card format.

* [program item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf7b58294793d1b5b305/tab/design?mode=preview) (individual program item)
* [program item group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bf85194db8298492b6ab/tab/design?mode=preview) (group of individual program items)

## **Content**

**Make sure you are using consistent content for each program "card". **

**Important! **The data for the high-level content that is displayed in the program list or card, is intentionally defined to provide a consistent, high-level, take-away view of the program. Essentially, we are providing a shopping experience for the site visitor and their ability to get a quick overview of the program is really important and to compare programs. The programs can easily be compared at-a-glance if we offer high-level information about the program in an apple-to-apples way. Consistency is key with the display of content so the end user experience is optimal and the content it easy to understand, consume and compare.

# Visual display

* Program "cards" whether in grid or list, horizontally or vertically stacked, mobile or desktop, *have * ***16px space between each*** *.*
* Each card should have the same data types.
* The 1x1 image is mandatory
* The image never appears in list view on mobile- whether landscape or portrait mode.
* All cards and containers in the group should be the same width
* All cards in groupings by breakpoint should be the same width and height.
* Lists will most likely be the same height but there is more allowance in permmisible height for the list view
* This component has a neutral background. For all breakpoints, the background only extends to full viewport width.
* Component padding is standardized.  For desktop padding top and bottom is 64px. For mobile, padding top and bottom is 48px.
* Width of the Programs Card/Grid Group is determined by the container they are placed in. For cards, pace between each is 16px and the width of each card expands to fill the balance of the space. (See the table below for column spans by breakpoint)
* Width of the Programs List Group is determined by the container they are placed in. The list container stretches on the x-axis to fill the container. The vertically stacked list has 16px between each item.
* Programs component will display as **list** by default. End user has the ability to change the display.
* Content display can change on selection without a full page refresh

## Background color options

Both the program list and card views can be on either a white or a neutral background.

**List view white background**

Individual list items have a white background and hover to neutral with elevation.

![1727463299096](image/programTeaserCards_funcD/1727463299096.png)

**List view neutral background**

Individual list items have a neutral background and hover to white with elevation.

![1727463306239](image/programTeaserCards_funcD/1727463306239.png)

**Grid view on light and neutral backgrounds with the middle card showing the hover state**

![1727463314506](image/programTeaserCards_funcD/1727463314506.png)

# Display by breakpoint

The Programs component can be displayed as either a list or a grid. This is true for every breakpoint. There are some differences though. The number of columns that the group spans varies by breakpoint and on mobile, the list view for portrait displays, does not display the image.  In landscape mode, however, the image does show for both list and grid displays on mobile.

The image shows in the grid display for all breakpoints.

The following table clarifies the column span for the component and some of the visual display differences by breakpoint for both the grid and list display types.

![1727463321672](image/programTeaserCards_funcD/1727463321672.png)

# Responsive notes

![1727463329594](image/programTeaserCards_funcD/1727463329594.png)

^ at the small-768 breakpoint, the grid view shows 3 program cards / row

![1727463338010](image/programTeaserCards_funcD/1727463338010.png)

^ on mobile landscape mode, the grid shows 2 programs / row. Above is the xxs-landscape view (568 wide)

![1727463347744](image/programTeaserCards_funcD/1727463347744.png)

^ On all mobile breakpoints, portrait or landscape, the image does not show in the  **list view** .

# Do's and Don'ts

For the list view, text in columns should still align left whether there is an image or not.

![1727463354863](image/programTeaserCards_funcD/1727463354863.png)

# Layouts and spacing

![1727463364407](image/programTeaserCards_funcD/1727463364407.png)

![1727463387540](image/programTeaserCards_funcD/1727463387540.png)

# Accessibility

* Semantic HTML: Use semantic HTML elements to provide meaningful structure to screen reader users
* Keyboard navigation: Users should be able to navigate the card/container component using the keyboard, allowing users to focus on different parts of the content
* Screen reader users should be able to select each program item (the entire container for each individual program is linked) and navigate to the destination link
* Screen reader support: The card/container component is announced to screen reader users and its content is read out loud, making it accessible to users with visual impairments
* Content editors should provide alternative text for images: Describe the image (using the alt attribute) for screen readers to announce to user

# More specs

Make sure you take a look at the layout and spacing specifications on the [Figma page](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64f0bfa41579dd36a26ce0fb/tab/puffyB!k3)@). Between inspecting the components and the Specs, there is a lot information there about padding, spacing, and layout.

![1727463418308](image/programTeaserCards_funcD/1727463418308.png)
