# Atoms

kicker

img_1x1

Ivy / Components / Atoms / Scrim / [scrimWhite](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63594cf6f0e1845da470bca1?mode=preview)

# Molecules

 Ivy / components / molecules / tabNavBar /[ tabNav](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6356dbd70703027f19afd3f1/tab/design?mode=preview)

Ivy / components / molecules / [tabNavBar](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6356df58af23c0ccf99a29d8/tab/design?mode=preview)

Ivy / components / molecules / controls / [swiperBtnGroup](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/634ed1d3e86ff664e9c25b05/tab/design?mode=preview)

Ivy / components / molecules / lists/ [listBare-default](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6356e0bb5374a47461ef27d0/tab/design?mode=preview)
