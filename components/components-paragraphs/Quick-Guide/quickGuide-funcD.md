# Intro

**The Quick Guide elevates links to deeper areas of the site. Links are related and grouped by categories accessible via internal tabNavigation. It’s a hard working component in a small amount of real estate.**

![alt text](image/quickGuide-funcD/1727464277424.png)

# Data

Kicker text

navTabBar labels

linkList content

1x1 image 1080x1080

# Behavior

The Quick Guide delivers a list of links by category. Each category is accessed via the tabNavigation. The entire body content area changes when the user selects a new tab.

Responsiveness

* Content stacks for breakpoints xxs-small.
* The linkList displays in 2 rows for breakpoints L and larger, xxs, xs, and small.
* The linkList displays as one column for the medium breakpoint
* The linkLlist text is the default size for all breakpoints

# TabNavBar behavior

TabNavBar is flush left

* TabNavBar has one size- 48px

See [tabNavBar](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6356df58af23c0ccf99a29d8/tab/design?mode=preview) for more information about this component.

# tabNavBar parameters

* No more than 7 tabs
* No fewer than 2 tabs
* First tab is selected on page-load
* Only one tab can be selected at a time
* Tabs become horizontally scrollable when the length of the tabNavBar exceeds the width of the container
* If tabs exceed width of their container, a gradient is added to the end (right) of tab container
* When scrolled to end of tabs, a gradient is added to the beginning (left) of tab container

# Do's / Don'ts

The layout does not mirror. The image is always on the right.

# Edge Cases

**Number of links in the linkList**

Minimum of 4 links per tab. Links are evenly divided between columns (except @m-1012 where this is one singe column for the link list)

**Minimum Number of Tabs in the tabNav**

Two

**Maximum number of tabs in the tabNav**

7
