# UVM uses Google to power the search on the site. The search functionality is launched via the search icon in the upper right of the header on all website pages. The search interface is a take-over. Users are able to perform either a people search or a website search.

![1727464444078](image/searchTakeover-cvr/1727464444078.png)
