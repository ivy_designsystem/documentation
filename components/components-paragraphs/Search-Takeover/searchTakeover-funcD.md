# UVM uses Google to power the search on the site. The search functionality is launched via the search icon in the upper right of the header on all website pages. The search interface is a take-over. Users are able to perform either a people search or a website search.

## Functionality on-load of the Search Takeover

The Search takeover launches on-click / on-tap of the search icon in the upper right of the website's header.

On-load, the searchHeader is at the top and the background of the viewport below the SearchHeader is 100% primaryDark.

![1727464538734](image/searchTakeover-funcD/1727464538734.png)


![1727464550657](image/searchTakeover-funcD/1727464550657.png)

![1727464571492](image/searchTakeover-funcD/1727464571492.png)

## Search Suggest

We are taking advantage of Google's Search Suggest functionality. When the user starts typing in the search text entry, query return suggestions appear dynamically below in a Search Suggest box. Please also [Take a look](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63444adcec779ad7d7606c85/tab/functional%20narrative?mode=preview) at the search suggest documentation as the suggested search returns appear on a white background and the size of that container vary by breakpoint.

## Search Returns

If the user does not click on a Search Suggestion, they get Search Results. Pickup existing formatting for the returns. They would be on a white background below the Search Header.
