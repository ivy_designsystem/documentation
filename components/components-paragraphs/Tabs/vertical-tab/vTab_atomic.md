# Atoms

**Vertical tab item**

Ivy / Components / Atoms / [Vertical tab nav item](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64d240f11579ddcf8a638657/tab/design?mode=preview)

## Molecules

**Vertical tab group**

Ivy / Components / Molecules / [Vertical Tab Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64d38d5448713a7b0d54d3ed/tab/design?mode=preview)
