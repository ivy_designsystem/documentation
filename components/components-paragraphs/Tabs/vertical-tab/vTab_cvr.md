# The Vertical Tab component offers a progressive reveal of content by presenting content categories in navigable tabs allowing the user to have a higher-level view of what’s on the page without being overwhelmed by the details of the content.

![1727466424736](image/vTab_cvr/1727466424736.png)
