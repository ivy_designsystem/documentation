# The vertical tab component is a vertically stacked list of headers that reveal associated content as selected.

# Overview

The vertical tab component condenses large amounts of content in a small space through progressive disclosure. Vertically stacked tabs and concise tab titles give the user a high-level overview of the content allowing the user to decide which sections to expand and read.

## Related components and display options

The vertical tab and the accordion are  related components. On mobile, the vertical tab is displayed as an accordion. The accordion, the vertical tab, and the infoBand have the same data, just the visual display and behavior differs.

## When to use the vertical tab

* To condense larger amounts of content into a smaller space
* To organize related information.
* To shorten pages and reduce scrolling when content is not crucial to read in full.
* When space is at a premium and long content cannot be displayed all at once, like on a mobile interface.
* Vertical tabs can be used to lead a site visitor through instructions

## When not to use the vertical tab

* The vertical tabs should never be used for primary navigation.
* No more than 6 tabs are recommended. If tabs become too complex, consider the overall information architecture of your website and if pages need to be worked into your site's main navigation
* Vertical tabs should not be used to indicate progress for step by step instructions or a sequence of events
* Vertical tabs should not be used for comparing information. The interface will not lend itself to this

# Usage Guidelines

The vertical tab component can make information consumption more effective. However, it has the burden of engagement from the user to reveal content. It’s important to take into consideration that this component with a progressive reveal hides content from the user and that a user may not access or read all of the content.

If you want all of the content revealed without requiring user interaction to expand, use the infoBand component instead or a full scrolling page with text and headers.

# Data

The vertical tab on mobile will be using the accordion display so it is critical that the data and the way it is coded is in alignment between the two components. Please see the [accordion component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview)for more details.

**vTab title:** contains the section title and provides the UI control to reveal content Simple text, not more than 100 characters recommended but tab can expand vertically across all breakpoints to accommodate title

**content panel: **contains WYSIWG content

All content is manually entered by the user.

# Component widths by placement

The vertical tab component fills the container it is placed in. It can be appear to the right of the section nav panel on desktop breakpoints, or it can be in a 12-column span container on it's own. The table below indicates the widths and column spans of the component, vertical tab group and content panel by breakpoint.

Note: The vertical tab component displays as an accordion on mobile. See documentation for the [accordion component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview)for more information.

![1734535968514](image/vTab_funcD/1734535968514.png)

# Vertical spacing

**desktop:** 96px padding top and bottom

**mobile:** 64px padding top and bottom

# Content tips

## Main elements

**VTab title **

* The title should be as brief as possible while still being clear and descriptive.
* It should provide confidence in the click and provide a clear indication of the content that will be revealed on engagement.
* All vTab titles should have a symantic head (h1-h6) that is appropriate for the information architecture of the page.

**Panel content **

* This is WYSIWYG content

## Scrolling content

When the content in the vertical tab component is longer than the viewport the page should vertically scroll. Content should not scroll inside of an individual panel. Content should never scroll horizontally in a panel.

# Functionality

On desktop, the component is displayed as a vertical tab. The vertical tab component has two primary states:

* Revealed
* Hidden

On mobile and container widths xxx and smaller, the vertical tab is displayed as an accordion. The accordion component has two primary states:

* expanded
* collapsed

Please see the [accordion component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64cd99adef92d3c8f95f9e8f/tab/design?mode=preview)for more details. The vertical tab on mobile will be using the accordion display so it is critical that the data and the way it is coded is in alignment between the two components.

# Interaction

**Individual Tabs**

Vertical tabs have three states: default, hover/on-tap, and selected states.

* On page load, the top tab is active and its related content is revealed to the right.
* On-click of another tab, the last active table self dismisses and closes and the newly selected tab opens.
* Two tabs cannot be revealed at the same time.
* If the user scrolls down the page away from the vertical tab and then scrolls back up, the last active tab should still be displayed
* Hover state: Instant transition

**Vertical tab component behavior**

On page load, the content for the first tab is displayed. The vertical tab component uses parallax with the vertical tabs sticky-persistent at the top left. (respecting 32px Left margin and padding-top for the component)

Parallax scroll takes over until the user scrolls past the height of the revealed content panel. The vTab group remains in place so the user can reveal the content for other tabs.

After parallax-scrolling to the bottom, the page scroll takes over and the user effortlessly continues down the page. This interface is Intuitive, does not impede the consumption of the content on the entire page and keeps tabs handy for viewing the contents of the vertical tabs.

[Here is an example of this](https://www.theblueground.com/furnished-apartments-london-uk/london-limehouse-337) on a website that leases luxury furnished apartments by the month.

In this example, the booking dates panel sticks on the right as the parallax scroll takes over the page scroll temporarily.

Developer note:You will need an anchor tag for the top of the vertical tab component so on-selection of a tab, target =#vTabTop. This keeps the focus on the content  at the top on selection. Overall height of vTabs component container changes depending on length of content for each tab and as user selects a tab.

# Responsive notes

See the "widths by placement" above.

**Over-arching:  **

* The vertical tab component is displayed as running text on mobile- just as the infoBand displays on mobile..
* The vTabGroup's width matches the width of the section nav Panel
* Text content width never exceeds the specified widths for text containers by breakpoint.
* The text panel has a 40px left-padding for medium and larger breakpoints but no padding for the small breakpoint

Again, see the "widths by placement" above.

# Anatomy

![1727466242100](image/vTab_funcD/1727466242100.png)

![1727466250759](image/vTab_funcD/1727466250759.png)

![1727466261721](image/vTab_funcD/1727466261721.png)
