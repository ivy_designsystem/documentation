# Links

**Component master in Figma**

https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2221%3A25047&mode=design&t=7XWM5YHrVAkwnmm0-1


**Specifications in Figma**

https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2221%3A25078&mode=design&t=7XWM5YHrVAkwnmm0-1


**Layout plan in Figma**

https://www.figma.com/file/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?type=design&node-id=2221%3A25046&mode=design&t=7XWM5YHrVAkwnmm0-1
