# Group of tiles of equal size in a grid, equal distance apart. These tiles have a title and a short descriptive sentence.

Status: Incomplete
This component has not been handed off.



![1727465542927](image/tileGrpTitleBody_funcD/1727465542927.png)

# Purpose

This tile group with each tile having a title, a short descriptive sentence, and an arrow, function as buttons to lead users to other areas of the website. The **title** needs to be concise and clear and provide a high level of confidence in the click and be a clear label for the destination page. The **short descriptive sentence **supports the title and provides further meaning, explanation or context.

# Data

Title: simple text, ~68 characters maximum (3 lines @xxs-320)

Body: simple text, ~114 characters maximum (3 lines @xxs-320)

# Assets

* SVG arrow icon
* Background vector art

# Tile and Tile group sizes

The tile group is layed-out on the grid of each breakpoint. For each breakpoint, the tiles span columns. They scale in size between breakpoints but each tile is always the same width and height. For that reason, the title text is limited as the tile group does not form a masonry grid. The tile group grid is equal in tile sizes and distance between.

# States

* Default
* Hover / on-tap

The last tile in this group is showing the hover state  primary3 bg)

![1727465554069](image/tileGrpTitleBody_funcD/1727465554069.png)

# Background vector art

**on page load: **tiles load with overall page loading strategy (the way we have on the home page now.) User does not wait for tile content to load on-scroll.

**on scroll:** vectors 01, 02, and 03 fade on quickly in the background behind the tiles. They fade on with a slight rotation to their resting place. Here are some starting values and ending values for each vector's rotation.

* vector 01:  -10° to  0°
* vector 02:  -10° to 5°
* vector 03: 110° to 120°

Note: we will need to work this out together. If I have the time to do some key-framing to tighten up directions and provide a prototype, I will. Just couldn't get this done for the handoff.

# Interaction

**Hit area: **the entire tile is the hit area. The user does not have to navigate to the text or the arrow for the link hit area.

**Transition between states: **Fade (see the image CTA for an example)
