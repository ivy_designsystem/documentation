# Atoms

**Individual tile with a title **

Ivy / Components / Molecules / Tile / [tile_title](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63bf1a742aecb4ccf589c5c3?mode=preview)

**Background vector art**

Yellow dotted vector curves

Ivy / Foundations / Vectors / [Yellow Dots](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/63c6d89ac75ee245725a500e?mode=preview)
