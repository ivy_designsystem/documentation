# Atoms

**Text button, icon right**

Ivy / Components / Atoms / Buttons / [Text Button Icon after](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6393b45fa8eb003116191aa8/tab/design?mode=preview)

# Molecules

**Text Group**

Ivy / Components / Molecules / Type Groups / [txtGrp](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6392084441981d35d9a34fc8/tab/design?mode=preview)

Note: heading size is smaller in this component as well as the margin-bottom for the head. The extra margin-top and bottom of the textGrp has also been removed within this component. Instead, spacing between the video and the text group is controlling the spacing.
