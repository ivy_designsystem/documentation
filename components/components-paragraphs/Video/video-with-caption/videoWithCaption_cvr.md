# **The 'Video with caption' is a component that displays a single video with an optional caption text group. This component can be stacked.**

![1727466609513](image/videoWithCaption_cvr/1727466609513.png)
