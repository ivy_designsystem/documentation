# **The 'Video with caption' is a component that displays a single video with an optional caption text group. This component can be stacked.**


![1727466714378](image/videoWithCaption_funcD/1727466714378.png)

**Usage**

Content editors choose this component if they want to display a single video larger than what can be displayed within the WYSIWYG text area with embed code from a 3rd party video host like YouTube or Vimeo. The text group and CTA are optional.  This component can be stacked to feature a few videos.

**Data**

**Mandatory content**

Media: link to 3rd party hosted video. Preview image comes from the video host.

**Optional content**

 **headline** : simple text

 **paragraph of simple text** , not WYSIWYG

 **Call to action link(s)** :

* Link List with arrow icon after

**Character Counts**

 **Heading: shortText, ** **[this can be increased- up for discussion] **recommended60 characters

**Body: short text, [this can be increased- up for discussion] **recommended max 200-300 characters, plainText (this may evolve to become WYSIWIG but we are going to roll it out with plain text)

**Optional text button:** no maximum, up to content editor discretion. Text button can wrap to 2 lines with the arrow icon following last character

**Optional Link List:** Text button turns into a linkList as soon as there are 2 links.  Maximum links 4

**Component dimensions**

* Width of media varies by breakpoint. XXS-320 - Small-768 the 16x9 media is 12 columns wide and for all larger breakpoints, it is 10 columns.
* Background of entire component scales to full breakpoint width (white or neutral background)
* Height of the component is dependent on how much text content is present
* Padding top / bottom is 64px for desktop and 48px for mobile

# **Functionality**

Videos use native control skin from 3rd party video host, usually YouTube. When user clicks/tap the PLAY button, the video plays in place. User can expand the video to a full-screen modal take-over. On click/tap of the close [x] button, modal take-over closes and user is still on the page, same vertical scroll point.

## **Stacking multiple components**

For all breakpoints, this component can be stacked vertically with no margin between.

# **Width of the Component**

The width of the component is 10 columns for desktop and 12 columns for mobile.



![1727466723542](image/videoWithCaption_funcD/1727466723542.png)

Note how the text has a L/R option for all breakpoints except xxs-320.

# **Variations**

## Background Color options

Light background options only:

White #FFF

Neutral #F7F7F7

**Optional CTA**

The caption and the CTA for the mediaCaptionStack component is optional. In addition, you can have either a single text button or a Link List. The examples shown have a single button.

**Interactions **

All are on scroll:

* image fades on
* text group [fade+shift](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?version=63a09f80836f090f00fb3ed0&mode=preview)

# **Breakpoint notes**

**Text Group**

The optional caption goes in the WYSIWYG container- or follows the same specs as the WYSIWYG container. for column span by breakpoint. The markup in the hand-off file will help bring clarity to this.



![1727466731924](image/videoWithCaption_funcD/1727466731924.png)

**Width of the Video: **

The video spans10 columns for breakpoints medium-1012 to xl-1440. For the xxs-320 to s-768 breakpoints, the video spans 12 columns.

![1727466747085](image/videoWithCaption_funcD/1727466747085.png)



![1727466762137](image/videoWithCaption_funcD/1727466762137.png)

The xxs-320 breakpoint - the small-768 breakpoint, the content is 12-columns.
