# The Media Card is a single card with an image or video. It is a molecule of the larger mediaCardGroup and is not used as a stand-alone element. It is a hard working, ubiquitous component as it can be used to elevate content and drive users to deeper levels of the site.

![1727987827218](image/test/1727987827218.png)

![1727987426835](image/test/1727987426835.png)
