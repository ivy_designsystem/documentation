# Molecules

## Dropdown form field

![1727467021668](image/wayfinder-atomic/1727467021668.png)



![1727467033427](image/wayfinder-atomic/1727467033427.png)



![1727467044307](image/wayfinder-atomic/1727467044307.png)

You can find the dropdown molecule here:

Figma / Atoms and Molecules / Form elements / [UI Form Elements](https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3472-11809&m=dev)

# ![](https://uvmedu.invisionapp.com/assets/A_MGFjZjlkZDY2YjhlM2JmOZYnRYfBmyWjnDmYax0zmns9Pb8QXUdQC5GeE9VYaXe9pX8VtXwssMMb7Zr3kqDj2CBIvdfD34vtZyFjwPPbaxDShNmLqXAVA-FJZ5tNyg_c1SFJHLfnv_rAp3qN16TavQ==?assetKey=university-of-vermont%2Fivy%2Fimg-1666731632084-4-Hy7__0TSEo.png) Atoms

Ivy / UI / [chevron SVGs at the bottom of the page](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

The chevron and states for the form [drop down are here.](https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3440-14627&m=dev) (Figma)

And you can [download the SVGs for the icons here.](https://uvmoffice.sharepoint.com/:u:/s/CommunicationsTEAM408/Eaof0MmTA9RHpMA8ELwcsd4Bmv_IsusI5sP5R9ouf64fwg?e=N26tdC)
