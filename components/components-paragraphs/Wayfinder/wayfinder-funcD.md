# The Wayfinder is a full page-width component consisting of a title, short body copy and dropdown. Its purpose is to help users discover the academic offerings for specific target user groups.



![1727467075135](image/wayfinder-funcD/1727467075135.png)

## The “Wayfinder” component is called:

wayfinder

## Manndatory design elements

### Text content:

Head

Body

Both head and body content are mandatory.

### Dropdown

The dropdown field is mandatory.

## Inputs



![1727467082831](image/wayfinder-funcD/1727467082831.png)



![1727467091326](image/wayfinder-funcD/1727467091326.png)

*The following entity spreadsheet is from Trail Mix but apart from the validation and NOTES, it/s accurate*

![1727467100195](image/wayfinder-funcD/1727467100195.png)

[https://docs.google.com/spreadsheets/d/1bZwldqPRBlcCILnpR7IGe9FF31RomvVSlCp0qwmxNNU/edit#gid=984365354](https://docs.google.com/spreadsheets/d/1bZwldqPRBlcCILnpR7IGe9FF31RomvVSlCp0qwmxNNU/edit#gid=984365354)

### Character Counts

 **Head: shortText, ~** 30 characters

**Body: longText, simple text: **max ~122 characters, plainText

*Note: Waiting on some more information regarding the character counts.*

## Variable or optional design elements

**For December 2023 launch: none.**

The Wayfinder is only offered on a neutral background and is on the home page of the website.

## Interactions

When a dropdown option is selected, you are taken directly to its associated link.

## Mobile

The head and body have mobile sizes.

## Breakpoint notes

**Desktop**

The text block and dropdown form element are in a 50% / 50% side-by-side layout.  (6 columns / 6 columns)

**Mobile**

The text block and dropdown form element stack vertically and take up 12 columns.

## Specifications by breakpoint

![1727467110459](image/wayfinder-funcD/1727467110459.png)



![1727467121042](image/wayfinder-funcD/1727467121042.png)



![1727467129927](image/wayfinder-funcD/1727467129927.png)
