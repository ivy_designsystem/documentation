# Links

## Figma page

https://www.figma.com/design/9Cg1j42yeOGqtIygxKVms6/%F0%9F%93%9A-ds__components-Ivy?node-id=4375-164845&m=dev

## Pill shaped UI form elements

Pill shaped UI form elements are used as interface elements in components. They are rounded because they are aligned with our buttons.

https://www.figma.com/design/Dw0blfgr94tEJrA33eTLeM/%F0%9F%93%9A--atoms-and-molecules-Ivy-NEW?node-id=3472-11809&m=dev
