Components are the interactive building blocks used to create our product interfaces. They can range in complexity, from simple components like buttons to more complex components such as fact bands, wayfinder or split content blocks.

We want to make it easy to create experiences utilizing our design system. Here you will find components that are made to be responsive and simple to design with for a range of use cases and patterns. Components can be combined to create larger interactive patterns or page layouts.
