# Atoms

**Foundations / Text**

Text draws on styles from the type system.

**Foundations / UI **

Ivy / Foundations / Icons / ui / ui/ [icn__arrow-right-white](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/60d48b76b25fd2bb3115c9b8?mode=preview)

![1727451153829](image/imgLinkGrpStacked_atomic/1727451153829.png)

# Molecules

 Text molecule: CTAsimple ( *just the type/arrow group* )

Ivy / Components / Molecules / Type Groups / [CTAsimple](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6351c480323a7308437efaca/tab/design?mode=preview)



![1727451159239](image/imgLinkGrpStacked_atomic/1727451159239.png)
