# This component is the same as the [Image  CTA Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635067288c0107416b0d06c5/tab/functional%20narrative?mode=edit)- the key differences being:

* This one has the option for 2x as many images (6 instead of 3)
* This one has an optional kicker and a headline
* This one only has light backgrounds- white or neutral

## Data

**Mandatory Data**

 **Images** : (3-6) cropped to squares. Should be 1080x1080 and Drupal will scale as needed.

Note: image data. TBD what the admin is going to be and whether it will be drag and drop. We will  need to communicate the order of images and what order they will appear in the display. Perhaps make a grid graphic with labels to identify the order for the content editor

**Text label:** Manually entered text for the CTAsimple text group. Target is 2 words but the text can wrap to 2 lines. (not recommended)

**Optional Data**

**Kicker: **Plain text. just a few words to provide a categorical context for the CTA links

**Header:** Plain text. Header length. Not a paragraph of text. One sentence header.

## Design Elements

**Scrim overlay**

The scrim is designed to shade the bottom part of the image to ensure adequate contrast for the the CTA type and overlay.

/* gradations/gradation-scrim */

background: linear-gradient(180deg, rgba(38, 41, 44, 0.0001) 26.14%, rgba(38, 41, 44, 0.848063) 100%);

mix-blend-mode: multiply;

**Image**

Square. Drupal to scale for display and optimize.

**CTA text** (1 or 2 action-oriented words)

**Right arrow SVG**

**Image Inner Border**

Each image has a 1px transparent inner border, 5% black. The intent with this border is to hold an edge if the image is white or very light. Otherwise, the border will not be so visible to the user. It's meant to be discreet or invisible if not needed.

![1727451200322](image/imgLinkGrpStacked_funcD/1727451200322.png)

issue: inner border will be double on the inside edges of images. It should be oK as it is so light but we'll test it during QA.

## Display

**Desktop**

Starting at the s-768 breakpoint, the images are in 2 rows of 3 columns (S-xL. The images are square, butt with no space between them, and take up 1/3 of the page content area not including the left and right margins.

## Internal padding and vertical spacing

Vertical spacing when components has stacked is a delicate thing. Too little and the page will be hard to consume. The user will feel cramped and the content will be uninviting. Too much space and the flow will be off. The page could feel disjointed.

In Ivy, our scalable design system, we are working towards global guidelines as much as possible. This is to make things easy as well as consistent. There is a 'sweet spot' - a balance, with the amount of white space. Here is what we are working with. Since we are working towards a "sweet spot" and haven't arrived yet, make sure that the padding values are easily editable.

**Desktop **

padding-top: 128px

padding-bottom: 64px

**Mobile**

padding-top: 96px

padding-bottom: 48px

Note that the padding-bottom is half of the padding-top. This is so that when they are stacked in a page layout, the space isn't doubled between each component.

**xxl-1920:** Image content is 1376 total width (xl-1440 less L/R 32px margins). Background continues to full xxl breakpoint. (image component does not scale up after 1440, just the background)

**Mobile**

The ImageCTA component for mobile (xxs320-xs480) the images are in 3 rows of 2 columns. The images are square, no space between them.The image group spans 12 columns- not including the left and right margins.

The CTA text on mobile is smaller as well as the padding. (8px padding on mobile)

![1727451236911](image/imgLinkGrpStacked_funcD/1727451236911.png)

![1727451260047](image/imgLinkGrpStacked_funcD/1727451260047.png)



**Background**

size: The background extends full browser width. Take note of the internal top and bottom padding for all breakpoints.

 *color* : This component has a solid background. Plan for it to be able to be changed by the content admin during a page edit. For the December 2022 launch of the updated home page, we only need the white background color shown in the handoff. For future releases, plan for the the additional neutral bg color to be added.

* white #FFF
* neutral #F7F7F7

## Image Guidelines

I**mage selection and style**

Photography should feel natural, engaged and 'in the moment'. It should not be self-conscious or posed. Avoid images where people are looking at the camera. It should feel like we are looking in. Experiential, hands-on learning that shows an engaged academic experience is preferred even if taking place within our classrooms and facilities. Photos are used in full color- not black and white, duotone or tinted.

**Diversity and Inclusion**

Make every effort to select photos that are diverse and inclusive without tokenizing. UVM and Vermont are not richly diverse but are trying. Be thoughtful and deliberate in the selection of photos to portray UVM as inclusive. The selection should feel natural and not forced.

**Select images to increase engagement**

Each image in this component is an integral part of a Call to Action. It's to get users to do something- to click on a link and take an action. The image needs to support the action that needs to be taken and be relevant.

Since 3 images are used in this component, and they butt without any space, it's important to also consider how the 3 images all work together. Even though they butt, the boundaries for each image should be visible. They should not appear to blend into one.

**Type Contrast**

A dark scrim is added programmatically to each image. The scrim is designed to shade the bottom part of the image. In a natural and smooth gradation, it goes from 100% transparency of our darkGray to 26% opacity of our darkGray at the bottom, to ensure adequate contrast for the white CTA type that overlays the image at the bottom.

**Image sizing**

Images should be cropped to square- recommend 600x600 size @72dpi, PNG or JPG 100% quality. Drupal will optimize and scale.

## Interaction

**Desktop**

onLoad:

background loads first and then each image

Images load with [fade d](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview)[elay](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5ff48b0911d8e84eb828ff26?mode=preview), consecutively and rapidly, left to right on dt and top to bottom on mobile.

**Micro-interaction: **on-hover of the CTA (entire image is hit area), the image turns to 100% primary3 yellow and the text turns to primary1-darkest

**Mobile**

Same as above. Image background change, if possible, is on-tap.

CTA Text Guidelines

Manually entered text for the CTAsimple text group. Target is 2 words but the text can wrap to 2 lines. (not recommended)

The following examples show why 1 or 2 words are recommended for this component for each CTA. In this example, "explore our academic ecosystem" is the edge case for a long CTA. It covers the image completely as a few breakpoints - it's too long.

The example also shows why all 3 CTAs should all have the same amount of text, or nearly the same amount of text.

G**oing to 3 lines of text is just bad.**

 Don't do it. The image is obliterated in many scenarios. Text should be kept to not more than 2 lines at all breakpoints.

T**ext Case:**

Text is sentence case: the initial character is capitalized and the rest is lower case.

E**xceptions to Text Casing**

Words can have initial caps if they are official titles- ie: The Davis Center

Small-768

Always the most difficult breakpoint. It's considered a desktop size so the H4 title looks a bit big.

![1727451282723](image/imgLinkGrpStacked_funcD/1727451282723.png)


## Developer's Note

Text is anchored at the bottom and **scales UP** from the bottom as text is added. The arrow and text block never shifts down.
