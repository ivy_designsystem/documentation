**Molecule: Individual Related News Teaser**

* media 16x9 (still image or video)
* headline
* secondary text
* box (teaser background)
* rule styles
* elevation

**Organism: Related News Teaser Group**

* Individual related news teaser molecule group
* box (background container)
* title (Related News)
* rule styles
* elevation
