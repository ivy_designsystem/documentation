# The Related News component provides links to news stories that are programmatically related. It appears at the bottom of the news story if the story has Related News content.

## Quantity of Related Stories

There are 3 setting options:

* None
* Top 3
* Curated

There can be unlimited (0 to infinite) related stories when Curated list is chosen for "Show Related Stories"

If Top 3 is chosen then we will automate Top 3 based on some undetermined at this time combination and weight of Story Topic, Areas of Study, College or School, Department, and keywords. Backend will not allow duplicate stories.

## **Mandatory Elements**

- Story Title (limited to xx characters)

## **Recommended but Optional Elements**

- Media (16x9 still image or video, displayed as a thumbnail)
- Thumbnail pulled from 1st slide show image if story has a featured slideshow
- Secondary text (appears above 480)

**secondary text:** maximum 300 characters including end ellipse (...)

For news stories that do not have a featured image, a UVM branded placeholder image with the university seal is used to hold that space.

![1730753832084](image/related-news_funcD/1730753832084.png)

## Link

* Title links to full story.
* Image or video do not link (because on click of video it plays)

## Interaction

Hover and on-tap state is for entire teaser block. Elevation of card raises on-hover / on-tap. Title underlines.

The entire RECOMMENDED NEWS block fades in on-scroll. The individual related news story teasers fade-build to to bottom on-scroll with a delay.

## Responsive Display Differences

* Displayed as a list of horizontal cards at screen widths of 480px and larger.
* Displayed as individual stacked cards at resolutions below 480.

**Desktop Display**

![1730753844460](image/related-news_funcD/1730753844460.png)

**Mobile Display**

Related news gets displayed as cards from 320-479 widths. The cards stack vertically. Width of each card is equal. Height of each of the cards varies depending on the length of the News Head that is displayed.

All cards have an (optional) 16x9 thumbnail image or video and a headline (story title). News subhead does not appear views below 479.

![1730753854890](image/related-news_funcD/1730753854890.png)

Mobile view ^
