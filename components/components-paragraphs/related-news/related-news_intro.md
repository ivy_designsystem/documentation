# The Related News component provides links to news stories that are programmatically related. They are not manually curated rather the relationships are established in the news tool database and they are dynamically displayed at the bottom of a news story, if applicable.

![1730753882765](image/related-news_intro/1730753882765.png)
