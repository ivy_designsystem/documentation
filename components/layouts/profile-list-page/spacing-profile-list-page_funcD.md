# Profile list pages display  lists of profiles by category in either card or list format.

![1729715825658](image/spacing-profile-list-page_funcD/1729715825658.png)

## Components used in this layout

In top down order, the components used in this layout include:

* [Short hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64aebed5194db88efe7fc1f1/tab/design?mode=preview) (Child hero, text only, home page)
* [Breadcrumb](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b07aa733f8e6e46266dd5/tab/design?mode=preview)
* [Section navigation](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b096106ea117c68a970cc/tab/design?mode=preview)
* [Profile list (card)](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/650adb5b08c5150461fb1439/tab/design?mode=preview)
* [Profile list (list)](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/650adb5b08c5150461fb1439/tab/design?mode=preview)
* [Footer](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61538e03fdd6ef2d4fc199ea/tab/design?mode=edit)
* [Sub footer](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63480a7e6a1a8f2b7c69f9f5/tab/design?mode=preview)

## Stacking / Spacing

Vertical spacing on our website is handled by padding top and bottom internal to the individual components. There is no additional spacing added between vertical stacks of components.

We are working to standardize vertical spacing.

On desktop, the padding top and bottom is 64px

On mobile, the padding top and bottom is 48px.
