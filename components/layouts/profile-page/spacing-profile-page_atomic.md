# Atoms

**Icons for Key Info**

Ivy / Foundations / icons / [profile icons](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/64e5160c7c57dc9aeebda9e0?mode=preview)

**Organisms / Components**

Components often used in profiles include:

* [Media with text group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63b5cfefc75ee24a9e574458/tab/design?mode=preview) (video or still image)
* [Media Card Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63dae0f201b1a64aab6638ed/tab/design?mode=preview) (Can be used to display portfolio items)
* [News teaser block](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/design?mode=preview)

**Components used on the page**

Header

* [white overlay](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6337385a6ac3f2c303dae1eb/tab/design?mode=preview)
* [parent header](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6426eaf1c59e3911be5fa66c/tab/design?mode=preview)
* [sticky](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/6345bb36dddd281bae73171c/tab/design?mode=preview)

[Profile hero](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e37cb348713aebc4593c99/tab/design?mode=preview)

[Section nav panel](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b096106ea117c68a970cc/tab/design?mode=preview)

[breadcrumb](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b07aa733f8e6e46266dd5/tab/design?mode=preview)

[Footer](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/61538e03fdd6ef2d4fc199ea/tab/design?mode=preview)

[Sub footer](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e4fc897c57dcd0e3bd98ce/tab/Sub%20footer)

[WYWIWYG styling](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/folder/64822ee804929a56cbcb6dcc?mode=preview)
