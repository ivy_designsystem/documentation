# UVM has profile - or bio - pages for people at the university including leadership, faculty, staff and students. Profile pages are a lot like CVs. They provide information about the person's area of expertise, experience, and key contact information among other relevant information.

## Data

See the [excel sheet which the webteam created that outlines the data.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64e4fc897c57dcd0e3bd98ce/tab/UVM%20has%20profile%20-%20or%20bio%20-%20pages%20for%20people%20at%20the%20university%20including%20leadership,%20faculty,%20staff%20and%20students.%20Profile%20pages%20are%20a%20lot%20like%20CVs.%20They%20provide%20information%20about%20the%20person's%20area%20of%20expertise,%20experience,%20and%20key%20contact%20information%20among%20other%20relevant%20information.)

## Display

### Key info

The key contact and at-a-glance information for the profiles appears in list form just below the breadcrumb. On desktop, some of this info appears in the hero with the balance appearing below the breadcrumb in list form. On mobile, all of it is shown below the hero.

**Key info includes:**

* Email
* Phone
* Pronouns
* Pronunciation
* LinkedIn social link
* Location
* Website link
* Downlaod vCard
* Download CV
* Department

**Icons**

A few of the key info items are displayed in lockup with icons

These include:

* Email
* Phone
* LinkedIn Social link
* Download vCard
* Download CV
* Website link
* Location

**Icon behavior**

The default/link color for the icons is lightGray.

On hover, the icon fill color changes to darkGray.

The icon should be included in the hyperlink in case someone clicks on it instead of the text.

### Profile information display

The balance of the core profile information displays using the vertical tab component. On mobile, the vertical tab component's content stacks vertically on the page with no user engagement needed to view all of the content.  See the [vertical tab component ](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/64d23fdd1579dd213d638485/tab/design?mode=preview)for more information.

## Layout

## Stacking  and spacing between components

Spacing between components is handled by internal top and bottom padding. Components stack with not space between them.

**Desktop: **64px top and bottom padding for components

**Mobile:** 64px top and bottom padding for components

## Text containers

The width of text containers varies by breakpoint to keep text line lengths accessible and readable.  Text containers should not exceed these column spans whether they are embedded within a larger component or free standing.

xxs-xs: 12 columns

s: 8 columns

medium-L: 8 columns with 40px left margin

xl, xxl: 7 colums with 40px left margin

## Section navigation panel

The section navigation panel overlaps the profile hero by 48px for all breakpoints.

The section navigation panel is open on page load for desktop breakpoints and closed on page load for mobile.

The width of the section navigation panel is determined by the column span. Column span varies by breakpoint.

M-xxl: 3 column span

small: 4 column span

xxxs-s: 12 columns whether portrait or landscape.

## Key contact info / list

* Key contact info is split between the hero and a 2-column list below the breadcrumb on desktop
* On mobile, all of the key contact info appears below the hero and breadcrumb.
* Key contact info appears as 2 columns on mobile in Landscape orientation.
* For the xs breakpoint, key contact info is 2 columns but for xxs, it appears as 1.

## Layout notes

On desktop, the breadcrumb and key profile lists appear to the right of the section navigation panel. Depending on the length of the section navigation, whether it is expanded or collapsed, the section nav panel may cover up on page-load  some 12-column span components  that appear below the key contact list.

![1729716001241](image/spacing-profile-page_funcD/1729716001241.png)

This is not a UX issue due to the on-scroll  behavior of the section nav panel.  [See the section nav panel for more information.](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/641b096106ea117c68a970cc/tab/functional%20narrative?mode=preview)

Components used in Profies

In addition to the data planned for profiles, profile pages can incorporate content in components that deliver value and a media rich experience to the site visitor. Additional components that can be added to profiles include:

* [Media with text group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63b5cfefc75ee24a9e574458/tab/design?mode=preview) (video or still image)
* [Media Card Group](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/63dae0f201b1a64aab6638ed/tab/design?mode=preview) (Can be used to display portfolio items)
* [News teaser block](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/635c36ff9c63da435b5e2b4a/tab/design?mode=preview)
