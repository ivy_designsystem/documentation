# Program pages are landing pages for individual academic programs.  These pages are integral to the website as they present the University's core offerings - our academic programs. They are highly visited pages with analytics often showing a higher visit rate than their respective department's home page.

## Page goals

The program pages are gateways to critical enrollment actions including inquiries, visits, applications, and enrollment. The goal of this page is to give a detailed but succinct presentation of a program to encourage a prospective student to convert to an appropriate enrollment action. The program page type should also be able to serve as an exclusive destination from the “academic program finder” allowing prospective students to have direct and easy access to important high-level information about each of UVM’s academic offerings with the ability to “drill in deeper,” as desired.



![1729716081241](image/spacing-program-page_funcD/1729716081241.png)

## Purpose

Create consistency among program pages so that prospective students can easily compare programs **and** highlight essential information in their program ‘selection’ process. We know that these program pages are highly visited (in many cases more often than department homepages) for both undergraduate **and** graduate programs.

Highlight a program’s value proposition; encourage application, enrollment or matriculation; provide details options, requirements, curriculum, outcomes, learning outcomes acquired. These pages are often the most highly visited pages in a department, often more visited than the department ‘homepage’ so it is important to (re)articulate departmental strengths as they pertain to the individual programs.

Facilitate an accurate and consistent listing of UVM programs in the UVM “Program Finder” and other filtered lists on college, school, admissions, and other relevant web pages.

## Components to be displayed on each page

It is critical that the content, its presentation and order are consistent across all program pages. The University is essentially providing a shopping experience of our course offerings and this high-level information about the programs will facilitate quick comprehension and comparison of our offerings.

This data and component is a harbinger to the Academic Program Finder application that we are planning for in the future. We are setting ourselves up for success by honing this data and creating a consistent display that will allow future, side by side, apples-to-apples comparison of programs.

Here are the components to be displayed on every program page:

**1) Hero **

(progress options depending on assets and goals)

* Short hero
* Tall hero
* Text hero

![1729716093300](image/spacing-program-page_funcD/1729716093300.png)

**2) Key program info**

![1729716100865](image/spacing-program-page_funcD/1729716100865.png)

**3) Breadcrumb**

![1729716108191](image/spacing-program-page_funcD/1729716108191.png)

**4) Intro (text)**

See above in lockup with the breadcrump.

**5) Vertical tab**

* Program Overview
* Concentrations
* Curriculum
* Outcomes and Careers
* Requirements and Deadlines

![1729716116895](image/spacing-program-page_funcD/1729716116895.png)

**6) Image CTA**

(Inquire / Vise / Apply) /*no CTA was in the wireframe but was included in requirements in creative brief*/

![1729716123892](image/spacing-program-page_funcD/1729716123892.png)

7) Spotlights (Media Card Group, manually curated, elevated content)

![1729716130407](image/spacing-program-page_funcD/1729716130407.png)

**8) Facts**

![1729716146324](image/spacing-program-page_funcD/1729716146324.png)

**9) Related Programs** (with list or grid display options)

![1729716156261](image/spacing-program-page_funcD/1729716156261.png)

Users can switch between grid and list vies via the icon controls.

![1729716162948](image/spacing-program-page_funcD/1729716162948.png)

**10) Footer and subFooter**

## Data

After compiling analytics results, examples, research data (Carnegie), and internal survey data (survey amongst UVM communicators, faculty and students), each program page should contain the following required, recommended and optional elements.

* Program name (official) **REQ**
* Degree type(s)* [separate from name, BA, BS, MA, PhD etc.] **REQ**
* Available completely online? [when applicable]**REQ**
* Image {can add value but is it necessary?}
* Description [quick elevator pitch, what makes UVM’s program special] **REQ**
* Contact information**REQ**
* Concentration and/or options **REC**
* Learning outcomes**REC**
* Curriculum/degree requirements [includes courses] **REC**
* Admissions requirements and deadlines **REC**
* Career paths and employers of alumni [interesting careers] **REC**
* Special scholarship opportunities **OPT**
* Program tagline **OPT** {should this be required?}
* Faculty [may be useful for some programs, particularly grad; names and links] **OPT**
* “Program Features” [a way to highlight program specific items, such as student or alumni spotlights, research or internship or study abroad opportunities, videos] **OPT**

**Design note:* the Carnegie survey indicated prospectives want a way to easily identify the program type and whether the program is offered online.

## Layout and spacing

Inspectable [layout comps can be found here. ](https://www.figma.com/file/PWTg6C6AQ9nvEuEBauEdIz/layouts?type=design&node-id=178-26391&mode=dev)

## Overarching layout specs

**Vertical stacking of components**

Spacing between components is standardized across the website and is controlled by internal padding-top and padding-bottom. Spacing varies between desktop and mobile as follows:

Desktop: 64px padding top and bottom

Mobile: 48px padding top and bottom

**Exception:**

There is ONE exception to this. The breadcrumb has a 32px padding-top for all breakpoints. Padding-bottom follows the global rule

Unit level navigation panel

Overlaps the hero 48px across all breakpoints

**Desktop text display**

There is a 40px padding-left for text containers on desktop. see below. It is carried into the 'Key Info Panel' below so the text aligns left.

![1729716172340](image/spacing-program-page_funcD/1729716172340.png)



![1729716183343](image/spacing-program-page_funcD/1729716183343.png)
