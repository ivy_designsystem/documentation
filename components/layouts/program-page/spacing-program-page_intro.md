# Program pages are landing pages for individual academic programs. These pages are integral to the website as they present the University's core offerings - our academic programs. They are highly visited pages with analytics often showing a higher visit rate than their respective department's home page.

![1729716062662](image/spacing-program-page_intro/1729716062662.png)
