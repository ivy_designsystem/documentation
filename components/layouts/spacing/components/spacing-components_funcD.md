# Global rules for vertical spacing between components.

## Component spacing

There is zero spacing between components. The spacing is handled by top and bottom padding internal to the components.

**Spacing between components;** 0px

## Internal padding by breakpoint

We are working towards consistency for internal padding across all of our components. The leadership at UVM is very sensitive to vertical spacing and the design team has not yet been able to establish global guidelines around this that will appease the leadership team's preference for minimal spacing and the design team's adoption of best practices for spacing that supports accessibility and enhances readability and comfortable consumption of content.

In general terms, we are working with:

**Desktop: **96px top and bottom padding

**Mobile:** 64px top and bottom padding

^ This is what we are using on the home page and top tier pages- which largely have marketing-focussed components rather than text-heavy components.

We are also considering this tighter model- which works better for text-heavy displays and text-heavy components like the vertical tab, infoBand and accordion.

**Desktop: **64px top and bottom padding

**Mobile:** 48px top and bottom padding
