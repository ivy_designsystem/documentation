# Text widths have been established to provide readable line lengths of text across all breakpoints.

![1729716287373](image/spacing-text-containers_funcD/1729716287373.png)

## Small breakpoint

![1729716297059](image/spacing-text-containers_funcD/1729716297059.png)

^ The content panel on the small breakpoint does not have any padding left.

## Medium and larger breakpoints

![1729716325866](image/spacing-text-containers_funcD/1729716325866.png)

^ There is a 40px left padding on the content panel container for medium and larger breakpoints.
