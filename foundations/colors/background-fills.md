# Background Fills

fills/backgrounds/neutral
light background fill option

#F7F7F7
fills/backgrounds/white
Light background fill option

#FFFFFF
fills/backgrounds/primaryDark
Catamount Green background fill

#154734

Sample
Name
Value
fills/backgrounds/neutralWarm
This fill only shows up in one place: the background for the call out in WYSIWYG

#EDEDE9

![1730422437815](image/background-fills/1730422437815.png)
