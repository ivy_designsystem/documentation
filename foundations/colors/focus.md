# Focus

focus/focus_onDark
Focus color to be used on Digital Catamount Green and Catamount Green and darker backgrounds, and dark mode

#94CCBE
focus/focus_onLight
Focus color to be used on white, neutral, and light backgrounds and light mode.

#359A81

![1730422458034](image/focus/1730422458034.png)
