# Grayscale

The Grayscale palette has greatest fidelity at the dark and light extremes as the true- silver, medium grays create a lot of contrast issues when applied to either type or background colors on the web.

darkGray is used primarily for body and heads.

Neutral is primarily used for light background fills for components.

Neutral--lightest will be used selectively as an alternate page background color when we desire whites to POP... it appears to be white until an actual white is placed upon it.

lightGray is the lightest that type should ever be set on a light background.

All grays lighter than lightGray exist for shading and fills and should not be used for type. See the Typography section for approaches to color variants using transparency- alpha for having type appear gray on different color backgrounds.

**grayscale/black**
Note: Black is in our palette but is seldom used. Go with darkGray
#000000

**grayscale/darkGray**
Use this color for body text and headlines.
rgb 38.41.44
#26292C

**grayscale/darkGray--lighter**
Not to be used for backgrounds or type.
#393D41

**grayscale/lightGray**
Use this color for image captions on white. This is the lightText.
rgb 93.100.107
#5D646B

**grayscale/lightGray-lighter**
Not to be used for backgrounds or type.
#A5AAAF

**grayscale/lightGray-lightest**
Not to be used for background or type.
#BEC3C8

**grayscale/neutral-darkest**
Used for contrast with the neutral color.
#E7E7E7

**grayscale/neutral-darker**
Used for contrast with the lighter neutral color.
#F2F2F2

**grayscale/neutral**
This is the default light fill for a container background.
#F7F7F7

**grayscale/neutral-lightest**
This color is sometimes used for the page background instead of white.
#FCFCFC

**grayscale/transparent**
white, 0% opacity. This is needed for the infinity gradation, for example.
#FFFFFF

**grayscale/white**
#FFFFFF

![1730422488718](image/grayscale/1730422488718.png)

### Overlay Colors

Overlay colors: These have transparency to can go on a dark or light background and appear similarly gray.

**type/body type colors/onLight**

72% of darkGray #26292c

**type/body type colors/onDark**

80% of white


![1730422496460](image/grayscale/1730422496460.png)
