# Modals

## Primary1

modal/primary1/modalPrimary1--20
#01533F
modal/primary1/modalPrimary1-30
#01533F
modal/primary1/modalPrimary1-40
#01533F
modal/primary1/modalPrimary1-80
#01533F

![1730422536712](image/modals/1730422536712.png)

## White

modal/white/modalWhite-20
#FFFFFF
modal/white/modalWhite-40
#FFFFFF
modal/white/modalWhite-50
#FFFFFF
modal/white/modalWhite-70
#FFFFFF
modal/white/modalWhite-80
#FFFFFF
modal/white/modalWhite-90
#FFFFFF
modal/white/modalWhite-90
#FFFFFF

![1730422543755](image/modals/1730422543755.png)

## Black

modal/black/modalBlack-0
#000000
modal/black/modalBlack-02
#000000
modal/black/modalBlack-05
#000000
modal/black/modalBlack--10
#000000
modal/black/modalBlack--20
#000000
modal/black/modalBlack--30
#000000
modal/black/modalBlack--40
#000000
modal/black/modalBlack--50
#000000
modal/black/modalBlack--60
#000000
modal/black/modalBlack--70
#000000
modal/black/modalBlack--80
#000000
modal/black/modalBlack--90
#000000

![1730422550638](image/modals/1730422550638.png)
