## Primary

Our primary color palette is designed for use throughout our digital products within interfaces and for branding. The primary brand dark (Catamount Green) and primaryLight (Catamount Gold) are displayed most frequently across our products' screens and components.

Primary colors are used for:

* Dark background fills
* Key actions such as primary call to action buttons
* Highlighted content

Component guidelines indicate how colors should be applied, noting where some should be utilized sparingly and with purpose for specific components like errors UI interface elements and background fields.

### Brand/Catamount Green

This is UVMs primary Brand Color. On the website, it is used mostly for dark background fills for specific components.  It is too dark to use for UI elements.

#154734

### Brand/Catamount Green Digital

This is our primary brand color for the website and is used for UI elements.

#1B6048

### Brand/Catamount Gold

#FFD100

![1730422578597](image/primary/1730422578597.png)

## Brand Accent Colors

There are 3 brand accent colors: Clear Sky, Old-Mill and Vermont Slate.

Vermont Slate has been applied to the component section titles, such as "UVM News". These are the category titles that describe the content in a component.

### Brand / Clear sky

#489FDF

### Brand / Old-Mill

#DC582A

### Brand / Vermont Slate

#00313C

![1730422595466](image/primary/1730422595466.png)

## Brand Neutral

Brand neutral (Mountain Fog) is used only as a light neutral background alternative to white.

### Brand / Mountain Fog

#F7F7F7

![1730422618918](image/primary/1730422618918.png)
