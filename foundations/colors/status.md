# Status Colors

Status colors communicate to the user either the status of something or alert the user to something they need to pay attention to. The colors here pass WCAG color as type on darkGray ##26292C and black #000. Or, these colors can be used as background colors with type a modalBlack-80 on top. All pass WCAG 2.0 4.51 contrast standards.

The Drupal admin interface uses red to communicate mandatory data. The error status color below can be used on the darkGray #26292C or black but not on white. Even the brightest red does not meet WCAG 4.51 contrast on white- so be careful with that. None of the colors below are designed to be text on white.

status/success, all good (usage TBD)
#94CCBE
status/Urgency Alert
#FF7F7A
notification, "hey Catamounts..."
#FFD100

![1730422653895](image/status/1730422653895.png)
