# Tints

### **Primary 1 tints**

primary1/primary1-darkest
#0F3327
primary1/primary1-dark
This is Catamount Green, primary for all non-digital materials

#154734
primary1/primary1
This is Catamount Green, primary for DIGITAL

#1B6048
primary1/primary1-pastel
#E7F4ED
primary1/primary1-faded
#DBE2E0

![1730422775927](image/tints/1730422775927.png)


### Primary 2 tints

primary2/primary2--lighter
#FFEBA6
primary2/primary2
#FFD100
primary2/primary2-darker
#FFC400


![1730422764520](image/tints/1730422764520.png)

### Secondary / Accent tints

Brand Tints/blueSky-darker
#1E7BC0
Sample
Name
Value
Brand Tints/OldMill-darker
#CD4B1E
Brand Tints/OldMill-lighter
#F06231
Brand Tints/OldMill-pastel
#FF8E67



![1730422747840](image/tints/1730422747840.png)

![1730422733289](image/tints/1730422733289.png)
