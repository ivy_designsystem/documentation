Try to use the html special character for a "curly" or "smart" apostrophe or quote rather than the straight one that appears directly from the keyboard when you type the apostrophe key. There are both keyboard combinations to type the "curly" or "smart" version as well as HTML entity codes for it.

# Single quotes and apostrophes

HTML CODE: &#39;

HTML ENTITY: &apos;



![1730470007080](image/apostrophes-and-quotes/1730470007080.png)

Here is an example with larger type. It's a bit easier to see the difference between the straight apostrophe and the curly apostrophe.



![1730470014286](image/apostrophes-and-quotes/1730470014286.png)

# Double quotes

Best practice for the typographical display of quotes is to use "curly" quotes instead of straight quotes.

### “	opening double quote

**Windows:** alt 0147

**Mac:** option + [

 **HTML entity: ** &ldquo;

### ”	closing double quote

**Windows: **alt 0148

**Mac:** option + shift + [

**HTML entity:** &rdquo;

![1730470043819](image/apostrophes-and-quotes/1730470043819.png)
