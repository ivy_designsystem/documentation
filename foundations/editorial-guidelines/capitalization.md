# Use sentence case

It is preferable to use sentence case rather than title case for

* All headlines except the H1 page title which will be in Title Case (camel case)
* UI labels for buttons
* UI labels for navigation
* Sentences
* Titles

Sentence case is when the initial character in a sentence is capitalized and the other words are not capitalized.

If there are official titles within the sentence, then those should adopt the appropriate casing and have an initial cap.

Using sentence case has proven to be easier for users to read and consume content. It is more inviting and less formal. Also, words or phrases embedded within a sentence that need to be capitalized will be easier to identify within the sentence.

**Guidelines for capitalization**

* Only capitalize the first word in a sentence, headline, UI label
* Always capitalize names and other proper nouns
* Capitalize acronyms as appropriate (e.g., CAS, CALS, CEMS etc.)
* Use U&lc for acronyms as mandated (eg., MoMa, CATcard)
