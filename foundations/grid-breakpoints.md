# Grid and breakpoints

## **UVM employs a responsive, mobile first design approach. A system of breakpoints and grids provides the foundation of the UVM design system and helps us maintain visual consistency and provide an optimal user experience throughout.**

### **Planning for different viewing scenarios**

**Mobile first**

We take a mobile-first approach as the limitations that are inherit to the mobile platform- ie: smaller viewing area, bandwidth, single-hand usage, viewing ‘on-the-go’, lack of hover effects and other interactions will force designers to create a leaner interface distilled down to offer the essentials.

**Progressive advancement strategy**

 After meeting the requirements for mobile first, we can employ progressive advancement to have additional features or interactions at larger screen sizes. Since the smaller mobile approach would already offer a fully functioning, intuitive interface, anything added would truly be extra and non-essential.

**Responsive breakpoints**

The UVM design system uses responsive breakpoints and grids to provide enough framework to layout content for an optimal user experience for different viewing scenarios, devices and screen resolutions. Breakpoints give us the opportunity to have a slightly (or completely different) layout at each breakpoint depending on the screen size it’s viewed on. Depending on the screen size, elements will stretch or shrink accordingly within their containers.

**Grid unit**

Our grids are based upon an 8px unit. 8px was chosen as the basic unit because it is easily divisible by 1,2,4 and itself. It's the foundation for creating consistent and visually appealing user interfaces (UIs) and is the basis for setting vertical and horizontal spacing for everything from type and line-height size, the foundation grids and spacing between, and within, all components.

**Margins**

Each breakpoint has 32px margins on the left and right. This space is intended to provide 32px on each side of the body content container at every breakpoint. It also provides enough space for the browser's scrollbar for vertically scrolling desktop scenarios. The gutters were made consistent in the grid for standardization of the components and how they sit in the grid and how they responsively scale.

Note that even though page margins have been made consistent in the grid, it doesn't prohibit full-browser width components and backgrounds.

**Gutters**

Each breakpoint uses 32px gutters. This space is intended to provide 16px on each side of a column at every breakpoint. The default gutter width of 32px is to make sure that type- whether within a container or not.

However, there are three different gutter structures:

* Super Wide (default) 32px
* Wide (16px)
* small (8px)
* x-small (4px)
* hairline (1px)
* none (0px)

These 6 gutter options make sure that type is adequately spaced in columns, provide space for hanging punctuation or other hanging content, and provide enough flexibility for content within organisms to be consistent and pleasing.

*Note: Gutter spacing is for * ***product designers*** *- not content editors. Gutter spacing gets baked-in to the components.*

**Responsive Design tip**

Employ a mobile-first design and then do versions for as many breakpoints as needed to show how your design should responsively scale. Not every breakpoint needs to be designed - just where the design "breaks" and the layout needs to change to show how the design should be handled at that particular width. Note that in some cases, hiding content on the smallest mobile widths is sometimes employed. The goal is for as much consistency as possible for both the user and the developer who is building it.

**Responsive grid and breakpoints**

Breakpoints allows content to dynamically respond to the users viewing scenario and is device and platform agnostic.

Responsive grid systems address the available content width on the page by percentages. 100% is the full, available width offered within a *breakpoint. Breakpoints* offer a transitory reset of the layout and grid dimensions as the layout fluidly scales. As customary in responsive grids, the available width gets divided in half to create columns, repeating the division as desired and as for optimal content presentation.

At each breakpoint, the column count is fixed. Margins and padding are fixed multiples of 8. In between waypoints, the actual column width is a percentage of the grid area and content responsively scales fluidly as the viewing width changes. Components scale proportionally. Text line length lengthens or shortens in lock-step with the fluid scaling of the container it’s in.

**Scaling of components**

* Some components scale larger than 1440, and others don't.
* A few components scale to full browser width.
* Backgrounds scale to full browser

![1727724857710](image/grid-breakpoints/1727724857710.png)

**Our desktop breakpoints: **

**XXL 1920:  **

12 columns centered on 1920.

Most components stop scaling at the 1440 grid but a select few scale up to the 1920 grid.

Most components max out at 1440-xl with just the backgrounds extending full browser width.

**XL 1440:**   12 columns 32px gutters, 32px margins

 **L 1280: ** (1280-1439) 12 columns 32px gutters, 32px margins

**M 1012:**  (1012-1279) 12 columns 32px gutters, 24px margins

**S 768: ** (768-1011)12 columns, 32px gutters, 32px margins

**Our mobile breakpoints:**

**XS 480 Landscape mobile:** (480-767) 4 columns, 32px gutters, 32px margins

**XXS 320:** (320-479) 320 4 columns, 32px gutters, 32px margins

# Inspectable breakpoint grids

~See Trail Mix / Components / Atoms / ~[~Breakpoints and Grids~](https://uvmedu.invisionapp.com/dsm/university-of-vermont/trail-mix/nav/5fa7cb6f8c01200018361129/folder/61f7fec95bd8a3d6c7656b55?mode=preview)

# Visual grid reference

![1727724876035](image/grid-breakpoints/1727724876035.png)

**1440 Breakpoint (some components do not scale larger than 1440)**

![1727724997109](image/grid-breakpoints/1727724997109.png)

**1280 Breakpoint**

![1727724989524](image/grid-breakpoints/1727724989524.png)

**1012 Breakpoint**

![1727725022518](image/grid-breakpoints/1727725022518.png)

**Left: 768 breakpoint**

**Center: 480 (landscape) breakpoint**

**Right: 320 breakpoint**
