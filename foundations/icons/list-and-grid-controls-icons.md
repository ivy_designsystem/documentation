# List and grid controls


# Below you will find both the individual List and Grid icons in all their states as well as the ListGrid icon group used as an interface element to allow users to select either a grid or a list display.

![1730487973386](image/list-and-grid-controls-icons/1730487973386.png)


Below are the individual List and Grid icons with default, hover, selected and focus states.

![1730488005892](image/list-and-grid-controls-icons/1730488005892.png)

![1730488018768](image/list-and-grid-controls-icons/1730488018768.png)



4Ivy / controls / list grid icon group / m_list-GridSelected-dt
100 x 48px
4Ivy / controls / list grid icon group / m_list-GridSelected-dt
4Ivy / controls / list grid icon group / m_listSelected-grid-mobile
116 x 56px
4Ivy / controls / list grid icon group / m_listSelected-grid-mobile
4Ivy / controls / list grid icon group / m_listSelected-grid-dt
100 x 48px
4Ivy / controls / list grid icon group / m_listSelected-grid-dt
4Ivy / controls / list grid icon group / m_list-gridSelected-mobile
116 x 56px
4Ivy / controls / list grid icon group / m_list-gridSelected-mobile
Below are the individual List and Grid icons with default, hover, selected and focus states.

Sample
Name
Size
Download
4Ivy/list grid icons/icn_grid-dt-default
56px
4Ivy/list grid icons/icn_grid-dt-default
4Ivy/list grid icons/icn_grid-dt-focus
56px
4Ivy/list grid icons/icn_grid-dt-focus
4Ivy/list grid icons/icn_grid-dt-hover
56px
4Ivy/list grid icons/icn_grid-dt-hover
4Ivy/list grid icons/icn_grid-dt-selected
56px
4Ivy/list grid icons/icn_grid-dt-selected
4Ivy/list grid icons/icn_grid-mobile-default
48px
4Ivy/list grid icons/icn_grid-mobile-default
4Ivy/list grid icons/icn_grid-mobile-focus
48px
4Ivy/list grid icons/icn_grid-mobile-focus
4Ivy/list grid icons/icn_grid-mobile-hover
48px
4Ivy/list grid icons/icn_grid-mobile-hover
4Ivy/list grid icons/icn_grid-mobile-selected
48px
4Ivy/list grid icons/icn_grid-mobile-selected
4Ivy/list grid icons/icn_list-dt-default
56px
4Ivy/list grid icons/icn_list-dt-default
4Ivy/list grid icons/icn_list-dt-focus
56px
4Ivy/list grid icons/icn_list-dt-focus
4Ivy/list grid icons/icn_list-dt-hover
56px
4Ivy/list grid icons/icn_list-dt-hover
4Ivy/list grid icons/icn_list-dt-selected
56px
4Ivy/list grid icons/icn_list-dt-selected
4Ivy/list grid icons/icn_list-mobile-default
48px
4Ivy/list grid icons/icn_list-mobile-default
4Ivy/list grid icons/icn_list-mobile-focus
48px
4Ivy/list grid icons/icn_list-mobile-focus
4Ivy/list grid icons/icn_list-mobile-hover
48px
4Ivy/list grid icons/icn_list-mobile-hover
4Ivy/list grid icons/icn_list-mobile-selected
48px
4Ivy/list grid icons/icn_list-mobile-selected
