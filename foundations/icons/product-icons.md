# Product icons

Product icons not only serve a function in our products, but are also a visual expression of our brand. Each has visual distinction to ensure customers can easily tell them apart. Our product icons differ from system icons, which symbolize common actions. Our product icons include more flourish than other icon uses.
