# Social icons![1730478095972](image/social/1730478095972.png)![1730478101284](image/social/1730478101284.png)

![1730478088130](image/social/1730478088130.png)


apps/icon/icn__facebook
24px
apps/icon/icn__facebook
apps/icon/icn__youTube
24px
apps/icon/icn__youTube
apps/icon/icn__wordpress
24px
apps/icon/icn__wordpress
apps/icon/icn__vimeo-square
24px
apps/icon/icn__vimeo-square
apps/icon/icn__share
24px
apps/icon/icn__share
apps/icon/icn__Instagram
24px
apps/icon/icn__Instagram
apps/icon/icn__LinkedinBoxed
24px
apps/icon/icn__LinkedinBoxed
apps/icon/icn__Pinterest
24px
apps/icon/icn__Pinterest
apps/icon/icn__snapchat
24px
apps/icon/icn__snapchat
apps/icon/icn__vimeo
24px
apps/icon/icn__vimeo
apps/icon/icn__RSS
24px
apps/icon/icn__RSS
apps/icon/icn__Google
24px
apps/icon/icn__Google
apps/icon/icn__Github
24px
apps/icon/icn__Github
apps/icon/icn__flickr
24px
apps/icon/icn__flickr
apps/icn__twitter
24px
apps/icn__twitter
Sample
Name
Size
Download
apps/base/icn_tikTok-40
40px
apps/base/icn_tikTok-40
apps/base/icn_tikTok-24
24px
apps/base/icn_tikTok-24
apps/icon/icn__twitter-24
24px
apps/icon/icn__twitter-24
Sample
Name
Size
Download
Add some icons
Sample
Name
Size
Download
apps/icn_twitter-outline-32
32px
apps/icn_twitter-outline-32
apps/icn_facebook-outline-32
32px
apps/icn_facebook-outline-32
apps/icn_instagram-outline-32
32px
apps/icn_instagram-outline-32
apps/icn_linkedIn-outline-32
32px
apps/icn_linkedIn-outline-32
apps/icn_tikTok-outline-32
32px
apps/icn_tikTok-outline-32
apps/icn_youTube-outline-32
32px
apps/icn_youTube-outline-32
Sample
Name
Size
Download
apps/icn_facebook-solidWhiteBG-40
Use for hover in section nav drawer with green bg

40px
apps/icn_facebook-solidWhiteBG-40
apps/icn_linkedIn-solidWhiteBG-40
40px
apps/icn_linkedIn-solidWhiteBG-40
apps/icn_tikTok-solidWhiteBG-40
40px
apps/icn_tikTok-solidWhiteBG-40
apps/icn_twitter-solidWhiteBG-40
40px
apps/icn_twitter-solidWhiteBG-40
apps/icn_youTube-solidWhiteBG-40
40px
apps/icn_youTube-solidWhiteBG-40
apps/icn_instagram-solidWhiteBG-40
40px
apps/icn_instagram-solidWhiteBG-40
Sample
Name
Size
Download
apps/icn_facebook-solid-32
32px
apps/icn_facebook-solid-32
apps/icn_instagram-solid-32
32px
apps/icn_instagram-solid-32
apps/icn_linkedIn-solid-32
32px
apps/icn_linkedIn-solid-32
apps/icn_tikTok-solid-32
32px
apps/icn_tikTok-solid-32
apps/icn_twitter-solid-32
32px
apps/icn_twitter-solid-32
apps/icn_youTube-solid-32
32px
apps/icn_youTube-solid-32
Sample
Name
Size
Download
apps/icn_facebook-outline-40
40px
apps/icn_facebook-outline-40
apps/icn_instagram-outline-40
40px
apps/icn_instagram-outline-40
apps/icn_linkedIn-outline-40
40px
apps/icn_linkedIn-outline-40
apps/icn_tikTok-outline-40
40px
apps/icn_tikTok-outline-40
apps/icn_twitter-outline-40
40px
apps/icn_twitter-outline-40
apps/icn_youTube-outline-40
40px
apps/icn_youTube-outline-40
Sample
Name
Size
Download
apps/icn_facebook-solid-40
40px
apps/icn_facebook-solid-40
apps/icn_instagram-solid-40
40px
apps/icn_instagram-solid-40
apps/icn_linkedIn-solid-40
40px
apps/icn_linkedIn-solid-40
apps/icn_tikTok-solid-40
40px
apps/icn_tikTok-solid-40
apps/icn_twitter-solid-40
40px
apps/icn_twitter-solid-40
apps/icn_youTube-solid-40
40px
apps/icn_youTube-solid-40
Sample
Name
Size
Download
icons/apps/circleSolid/icnPrimary3/icn__tikTokPrimary3-hover
hover

32px
icons/apps/circleSolid/icnPrimary3/icn__tikTokPrimary3-hover
icons/apps/circleSolid/icnPrimary3/icn__tikTokPrimary3-default
default

32px
icons/apps/circleSolid/icnPrimary3/icn__tikTokPrimary3-default
social/circleSolid/icnPrimaryDark/icn__fbPrimaryDark-hover
hover

32px
social/circleSolid/icnPrimaryDark/icn__fbPrimaryDark-hover
social/circleSolid/icnPrimaryDark/icn__twPrimaryDark-hover
hover

32px
social/circleSolid/icnPrimaryDark/icn__twPrimaryDark-hover
social/circleSolid/icnPrimaryDark/icn__twPrimaryDark-default
default

32px
social/circleSolid/icnPrimaryDark/icn__twPrimaryDark-default
social/circleSolid/icnPrimaryDark/icn__fbPrimaryDark-default
default

32px
social/circleSolid/icnPrimaryDark/icn__fbPrimaryDark-default
social/circleSolid/icnPrimaryDark/icn__instaPrimaryDark-default
default

32px
social/circleSolid/icnPrimaryDark/icn__instaPrimaryDark-default
social/circleSolid/icnPrimaryDark/icn__instaPrimaryDark-hover
hover

32px
social/circleSolid/icnPrimaryDark/icn__instaPrimaryDark-hover
social/circleSolid/icnPrimaryDark/icn__LinPrimaryDark-default
default

32px
social/circleSolid/icnPrimaryDark/icn__LinPrimaryDark-default
social/circleSolid/icnPrimaryDark/icn__LinPrimaryDark-hover
hover

32px
social/circleSolid/icnPrimaryDark/icn__LinPrimaryDark-hover
social/circleSolid/icnPrimaryDark/icn__ytPrimaryDark-hover
hover

32px
social/circleSolid/icnPrimaryDark/icn__ytPrimaryDark-hover
social/circleSolid/icnPrimaryDark/icn__ytPrimaryDark-default
default

32px
social/circleSolid/icnPrimaryDark/icn__ytPrimaryDark-default
Sample
Name
Size
Download
social/circleOutline/icnPrimaryLight/icn__fbLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__fbLight-32-default
social/circleOutline/icnPrimaryLight/icn__fbLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__fbLight-32-hover
social/circleOutline/icnPrimaryLight/icn__flickrLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__flickrLight-32-default
social/circleOutline/icnPrimaryLight/icn__flickrLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__flickrLight-32-hover
social/circleOutline/icnPrimaryLight/icn__instaLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__instaLight-32-default
social/circleOutline/icnPrimaryLight/icn__instaLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__instaLight-32-hover
social/circleOutline/icnPrimaryLight/icn__LiLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__LiLight-32-default
social/circleOutline/icnPrimaryLight/icn__LiLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__LiLight-32-hover
social/circleOutline/icnPrimaryLight/icn__tikTokLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__tikTokLight-32-default
social/circleOutline/icnPrimaryLight/icn__tikTokLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__tikTokLight-32-hover
social/circleOutline/icnPrimaryLight/icn__XLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__XLight-32-default
social/circleOutline/icnPrimaryLight/icn__XLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__XLight-32-hover
social/circleOutline/icnPrimaryLight/icn__ytLight-32-default
32px
social/circleOutline/icnPrimaryLight/icn__ytLight-32-default
social/circleOutline/icnPrimaryLight/icn__ytLight-32-hover
32px
social/circleOutline/icnPrimaryLight/icn__ytLight-32-hover
