# Ivy has a standard for the hover state for images on the website.

# This applies to images that don't have live text on top and does not apply to heroes. You can see this in the [Featured News](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563672e983f5ea83a7d85e/tab/design?mode=preview),  [News Listing](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/655635903a1e277b4133921b/tab/design?mode=preview), and [News List](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361129/asset/65563655663f0b1c2266e420/tab/functional%20narrative?mode=preview) components.

First, make sure that the image container has a background of #FFF white.

On-hover / on-tap, image has a color shift. Here is the 'recipe':

* 115% contrast
* 105% brightness
* 95% opacity
