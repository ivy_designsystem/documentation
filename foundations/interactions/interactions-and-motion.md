# We are committed to a **purposeful** use of motion and interactivity that **adds value** to the user experience of our brand and website. It is used intentionally, tastefully and not be used gratuitously as eye candy.

We are including motion and interactions in our design system in an attempt to both document patterns along with their purpose and how and when to use them. Systemization will save time, allow repeatability of the pattern and contribute to a consistent user experience.

At minimum, interactions should inform a user when something is actionable. Navigation, text links, button, navigation arrows, toggles, etc., all should have clear indications for states depending on the platform and whether the user is accessing the site on desktop, mobile, or via accessibility readers to accommodate users with disabilities.

## Foundations

We have established 3 motion and interaction categories that provide some guiding principles for how we integrate motion into UVM’s digital experiences:

**Productive Usability:** used to communicate states like link/hover/active/disabled, toggles, and modals. It can provide necessary feedback to the user when interacting with the website, helps focus attention on the task and is satisfying for the user as they are making something happen.

**Transition:** Enter and Exit: The appearance or disappearance of an object on the screen, how components and layouts load, on-scroll reveals. The trigger varies.

**Brand and Engagement:** Non gratuitous motion that adds value to the experience and increases engagement with the content. This sort of motion may surprise and delight.

We will be adding to this documentation as our motion and interaction library grows and evolves.

# **UVM Motion Library**

**//** Under development now. This will be updated. Here are some starting points for us to code. **//**

**1. Fade in place**

Fade in behavior lets you reveal an object by changing it’s opacity from 0 to 100 percent. Opacity, time and easing are all variables.

Easing is recommended to create a more natural transition.

Here are some starting points for us.

https://codepen.io/bequiagal/pen/abmJmvM

{animation-timing-function: cubic-bezier (.645,.045,.355,1);}

**2. Fade reveal delay**

{animation-timing-function: cubic-bezier (.645,.045,.355,1);}

The first subsequent element fade-reveals reveal .5s animation-delay

And each next reveal happening with .1s afterwards.

.5, .6 , .7 etc.

**3. Fade and shift**

Here’s an example. The background fades-in in place and the type fades and shifts from the bottom :

https://codepen.io/fabiosangregorio/details/WmGRxK

.item

  .overlay

    opacity:0

    visibility:hidden

    transition:all.3sease-in-out

    div

    padding:10px

    opacity:0

    transform:translateY(0.4em)

    transition:all.3s

  &:hover

    .overlay

    opacity:1

    visibility:visible

    div

    opacity:1

    transform:none

    &:first-child

    transition:transform.4s.2s, opacity.5sease-out.2s

    &:last-child

    transition:transform.3sease-out.3s, opacity.5sease-out.3s

This example could be applied to the Featured Media component whereas each element reveals:

• background image fades in place to 0 opacity

• modal overlay fades in place with a .1s reveal delay to transparency of modal specified in the layout

• text fades-in and shifts on the Y-axis 0.4em

### Easing:

Easing is recommended as it can make the transition or motion feel more natural. Easing defines the level of smoothness of a transition as a parameter over time.

### Time:

In general, the faster the time, the shorter the distance to travel and more suited for smaller objects.

Objects should fade on faster with ease and on exit, fade out slower- with ease-in.

### Triggers

• On scroll-enter

• On scroll-exit

• On page load

• On mouse-over / hover

• On-tap

• On-click

**Fade-in**

right, left, up and down

example from the home page hero
