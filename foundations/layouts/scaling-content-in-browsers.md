# Scaling content in browsers

# We have made some decisions about the upper limit of how things scale in the browser.

In general, on websites where there is a lot of control over the content, full browser window-width images and videos can be used with some degree of success. The UVM site has 1,000 content editors with varying degrees of asset production skills. Many don't have access to professional graphic design tools to crop image assets so rely on free tools available online. Full browser width images have visual challenges as they scale. Even if we establish a focal point to scale around, there is still a chance that at some breakpoint the image will be centered on a part of the image that is not adding value to the user.

For these reasons and more, we have established an upper limit to the size that images can scale rather than having them go full browser width. This impacts a few components- for most, it's not an issue.

We also employ the "vh" visual height CSS class to hero images to crop the height where we want in relation to the visual height of the viewer's browser. Look out for that when you view the components as the % of visual height varies between 100% vh and XXvh.

## Things that scale full browser width at all breakpoints

* Component background fills
* Footer and sub-Footer backgrounds
* Header and sticky-header backgrounds
* Navigation drawer background (does a full take-over)

# xxl-1920 is the largest breakpoint.

No components scale past the 1920 width although the backgrounds of components always scale full browser width. Only a few components actually do scale Up to the 1920 width. The xxl-1920 breakpoint uses the 1440 grid centered.

## Developer Note: plan for change

Obviously, resolutions on all screens and devices will continue to increase. So the upper and lower limits of the breakpoints are moving targets and dependent upon the latest technologies. Plan for these values the change over time.


![1730470114467](image/scaling-content-in-browsers/1730470114467.png)

On mobile and tablet, the hero story scroll image goes to full browser width. (100% vh)

![1730470122732](image/scaling-content-in-browsers/1730470122732.png)

On desktop, the image scales up to 1920 width while the height crops to 100%vh.

The same is true for all hero banners.

The 5050-Big component shown below also scales up to 1920. Consider the bounding box of the entire component, not one side or the other.

![1730470130969](image/scaling-content-in-browsers/1730470130969.png)


![1730470146357](image/scaling-content-in-browsers/1730470146357.png)

The example above shows the layout of the Campus Life page at xxl-1920 breakpoint. You can see that:

* The footer and sub-footer backgrounds scale full browser width
* The neutral background for all the 5050s scales full browser width
* The hero at the top scales to 1920

## Header

The background for the header also scales full browser width but it is not noticeable since it is transparent. However, the header changes to a sticky header with a white background on page scroll, and so on-scroll, you can see a full browser width white background.

![1730470161192](image/scaling-content-in-browsers/1730470161192.png)

![1730470169654](image/scaling-content-in-browsers/1730470169654.png)

**Top** header shown on page load

**Bottom **sticky header with white background shown on-scroll
