## Note:

**TO DO: **Goal is to get spacing relationships to be defined with percentages... as fixed spacing won't work for all head sizes, for example.

**Desktop**

**Head**

64px top [8 spacer]

40px below [5 spacer]

**Within components**

padding-top and padding bottom: 64px top [8 spacer]

For rich media / marketing pages, the vertical spacing is more open: 160px between each.

**Between components**

64px top [8 spacer]

## **Mobile**

**Head**

48px top [6 spacer]

32px bottom [4 spacer]

**Within components**

padding-top and padding bottom: 48px top [6 spacer]

For rich media / marketing pages, the vertical spacing is more open: 112px between each on mobile.

**Between components**

48px top [6 spacer]

[10:55](https://colab-coop.slack.com/archives/D01EX9WRZGR/p1631890559006500)

General rules around spacing with in layouts. Heads don’t have correct spacing in the news stories yet.

Relates to ticket UVM-602.

Ratio: Mobile is 75% percent smaller relationally than desktop (rounded to the nearest 8px on the grid)
