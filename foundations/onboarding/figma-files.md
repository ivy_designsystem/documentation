# All of the design system style and component library files in UVM's Ivy design system were created with Figma. Each component on the site has an associated Figma design file.

[Please see this video walk through](https://uvmoffice.sharepoint.com/:v:/s/UVMWebsiteRedesign/EW_JJpL3KqpEnRPmprKNYCkBfSN9XeRs6_vmXvKTCCfrwA?e=iVFV2t) of the Figma design files and how to access them from Ivy.

**Pages in Figma**

For larger components, the Figma design file is organized by page with each component having its own page.

**Artboards on each page**

Each page is organized into a number of labeled artboards to make it easier to understand what is what.

# Artboard structure

## Overview

Provides a description of the component and its purpose, links to the design and functional narrative in Ivy, the component status and team members who worked on it.

## Handoff

The handoff artboard is where the developers should look to inspect the final component that is ready for development. These are the components that get synced to Ivy.

![1730470224164](image/figma-files/1730470224164.png)

## Design

The design artboard reflects the design process. There may be some legacy work here along with what was approved. The Design artboards are not to be inspected as they are not pre-flighted for handoff to the development team.

## Wireframes

Wireframes are often designed to quickly iterate the interface and workout the UI, UX and functionality. They are rough and allow the product designer to quickly iterate without the visual skin of the look and feel getting in the way.

## Components

Figma components are often grouped as component variables. These are master components for use when designing layouts and pages in Figma. They are the core of the style and component libraries- the originals.

Often a copy of these are made with  "4Ivy" in the name. InVision does not respect the folder organization of the master components so when importing into Ivy, all the component files appear loose and are difficult to find. Until InVision improves this, the workaround is to make an instance of the master components and then resave as a duplicate "4Ivy" component. These appear in tidy folders and are easy to find when populating the Ivy design system with components. On the Figma side, since they are components based upon instances of the master, when the master gets updated, the changes propagate to the components synced with Ivy.

## Inspo

A scrapbook of grabs, likes, dislikes and behind-the-scenes inspiration for the component.

# Condensed format in Figma

Sometimes it doesn't make sense for every component to have it's own page. This is often the case with atoms and molecules. For example, buttons all on one page with component artboards for each button. Regardless, artboards still follow the same naming structure as above.
