# As we bring new people into our team and collaborate on more files and projects, it’s important to share some common practices around work process, organization, and the naming of things.

*NOTE: this page is in the process of being updated. Some of the content below reflects Trail Mix, our old design system and workflow with Sketch / Abstract / Invision. *

This allows for better team collaboration and communication as well as easier onboarding of new team members. To this end, we have implemented a well-defined folder structure and naming convention across the different tools that we work with to help.

If the naming of files, pages, artboards, components, design elements, layers and Projects we practice consistent naming and good file hygiene to make it easier to find things as well as onboard new team members easily without confusion. The naming of things should support the process and when universally adopted, become transparent so the team can focus on important things like design, coding, and strategy.

## Figma

We use Figma to design for the web and digital platforms.

We are organizing our Figma and InVision projects first by project and secondly by breakpoint. Your Invision Prototype name and Figma file name should all match or be close enough to be self-evidently released.

The organization in Figma varies slightly depending on the complexity of the component, but every Figma project document will have separate pages for each breakpoint. The number of breakpoints for your project will be determined by the responsiveness of the components and elements on the page. Breakpoints mark a reset of the responsiveness where the layout elements may reconfigure at the point before responsively scaling to the next larger breakpoint.

For example, if an element needs to transition from a card view to a list view at the 480 breakpoint such as the News Teasers do, then you would want to make sure you have a 320, 480 and larger breakpoint design in your layout so the front end developer understands the transition desired there for optimal viewing.

### Naming Pages in your Figma File

You should have a separate page in your Figma file for each breakpoint. The page name should reference the project and the breakpoint. (The platform will be evident from the breakpoint). Versioning is handled by Abstract so is not a part of the Figma file, page or artboard naming.

For a home page redesign, for example, your Sketch page names would look like this:

### Project—breakpoints

Home-Page design--XXL-1920

Home Page design—XL-1440

Home Page design— L-1280

Home Page design— M-1012

Home Page design—S-768

Home Page design— XS-480

Home Page design—XXS-320

### You could also shorten these to:

HP-design__XL-1440

HP-design__L-1280

Important: When projects are complex and big, I’ve also divided up the desktop and mobile approaches into separate files.

Ie:

navigation-redesign__dt

Navigation-redesign__m

## Naming of Artboards in Figma

Each page in your Figma file will have all the artboards for each breakpoint needed to define the project.

We follow this naming convention for artboards in Figma:

### X.X.X.X.X.X

Project. DepthLevel. Variation. state .Breakpoint.

max-navigation.1.1.A.1 - default

And

navDt.Academics.1.A.default—max

navDt.Academics.1.B.hover—max

Here is an article about this practice. https://medium.com/@grantpetrosyan/5x-or-one-the-best-way-to-organize-your-design-document-982a0e49366b

## Artboards and grids

Pickup from the ds__grids library in the design system.

There are artboards and layout settings for all breakpoints. The “toSize” settings are for dartboards the match the breakpoints and the “extendedWidth” settings are for dartboards where you want to extend background fills or components beyond the page width and visualize that in your design.

### Working with the design system libraries in Figma

Everything in Figma is connected to the design system style and component libraries.

Work with the libraries exclusively for colors, layer fills, elevations, borders and rules and anything else as much as possible.

Figma files should not have local text styles, layer styles, fill styles- they should use the styles in the design system library.

• Components should be linked to the design system libraries and not locally.

The design system provides a framework to keep design on-brand. Enough flexibility is built in so as to not feel restricted so designers don't feel the need to go out of the box to introduce hybrid, off-brand work.

### Syncing with an InVision Project

Even though your Figma document will have separate pages for each breakpoint, the InVision Prototypes are platform dependent— when creating an Invision Prototype, you need to specify if it’s for desktop/web, mobile, tablet etc. Your Invision Prototype name and Figma file name should all match or be close enough to be self-evidently released.

After setting up InVision Prototypes for each breakpoint of your project, please group them under a new Space under InVision/Spaces. For example,

**Figma file:** hp-redesign.figma

**InVision Space: **Home Page Redesign

**InVision prototypes: **

Home Page design—XL

Home Page design— L

Home Page design— M

etc.

XL 1440= desktop

L 1280= desktop

M 1012= desktop (it can also be viewed on a tablet horizontal)

S 768 = tablet portrait

XS 480= iPhone horizontal

XXS 320 = iPhone vertical

### Layers in Figma

Less work is put into layer names in Figma than we had to do for Sketch. Figma automatically picks up the name from the  Layer names in the Sketch files have been named to make hierarchical sense

ie: Component/Content/Content type

In our sketch file, we have, for example

Featured Media--video/Caption/Text

It was decided that the naming of type would be gleaned from it’s location in the tree structure of the layers in the Sketch file.

so you can name the text style in the CSS in rapport to the component it is in reflecting the hierarchy shown in the Sketch layers.

### All layers should be named.

• Layers on the top are higher up on the page than the layers below.

• Leave no empty folders. Delete them.

• File should not have native text styles, layer styles, fill styles- they should use the styles in the design system library.

• Symbols should be linked to the design system libraries and not be local to your document.

### The library document:

ds__grid has artboards and layout settings for designing to the break point width as well as extending beyond it. Artboards ending -extendedWidth are larger than the breakpoint size so you have more room to show what will be extending. ( *Does this make sense? Do we need to trim these down to the breakpoint size before handing off? So the numbers make sense. Note: Tablet and mobile sizes never have extended artboards although the component build can account for that.* )

============

Here’s what I have been trying to do with naming in Figma:

Component Naming

I have been trying to follow the BEM model for naming with some variation.

* **Structure:** block name/element name/ modifier or organism/molecule/atom/variation

Camel Case

We use camel case within and separate block and element with a hyphen.

I have been having a “--” double hyphen before the modifier but I think Drupal and React strip that out- So just separate block-element-modifier with a single hyphen.

you can shorten names of components. ie:

Don’t DO

featured-media--video

Do:

featuredMedia-video

it can also be shortened to:

fm-video

I have also done

o__fm-video

Where o__ is the atomic definition— in this case an organism.

relatedContent > rc

rc__rcList-default

rc__rcList-hover
