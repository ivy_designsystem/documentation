Purpose and Principles











## Every thing we design is in alignment with our guiding principles. These principles help in end-to-end decision making about design, user interface, coding, copy and content creation, user journeys- you name it. They inform decisions, and they keep us from going off the rails. They are baked in. 



**We design for people.** Our work is user-centric. We don't loose track of who we are communicating to and we don't arrogantly assume we know what our users' need or like. We advocate continually testing with target user groups so we can learn about and understand our users so we can create products and communications that add value to our users.





**We embrace accessibility.** We embrace diversity and strive to create an inclusive, accessible experience for everyone. We embrace accessibility guidelines and take them into consideration in our end-to-end process. They are not an after thought.  They are baked in to our process and everything we do.





**We build consistency and scalability.** Consistency is how we align on our values, share our learnings, and design using a common language. Every component was created to work together and scale to help create a consistent customer experience; each touchpoint should feel familiar and meet their needs in the simplest way possible.





**We design for flexibility.** Every user is different and so is the device and their internet access. The foundation of our system has been designed and built to gracefully scale across different breakpoints, devices and platforms so as to offer the most consistent and optimal experience possible. 