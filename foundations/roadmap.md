# The Road Map for Ivy is to  communicate status, priorities, milestones and anticipated next steps.

### Status

Ivy has been the foundation for the digital brand and has enabled us to maintain brand and style consistency during the redesign process. It has also streamlined handoff of design to the development team resulting in efficiencies, better communication, and improved translation of design product to live.

## Platform Change

InVision has sold their Freehand whiteboard product to Miro and has announced shuttering the business on 12/31/24 including the Design System Manager (DSM) product that Ivy resides in. We are evaluating alternate platforms for Ivy and will be making recommendations for a new platform by the end of 2Q 2024.

### Time and capacity

Ivy and all of it's content, screen grabs and documentation need to be backed up to Teams so we have a copy. All of the content in Ivy needs to be moved to the new, as yet undecided, platform.

### Payments to InVision and ensuring access through the end of 2024

Historically, payment processes have been slow at UVM as they are complex and involve a lot of steps. Our DSM contract renewal would normally happen on  May 24, 2024. However, with the business shuttering at the end of the year, InVision will only be able to renew our contract for 6 months. We will be able to pay this in one fee rather than having to go to a month by month payment plan.

## Brand

UVM announced the release of a new brand on 2/15/24 and the website design and Ivy are going to evolve to incorporate the new brand. Initial brand changes include palette, and logo. Typographical changes are complex so will be tested thoroughly before being released to the website.

Except for components like the header, sticky header and navDrawer where the logo appears, design and functionality of components are not expected to change.

### Documentation as the brand evolves

As close to February 15th, 2024 as possible, the palette and logo are going to be changed to the new brand and pushed out to Ivy. All of the FIGMA files will reflect this with the changes pushed out to the atomic level. Components synced with InVision will also be updated and have the new colors.

Wherever Hex values are mentioned within the text documentation, that content is not going to get updated immediately-- we may even wait until the content is backed up on Teams before doing those changes to the documentation as we will have a Find and Replace text search to help make those edits easier.

Inspecting components in FIGMA is recommended as the source of truth to hint out palette changes.

## Mapping old to new palette colors

To facilitate the change to the new palette, Susy will create a swatch conversion reference in Figma to show

* palette shift for brand colors
* palette consolidation (colors merging and being over-riden by others)
* palette elimination (colors that were used in the Trail Mix brand but not in Ivy that are going away)

Palette change will be well documented so we have a clear record of before and after.

Ivy FiGMA files will all be archived and stored on Teams prior to the brand shift.

# Component iterations, requests, and wish list

**PROFILE SPOTLIGHTS**

People want to display in depth, inline profiles to highlight people. There are no bios related to this.  We are using the InfoBand component for this but it could be improved. It is such a common need, it would be good to optimize this experience as it should learning outcomes and highlights achievements-- important value added content for prospective students.

Example:

[https://uvmd10.drup2.uvm.edu/cess/experiential-learning-and-internships](https://uvmd10.drup2.uvm.edu/cess/experiential-learning-and-internships)

**INFOBAND**

Could we have an optional image below the infoband title? (left col dt). If so, would this remove it from the progressive design triptych of infoBand / accordion / vTab?

**`<HR>` decorative**

Campus scape hRule spacer

**PROGRAM Descriptions in list form**

Programs were intended to have full page descriptions with teasers linking to them from Program listings.

However, in the wild, many programs don't have program pages, they have fuller listings of all programs on one page. We need to think through this user journey, what content is appropriate to provide a consistent UX for our program offerings and where we need diferent displays -- what this looks like and how user navigates and engages with list.

[](https://uvmedu.invisionapp.com/dsm/university-of-vermont)
