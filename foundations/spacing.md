## The UVM spacing system is based on 8px units. 

The scaling system uses fractions and multiples of the base size (8px). Spacing *within* components is color coded cyan. Layout spacing *between* components is colored magenta.

**For text-heavy pages, vertical spacing between components is: **

• 64px on desktop

• 48px on mobile

Pages with more marketing-related components have more vertical space and are a bit more open than text-heavy pages.

• 160px on desktop

• 112px on mobile


![1727724703303](image/spacing/1727724703303.png)





![1727724734909](image/spacing/1727724734909.png)
