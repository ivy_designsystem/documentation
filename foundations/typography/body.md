# Body type is all set in Roboto, left, font-weight 400. The primary setting for body copy is darkGray #26292C. Body type is the same setting by default for both mobile and desktop.

### **Size variations of supporting type styles **

Large

Medium

Small

X-Small

### **Weight variations **

• Regular 400

• Bold 700

### Case

Use Upper and lowercase.

Uppercase exists for many of these styles but use sparingly-- uppercase is generally not recommended. It would be used for EYEBROWS.

### Setting type over images

Only body copy Large, Medium and P-regular can be set over images in either darkGray, or white. A WCAG contrast of AA 4.51 must be achieved.

### Setting body type over solid backgrounds:

All sizes of type can be set over light or dark solid backgrounds but note that a WCAG contrast of AA 4.51 must be achieved.

P-regular, P-strong, P-small, and P-small-strong have a light and dark alpha variant to allow them to be set over both solid and light backgrounds and appear as a secondary gray.

• Over light backgrounds, the color is #26292C at 72% alpha. (called onLight)

• Over dark backgrounds, the color is #FFF at 80% alpha. (called onDark)

![1730421360037](image/body/1730421360037.png)
