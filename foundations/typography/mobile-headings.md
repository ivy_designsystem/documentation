Mobile Headings are scaled down in size (not weight) to fit proportionally within a mobile touch environment.  They are to be used flush left.

The primary color setting is darkGray #26292C.

The larger sizes- H1 display, H1, H2 and H3 have color variants that can be used for visual contrast. These include: white #FFF, primary1 #225C4C, and primary2 #367F17, or lightGray #5D646B.

Sizes H4 and below can only be set in darkGray #26292C.

![1730421408052](image/mobile-headings/1730421408052.png)
