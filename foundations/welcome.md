
# Welcome to Ivy- UVM's official design system.

**Ivy** has been built to support our teams of communicators, designers and developers in creating best in class, consistent, digital products and experiences.

We are working towards a design system that consists of: our design principles, foundational design language, brand guidelines, human interface guidelines, accessibility guidelines, interaction guidelines, and working code. Product Designer [Susy Mason Lazarev](mailto:smasonla@uvm.edu) is the project lead and author. Expect continued improvements and releases.
