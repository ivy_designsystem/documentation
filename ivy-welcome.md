![1730472104255](image/ivy-welcome/1730472104255.png)



Ivy is UVM's evolving design system. It allows the web team and product designers at the University of Vermont to design, develop and share reusable components to avoid repeating work that has already been done as well as create consistency in our digital brand.

The design system also helps design and development teams work more closely, share the same naming of things, and share style and component libraries that are connected to both the design and development work flows.

Ivy also provides visual reference to our typography system, digital palette, iconography, website grids, and interaction library. Icons can be downloaded as SVGs.

Please see the [Getting Started](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/5f82e4d10fa0e2105576fe92?mode=preview) page as well as the [Onboarding Figma files](https://uvmedu.invisionapp.com/dsm/university-of-vermont/ivy/nav/5fa7cb6f8c01200018361128/folder/6400af8970516ca17227c245?mode=preview) page  if you are new to Ivy.

---

**Have questions about UVM's Ivy Design System?**

**Susan Mason Lazarev**

UX and Product Design

Design Lead for the university's website

Author of Ivy

[smasonla@uvm.edu](mailto:smasonla@uvm.edu)
